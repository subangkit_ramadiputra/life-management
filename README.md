# Life Management

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Life Management

Life Management is a tools to manage your life specially to store and analyze every data in your brain. 
Currently already some CRUD that basically help me to manage my brain.
1. Concepts
2. Files, able to integrate to all models using uploadable
3. Tagable, able to integrate to all models using tagable
4. Applications
5. Features, able to integrate to all models using featureable
6. Generate relation between entities as feature workflow
7. Entities, able to integrate to all models using entitiable
8. Clusters
9. Servers
10. ETL's
11. Workflow Places, able to integrate to all models using placeable
12. Workflow Transitions, able to integrate to all models using transitionable
13. Generate workflow based on package symphony/workflow

## License

Life Management is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
