import './bootstrap';

import Alpine from 'alpinejs';
import Flatpickr from 'flatpickr';
import Clipboard from "@ryangjchandler/alpine-clipboard"
import BpmnModeler from 'bpmn-js'
import BpmnJS from 'bpmn-js'

Alpine.plugin(Clipboard)

window.Alpine = Alpine;
window.Flatpickr = Flatpickr;
window.tinymce = tinymce;
window.clipboard = Clipboard;
window.BpmnModeler = BpmnModeler;
window.BpmnJS = BpmnJS;

Alpine.start();
