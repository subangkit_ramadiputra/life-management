<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
             role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        @foreach($recent_uploads as $upload)
                            <div class="text-xs inline-flex font-medium text-gray-600 rounded-full text-center px-2.5 py-1 mx-0.5">
                                <span class="inline-flex btn border-gray-200 hover:border-gray-300 mt-1">
                                    <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path></svg>
                                </span>
                                <a href="{{ $upload->url }}" target="_blank">
                                    {{ $upload->file_name }}
                                    <span class="text-xs inline-flex cursor-pointer font-medium bg-gray-100 text-gray-600 rounded-full text-center px-2.5 py-1 mt-1 mx-0.5">{{ number_format($upload->file_size/1024/1024,1) }}MB</span>
                                    <span class="text-xs inline-flex cursor-pointer font-medium bg-teal-100 text-teal-600 rounded-full text-center px-2.5 py-1 mt-1 mx-0.5">{{ $upload->file_extension }}</span>
                                </a>&nbsp;
                                <a wire:click="deleteExistingUpload({{ $upload->id }})">
                                    <span class="text-xs inline-flex cursor-pointer font-medium bg-red-100 text-red-600 rounded-full text-center cursor-pointer px-2.5 py-1 mt-1 mx-0.5">x</span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <label for="files"
                                   class="block text-gray-700 text-sm font-bold mb-2">Upload File</label>
                            <input id="files" type="file" wire:model.lazy="files" multiple class="form-input w-full">
                            @error('files.*') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">

                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignUpload')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Upload
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeUploadModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
