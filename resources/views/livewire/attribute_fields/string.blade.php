<div class="mb-6" wire:key="attribute-{{ $attribute->field_name }}">
    <div class="flex items-center justify-between">
        <label class="block text-sm font-medium mb-1" for="{{ $attribute->field_name }}">{{ $attribute->title }}</label>
        @if($attribute->nullable == 1)
        <div class="text-sm text-gray-400 italic">optional</div>
        @endif
    </div>
    <input
        id="{{ $attribute->field_name }}" class="form-input w-full mb-2" type="text" wire:model.defer="values.{{ $attribute->field_name }}" />
</div>
