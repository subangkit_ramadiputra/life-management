<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{editor:null, fileEditorShow: @entangle('fileEditorShow'), fileFormShow: @entangle('fileFormShow'), file_id: @entangle('file_id')}" >
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full" style="min-width: 1200px;"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full" style="display: block;height: 300px;overflow-y: scroll;">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">File Name</th>
                                <th class="px-4 py-2">Summary</th>
                                <th class="px-4 py-2">File Path</th>
                                <th class="px-4 py-2">Editor Mode</th>


                                <th class="px-4 py-2" width="300">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_files as $file)
                                <tr>
                                    <td class="border px-4 py-2">{{ $file->id }}</td>
                                    <td class="border px-4 py-2">{{ $file->name }}</td>
                                    <td class="border px-4 py-2" style="white-space: pre-wrap;word-wrap: break-word;">{{ $file->summary }}</td>
                                    <td class="border px-4 py-2" style="word-wrap: break-word;">{{ $file->file_path }}</td>
                                    <td class="border px-4 py-2">{{ $file->Mode }}</td>


                                    <td class="border px-4 py-2">
                                        <span x-on:click="@this.call('editFileContent',{{ $file->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            Edit Content
                                        </span>
                                        <span wire:click="editFile({{ $file->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteFile({{ $concept_id }},{{ $file->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4" >
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                                    <div class="mb-4" >
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">File Content</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     watcher : null,
                                                     value: @entangle('file_content_txt'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this
                                                        if (editor == null) {
                                                            let editorElement = document.getElementById('file_content_txt');
                                                            // pass options to ace.edit
                                                            editor = ace.edit(document.getElementById('file_content_txt'), {
                                                                                            mode: 'ace/mode/{{ ($file_Mode != '') ? $file_Mode : 'html' }}',
                                                                                            theme: 'ace/theme/monokai',
                                                                                            maxLines: 50,
                                                                                            minLines: 10,
                                                                                            fontSize: 18
                                                                                            })
                                                                                            // use setOptions method to set several options at once
                                                            editor.setOptions({
                                                                autoScrollEditorIntoView: true,
                                                                copyWithEmptySelection: true
                                                            });
                                                            editor.on('change', function(e){
                                                            });

                                                            window.clear = function(){
                                                                editor.session.setValue('');
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         editor = null
                                                         watcher = $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="file_content_txt" placeholder="Enter File Content" wire:model.defer="file_content_txt"></textarea>
                                        @error('file_content_txt') <span class="text-red-500">{{ $message }}</span>@enderror
                                        <button x-on:click="@this.call('saveFileContent',file_id, editor.session.getValue())" type="button"
                                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                            Save Content
                                        </button>
                                    </div>
                        <div class="mb-4" x-show="fileFormShow">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">File Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter File Name" wire:model.lazy="file_name">
                            @error('file_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4" x-show="fileFormShow">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="file_summary"></textarea>
                            @error('file_summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4" x-show="fileFormShow">
                            <label for="file_path"
                                class="block text-gray-700 text-sm font-bold mb-2">File Path</label>
                            <textarea class="form-input w-full"
                                id="file_path" placeholder="Enter File Path" wire:model.lazy="file_file_path"></textarea>
                            @error('file_file_path') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4" x-show="fileFormShow">
                            <label for="Mode"
                                class="block text-gray-700 text-sm font-bold mb-2">Editor Mode</label>
                            <input type="text"
                                class="form-input w-full"
                                id="Mode" placeholder="Enter Editor Mode" wire:model.lazy="file_Mode">
                            @error('file_Mode') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetFileCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignFile')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button x-on:click="editor = null; @this.call('closeFileModalPopover')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
