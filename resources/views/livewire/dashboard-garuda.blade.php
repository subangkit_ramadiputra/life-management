<div class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
    @include('livewire/alert-message-render',['clearSession' => true])
    <div class="grid grid-cols-12 gap-6">
        @include('livewire.garudaku.links')
        @include('livewire.garudaku.card-stats-players')
        <br/>
        @include('livewire.garudaku.table-stats-players')
    </div>
</div>
