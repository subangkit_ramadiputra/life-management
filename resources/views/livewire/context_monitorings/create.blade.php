<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{wycwyg: null}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="parent_id" class="block text-gray-700 text-sm font-bold mb-2">Parent Monitoring</label>
                            <select class="form-select" id="parent_id" placeholder="Choose One" wire:model.lazy="parent_id">
                                <option value="0">ROOT</option>
                                @foreach($context_monitor_list as $context_monitor)
                                    <option value="{{$context_monitor->id}}">{{$context_monitor->title}}</option>
                                @endforeach
                            </select>
                            @error('parent_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('summary'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorsummary != null) {
                                                                tinymce.activeEditorsummary.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#summary',
                                                                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                                                                toolbar: 'undo redo | bold italic underline strikethrough | numlist bullist indent outdent | blocks fontfamily fontsize | link image media table | align lineheight | emoticons charmap | removeformat',

                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="summary" placeholder="Enter Summary" wire:model.defer="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="progress"
                                class="block text-gray-700 text-sm font-bold mb-2">% Progress</label>
                            <input type="text"
                                class="form-input w-full"
                                id="progress" placeholder="Enter % Progress" wire:model.lazy="progress">
                            @error('progress') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Strategy</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('strategy'),
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (wycwyg == null) {
                                                            if (tinymce.activeEditorstrategy != null) {
                                                                tinymce.activeEditorstrategy.destroy();
                                                            }
                                                            wycwyg = window.tinymce.init({
                                                                selector: '#strategy',
                                                                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                                                                toolbar: 'undo redo | bold italic underline strikethrough | numlist bullist indent outdent | blocks fontfamily fontsize | link image media table | align lineheight | emoticons charmap | removeformat',

                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="strategy" placeholder="Enter Strategy" wire:model.defer="strategy"></textarea>
                            @error('strategy') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('summary',document.getElementById('summary').value);@this.set('strategy',document.getElementById('strategy').value);@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
