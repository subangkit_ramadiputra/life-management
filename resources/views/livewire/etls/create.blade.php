<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">ETL Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter ETL Name" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="schedule"
                                class="block text-gray-700 text-sm font-bold mb-2">Schedule</label>
                            <input type="text"
                                class="form-input w-full"
                                id="schedule" placeholder="Enter Schedule" wire:model.lazy="schedule">
                            @error('schedule') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="request_by"
                                class="block text-gray-700 text-sm font-bold mb-2">Request By</label>
                            <input type="text"
                                class="form-input w-full"
                                id="request_by" placeholder="Enter Request By" wire:model.lazy="request_by">
                            @error('request_by') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="source_data"
                                class="block text-gray-700 text-sm font-bold mb-2">Source Data</label>
                            <textarea class="form-input w-full"
                                id="source_data" placeholder="Enter Source Data" wire:model.lazy="source_data"></textarea>
                            @error('source_data') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="transform_summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Transform Summary</label>
                            <textarea class="form-input w-full"
                                id="transform_summary" placeholder="Enter Transform Summary" wire:model.lazy="transform_summary"></textarea>
                            @error('transform_summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="target_data"
                                class="block text-gray-700 text-sm font-bold mb-2">Target Data</label>
                            <textarea class="form-input w-full"
                                id="target_data" placeholder="Enter Target Data" wire:model.lazy="target_data"></textarea>
                            @error('target_data') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="consumed_by"
                                class="block text-gray-700 text-sm font-bold mb-2">Consumed By</label>
                            <textarea class="form-input w-full"
                                id="consumed_by" placeholder="Enter Consumed By" wire:model.lazy="consumed_by"></textarea>
                            @error('consumed_by') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
