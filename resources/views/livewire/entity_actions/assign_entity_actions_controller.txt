Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/entity_actions/assign_entity_actions_template.blade.php to resources/views/livewire/parent/assign_entity_actions.blade.php
or
cp resources/views/livewire/entity_actions/assign_entity_actions_template.blade.php resources/views/livewire/parents/assign_entity_actions.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isEntityActionModalOpen)
            @include('livewire.parents.assign_entity_actions')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">EntityActions</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->entity_actions()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">EntityActions</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">EntityAction</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addEntityAction({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        EntityAction
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\EntityAction;


    /**
     * Get all of the parents entity_actions.
     */
    public function entity_actions()
    {
        return $this->morphMany(EntityAction::class, 'entity_actionable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function entity_actions()
        {
            return $this->hasMany(EntityAction::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // EntityAction Section
    public $isEntityActionModalOpen = 0;
    public $recent_entity_actions = [];
    public $entity_actions = [];
    public function addEntityAction($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_entity_actions = $parent->entity_actions()->get();
        $this->resetEntityActionCreateForm();
        $this->openEntityActionModalPopover();
    }

    public function openEntityActionModalPopover()
    {
        $this->isEntityActionModalOpen = true;
    }
    public function closeEntityActionModalPopover()
    {
        $this->isEntityActionModalOpen = false;
    }

    public function editEntityAction($entity_action_id) {
        $entity_action = EntityAction::findOrFail($entity_action_id);
        $this->entity_action_id = $entity_action_id;
        $this->entity_action_title = $entity_action->title;
        $this->entity_action_summary = $entity_action->summary;
        $this->entity_action_body_format = $entity_action->body_format;
        $this->entity_action_body = $entity_action->body;
        $this->entity_action_actionable = $entity_action->actionable;
        $this->entity_action_parameters = $entity_action->parameters;
        $this->entity_action_method = $entity_action->method;
        $this->entity_action_tags = $entity_action->tags;
        $this->entity_action_responses = $entity_action->responses;

    }

    public function resetEntityActionCreateForm(){
        $this->entity_action_id = '';
        $this->entity_action_title = '';
        $this->entity_action_summary = '';
        $this->entity_action_body_format = 'markdown';
        $this->entity_action_body = '';
        $this->entity_action_actionable = '';
        $this->entity_action_parameters = 'username|path';
        $this->entity_action_method = 'POST';
        $this->entity_action_tags = '';
        $this->entity_action_responses = '200|Success,400|Has Error';

    }

    public function assignEntityAction() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->entity_action_id == '') {
            $entity_action = new EntityAction();
            $entity_action->title = $this->entity_action_title;
            $entity_action->summary = $this->entity_action_summary;
            $entity_action->body_format = $this->entity_action_body_format;
            $entity_action->body = $this->entity_action_body;
            $entity_action->parameters = $this->entity_action_parameters;
            $entity_action->method = $this->entity_action_method;
            $entity_action->tags = $this->entity_action_tags;
            $entity_action->responses = $this->entity_action_responses;

            $parent->entity_actions()->save($entity_action);

            $this->alertSuccessMessage('EntityAction updated.');
            //$this->closeEntityActionModalPopover();
            $this->resetEntityActionCreateForm();
        } else {
            EntityAction::updateOrCreate(['id' => $this->entity_action_id], [
                'title' => $this->entity_action_title,
                'summary' => $this->entity_action_summary,
                'body_format' => $this->entity_action_body_format,
                'body' => $this->entity_action_body,
                'parameters' => $this->entity_action_parameters,
                'method' => $this->entity_action_method,
                'tags' => $this->entity_action_tags,
                'responses' => $this->entity_action_responses,

            ]);
        }

        $this->recent_entity_actions = $parent->entity_actions()->get();
    }

    public function deleteEntityAction($parent_id, $entity_action_id) {
        $parent = Parent::findOrFail($parent_id);

        $entity_action = EntityAction::find($entity_action_id);
        if ($entity_action) {
            $parent->entity_actions()->where('id', '=', $entity_action->id)->delete();
        }

        $this->recent_entity_actions = $parent->entity_actions()->get();

        $this->alertSuccessMessage('EntityAction deleted.');
    }

    public function deleteExistingEntityAction($entity_action_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $entity_action = EntityAction::find($entity_action_id);
        if ($entity_action) {
            $parent->entity_actions()->where('id', '=', $entity_action->id)->delete();
        }

        $this->recent_entity_actions = $parent->entity_actions()->get();

        $this->alertSuccessMessage('EntityAction deleted.');
    }
    // End of Section
