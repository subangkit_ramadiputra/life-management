<div class="col-span-full">
@foreach($files as $upload)
    <div class="btn text-xs inline-flex font-medium text-gray-600 rounded-full text-center px-2.5 py-1 mx-0.5">
                                <span class="inline-flex mt-1">
                                    <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M7 21h10a2 2 0 002-2V9.414a1 1 0 00-.293-.707l-5.414-5.414A1 1 0 0012.586 3H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path></svg>
                                </span>
        <a href="{{ $upload->url }}" target="_blank">
            {{ $upload->file_name }}
            <span class="text-xs inline-flex cursor-pointer font-medium bg-gray-100 text-gray-600 rounded-full text-center px-2.5 py-1 mt-1 mx-0.5">{{ number_format($upload->file_size/1024/1024,1) }}MB</span>
            <span class="text-xs inline-flex cursor-pointer font-medium bg-teal-100 text-teal-600 rounded-full text-center px-2.5 py-1 mt-1 mx-0.5">{{ $upload->file_extension }}</span>
        </a>&nbsp;
        <a wire:click="deleteExistingUpload({{ $upload->id }})">
            <span class="text-xs inline-flex cursor-pointer font-medium bg-red-100 text-red-600 rounded-full text-center cursor-pointer px-2.5 py-1 mt-1 mx-0.5">x</span>
        </a>
    </div>
@endforeach
</div>
