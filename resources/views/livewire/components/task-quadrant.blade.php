<div class="col-span-full rounded-sm">
    <div class="flex flex-wrap -mx-1 overflow-hidden">

        <div class="my-1 px-1 w-1/2 overflow-hidden sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2">
            <h1 class=" text-xl not-italic font-bold text-right pr-2 pt-2 bg-white text-gray-900">Quadrant 1</h1>
            <p class=" text-sm not-italic text-right p-2 bg-white text-gray-900">
                <strong class="text-red-500">Urgent</strong> and <strong class="text-red-500">important</strong><br/>
                <strong>DO</strong>
            </p>
            <div class="grid">
            @foreach($q1 as $task)
                <!-- Card Task -->
                    <div class="col-span-full bg-white shadow-lg rounded-sm border border-gray-200 p-4">
                        <!-- Body -->
                        <div class="mb-3">
                            @if($task->status == 'done')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-check" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M5 12l5 5l10 -10" />
                                    </svg>
                                    Done
                                </div>
                            @endif
                            @if($task->status == 'in_progress')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-clock" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="12" r="9" />
                                        <polyline points="12 7 12 12 15 15" />
                                    </svg>
                                    In Progress
                                </div>
                            @endif
                            @if($task->status == 'todo')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-asana" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="7" r="3" />
                                        <circle cx="17" cy="16" r="3" />
                                        <circle cx="7" cy="16" r="3" />
                                    </svg>
                                    To Do
                                </div>
                        @endif
                        <!-- Title -->
                            <h2 class="font-semibold text-gray-800 mb-1">{{ $task->title }}</h2>
                            <!-- Content -->
                            <p class="break-words">{{ $task->summary }}</p>
                            <div>
                                <div class="text-sm">#{{ $task->id }} at {{ $task->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                        <!-- Footer -->
                        <div class="flex items-center justify-between">
                            <!-- Left side -->
                            <div class="flex shrink-0 -space-x-3 -ml-px">
                                <div>
                                    <a href="{{ $task->taskable->taskable_url }}" target="_blank" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
                                    </a>
                                    @php
                                        $taskables = explode(' :: ',$task->taskable->taskable_name);
                                        $colors = ['green','blue','red'];
                                    @endphp
                                    @foreach($taskables as $index => $task_info)
                                        <div class="text-xs inline-flex cursor-pointer font-medium bg-{{$colors[$index]}}-100 text-{{$colors[$index]}}-600 rounded-full text-center px-2.5 py-1 mt-1">
                                            {{ $task_info }}
                                        </div><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="my-1 px-1 w-1/2 overflow-hidden sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2">
            <h1 class=" text-xl not-italic font-bold text-right pr-2 pt-2 bg-white text-gray-900">Quadrant 2</h1>
            <p class=" text-sm not-italic text-right p-2 bg-white text-gray-900">
                <strong class="text-purple-500">Not</strong> urgent but <strong class="text-red-500">important</strong><br/>
                <strong>PLAN</strong>
            </p>
            <div class="grid">
            @foreach($q2 as $task)
                <!-- Card Task -->
                    <div class="col-span-full bg-white shadow-lg rounded-sm border border-gray-200 p-4">
                        <!-- Body -->
                        <div class="mb-3">
                            @if($task->status == 'done')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-check" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M5 12l5 5l10 -10" />
                                    </svg>
                                    Done
                                </div>
                            @endif
                            @if($task->status == 'in_progress')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-clock" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="12" r="9" />
                                        <polyline points="12 7 12 12 15 15" />
                                    </svg>
                                    In Progress
                                </div>
                            @endif
                            @if($task->status == 'todo')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-asana" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="7" r="3" />
                                        <circle cx="17" cy="16" r="3" />
                                        <circle cx="7" cy="16" r="3" />
                                    </svg>
                                    To Do
                                </div>
                        @endif
                        <!-- Title -->
                            <h2 class="font-semibold text-gray-800 mb-1">{{ $task->title }}</h2>
                            <!-- Content -->
                            <p class="break-words">{{ $task->summary }}</p>
                            <div>
                                <div class="text-sm">#{{ $task->id }} at {{ $task->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                        <!-- Footer -->
                        <div class="flex items-center justify-between">
                            <!-- Left side -->
                            <div class="flex shrink-0 -space-x-3 -ml-px">
                                <div>
                                    <a href="{{ $task->taskable->taskable_url }}" target="_blank" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
                                    </a>
                                    @php
                                        $taskables = explode(' :: ',$task->taskable->taskable_name);
                                        $colors = ['green','blue','red'];
                                    @endphp
                                    @foreach($taskables as $index => $task_info)
                                        <div class="text-xs inline-flex cursor-pointer font-medium bg-{{$colors[$index]}}-100 text-{{$colors[$index]}}-600 rounded-full text-center px-2.5 py-1 mt-1">
                                            {{ $task_info }}
                                        </div><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="my-1 px-1 w-1/2 overflow-hidden sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2">
            <h1 class=" text-xl not-italic font-bold text-right pr-2 pt-2 bg-white text-gray-900">Quadrant 3</h1>
            <p class=" text-sm not-italic text-right p-2 bg-white text-gray-900">
                <strong class="text-red-500">Urgent</strong> but <strong class="text-purple-500">not</strong> important<br/>
                <strong>DELEGATE</strong>
            </p>
            <div class="grid">
            @foreach($q3 as $task)
                <!-- Card Task -->
                    <div class="col-span-full bg-white shadow-lg rounded-sm border border-gray-200 p-4">
                        <!-- Body -->
                        <div class="mb-3">
                            @if($task->status == 'done')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-check" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M5 12l5 5l10 -10" />
                                    </svg>
                                    Done
                                </div>
                            @endif
                            @if($task->status == 'in_progress')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-clock" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="12" r="9" />
                                        <polyline points="12 7 12 12 15 15" />
                                    </svg>
                                    In Progress
                                </div>
                            @endif
                            @if($task->status == 'todo')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-asana" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="7" r="3" />
                                        <circle cx="17" cy="16" r="3" />
                                        <circle cx="7" cy="16" r="3" />
                                    </svg>
                                    To Do
                                </div>
                        @endif
                        <!-- Title -->
                            <h2 class="font-semibold text-gray-800 mb-1">{{ $task->title }}</h2>
                            <!-- Content -->
                            <p class="break-words">{{ $task->summary }}</p>
                            <div>
                                <div class="text-sm">#{{ $task->id }} at {{ $task->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                        <!-- Footer -->
                        <div class="flex items-center justify-between">
                            <!-- Left side -->
                            <div class="flex shrink-0 -space-x-3 -ml-px">
                                <div>
                                    <a href="{{ $task->taskable->taskable_url }}" target="_blank" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
                                    </a>
                                    @php
                                        $taskables = explode(' :: ',$task->taskable->taskable_name);
                                        $colors = ['green','blue','red'];
                                    @endphp
                                    @foreach($taskables as $index => $task_info)
                                        <div class="text-xs inline-flex cursor-pointer font-medium bg-{{$colors[$index]}}-100 text-{{$colors[$index]}}-600 rounded-full text-center px-2.5 py-1 mt-1">
                                            {{ $task_info }}
                                        </div><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <div class="my-1 px-1 w-1/2 overflow-hidden sm:w-full md:w-1/2 lg:w-1/2 xl:w-1/2">
            <h1 class=" text-xl not-italic font-bold text-right pr-2 pt-2 bg-white text-gray-900">Quadrant 4</h1>
            <p class=" text-sm not-italic text-right p-2 bg-white text-gray-900">
                <strong class="text-purple-500">Not</strong> urgent and <strong class="text-purple-500">not</strong> important<br/>
                <strong>ELIMINATE</strong>
            </p>
            <div class="grid">
            @foreach($q4 as $task)
                <!-- Card Task -->
                    <div class="col-span-full bg-white shadow-lg rounded-sm border border-gray-200 p-4">
                        <!-- Body -->
                        <div class="mb-3">
                            @if($task->status == 'done')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-check" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <path d="M5 12l5 5l10 -10" />
                                    </svg>
                                    Done
                                </div>
                            @endif
                            @if($task->status == 'in_progress')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-clock" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="12" r="9" />
                                        <polyline points="12 7 12 12 15 15" />
                                    </svg>
                                    In Progress
                                </div>
                            @endif
                            @if($task->status == 'todo')
                                <div class="text-xs inline-flex cursor-pointer font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1 mt-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-asana" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="#2c3e50" fill="none" stroke-linecap="round" stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                        <circle cx="12" cy="7" r="3" />
                                        <circle cx="17" cy="16" r="3" />
                                        <circle cx="7" cy="16" r="3" />
                                    </svg>
                                    To Do
                                </div>
                        @endif
                        <!-- Title -->
                            <h2 class="font-semibold text-gray-800 mb-1">{{ $task->title }}</h2>
                            <!-- Content -->
                            <p class="break-words">{{ $task->summary }}</p>
                            <div>
                                <div class="text-sm">#{{ $task->id }} at {{ $task->created_at->diffForHumans() }}</div>
                            </div>
                        </div>
                        <!-- Footer -->
                        <div class="flex items-center justify-between">
                            <!-- Left side -->
                            <div class="flex shrink-0 -space-x-3 -ml-px">
                                <div>
                                    <a href="{{ $task->taskable->taskable_url }}" target="_blank" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
                                    </a>
                                    @php
                                        $taskables = explode(' :: ',$task->taskable->taskable_name);
                                        $colors = ['green','blue','red'];
                                    @endphp
                                    @foreach($taskables as $index => $task_info)
                                        <div class="text-xs inline-flex cursor-pointer font-medium bg-{{$colors[$index]}}-100 text-{{$colors[$index]}}-600 rounded-full text-center px-2.5 py-1 mt-1">
                                            {{ $task_info }}
                                        </div><br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</div>
