<!-- Card 1 -->
@foreach($notes as $note)
<div class="col-span-full sm:col-span-6 xl:col-span-4 bg-white shadow-lg rounded-sm border border-gray-200">
    <div class="flex flex-col h-full p-5">
        <header>
            <div class="flex items-center justify-between">
                <svg xmlns="http://www.w3.org/2000/svg" class="w-10 h-10 rounded-full flex items-center justify-center shrink-0 bg-red-500" width="16" height="16" viewBox="0 0 16 16"><title>file-text</title><g stroke-width="1" fill="#212121" stroke="#212121"><line fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="4.5" y1="11.5" x2="11.5" y2="11.5"></line> <line fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="4.5" y1="8.5" x2="11.5" y2="8.5"></line> <line fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="4.5" y1="5.5" x2="6.5" y2="5.5"></line> <polygon fill="none" stroke="#212121" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="9.5,0.5 1.5,0.5 1.5,15.5 14.5,15.5 14.5,5.5 "></polygon> <polyline fill="none" stroke="#212121" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="9.5,0.5 9.5,5.5 14.5,5.5 "></polyline></g></svg>
            </div>
        </header>
        <div class="grow mt-2">
            <a class="inline-flex text-gray-800 hover:text-gray-900 mb-1" href="#0">
                <h2 class="text-xl leading-snug font-semibold">{{ $note->title }}</h2>
            </a>
            <div class="text-sm prose">{!! $note->body !!}</div>
        </div>
        <footer class="mt-5">
            <div class="text-sm font-medium text-gray-500 mb-2">{{ $note->created_at->diffForHumans() }}</div>
            <div class="flex justify-between items-center">
                <div>
                    <div class="text-xs inline-flex font-medium bg-green-100 text-green-600 rounded-full text-center px-2.5 py-1">One-Time</div>
                </div>
                <div>
                    <a class="text-sm font-medium text-indigo-500 hover:text-indigo-600" href="#0">View -&gt;</a>
                </div>
            </div>
        </footer>
    </div>
</div>
@endforeach
