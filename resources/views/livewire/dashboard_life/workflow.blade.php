<div class="flex flex-col col-span-full xl:col-span-8 bg-white shadow-lg rounded-sm border border-gray-200">
    <header class="px-5 py-4 border-b border-gray-100 flex items-center">
        <h2 class="font-semibold text-gray-800">Workflows</h2>
    </header>
    <div class="px-5 py-1">
        <div class="flex flex-wrap">
            <!-- Places -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($workflows['places']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Places</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Transitions -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($workflows['transitions']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Transitions</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>

        </div>
    </div>
</div>
