<div class="flex flex-col col-span-full xl:col-span-8 bg-white shadow-lg rounded-sm border border-gray-200">
    <header class="px-5 py-4 border-b border-gray-100 flex items-center">
        <h2 class="font-semibold text-gray-800">Analytics</h2>
    </header>
    <div class="px-5 py-1">
        <div class="flex flex-wrap">
            <!-- Concepts -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($analytics['concepts']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Concepts</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Applications -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($analytics['applications']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Applications</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Features -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($analytics['features']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Features</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Entities -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($analytics['entities']) }}</div>
                    </div>
                    <div class="text-sm text-gray-500">Entities</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>

        </div>
    </div>
    <!-- Table -->
    <div class="grow px-5 pt-3 pb-1">
        <div class="overflow-visible">
            <table class="table-auto w-full">
                <!-- Table header -->
                <thead class="text-xs uppercase text-gray-400">
                <tr>
                    <th class="py-2">
                        <div class="font-semibold text-left">Top Concepts</div>
                    </th>
                    <th class="py-2">
                        <div class="font-semibold text-right">Applications</div>
                    </th>
                </tr>
                </thead>
                <!-- Table body -->
                <tbody class="text-sm divide-y divide-gray-100">
                @foreach($priority_concepts as $concept)
                <!-- Row -->
                <tr>
                    <td class="py-2">
                        <div class="text-left">{{ $concept->title }}</div>
                    </td>
                    <td class="py-2">
                        <div class="font-medium text-right text-gray-800">{{ $concept->applications()->count() }}</div>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
