<div
    x-data="{ attribute_type : @entangle('attribute_type') }"
    class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2 w-20">Field Name
                                </th>
                                <th class="px-4 py-2 w-20">Nullable ?
                                </th>
                                <th class="px-4 py-2 w-20">Title
                                </th>
                                <th class="px-4 py-2 w-20">Field Type
                                </th>
                                <th class="px-4 py-2 w-20">Length
                                </th>
                                <th class="px-4 py-2 w-20">Default Value
                                </th>
                                <th class="px-4 py-2 w-20">View On Table ?
                                </th>
                                <th class="px-4 py-2 w-20">View On Create ?
                                </th>

                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_attributes as $attribute)
                                <tr>
                                    <td class="border px-4 py-2">{{ $attribute->id }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->field_name }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->nullable }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->title }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->type }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->length }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->default }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->view_table }}</td>
                                    <td class="border px-4 py-2">{{ $attribute->view_create }}</td>

                                    <td class="border px-4 py-2">
                                        <span wire:click="editAttribute({{ $attribute->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteAttribute({{ $entity_id }},{{ $attribute->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                                    <div class="mb-4">
                                        <label for="field_name"
                                               class="block text-gray-700 text-sm font-bold mb-2">Field Name</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="field_name" attributeholder="Enter Field Name" wire:model.lazy="attribute_field_name">
                                        @error('attribute_field_name') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                        <input type="checkbox" wire:model.lazy="attribute_nullable" /> Nullable
                                        <input type="checkbox" wire:model.lazy="attribute_view_table" /> View On Table
                                        <input type="checkbox" wire:model.lazy="attribute_view_create" /> View On Create
                                    </div>
                                    <div class="mb-4">
                                        <label for="title"
                                               class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="title" attributeholder="Enter Title" wire:model.lazy="attribute_title">
                                        @error('attribute_title') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="type"
                                               class="block text-gray-700 text-sm font-bold mb-2">Type</label>
                                        <select class="form-input w-full"
                                               id="type" wire:model.lazy="attribute_type" wire:change="changeType">
                                            <option value="bigIncrements">Big Increments</option>
                                            <option value="bigInteger">Big Integer</option>
                                            <option value="binary">Binary</option>
                                            <option value="boolean">Boolean</option>
                                            <option value="char">Char</option>
                                            <option value="date">Date</option>
                                            <option value="dateTime">Date Time</option>
                                            <option value="decimal">Decimal</option>
                                            <option value="double">Double</option>
                                            <option value="enum">Enum</option>
                                            <option value="float">Float</option>
                                            <option value="increments">Increments</option>
                                            <option value="integer">Integer</option>
                                            <option value="longText">Long Text</option>
                                            <option value="mediumInteger">Medium Integer</option>
                                            <option value="mediumText">Medium Text</option>
                                            <option value="smallInteger">Small Integer</option>
                                            <option value="tinyInteger">Tiny Integer</option>
                                            <option value="string">String</option>
                                            <option value="text">Text</option>
                                            <option value="time">Time</option>
                                            <option value="timestamp">Time Stamp</option>
                                            <option value="morphs">Polimorphic</option>
                                        </select>
                                        @error('attribute_title') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4 {{ !in_array($attribute_type,['string','char']) ? 'hidden' : '' }}">
                                        <label for="length"
                                               class="block text-gray-700 text-sm font-bold mb-2">Length {{ $attribute_type }}</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="length" attributeholder="Enter Length" wire:model.lazy="attribute_length">
                                        @error('attribute_length') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4 {{ !in_array($attribute_type,['dateTimeTz','dateTime','decimal']) ? 'hidden' : '' }}">
                                        <label for="precision"
                                               class="block text-gray-700 text-sm font-bold mb-2">Precission {{ $attribute_type }}</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="precision" attributeholder="Enter Precission" wire:model.lazy="attribute_precision">

                                        @error('attribute_precision') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="mb-4 {{ !in_array($attribute_type,['decimal']) ? 'hidden' : '' }}">
                                        <label for="scale"
                                               class="block text-gray-700 text-sm font-bold mb-2">Scale {{ $attribute_type }}</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="scale" attributeholder="Enter Scale" wire:model.lazy="attribute_scale">

                                        @error('attribute_precision') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="mb-4 {{ !in_array($attribute_type,['double','float']) ? 'hidden' : '' }}">
                                        <label for="digits"
                                               class="block text-gray-700 text-sm font-bold mb-2">Scale {{ $attribute_type }}</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="digits" attributeholder="Enter Digits" wire:model.lazy="attribute_digits">

                                        @error('attribute_digits') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>

                                    <div class="mb-4 {{ !in_array($attribute_type,['double','float']) ? 'hidden' : '' }}">
                                        <label for="decimal"
                                               class="block text-gray-700 text-sm font-bold mb-2">Decimal {{ $attribute_type }}</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="decimal" attributeholder="Enter Decimal" wire:model.lazy="attribute_decimal">

                                        @error('attribute_decimal') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="default"
                                               class="block text-gray-700 text-sm font-bold mb-2">Default Value</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="default" attributeholder="Enter Default" wire:model.lazy="attribute_default">
                                        @error('attribute_default') <span
                                            class="text-red-500">{{ $message }}</span>@enderror
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetAttributeCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignAttribute')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeAttributeModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
