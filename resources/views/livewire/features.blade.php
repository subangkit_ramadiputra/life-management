<div x-data="{modalOpen : false}" class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
    @if($isModalOpen)
        @include('livewire.features.create')
    @endif
    @if($isEntityModalOpen)
        @include('livewire.features.assign_entity')
    @endif

    @if($isTransitionModalOpen)
        @include('livewire.features.assign_transition')
    @endif

    @if($isUploadModalOpen)
        @include('livewire.polimorph.assign_file')
    @endif
        @if($isSopModalOpen)
            @include('livewire.features.assign_sops')
        @endif
        @if($isLinkModalOpen)
            @include('livewire.features.assign_links')
        @endif
    <div class="sm:flex sm:justify-between sm:items-center mb-8">
        <!-- Left: Title -->
        <div class="mb-4 sm:mb-0">
            <h1 class="text-2xl md:text-3xl text-gray-800 font-bold">Features</h1>
        </div>
        <!-- Right: Actions -->
        <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">

            <!-- Add customer button -->
            <button class="btn bg-indigo-500 hover:bg-indigo-600 text-white" wire:click="create()">
                <svg class="w-4 h-4 fill-current opacity-50 shrink-0" viewBox="0 0 16 16">
                    <path d="M15 7H9V1c0-.6-.4-1-1-1S7 .4 7 1v6H1c-.6 0-1 .4-1 1s.4 1 1 1h6v6c0 .6.4 1 1 1s1-.4 1-1V9h6c.6 0 1-.4 1-1s-.4-1-1-1z" />
                </svg>
                <span class="xs:block ml-2">Add Feature</span>
            </button>

        </div>
    </div>
        <!-- Filter Section -->
        <div class="flex bg-white">
            <div class="bg-white p-2 pl-5">
            <div class="">
                <div class="mb-4">
                    <label for="concept_id"
                           class="block text-gray-700 text-sm font-bold mb-2">Concept</label>
                    <select class="form-select" id="concept_id" placeholder="Enter Concept" wire:model.lazy="filter_concept_id" wire:change="changeFilter">
                        <option value="0">-</option>
                        @foreach($concepts as $concept)
                            <option value="{{$concept->id}}">{{$concept->title}}</option>
                        @endforeach
                    </select>
                    @error('concept_id') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
            </div>
        </div>
            <div class="bg-white p-2">
            <div class="">
                <div class="mb-4">
                    <label for="filter_application_id"
                           class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                    <select class="form-select" id="filter_application_id" placeholder="Enter Concept" wire:model.lazy="filter_application_id" wire:change="changeFilter">
                        <option value="0">Show All Applications</option>
                        @foreach($applications as $application)
                            <option value="{{$application->id}}">{{$application->name}}</option>
                        @endforeach
                    </select>
                    @error('filter_application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                </div>
            </div>
        </div>
        </div>
        <!-- End Filter Section -->
    <div class="bg-white shadow-lg rounded-sm border border-gray-200">
        @include('livewire/alert-message-render')

        <header class="px-5 py-4">
            <h2 class="font-semibold text-gray-800">All Features <span class="text-gray-400 font-medium">{{ $total_rows }}</span></h2>
        </header>
        <div>
            <!-- Table -->
            <div class="overflow-visible">
                <table class="table-auto w-full">
                    <!-- Table header -->
                    <thead class="text-xs font-semibold uppercase text-gray-500 bg-gray-50 border-t border-b border-gray-200">
                    <tr>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">No</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3">
                            <div class="font-semibold text-left">Title</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Parent</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3">
                            <div class="font-semibold">Workflow Entity</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Sops</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Files</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap" style="min-width: 100px;>
                            <span class="sr-only">Menu</span>
                        </th>
                    </tr>
                    </thead>
                    <!-- Table body -->
                    <tbody class="text-sm divide-y divide-gray-200">
                    @foreach($features as $feature)
                        <tr>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div class="text-left">{{ $feature->id }}</div></td>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div class="text-left">
                                    <div class="mb-2">
                                        <h3 class="text-lg text-gray-800 font-semibold mb-1">{{ $feature->title }}</h3>
                                        <div class="text-sm" style="white-space: pre-wrap;">{{ $feature->summary }}</div>
                                    </div>
                                </div>
                                <div class="flex flex-wrap -space-x-px">
                                    @if($feature->featureable !== null)
                                        <a href="/entities?filter_feature_id={{ $feature->id }}&filter_application_id={{ $feature->featureable->id }}&filter_concept_id={{ $feature->featureable->concept_id }}" class="btn rounded-none border-l-gray-400 first:rounded-l last:rounded-r first:border-r-transparent bg-gray-300 hover:bg-gray-400 text-gray-800 py-1 px-2">
                                            <span>
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                                            </span>
                                            Entity
                                        </a>
                                    @else
                                        <a href="/entities?filter_feature_id={{ $feature->id }}&filter_application_id=-1&filter_concept_id=-1" class="btn rounded-none border-l-gray-400 first:rounded-l last:rounded-r first:border-r-transparent bg-gray-300 hover:bg-gray-400 text-gray-800 py-1 px-2">
                                            <span>
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5l7 7-7 7"></path></svg>
                                            </span>
                                            Entity
                                        </a>
                                    @endif
                                </div>
                            </td>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div >
                                <a href="{{ $feature->featureable->featureable_url }}" target="_blank" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                    <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path></svg>
                                </a>
                                @php
                                    $featureables = explode(' :: ',$feature->featureable->featureable_name);
                                    $colors = ['green','blue','red'];
                                @endphp
                                @foreach($featureables as $index => $feature_info)
                                    <div class="text-xs inline-flex cursor-pointer font-medium bg-{{$colors[$index]}}-100 text-{{$colors[$index]}}-600 rounded-full text-center px-2.5 py-1 mt-1">
                                        {{$feature_info}}
                                    </div><br/>
                                @endforeach
                                </div>
                            </td>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $feature->entities()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Entities</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Entity</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addEntity({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>Entity
                                    </button>
                                </div>
                                <div>
                                    @php
                                        $countB = $feature->transitions()->count();
                                    @endphp
                                    @if($countB > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countB }}</div>
                                            <div class="text-sm text-gray-500">Transitions</div>
                                        </div>
                                    @elseif ($countB > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countB }}</div>
                                            <div class="text-sm text-gray-500">Transition</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addTransition({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        Transition
                                    </button>
                                </div>
                                @if($countA > 0 && $countB > 0)
                                    <div>
                                        <button wire:click="generateWorkflow({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                            Show Workflow
                                        </button>
                                    </div>
                                @endif
                            </td>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $feature->sops()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Sops</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Sop</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addSop({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>SOP
                                    </button>
                                </div>
                            </td>
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $count = $feature->uploads()->count();
                                    @endphp
                                    @if($count > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $count }}</div>
                                            <div class="text-sm text-gray-500">Files</div>
                                        </div>
                                    @elseif ($count > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $count }}</div>
                                            <div class="text-sm text-gray-500">File</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addUpload({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>File
                                    </button>
                                </div>
                                <div>
                                    @php
                                        $countA = $feature->links()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Links</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Link</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addLink({{ $feature->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>Link
                                    </button>
                                </div>
                            </td>

                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap w-px">
                                <!-- Start -->
                                <div class="relative inline-flex" x-data="{ open: false }">
                                    <button
                                        class="text-gray-400 hover:text-gray-500 rounded-full"
                                        :class="{ 'bg-gray-100 text-gray-500': open }"
                                        aria-haspopup="true"
                                        @click.prevent="open = !open"
                                        :aria-expanded="open"
                                    >
                                        <span class="sr-only">Menu</span>
                                        <svg class="w-8 h-8 fill-current" viewBox="0 0 32 32">
                                            <circle cx="16" cy="16" r="2" />
                                            <circle cx="10" cy="16" r="2" />
                                            <circle cx="22" cy="16" r="2" />
                                        </svg>
                                    </button>
                                    <div
                                        class="origin-top-right z-10 absolute top-full left-0 min-w-36 bg-white border border-gray-200 py-1.5 rounded shadow-lg overflow-hidden mt-1"
                                        @click.outside="open = false"
                                        @keydown.escape.window="open = false"
                                        x-show="open"
                                        x-transition:enter="transition ease-out duration-200 transform"
                                        x-transition:enter-start="opacity-0 -translate-y-2"
                                        x-transition:enter-end="opacity-100 translate-y-0"
                                        x-transition:leave="transition ease-out duration-200"
                                        x-transition:leave-start="opacity-100"
                                        x-transition:leave-end="opacity-0"
                                        x-cloak
                                    >
                                        <ul>
                                            <li>
                                                <a class="font-medium text-sm text-gray-600 hover:text-gray-800 flex py-1 px-3" wire:click="edit({{ $feature->id }})" @click="open = false" @focus="open = true" @focusout="open = false">Edit</a>
                                            </li>
                                            <li>
                                                <!-- Start -->
                                                <div x-data="{ modalOpen: false }">
                                                    <a class="font-medium text-sm text-red-500 hover:text-red-600 flex py-1 px-3" @click.prevent="modalOpen = true" @focus="open = true" @focusout="open = false">Delete</a>
                                                    <!-- Modal backdrop -->
                                                    <div
                                                        class="fixed inset-0 bg-gray-900 bg-opacity-30 z-50 transition-opacity"
                                                        x-show="modalOpen"
                                                        x-transition:enter="transition ease-out duration-200"
                                                        x-transition:enter-start="opacity-0"
                                                        x-transition:enter-end="opacity-100"
                                                        x-transition:leave="transition ease-out duration-100"
                                                        x-transition:leave-start="opacity-100"
                                                        x-transition:leave-end="opacity-0"
                                                        aria-hidden="true"
                                                        x-cloak
                                                    ></div>
                                                    <!-- Modal dialog -->
                                                    <div
                                                        id="danger-modal"
                                                        class="fixed inset-0 z-50 overflow-hidden flex items-center my-4 justify-center transform px-4 sm:px-6"
                                                        role="dialog"
                                                        aria-modal="true"
                                                        x-show="modalOpen"
                                                        x-transition:enter="transition ease-in-out duration-200"
                                                        x-transition:enter-start="opacity-0 translate-y-4"
                                                        x-transition:enter-end="opacity-100 translate-y-0"
                                                        x-transition:leave="transition ease-in-out duration-200"
                                                        x-transition:leave-start="opacity-100 translate-y-0"
                                                        x-transition:leave-end="opacity-0 translate-y-4"
                                                        x-cloak
                                                    >
                                                        <div class="bg-white rounded shadow-lg overflow-auto max-w-lg w-full max-h-full" @click.outside="modalOpen = false" @keydown.escape.window="modalOpen = false">
                                                            <div class="p-5 flex space-x-4">
                                                                <!-- Icon -->
                                                                <div class="w-10 h-10 rounded-full flex items-center justify-center shrink-0 bg-red-100">
                                                                    <svg class="w-4 h-4 shrink-0 fill-current text-red-500" viewBox="0 0 16 16">
                                                                        <path d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm0 12c-.6 0-1-.4-1-1s.4-1 1-1 1 .4 1 1-.4 1-1 1zm1-3H7V4h2v5z" />
                                                                    </svg>
                                                                </div>
                                                                <!-- Content -->
                                                                <div class="w-full">
                                                                    <!-- Modal header -->
                                                                    <div class="mb-2">
                                                                        <div class="text-lg font-semibold text-gray-800">Delete 1 feature?</div>
                                                                    </div>
                                                                    <!-- Modal content -->
                                                                    <div class="text-sm mb-10">
                                                                        <div class="space-y-2">
                                                                            <p class="w-full">{{ $feature->name }}</p>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Modal footer -->
                                                                    <div class="flex flex-wrap justify-end space-x-2">
                                                                        <button class="btn-sm border-gray-200 hover:border-gray-300 text-gray-600" @click="modalOpen = false; open = false">Cancel</button>
                                                                        <button class="btn-sm bg-red-500 hover:bg-red-600 text-white" wire:click="delete({{ $feature->id }})" @click="modalOpen = false; open = false">Yes, Delete it</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End -->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End -->
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
