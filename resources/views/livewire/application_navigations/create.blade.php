<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Navigation Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Navigation Name" wire:model.lazy="name">
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title_format"
                                class="block text-gray-700 text-sm font-bold mb-2">Navigation Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title_format" placeholder="Enter Navigation Title" wire:model.lazy="title_format">
                            @error('title_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="seo_url_format"
                                class="block text-gray-700 text-sm font-bold mb-2">URL Format ( SEO Friendly )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="seo_url_format" placeholder="Enter URL Format ( SEO Friendly )" wire:model.lazy="seo_url_format">
                            @error('seo_url_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="page_type"
                                class="block text-gray-700 text-sm font-bold mb-2">Type ( Page or Link )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="page_type" placeholder="Enter Type ( Page or Link )" wire:model.lazy="page_type">
                            @error('page_type') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="navigation_group_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Navigation Group</label>
                            <input type="text"
                                class="form-input w-full"
                                id="navigation_group_id" placeholder="Enter Navigation Group" wire:model.lazy="navigation_group_id">
                            @error('navigation_group_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="navigation_back_schema"
                                class="block text-gray-700 text-sm font-bold mb-2">Navigation Back Schema ( Stack | Renavigate to )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="navigation_back_schema" placeholder="Enter Navigation Back Schema ( Stack | Renavigate to )" wire:model.lazy="navigation_back_schema">
                            @error('navigation_back_schema') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="reset_navigation_back"
                                class="block text-gray-700 text-sm font-bold mb-2">Renavigate Back to URL</label>
                            <input type="text"
                                class="form-input w-full"
                                id="reset_navigation_back" placeholder="Enter Renavigate Back to URL" wire:model.lazy="reset_navigation_back">
                            @error('reset_navigation_back') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
