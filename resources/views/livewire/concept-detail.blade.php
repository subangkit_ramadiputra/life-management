<div x-data="{modalOpen : false}" class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
    <!-- Page header -->
    <div class="sm:flex sm:justify-between sm:items-center mb-8">

        <!-- Left: Title -->
        <div class="mb-4 sm:mb-0">
            <h1 class="text-2xl md:text-3xl text-gray-800 font-bold">Concept :: {{ $concept->title }} ✨</h1>
        </div>

        <!-- Right: Actions -->
        <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">

            <!-- Datepicker built with flatpickr -->
            <div class="relative">
            </div>

        </div>

    </div>

    @include('livewire/alert-message-render',['clearSession' => true])
    <div class="grid grid-cols-12 gap-6">
        @include('livewire.components.concept-stat')
        @include('livewire.garudaku.links')
        @include('livewire.components.files')
        @include('livewire.components.task-quadrant')
        @include('livewire.components.notes')
    </div>
</div>
