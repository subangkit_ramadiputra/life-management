<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="concept_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Category</label>
                            <select class="form-select" id="concept_id"  placeholder="Enter Category" wire:model.lazy="category_id">
                                <option value="0">-</option>
                                @foreach($vendor_categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('category_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Name" wire:model.lazy="name">
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="short_name"
                                class="block text-gray-700 text-sm font-bold mb-2">Short Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="short_name" placeholder="Enter Short Name" wire:model.lazy="short_name">
                            @error('short_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="address"
                                class="block text-gray-700 text-sm font-bold mb-2">Address</label>
                            <textarea class="form-input w-full"
                                id="address" placeholder="Enter Address" wire:model.lazy="address"></textarea>
                            @error('address') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="short_address"
                                class="block text-gray-700 text-sm font-bold mb-2">Invoice Address</label>
                            <input type="text"
                                class="form-input w-full"
                                id="short_address" placeholder="Enter Invoice Address" wire:model.lazy="short_address">
                            @error('short_address') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="phone"
                                class="block text-gray-700 text-sm font-bold mb-2">Phone</label>
                            <input type="text"
                                class="form-input w-full"
                                id="phone" placeholder="Enter Phone" wire:model.lazy="phone">
                            @error('phone') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="wa"
                                class="block text-gray-700 text-sm font-bold mb-2">Whatsapp</label>
                            <input type="text"
                                class="form-input w-full"
                                id="wa" placeholder="Enter Whatsapp" wire:model.lazy="wa">
                            @error('wa') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
