Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/vendors/assign_vendors_template.blade.php to resources/views/livewire/parent/assign_vendors.blade.php
or
cp resources/views/livewire/vendors/assign_vendors_template.blade.php resources/views/livewire/parents/assign_vendors.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isVendorModalOpen)
            @include('livewire.parents.assign_vendors')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Vendors</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->vendors()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Vendors</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Vendor</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addVendor({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        Vendor
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\Vendor;


    /**
     * Get all of the parents vendors.
     */
    public function vendors()
    {
        return $this->morphMany(Vendor::class, 'vendorable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function vendors()
        {
            return $this->hasMany(Vendor::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // Vendor Section
    public $isVendorModalOpen = 0;
    public $recent_vendors = [];
    public $vendors = [];
    public function addVendor($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_vendors = $parent->vendors()->get();
        $this->resetVendorCreateForm();
        $this->openVendorModalPopover();
    }

    public function openVendorModalPopover()
    {
        $this->isVendorModalOpen = true;
    }
    public function closeVendorModalPopover()
    {
        $this->isVendorModalOpen = false;
    }

    public function editVendor($vendor_id) {
        $vendor = Vendor::findOrFail($vendor_id);
        $this->vendor_id = $vendor_id;
        $this->vendor_name = $vendor->name;
        $this->vendor_short_name = $vendor->short_name;
        $this->vendor_address = $vendor->address;
        $this->vendor_short_address = $vendor->short_address;
        $this->vendor_phone = $vendor->phone;
        $this->vendor_wa = $vendor->wa;
        $this->vendor_category_id = $vendor->category_id;

    }

    public function resetVendorCreateForm(){
        $this->vendor_id = '';
        $this->vendor_name = '';
        $this->vendor_short_name = '';
        $this->vendor_address = '';
        $this->vendor_short_address = '';
        $this->vendor_phone = '';
        $this->vendor_wa = '';
        $this->vendor_category_id = '';

    }

    public function assignVendor() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->vendor_id == '') {
            $vendor = new Vendor();
            $vendor->name = $this->vendor_name;
            $vendor->short_name = $this->vendor_short_name;
            $vendor->address = $this->vendor_address;
            $vendor->short_address = $this->vendor_short_address;
            $vendor->phone = $this->vendor_phone;
            $vendor->wa = $this->vendor_wa;
            $vendor->category_id = $this->vendor_category_id;

            $parent->vendors()->save($vendor);

            session()->flash('message', 'Vendor updated.');
            //$this->closeVendorModalPopover();
            $this->resetVendorCreateForm();
        } else {
            Vendor::updateOrCreate(['id' => $this->vendor_id], [
                'name' => $this->vendor_name,
                'short_name' => $this->vendor_short_name,
                'address' => $this->vendor_address,
                'short_address' => $this->vendor_short_address,
                'phone' => $this->vendor_phone,
                'wa' => $this->vendor_wa,
                'category_id' => $this->vendor_category_id,

            ]);
        }

        $this->recent_vendors = $parent->vendors()->get();
    }

    public function deleteVendor($parent_id, $vendor_id) {
        $parent = Parent::findOrFail($parent_id);

        $vendor = Vendor::find($vendor_id);
        if ($vendor) {
            $parent->vendors()->where('id', '=', $vendor->id)->delete();
        }

        $this->recent_vendors = $parent->vendors()->get();

        session()->flash('message', 'Vendor deleted.');
    }

    public function deleteExistingVendor($vendor_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $vendor = Vendor::find($vendor_id);
        if ($vendor) {
            $parent->vendors()->where('id', '=', $vendor->id)->delete();
        }

        $this->recent_vendors = $parent->vendors()->get();

        session()->flash('message', 'Vendor deleted.');
    }
    // End of Section
