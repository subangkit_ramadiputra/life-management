<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Name</th>
                                <th class="px-4 py-2">Description</th>
                                <th class="px-4 py-2">Method ( GET | POST | PUT | DELETE | OPTION )</th>
                                <th class="px-4 py-2">URL_Format</th>
                                <th class="px-4 py-2">Query String Sample</th>
                                <th class="px-4 py-2">POST Sample</th>
                                <th class="px-4 py-2">Application</th>


                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_application_apis as $application_api)
                                <tr>
                                    <td class="border px-4 py-2">{{ $application_api->id }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->name }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->description }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->method }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->url_format }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->query_string_sample }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->post_sample }}</td>
                                    <td class="border px-4 py-2">{{ $application_api->application_id }}</td>


                                    <td class="border px-4 py-2">
                                        <span wire:click="editApplicationApi({{ $application_api->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteApplicationApi({{ $parent_id }},{{ $application_api->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Name" wire:model.lazy="application_api_name">
                            @error('application_api_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="description"
                                class="block text-gray-700 text-sm font-bold mb-2">Description</label>
                            <textarea class="form-input w-full"
                                id="description" placeholder="Enter Description" wire:model.lazy="application_api_description"></textarea>
                            @error('application_api_description') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="method"
                                class="block text-gray-700 text-sm font-bold mb-2">Method ( GET | POST | PUT | DELETE | OPTION )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="method" placeholder="Enter Method ( GET | POST | PUT | DELETE | OPTION )" wire:model.lazy="application_api_method">
                            @error('application_api_method') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="url_format"
                                class="block text-gray-700 text-sm font-bold mb-2">URL_Format</label>
                            <textarea class="form-input w-full"
                                id="url_format" placeholder="Enter URL_Format" wire:model.lazy="application_api_url_format"></textarea>
                            @error('application_api_url_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="query_string_sample"
                                class="block text-gray-700 text-sm font-bold mb-2">Query String Sample</label>
                            <textarea class="form-input w-full"
                                id="query_string_sample" placeholder="Enter Query String Sample" wire:model.lazy="application_api_query_string_sample"></textarea>
                            @error('application_api_query_string_sample') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="post_sample"
                                class="block text-gray-700 text-sm font-bold mb-2">POST Sample</label>
                            <textarea class="form-input w-full"
                                id="post_sample" placeholder="Enter POST Sample" wire:model.lazy="application_api_post_sample"></textarea>
                            @error('application_api_post_sample') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="application_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                            <input type="text"
                                class="form-input w-full"
                                id="application_id" placeholder="Enter Application" wire:model.lazy="application_api_application_id">
                            @error('application_api_application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetApplicationApiCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignApplicationApi')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeApplicationApiModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
