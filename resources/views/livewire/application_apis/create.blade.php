<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Name" wire:model.lazy="name">
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="description"
                                class="block text-gray-700 text-sm font-bold mb-2">Description</label>
                            <textarea class="form-input w-full"
                                id="description" placeholder="Enter Description" wire:model.lazy="description"></textarea>
                            @error('description') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="method"
                                class="block text-gray-700 text-sm font-bold mb-2">Method ( GET | POST | PUT | DELETE | OPTION )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="method" placeholder="Enter Method ( GET | POST | PUT | DELETE | OPTION )" wire:model.lazy="method">
                            @error('method') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="url_format"
                                class="block text-gray-700 text-sm font-bold mb-2">URL_Format</label>
                            <textarea class="form-input w-full"
                                id="url_format" placeholder="Enter URL_Format" wire:model.lazy="url_format"></textarea>
                            @error('url_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="query_string_sample"
                                class="block text-gray-700 text-sm font-bold mb-2">Query String Sample</label>
                            <textarea class="form-input w-full"
                                id="query_string_sample" placeholder="Enter Query String Sample" wire:model.lazy="query_string_sample"></textarea>
                            @error('query_string_sample') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="post_sample"
                                class="block text-gray-700 text-sm font-bold mb-2">POST Sample</label>
                            <textarea class="form-input w-full"
                                id="post_sample" placeholder="Enter POST Sample" wire:model.lazy="post_sample"></textarea>
                            @error('post_sample') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="application_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                            <input type="text"
                                class="form-input w-full"
                                id="application_id" placeholder="Enter Application" wire:model.lazy="application_id">
                            @error('application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
