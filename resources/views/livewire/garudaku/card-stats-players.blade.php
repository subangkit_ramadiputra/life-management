<div class="flex flex-col col-span-full md:col-span-11 xl:col-span-8 bg-white shadow-lg rounded-sm border border-gray-200 mt-3">
    <header class="px-5 py-4 border-b border-gray-100 flex items-center">
        <h2 class="font-semibold text-gray-800">Players</h2>
    </header>
    <div class="px-5 py-1">
        <div class="flex flex-wrap">
            <!-- Total Players -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($player_stats['TOTAL_ROW_PLAYER']) }}</div>
                        <div class="text-sm font-medium text-green-500">+49%</div>
                    </div>
                    <div class="text-sm text-gray-500">Total Players</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Verified Players -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($player_stats['VERIFIED']) }}</div>
                        <div class="text-sm font-medium text-green-500">+49%</div>
                    </div>
                    <div class="text-sm text-gray-500" wire:click="fetchPlayerStatistic()">Verified</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
            <!-- Verified Players -->
            <div class="flex items-center py-2">
                <div class="mr-5">
                    <div class="flex items-center">
                        <div class="text-3xl font-bold text-gray-800 mr-2">{{ number_format($player_stats['TOTAL_SMS_SENT']) }}</div>
                        <div class="text-sm font-medium text-green-500">+49%</div>
                    </div>
                    <div class="text-sm text-gray-500" wire:click="fetchPlayerStatistic()">SMS Sent</div>
                </div>
                <div class="hidden md:block w-px h-8 bg-gray-200 mr-5" aria-hidden="true"></div>
            </div>
        </div>
    </div>
</div>
