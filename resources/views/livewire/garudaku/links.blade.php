<div class="col-span-full gap-1 flex flex-wrap items-center mt-3">
    @foreach($links as $link)
    <div class="m-1.5">
        <!-- Start -->
        <div >
            <a class="btn bg-indigo-500 hover:bg-indigo-600 text-white" href="{{ $link->url }}" target="_blank"
            >{{ $link->title }}</a>
        </div>
        <!-- End -->
    </div>
    @endforeach
</div>
