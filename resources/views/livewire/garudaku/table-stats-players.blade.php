<div class="col-span-full md:col-span-8 xl:col-span-4 bg-white shadow-lg rounded-sm border border-gray-200 mt-3">
    <header class="px-5 py-4 border-b border-gray-100">
        <h2 class="font-semibold text-gray-800">KPI Player</h2>
    </header>
    <div class="p-3">

        <!-- Table -->
        <div class="overflow-visible">
            <table class="table-auto w-full">
                <!-- Table header -->
                <thead class="text-xs uppercase text-gray-400 bg-gray-50 rounded-sm">
                <tr>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-left">KPI</div>
                    </th>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-left">Jumlah</div>
                    </th>

                </tr>
                </thead>
                <!-- Table body -->
                <tbody class="text-sm divide-y divide-gray-100">
                <!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">TOTAL_ROW_PLAYER</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['TOTAL_ROW_PLAYER']) }}</div>
                    </td>
                </tr>
                <!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">VERIFIED</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['VERIFIED']) }}</div>
                    </td>
                </tr><!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">ADDPROFILE</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['ADDPROFILE']) }}</div>
                    </td>
                </tr><!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">MALE</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['MALE']) }}</div>
                    </td>
                </tr><!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">FEMALE</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['FEMALE']) }}</div>
                    </td>
                </tr>
                <!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">JKNULL</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['JKNULL']) }}</div>
                    </td>
                </tr>
                <!-- Row -->
                <tr>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-center">TOTAL_SMS_SENT</div>
                    </td>
                    <td class="p-2 whitespace-nowrap">
                        <div class="text-lg text-center">{{ number_format($player_stats['TOTAL_SMS_SENT']) }}</div>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
