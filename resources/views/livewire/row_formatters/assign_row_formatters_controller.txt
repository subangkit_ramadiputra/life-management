Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/row_formatters/assign_row_formatters_template.blade.php to resources/views/livewire/parent/assign_row_formatters.blade.php
or
cp resources/views/livewire/row_formatters/assign_row_formatters_template.blade.php resources/views/livewire/parents/assign_row_formatters.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isRowFormatterModalOpen)
            @include('livewire.parents.assign_row_formatters')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">RowFormatters</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->row_formatters()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">RowFormatters</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">RowFormatter</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addRowFormatter({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        RowFormatter
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\RowFormatter;


    /**
     * Get all of the parents row_formatters.
     */
    public function row_formatters()
    {
        return $this->morphMany(RowFormatter::class, 'row_formatterable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function row_formatters()
        {
            return $this->hasMany(RowFormatter::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // RowFormatter Section
    public $isRowFormatterModalOpen = 0;
    public $recent_row_formatters = [];
    public $row_formatters = [];
    public function addRowFormatter($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_row_formatters = $parent->row_formatters()->get();
        $this->resetRowFormatterCreateForm();
        $this->openRowFormatterModalPopover();
    }

    public function openRowFormatterModalPopover()
    {
        $this->isRowFormatterModalOpen = true;
    }
    public function closeRowFormatterModalPopover()
    {
        $this->isRowFormatterModalOpen = false;
    }

    public function editRowFormatter($row_formatter_id) {
        $row_formatter = RowFormatter::findOrFail($row_formatter_id);
        $this->row_formatter_id = $row_formatter_id;
        $this->row_formatter_title = $row_formatter->title;
        $this->row_formatter_summary = $row_formatter->summary;
        $this->row_formatter_query = $row_formatter->query;
        $this->row_formatter_row_template = $row_formatter->row_template;
        $this->row_formatter_connection = $row_formatter->connection;
        $this->row_formatter_parameters = $row_formatter->parameters;

    }

    public function resetRowFormatterCreateForm(){
        $this->row_formatter_id = '';
        $this->row_formatter_title = '';
        $this->row_formatter_summary = '';
        $this->row_formatter_query = '';
        $this->row_formatter_row_template = '';
        $this->row_formatter_connection = 'mysql';
        $this->row_formatter_parameters = '';

    }

    public function assignRowFormatter() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->row_formatter_id == '') {
            $row_formatter = new RowFormatter();
            $row_formatter->title = $this->row_formatter_title;
            $row_formatter->summary = $this->row_formatter_summary;
            $row_formatter->query = $this->row_formatter_query;
            $row_formatter->row_template = $this->row_formatter_row_template;
            $row_formatter->connection = $this->row_formatter_connection;
            $row_formatter->parameters = $this->row_formatter_parameters;

            $parent->row_formatters()->save($row_formatter);

            $this->alertSuccessMessage('RowFormatter created.');
            //$this->closeRowFormatterModalPopover();
            $this->resetRowFormatterCreateForm();
        } else {
            RowFormatter::updateOrCreate(['id' => $this->row_formatter_id], [
                'title' => $this->row_formatter_title,
                'summary' => $this->row_formatter_summary,
                'query' => $this->row_formatter_query,
                'row_template' => $this->row_formatter_row_template,
                'connection' => $this->row_formatter_connection,
                'parameters' => $this->row_formatter_parameters,

            ]);
            $this->alertSuccessMessage('RowFormatter updated.');
        }

        $this->recent_row_formatters = $parent->row_formatters()->get();
    }

    public function deleteRowFormatter($parent_id, $row_formatter_id) {
        $parent = Parent::findOrFail($parent_id);

        $row_formatter = RowFormatter::find($row_formatter_id);
        if ($row_formatter) {
            $parent->row_formatters()->where('id', '=', $row_formatter->id)->delete();
        }

        $this->recent_row_formatters = $parent->row_formatters()->get();

        $this->alertSuccessMessage('RowFormatter deleted.');
    }

    public function deleteExistingRowFormatter($row_formatter_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $row_formatter = RowFormatter::find($row_formatter_id);
        if ($row_formatter) {
            $parent->row_formatters()->where('id', '=', $row_formatter->id)->delete();
        }

        $this->recent_row_formatters = $parent->row_formatters()->get();

        $this->alertSuccessMessage('RowFormatter deleted.');
    }
    // End of Section
