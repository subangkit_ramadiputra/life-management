<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{jsoneditor:null, jsoneditor2:null}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" style="min-width: 1000px;"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="connection"
                                   class="block text-gray-700 text-sm font-bold mb-2">DB Connection</label>
                            <input type="text"
                                   class="form-input w-full"
                                   id="connection" placeholder="Enter DB Connection" wire:model.lazy="connection">
                            @error('connection') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="connection"
                                   class="block text-gray-700 text-sm font-bold mb-2">DB Connection</label>
                            <select class="form-select" id="connection" wire:model.lazy="connection">
                                <option value="0">-</option>
                                @foreach($connections as $connection)
                                    <option value="{{$connection->name}}">{{$connection->name}} ({{$connection->driver}})</option>
                                @endforeach
                            </select>
                            @error('context_definition') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Parameters</label>
                            <textarea class="form-input w-full hidden" id="parameters_existing_value" wire:model.lazy="parameters"></textarea>
                            <div id="parameters_editor" style="width: 100%; height: 400px;"></div>
                            <div wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('parameters'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('parameters');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                const options = {}
                                                                this.instance = true
                                                                jsoneditor2 = new JSONEditor(document.getElementById('parameters_editor'), options)

                                                                // set json
                                                                const value = document.getElementById('parameters_existing_value').value
                                                                const initialJson = (value !== '') ? JSON.parse(value) : {attribute_name:'Default Value'}
                                                                jsoneditor2.set(initialJson)

                                                                window.clear = function(){
                                                                    jsoneditor2.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="parameters" placeholder="Enter Parameters" wire:model.defer="parameters">
                                @error('parameters') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Query</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('query'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('query');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                editor = ace.edit(document.getElementById('query'), {
                                                                    mode: 'ace/mode/sql',
                                                                                        theme: 'ace/theme/sql',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                editor.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                editor.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    editor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="query" placeholder="Enter Query" wire:model.defer="query"></textarea>
                            <br/>
                            <a href="#" x-on:click="@this.set('parameters',JSON.stringify(jsoneditor2.get()));@this.set('row_template', editor2.getValue());@this.set('query', editor.getValue());@this.call('testQuery'); return false;" class="justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">Run Query</a>
                            @error('query') <span class="text-red-500">{{ $message }}</span>@enderror
                            <div style="white-space: pre">{{ $query_results }}</div>
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Row Template</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('row_template'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('row_template');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                editor2 = ace.edit(document.getElementById('row_template'), {
                                                                    mode: 'ace/mode/twig',
                                                                                        theme: 'ace/theme/monokai',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                editor2.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                editor2.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    editor2.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="row_template" placeholder="Enter Row Template" wire:model.defer="row_template"></textarea>
                            <br/>
                            <a href="#" x-on:click="@this.set('parameters',JSON.stringify(jsoneditor2.get()));@this.set('row_template', editor2.getValue());@this.call('testTemplate'); return false;" class="justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">Run Template</a>
                            @error('row_template') <span class="text-red-500">{{ $message }}</span>@enderror
                            <div style="white-space: pre">{{ $template_result }}</div>
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('parameters',JSON.stringify(jsoneditor2.get()));@this.set('query', editor.getValue());@this.set('row_template', editor2.getValue());@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
