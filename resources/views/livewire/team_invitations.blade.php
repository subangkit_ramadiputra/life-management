<div x-data="{modalOpen : false}" class="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl mx-auto">
    @if($isModalOpen)
        @include('livewire.team_invitations.create')
    @endif
    <div class="sm:flex sm:justify-between sm:items-center mb-8">
        <!-- Left: Title -->
        <div class="mb-4 sm:mb-0">
            <h1 class="text-2xl md:text-3xl text-gray-800 font-bold">TeamInvitations</h1>
        </div>
        <!-- Right: Actions -->
        <div class="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">

            <!-- Add customer button -->
            <button class="btn bg-indigo-500 hover:bg-indigo-600 text-white" wire:click="create()">
                <svg class="w-4 h-4 fill-current opacity-50 shrink-0" viewBox="0 0 16 16">
                    <path d="M15 7H9V1c0-.6-.4-1-1-1S7 .4 7 1v6H1c-.6 0-1 .4-1 1s.4 1 1 1h6v6c0 .6.4 1 1 1s1-.4 1-1V9h6c.6 0 1-.4 1-1s-.4-1-1-1z" />
                </svg>
                <span class="xs:block ml-2">Add TeamInvitation</span>
            </button>

        </div>
    </div>

    <div class="bg-white shadow-lg rounded-sm border border-gray-200">
        @include('livewire/alert-message-render')

        <header class="px-5 py-4">
            <h2 class="font-semibold text-gray-800">All TeamInvitations <span class="text-gray-400 font-medium">{{ $total_rows }}</span></h2>
        </header>
        <div>
            <!-- Table -->
            <div class="overflow-visible">
                <table class="table-auto w-full">
                    <!-- Table header -->
                    <thead class="text-xs font-semibold uppercase text-gray-500 bg-gray-50 border-t border-b border-gray-200">
                    <tr>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">No</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Team</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Email</div>
                        </th>
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Role</div>
                        </th>

                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <span class="sr-only">Menu</span>
                        </th>
                    </tr>
                    </thead>
                    <!-- Table body -->
                    <tbody class="text-sm divide-y divide-gray-200">
                        @foreach($team_invitations as $team_invitation)
                    <!-- Row -->
                    <tr>
                        <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="text-left">{{ $team_invitation->id }}</div>
                        </td>
                        <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="text-left">{{ $team_invitation->team_id }}</div>
                        </td>
                        <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="text-left">{{ $team_invitation->email }}</div>
                        </td>
                        <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="text-left">{{ $team_invitation->role }}</div>
                        </td>

                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap w-px" style="min-width: 100px;">
                                <!-- Start -->
                                <div class="relative inline-flex" x-data="{ open: false }">
                                    <button
                                        class="text-gray-400 hover:text-gray-500 rounded-full"
                                        :class="{ 'bg-gray-100 text-gray-500': open }"
                                        aria-haspopup="true"
                                        @click.prevent="open = !open"
                                        :aria-expanded="open"
                                    >
                                        <span class="sr-only">Menu</span>
                                        <svg class="w-8 h-8 fill-current" viewBox="0 0 32 32">
                                            <circle cx="16" cy="16" r="2" />
                                            <circle cx="10" cy="16" r="2" />
                                            <circle cx="22" cy="16" r="2" />
                                        </svg>
                                    </button>
                                    <div
                                        class="origin-top-right z-10 absolute top-full left-0 min-w-36 bg-white border border-gray-200 py-1.5 rounded shadow-lg overflow-hidden mt-1"
                                        @click.outside="open = false"
                                        @keydown.escape.window="open = false"
                                        x-show="open"
                                        x-transition:enter="transition ease-out duration-200 transform"
                                        x-transition:enter-start="opacity-0 -translate-y-2"
                                        x-transition:enter-end="opacity-100 translate-y-0"
                                        x-transition:leave="transition ease-out duration-200"
                                        x-transition:leave-start="opacity-100"
                                        x-transition:leave-end="opacity-0"
                                        x-cloak
                                    >
                                        <ul>
                                            <li>
                                                <a class="font-medium text-sm text-gray-600 hover:text-gray-800 flex py-1 px-3" wire:click="edit({{ $team_invitation->id }})" @click="open = false" @focus="open = true" @focusout="open = false">Edit</a>
                                            </li>
                                            <li>
                                                <!-- Start -->
                                                <div x-data="{ modalOpen: false }">
                                                    <a class="font-medium text-sm text-red-500 hover:text-red-600 flex py-1 px-3" @click.prevent="modalOpen = true" @focus="open = true" @focusout="open = false">Delete</a>
                                                    <!-- Modal backdrop -->
                                                    <div
                                                        class="fixed inset-0 bg-gray-900 bg-opacity-30 z-50 transition-opacity"
                                                        x-show="modalOpen"
                                                        x-transition:enter="transition ease-out duration-200"
                                                        x-transition:enter-start="opacity-0"
                                                        x-transition:enter-end="opacity-100"
                                                        x-transition:leave="transition ease-out duration-100"
                                                        x-transition:leave-start="opacity-100"
                                                        x-transition:leave-end="opacity-0"
                                                        aria-hidden="true"
                                                        x-cloak
                                                    ></div>
                                                    <!-- Modal dialog -->
                                                    <div
                                                        id="danger-modal"
                                                        class="fixed inset-0 z-50 overflow-hidden flex items-center my-4 justify-center transform px-4 sm:px-6"
                                                        role="dialog"
                                                        aria-modal="true"
                                                        x-show="modalOpen"
                                                        x-transition:enter="transition ease-in-out duration-200"
                                                        x-transition:enter-start="opacity-0 translate-y-4"
                                                        x-transition:enter-end="opacity-100 translate-y-0"
                                                        x-transition:leave="transition ease-in-out duration-200"
                                                        x-transition:leave-start="opacity-100 translate-y-0"
                                                        x-transition:leave-end="opacity-0 translate-y-4"
                                                        x-cloak
                                                    >
                                                        <div class="bg-white rounded shadow-lg overflow-auto max-w-lg w-full max-h-full" @click.outside="modalOpen = false" @keydown.escape.window="modalOpen = false">
                                                            <div class="p-5 flex space-x-4">
                                                                <!-- Icon -->
                                                                <div class="w-10 h-10 rounded-full flex items-center justify-center shrink-0 bg-red-100">
                                                                    <svg class="w-4 h-4 shrink-0 fill-current text-red-500" viewBox="0 0 16 16">
                                                                        <path d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm0 12c-.6 0-1-.4-1-1s.4-1 1-1 1 .4 1 1-.4 1-1 1zm1-3H7V4h2v5z" />
                                                                    </svg>
                                                                </div>
                                                                <!-- Content -->
                                                                <div class="w-full">
                                                                    <!-- Modal header -->
                                                                    <div class="mb-2">
                                                                        <div class="text-lg font-semibold text-gray-800">Delete 1 team_invitation?</div>
                                                                    </div>
                                                                    <!-- Modal content -->
                                                                    <div class="text-sm mb-10">
                                                                        <div class="space-y-2">
                                                                            <p class="w-full">{{ $team_invitation->title }}</p>
                                                                        </div>
                                                                    </div>
                                                                    <!-- Modal footer -->
                                                                    <div class="flex flex-wrap justify-end space-x-2">
                                                                        <button class="btn-sm border-gray-200 hover:border-gray-300 text-gray-600" @click="modalOpen = false; open = false">Cancel</button>
                                                                        <button class="btn-sm bg-red-500 hover:bg-red-600 text-white" wire:click="delete({{ $team_invitation->id }})" @click="modalOpen = false; open = false">Yes, Delete it</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- End -->
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End -->
                            </td>

                    </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
