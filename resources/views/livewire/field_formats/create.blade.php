<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{jsoneditor: null}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="format_name"
                                class="block text-gray-700 text-sm font-bold mb-2">Field Format Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="format_name" placeholder="Enter Field Format Name" wire:model.lazy="format_name">
                            @error('format_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Input Requirement (JSON)</label>
                            <textarea class="form-input w-full hidden" id="input_requirement_existing_value" wire:model.lazy="input_requirement"></textarea>
                            <div id="input_requirement_editor" style="width: 100%; height: 400px;"></div>
                            <div wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('input_requirement'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('input_requirement');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                const options = {
                                                                    mode: 'tree',
                                                                    forceUnique: false
                                                                }
                                                                this.instance = true
                                                                jsoneditor = new JSONEditor(document.getElementById('input_requirement_editor'), options)

                                                                // set json
                                                                const value = document.getElementById('input_requirement_existing_value').value
                                                                const initialJson = (value !== '') ? JSON.parse(value) : {attribute_name:'value'}
                                                                jsoneditor.set(initialJson)

                                                                window.clear = function(){
                                                                    jsoneditor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="input_requirement" placeholder="Enter Input Requirement (JSON)" wire:model.defer="input_requirement">
                                @error('input_requirement') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>Testing <p></p>
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('input_requirement',JSON.stringify(jsoneditor.get()));@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
