<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body"
                                   class="block text-gray-700 text-sm font-bold mb-2">Detail</label>
                            <textarea
                                wire:key="{{ rand() }}"
                                x-data="{
                                                     value: @entangle('body'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this
                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditor != null) {
                                                                tinymce.activeEditor.destroy();
                                                            }
                                                            this.instance = tinymce.init({
                                                                selector: '#body',
                                                                plugins: 'anchor autolink charmap codesample emoticons image link lists media searchreplace table visualblocks wordcount',
                                                                toolbar: 'undo redo | bold italic underline strikethrough | numlist bullist indent outdent | blocks fontfamily fontsize | link image media table | align lineheight | emoticons charmap | removeformat',

                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }"
                                class="form-input w-full" rows="20"
                                id="body" placeholder="Enter Detail" wire:model.defer="body"></textarea>
                            @error('body') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="noteable"
                                class="block text-gray-700 text-sm font-bold mb-2">Object</label>
                            <input type="text"
                                class="form-input w-full"
                                id="noteable" placeholder="Enter Object" wire:model.lazy="noteable">
                            @error('noteable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('body',document.getElementById('body').value);@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
