<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{editor:null,testeditortemplate:null,jsoneditor:null}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="context_definition_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">context_definition</label>
                            <select class="form-select" id="context_definition_id" wire:model.lazy="context_definition_id" wire:change="changeDefinition">
                                <option value="0">-</option>
                                @foreach($context_definitions as $context_definition)
                                    <option value="{{$context_definition->id}}">{{$context_definition->context_name}}</option>
                                @endforeach
                            </select>
                            @error('context_definition') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="framework_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Framework</label>
                            <select class="form-select" id="framework_id" wire:model.lazy="framework_id">
                                <option value="0">-</option>
                                @foreach($frameworks as $framework)
                                    <option value="{{$framework->id}}">{{$framework->Framework}}</option>
                                @endforeach
                            </select>
                            @error('framework') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="scope_generation"
                                   class="block text-gray-700 text-sm font-bold mb-2">scope</label>
                            <select class="form-select" id="scope_generation" wire:model.lazy="scope_generation">
                                <option value="0">-</option>
                                @foreach($scopes as $scope)
                                    <option value="{{$scope}}">{{$scope}}</option>
                                @endforeach
                            </select>
                            @error('scope_generation') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Custom Data</label>
                            <textarea class="form-input w-full hidden" id="custom_data_existing_value" wire:model.lazy="custom_data"></textarea>
                            <div id="custom_data_editor" style="width: 100%; height: 400px;"></div>
                            <div wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('custom_data'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('custom_data');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                const options = {}
                                                                this.instance = true
                                                                jsoneditor = new JSONEditor(document.getElementById('custom_data_editor'), options)

                                                                // set json
                                                                const value = document.getElementById('custom_data_existing_value').value
                                                                const initialJson = (value !== '') ? JSON.parse(value) : {users:'SELECT * FROM users'}
                                                                jsoneditor.set(initialJson)

                                                                window.clear = function(){
                                                                    jsoneditor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="custom_data" placeholder="Enter Custom Data" wire:model.defer="custom_data">
                                @error('custom_data') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>Testing <p></p>
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Template</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('template'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('template');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                editor = ace.edit(document.getElementById('template'), {
                                                                    mode: 'ace/mode/{{ ($mode != '') ? $mode : 'html' }}',
                                                                                        theme: 'ace/theme/monokai',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                editor.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                editor.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    editor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="template" placeholder="Enter Template" wire:model.defer="template"></textarea>
                            @error('template') <span class="text-red-500">{{ $message }}</span>@enderror
                            <button x-on:click="@this.set('template', editor.getValue());@this.set('custom_data',JSON.stringify(jsoneditor.get()));@this.call('testTemplate')" type="button"
                                    class="inline-flex justify-center rounded-md border border-transparent px-4 py-2 bg-emerald-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-emerald-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                Test
                            </button>
                            @if($testing_template != '')
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('testing_template'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('testing_template');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                testeditortemplate = ace.edit(document.getElementById('testing_template'), {
                                                                    mode: 'ace/mode/{{ ($mode != '') ? $mode : 'html' }}',
                                                                                        theme: 'ace/theme/monokai',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                testeditortemplate.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                testeditortemplate.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    testeditortemplate.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="testing_template" placeholder="Press Test" wire:model.defer="testing_template" readonly></textarea>
                            @endif
                        </div>
                        <div class="mb-4">
                            <label for="mode"
                                   class="block text-gray-700 text-sm font-bold mb-2">Mode</label>
                            <input type="text"
                                   class="form-input w-full"
                                   id="mode" placeholder="Enter Mode" wire:model.lazy="mode">
                            @error('mode') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('template', editor.getValue());@this.set('custom_data',JSON.stringify(jsoneditor.get()));@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
