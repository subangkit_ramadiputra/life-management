<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="vendor_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Vendor</label>
                            <select class="form-select" id="vendor_id"  placeholder="Enter Vendor" wire:model.lazy="vendor_id">
                                <option value="0">Master</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                @endforeach
                            </select>
                            @error('vendor_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="parent_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Parent Navigation</label>
                            <select class="form-select" id="parent_id"  placeholder="Enter Category" wire:model.lazy="parent_id">
                                <option value="0">ROOT</option>
                                @foreach($nav_parents as $navigation)
                                    <option value="{{$navigation->id}}">{{$navigation->title}}</option>
                                @endforeach
                            </select>
                            @error('parent_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="application_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                            <select class="form-select" id="application_id"  placeholder="Enter Application" wire:model.lazy="application_id">
                                <option value="0">Master</option>
                                @foreach($applications as $application)
                                    <option value="{{$application->id}}">{{$application->name}}</option>
                                @endforeach
                            </select>
                            @error('application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div x-data="{use_other: false}" >
                            <label for="group"
                                   class="block text-gray-700 text-sm font-bold mb-2">Nav Group</label>
                            <input class="form-check mb-2" type="checkbox" x-model="use_other" name="use_other" id="use_other" value="Yes"> Create New
                            <div x-show="!use_other" class="mb-4">
                                <select class="form-select" id="group"  placeholder="Enter Group" wire:model.lazy="group">
                                    <option value="0">None</option>
                                    @foreach($groups as $group)
                                        <option value="{{$group->id}}">{{$group->name}}</option>
                                    @endforeach
                                </select>
                                @error('group') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>

                            <div x-show="use_other" class="mb-4">
                                <input type="text"
                                       class="form-input w-full"
                                       id="new_group" placeholder="Enter Navigation Group" wire:model.lazy="new_group">
                                @error('new_group') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="controller"
                                   class="block text-gray-700 text-sm font-bold mb-2">Controller</label>
                            <select class="form-select" id="controller"  placeholder="Enter Controller" wire:model.lazy="controller">
                                @foreach($controllers as $controller)
                                    <option value="\App\Http\Livewire\{{$controller}}">\App\Http\Livewire\{{$controller}}</option>
                                @endforeach
                            </select>
                            @error('controller') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="function"
                                class="block text-gray-700 text-sm font-bold mb-2">Function</label>
                            <input type="text"
                                class="form-input w-full"
                                id="function" placeholder="Enter Function" wire:model.lazy="function">
                            @error('function') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="route"
                                class="block text-gray-700 text-sm font-bold mb-2">URL</label>
                            <input type="text"
                                class="form-input w-full"
                                id="route" placeholder="Enter URL" wire:model.lazy="route">
                            @error('route') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="is_show"
                                class="block text-gray-700 text-sm font-bold mb-2">Show Navigation</label>
                            <input type="text"
                                class="form-input w-full"
                                id="is_show" placeholder="Enter Show Navigation" wire:model.lazy="is_show">
                            @error('is_show') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="is_enable"
                                class="block text-gray-700 text-sm font-bold mb-2">Enable Navigation</label>
                            <input type="text"
                                class="form-input w-full"
                                id="is_enable" placeholder="Enter Enable Navigation" wire:model.lazy="is_enable">
                            @error('is_enable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="class"
                                   class="block text-gray-700 text-sm font-bold mb-2">CSS Class</label>
                            <input type="text"
                                   class="form-input w-full"
                                   id="class" placeholder="Enter CSS Class" wire:model.lazy="class">
                            @error('class') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="svg"
                                   class="block text-gray-700 text-sm font-bold mb-2">SVG Icon</label>
                            <textarea class="form-input w-full"
                                      id="svg" placeholder="Enter SVG Icon" wire:model.lazy="svg"></textarea>
                            @error('svg') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="weight"
                                class="block text-gray-700 text-sm font-bold mb-2">Weight</label>
                            <input type="text"
                                class="form-input w-full"
                                id="weight" placeholder="Enter Weight" wire:model.lazy="weight">
                            @error('weight') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
