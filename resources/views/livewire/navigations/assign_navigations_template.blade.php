<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Vendor</th>
                                <th class="px-4 py-2">Nav Group</th>
                                <th class="px-4 py-2">Title</th>
                                <th class="px-4 py-2">Controller</th>
                                <th class="px-4 py-2">Function</th>
                                <th class="px-4 py-2">URL</th>
                                <th class="px-4 py-2">Parent Navigation</th>
                                <th class="px-4 py-2">Show Navigation</th>
                                <th class="px-4 py-2">Enable Navigation</th>
                                <th class="px-4 py-2">Application</th>
                                <th class="px-4 py-2">CSS Class</th>
                                <th class="px-4 py-2">Weight</th>
                                <th class="px-4 py-2">Navigation Sort Code</th>


                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_navigations as $navigation)
                                <tr>
                                    <td class="border px-4 py-2">{{ $navigation->id }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->vendor_id }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->group }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->title }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->controller }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->function }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->route }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->parent_id }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->is_show }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->is_enable }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->application_id }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->class }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->weight }}</td>
                                    <td class="border px-4 py-2">{{ $navigation->nav_sort }}</td>


                                    <td class="border px-4 py-2">
                                        <span wire:click="editNavigation({{ $navigation->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteNavigation({{ $parent_id }},{{ $navigation->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                        <div class="mb-4">
                            <label for="vendor_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Vendor</label>
                            <input type="text"
                                class="form-input w-full"
                                id="vendor_id" placeholder="Enter Vendor" wire:model.lazy="navigation_vendor_id">
                            @error('navigation_vendor_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="group"
                                class="block text-gray-700 text-sm font-bold mb-2">Nav Group</label>
                            <input type="text"
                                class="form-input w-full"
                                id="group" placeholder="Enter Nav Group" wire:model.lazy="navigation_group">
                            @error('navigation_group') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="navigation_title">
                            @error('navigation_title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="controller"
                                class="block text-gray-700 text-sm font-bold mb-2">Controller</label>
                            <input type="text"
                                class="form-input w-full"
                                id="controller" placeholder="Enter Controller" wire:model.lazy="navigation_controller">
                            @error('navigation_controller') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="function"
                                class="block text-gray-700 text-sm font-bold mb-2">Function</label>
                            <input type="text"
                                class="form-input w-full"
                                id="function" placeholder="Enter Function" wire:model.lazy="navigation_function">
                            @error('navigation_function') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="route"
                                class="block text-gray-700 text-sm font-bold mb-2">URL</label>
                            <input type="text"
                                class="form-input w-full"
                                id="route" placeholder="Enter URL" wire:model.lazy="navigation_route">
                            @error('navigation_route') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="parent_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Parent Navigation</label>
                            <input type="text"
                                class="form-input w-full"
                                id="parent_id" placeholder="Enter Parent Navigation" wire:model.lazy="navigation_parent_id">
                            @error('navigation_parent_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="is_show"
                                class="block text-gray-700 text-sm font-bold mb-2">Show Navigation</label>
                            <input type="text"
                                class="form-input w-full"
                                id="is_show" placeholder="Enter Show Navigation" wire:model.lazy="navigation_is_show">
                            @error('navigation_is_show') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="is_enable"
                                class="block text-gray-700 text-sm font-bold mb-2">Enable Navigation</label>
                            <input type="text"
                                class="form-input w-full"
                                id="is_enable" placeholder="Enter Enable Navigation" wire:model.lazy="navigation_is_enable">
                            @error('navigation_is_enable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="application_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                            <input type="text"
                                class="form-input w-full"
                                id="application_id" placeholder="Enter Application" wire:model.lazy="navigation_application_id">
                            @error('navigation_application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="class"
                                class="block text-gray-700 text-sm font-bold mb-2">CSS Class</label>
                            <input type="text"
                                class="form-input w-full"
                                id="class" placeholder="Enter CSS Class" wire:model.lazy="navigation_class">
                            @error('navigation_class') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="svg"
                                class="block text-gray-700 text-sm font-bold mb-2">SVG Icon</label>
                            <textarea class="form-input w-full"
                                id="svg" placeholder="Enter SVG Icon" wire:model.lazy="navigation_svg"></textarea>
                            @error('navigation_svg') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="weight"
                                class="block text-gray-700 text-sm font-bold mb-2">Weight</label>
                            <input type="text"
                                class="form-input w-full"
                                id="weight" placeholder="Enter Weight" wire:model.lazy="navigation_weight">
                            @error('navigation_weight') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="nav_sort"
                                class="block text-gray-700 text-sm font-bold mb-2">Navigation Sort Code</label>
                            <input type="text"
                                class="form-input w-full"
                                id="nav_sort" placeholder="Enter Navigation Sort Code" wire:model.lazy="navigation_nav_sort">
                            @error('navigation_nav_sort') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetNavigationCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignNavigation')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeNavigationModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
