@if(Auth::user() != null)
<div
    x-data="{ page: @entangle('page'),
    menu_data: ['clusters','etls'],
    menu_infra: ['servers'],
    menu_application: ['applications','features', 'entities'],
    menu_vendor: ['vendors','vendor_members','vendor_categories'],
    menu_authentication: ['users','teams','team_invitations','team_users'],
    menu_navigation: ['navigations','navigation_groups'],
    @foreach($view_navigations as $navigation)
    @if(count($navigation['children']) > 0)
    @php
        $menu_array = "";
        foreach($navigation['children'] as $child) { $menu_array .= "'".$child['route']."',"; }
        $menu_array = substr($menu_array,0, strlen($menu_array) -1);
    @endphp
        menu_{{ $navigation['id'] }}: [{{ $menu_array }}],
    @endif
    @endforeach

    }">
    <!-- Sidebar backdrop (mobile only) -->
    <div
        class="fixed inset-0 bg-gray-900 bg-opacity-30 z-40 lg:hidden lg:z-auto transition-opacity duration-200"
        :class="sidebarOpen ? 'opacity-100' : 'opacity-0 pointer-events-none'"
        aria-hidden="true"
        x-cloak
    ></div>

    <!-- Sidebar -->
    <div
        id="sidebar"
        class="flex flex-col absolute z-40 left-0 top-0 lg:static lg:left-auto lg:top-auto lg:translate-x-0 transform h-screen overflow-y-scroll lg:overflow-y-auto no-scrollbar w-64 lg:w-20 lg:sidebar-expanded:!w-64 2xl:!w-64 shrink-0 bg-gray-800 p-4 transition-all duration-200 ease-in-out"
        :class="sidebarOpen ? 'translate-x-0' : '-translate-x-64'"
        @click.outside="sidebarOpen = false"
        @keydown.escape.window="sidebarOpen = false"
        x-cloak="lg"
    >
        <!-- Sidebar header -->
        <div class="flex justify-between mb-10 pr-3 sm:px-2">
            <!-- Close button -->
            <button class="lg:hidden text-gray-500 hover:text-gray-400" @click.stop="sidebarOpen = !sidebarOpen" aria-controls="sidebar" :aria-expanded="sidebarOpen">
                <span class="sr-only">Close sidebar</span>
                <svg class="w-6 h-6 fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.7 18.7l1.4-1.4L7.8 13H20v-2H7.8l4.3-4.3-1.4-1.4L4 12z" />
                </svg>
            </button>
            <!-- Logo -->
            <a class="block" href="index.html">
                <svg width="32" height="32" viewBox="0 0 32 32">
                    <defs>
                        <linearGradient x1="28.538%" y1="20.229%" x2="100%" y2="108.156%" id="logo-a">
                            <stop stop-color="#A5B4FC" stop-opacity="0" offset="0%" />
                            <stop stop-color="#A5B4FC" offset="100%" />
                        </linearGradient>
                        <linearGradient x1="88.638%" y1="29.267%" x2="22.42%" y2="100%" id="logo-b">
                            <stop stop-color="#38BDF8" stop-opacity="0" offset="0%" />
                            <stop stop-color="#38BDF8" offset="100%" />
                        </linearGradient>
                    </defs>
                    <rect fill="#6366F1" width="32" height="32" rx="16" />
                    <path d="M18.277.16C26.035 1.267 32 7.938 32 16c0 8.837-7.163 16-16 16a15.937 15.937 0 01-10.426-3.863L18.277.161z" fill="#4F46E5" />
                    <path d="M7.404 2.503l18.339 26.19A15.93 15.93 0 0116 32C7.163 32 0 24.837 0 16 0 10.327 2.952 5.344 7.404 2.503z" fill="url(#logo-a)" />
                    <path d="M2.223 24.14L29.777 7.86A15.926 15.926 0 0132 16c0 8.837-7.163 16-16 16-5.864 0-10.991-3.154-13.777-7.86z" fill="url(#logo-b)" />
                </svg>
            </a>
        </div>

        <!-- Links -->
        <div class="space-y-8">
            <!-- Pages group -->
            <div>
                <h3 class="text-xs uppercase text-gray-500 font-semibold pl-3">
                    <span class="hidden lg:block lg:sidebar-expanded:hidden 2xl:hidden text-center w-6" aria-hidden="true">•••</span>
                    <span class="lg:hidden lg:sidebar-expanded:block 2xl:block">Management</span>
                </h3>
                <ul class="mt-3">
                    <!-- Dashboard -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="page === 'dashboard' && 'bg-gray-900'">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="page === 'dashboard' && 'hover:text-gray-200'" href="/dashboard">
                            <div class="flex items-center">
                                <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                    <path class="fill-current text-gray-400" :class="page === 'dashboard' && '!text-indigo-500'" d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0z" />
                                    <path class="fill-current text-gray-600" :class="page === 'dashboard' && 'text-indigo-600'" d="M12 3c-4.963 0-9 4.037-9 9s4.037 9 9 9 9-4.037 9-9-4.037-9-9-9z" />
                                    <path class="fill-current text-gray-400" :class="page === 'dashboard' && 'text-indigo-200'" d="M12 15c-1.654 0-3-1.346-3-3 0-.462.113-.894.3-1.285L6 6l4.714 3.301A2.973 2.973 0 0112 9c1.654 0 3 1.346 3 3s-1.346 3-3 3z" />
                                </svg>
                                <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Dashboard</span>
                            </div>
                        </a>
                    </li>
                    <!-- Garudaku -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="page === 'garudaku' && 'bg-gray-900'">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="page === 'garudaku' && 'hover:text-gray-200'" href="/garudaku">
                            <div class="flex items-center">
                                <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                    <path class="fill-current text-gray-400" :class="page === 'garudaku' && '!text-indigo-500'" d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0z" />
                                    <path class="fill-current text-gray-600" :class="page === 'garudaku' && 'text-indigo-600'" d="M12 3c-4.963 0-9 4.037-9 9s4.037 9 9 9 9-4.037 9-9-4.037-9-9-9z" />
                                    <path class="fill-current text-gray-400" :class="page === 'garudaku' && 'text-indigo-200'" d="M12 15c-1.654 0-3-1.346-3-3 0-.462.113-.894.3-1.285L6 6l4.714 3.301A2.973 2.973 0 0112 9c1.654 0 3 1.346 3 3s-1.346 3-3 3z" />
                                </svg>
                                <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Garudaku</span>
                            </div>
                        </a>
                    </li>
                    <!-- Concepts -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="page === 'concepts' && 'bg-gray-900'">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="page === 'concepts' && 'hover:text-gray-200'" href="/concepts">
                            <div class="flex items-center">
                                <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                    <path class="fill-current text-gray-600" :class="page === 'analytics' && 'text-indigo-500'" d="M0 20h24v2H0z" />
                                    <path class="fill-current text-gray-400" :class="page === 'analytics' && 'text-indigo-300'" d="M4 18h2a1 1 0 001-1V8a1 1 0 00-1-1H4a1 1 0 00-1 1v9a1 1 0 001 1zM11 18h2a1 1 0 001-1V3a1 1 0 00-1-1h-2a1 1 0 00-1 1v14a1 1 0 001 1zM17 12v5a1 1 0 001 1h2a1 1 0 001-1v-5a1 1 0 00-1-1h-2a1 1 0 00-1 1z" />
                                </svg>
                                <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Concepts</span>
                            </div>
                        </a>
                    </li>
                    <!-- Authentication Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_authentication.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_authentication.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_authentication.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_authentication.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_authentication.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_authentication.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Authentication</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'users' && '!text-indigo-500'" href="/users">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Users</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'teams' && '!text-indigo-500'" href="/teams">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Teams</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'team_invitations' && '!text-indigo-500'" href="/team_invitations">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Team Invitations</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Application Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_application.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_application.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_application.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_application.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_application.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_application.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Application</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'applications' && '!text-indigo-500'" href="/applications">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Applications</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'features' && '!text-indigo-500'" href="/features">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Features</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'entities' && '!text-indigo-500'" href="/entities">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Entities</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Infrastructure Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_infra.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_data.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_infra.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_infra.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_infra.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_infra.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Infrastructure</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'servers' && '!text-indigo-500'" href="/servers">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Servers</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Data Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_data.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_data.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_data.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_data.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_data.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_data.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Data</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'clusters' && '!text-indigo-500'" href="/clusters">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Clusters</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'etls' && '!text-indigo-500'" href="/etls">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">ETL</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Vendor Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_vendor.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_vendor.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_vendor.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_vendor.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_vendor.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_vendor.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Vendor</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'vendor_categories' && '!text-indigo-500'" href="/vendor_categories">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Category</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'vendors' && '!text-indigo-500'" href="/vendors">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Vendor</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'vendor_members' && '!text-indigo-500'" href="/vendor_members">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Vendor Member</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Navigation Management -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_navigation.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_navigation.includes(page))">
                        <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_navigation.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-400" :class="menu_navigation.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                        <path class="fill-current text-gray-700" :class="menu_navigation.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                        <path class="fill-current text-gray-600" :class="menu_navigation.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Navigation</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'navigation_groups' && '!text-indigo-500'" href="/navigation_groups">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Group</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'navigations' && '!text-indigo-500'" href="/navigations">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Navigation</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>


                    @foreach($view_navigations as $navigation)
                        @if(count($navigation['children']) > 0)
                                <!-- Menu {{ $navigation['id'] }} -->
                                <li wire:key="{{ rand() }}" class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_{{ $navigation['id'] }}.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_{{ $navigation['id'] }}.includes(page))">
                                    <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_{{ $navigation['id'] }}.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                                        <div class="flex items-center justify-between">
                                            <div class="flex items-center">
                                                <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                                    <path class="fill-current text-gray-400" :class="menu_{{ $navigation['id'] }}.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                                    <path class="fill-current text-gray-700" :class="menu_{{ $navigation['id'] }}.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                                    <path class="fill-current text-gray-600" :class="menu_{{ $navigation['id'] }}.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                                </svg>
                                                <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $navigation['title'] }}</span>
                                            </div>
                                            <!-- Icon -->
                                            <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                                <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                                    <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                                        <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                            @foreach($navigation['children'] as $child)
                                            <li class="mb-1 last:mb-0">
                                                <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === '{{ $child['route'] }}' && '!text-indigo-500'" href="/{{ $child['route'] }}">
                                                    <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $child['title'] }}</span>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                        @else
                        <li wire:key="{{ rand() }}" class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="page === '{{ $navigation['route'] }}' && 'bg-gray-900'">
                            <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="page === '{{ $navigation['route'] }}' && 'hover:text-gray-200'" href="/{{ $navigation['route'] }}">
                                <div class="flex items-center">
                                    {!! $navigation['svg'] !!}
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $navigation['title'] }}</span>
                                </div>
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <!-- More group -->
            <div>
                <h3 class="text-xs uppercase text-gray-500 font-semibold pl-3">
                    <span class="hidden lg:block lg:sidebar-expanded:hidden 2xl:hidden text-center w-6" aria-hidden="true">•••</span>
                    <span class="lg:hidden lg:sidebar-expanded:block 2xl:block">More</span>
                </h3>
                <ul class="mt-3">
                    <!-- Authentication -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" x-data="{ open: false }">
                        <a class="sidebar-expander-link block text-gray-200 hover:text-white transition duration-150" :class="open && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-600" d="M8.07 16H10V8H8.07a8 8 0 110 8z" />
                                        <path class="fill-current text-gray-400" d="M15 12L8 6v5H0v2h8v5z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Authentication</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="{ 'transform rotate-180': open }" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="{ 'hidden': !open }" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="signin.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Sign In</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="signup.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Sign up</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="reset-password.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Reset Password</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Onboarding -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" x-data="{ open: false }">
                        <a class="sidebar-expander-link block text-gray-200 hover:text-white transition duration-150" :class="open && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <path class="fill-current text-gray-600" d="M19 5h1v14h-2V7.414L5.707 19.707 5 19H4V5h2v11.586L18.293 4.293 19 5Z" />
                                        <path class="fill-current text-gray-400" d="M5 9a4 4 0 1 1 0-8 4 4 0 0 1 0 8Zm14 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8ZM5 23a4 4 0 1 1 0-8 4 4 0 0 1 0 8Zm14 0a4 4 0 1 1 0-8 4 4 0 0 1 0 8Z" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Onboarding</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="{ 'transform rotate-180': open }" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="{ 'hidden': !open }" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="onboarding-01.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Step 1</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="onboarding-02.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Step 2</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="onboarding-03.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Step 3</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" href="onboarding-04.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Step 4</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- Components -->
                    <li class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': page.startsWith('component-') }" x-data="{ open: false }" x-init="$nextTick(() => open = page.startsWith('component-'))">
                        <a class="sidebar-expander-link block text-gray-200 hover:text-white transition duration-150" :class="open && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                            <div class="flex items-center justify-between">
                                <div class="flex items-center">
                                    <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                        <circle class="fill-current text-gray-600" :class="page.startsWith('component-') && 'text-indigo-500'" cx="16" cy="8" r="8" />
                                        <circle class="fill-current text-gray-400" :class="page.startsWith('component-') && 'text-indigo-300'" cx="8" cy="16" r="8" />
                                    </svg>
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Components</span>
                                </div>
                                <!-- Icon -->
                                <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                    <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="{ 'transform rotate-180': open }" viewBox="0 0 12 12">
                                        <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                    </svg>
                                </div>
                            </div>
                        </a>
                        <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                            <ul class="pl-9 mt-1" :class="{ 'hidden': !open }" x-cloak>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-button' && '!text-indigo-500'" href="component-button.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Button</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-form' && '!text-indigo-500'" href="component-form.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Input Form</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-dropdown' && '!text-indigo-500'" href="component-dropdown.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Dropdown</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-alert' && '!text-indigo-500'" href="component-alert.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Alert & Banner</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-modal' && '!text-indigo-500'" href="component-modal.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Modal</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-pagination' && '!text-indigo-500'" href="component-pagination.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Pagination</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-tabs' && '!text-indigo-500'" href="component-tabs.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Tabs</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-breadcrumb' && '!text-indigo-500'" href="component-breadcrumb.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Breadcrumb</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-badge' && '!text-indigo-500'" href="component-badge.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Badge</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-avatar' && '!text-indigo-500'" href="component-avatar.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Avatar</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-tooltip' && '!text-indigo-500'" href="component-tooltip.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Tooltip</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-accordion' && '!text-indigo-500'" href="component-accordion.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Accordion</span>
                                    </a>
                                </li>
                                <li class="mb-1 last:mb-0">
                                    <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === 'component-icons' && '!text-indigo-500'" href="component-icons.html">
                                        <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">Icons</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <div x-text="sidebarExpanded ? 'expand' : 'hide'"></div>
        <!-- Expand / collapse button -->
        <div class="pt-3 hidden lg:inline-flex 2xl:hidden justify-end mt-auto">
            <div class="px-3 py-2">
                <button @click="sidebarExpanded = !sidebarExpanded">
                    <span class="sr-only">Expand / collapse sidebar</span>
                    <svg class="w-6 h-6 fill-current sidebar-expanded:rotate-180" viewBox="0 0 24 24">
                        <path class="text-gray-400" d="M19.586 11l-5-5L16 4.586 23.414 12 16 19.414 14.586 18l5-5H7v-2z" />
                        <path class="text-gray-600" d="M3 23H1V1h2z" />
                    </svg>
                </button>
            </div>
        </div>

    </div>
</div>
@endif
