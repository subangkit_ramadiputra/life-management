<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all w-full sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline"
             style="min-width: 1000px;"
        >
            <form>

                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <table class="table-auto w-full">
                            <thead>
                            <tr>
                                <th width="30%">&nbsp;</th>
                                <th width="40%">&nbsp;</th>
                                <th width="30%">&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="3">
                                    <div class="mb-4">
                                        <label for="group"
                                               class="block text-gray-700 text-sm font-bold mb-2">Knowledge Group</label>
                                        <input type="text"
                                               class="form-input w-full"
                                               id="group" placeholder="Enter Knowledge Group" wire:model.lazy="group">
                                        @error('group') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Definition</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('definition'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorDefinition != null) {
                                                                tinymce.activeEditorDefinition.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#definition',


                                                                height: 200,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="3" id="definition" placeholder="Enter Definition" wire:model.defer="definition" spellcheck="false"></textarea>
                                        @error('definition') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Goals</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('goals'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorgoals != null) {
                                                                tinymce.activeEditorgoals.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#goals',


                                                                height: 200,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="5" id="goals" placeholder="Enter Goals" wire:model.defer="goals" spellcheck="false"></textarea>
                                        @error('goals') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <h3>Business Drivers</h3>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>arrow_downward</title><g fill="none"><path d="M20 12l-1.41-1.41L13 16.17V4h-2v12.17l-5.58-5.59L4 12l8 8 8-8z" fill="#212121"></path></g></svg>
                                </td>
                            </tr>
                            <tr>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Inputs</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('inputs'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorinputs != null) {
                                                                tinymce.activeEditorinputs.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#inputs',


                                                                height: 500,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="inputs" placeholder="Enter Inputs" wire:model.defer="inputs" spellcheck="false"></textarea>
                                        @error('inputs') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Activities</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('activities'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditoractivities != null) {
                                                                tinymce.activeEditoractivities.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#activities',


                                                                height: 500,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="activities" placeholder="Enter Activities" wire:model.defer="activities" spellcheck="false"></textarea>
                                        @error('activities') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Deliverables</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('deliverables'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditordeliverables != null) {
                                                                tinymce.activeEditordeliverables.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#deliverables',


                                                                height: 500,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="deliverables" placeholder="Enter Deliverables" wire:model.defer="deliverables" spellcheck="false"></textarea>
                                        @error('deliverables') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                            </tr>
                            <tr>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Suppliers</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('suppliers'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorsuppliers != null) {
                                                                tinymce.activeEditorsuppliers.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#suppliers',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="suppliers" placeholder="Enter Suppliers" wire:model.defer="suppliers" spellcheck="false"></textarea>
                                        @error('suppliers') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Participants</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('participants'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorparticipants != null) {
                                                                tinymce.activeEditorparticipants.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#participants',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="participants" placeholder="Enter Participants" wire:model.defer="participants" spellcheck="false"></textarea>
                                        @error('participants') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Consumers</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('consumers'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditorconsumers != null) {
                                                                tinymce.activeEditorconsumers.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#consumers',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="consumers" placeholder="Enter Consumers" wire:model.defer="consumers" spellcheck="false"></textarea>
                                        @error('consumers') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><title>arrow_upward</title><g fill="none"><path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z" fill="#212121"></path></g></svg>
                                    <h3>Technical Drivers</h3>
                                </td>
                            </tr>
                            <tr>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Techniques</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('techniques'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditortechniques != null) {
                                                                tinymce.activeEditortechniques.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#techniques',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="techniques" placeholder="Enter Techniques" wire:model.defer="techniques" spellcheck="false"></textarea>
                                        @error('techniques') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Tools</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('tools'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditortools != null) {
                                                                tinymce.activeEditortools.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#tools',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="tools" placeholder="Enter Tools" wire:model.defer="tools" spellcheck="false"></textarea>
                                        @error('tools') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                                <td><div class="mb-4">
                                        <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Metrics</label>
                                        <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('metrics'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            if (tinymce.activeEditormetrics != null) {
                                                                tinymce.activeEditormetrics.destroy();
                                                            }
                                                            this.instance = window.tinymce.init({
                                                                selector: '#metrics',


                                                                height: 300,
                                                                setup: function (editor) {
                                                                    editor.on('init change', function () {
                                                                        editor.save();
                                                                    });
                                                                }
                                                            });
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="metrics" placeholder="Enter Metrics" wire:model.defer="metrics" spellcheck="false"></textarea>
                                        @error('metrics') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div></td>
                            </tr>
                            <tr>
                                <td colspan="3">(P) Planning, (C) Control, (D) Development, (O) Operation,</td>
                            </tr>
                            </tbody>
                        </table>












                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
