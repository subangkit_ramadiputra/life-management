<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Knowledge Group</th>
                                <th class="px-4 py-2">Knowledge Context</th>
                                <th class="px-4 py-2">Definition</th>
                                <th class="px-4 py-2">Goals</th>
                                <th class="px-4 py-2">Inputs</th>
                                <th class="px-4 py-2">Suppliers</th>
                                <th class="px-4 py-2">Activities</th>
                                <th class="px-4 py-2">Participants</th>
                                <th class="px-4 py-2">Deliverables</th>
                                <th class="px-4 py-2">Consumers</th>
                                <th class="px-4 py-2">Techniques</th>
                                <th class="px-4 py-2">Tools</th>
                                <th class="px-4 py-2">Metrics</th>


                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_generic_contexts as $generic_context)
                                <tr>
                                    <td class="border px-4 py-2">{{ $generic_context->id }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->group }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->context_name }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->definition }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->goals }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->inputs }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->suppliers }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->activities }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->participants }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->deliverables }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->consumers }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->techniques }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->tools }}</td>
                                    <td class="border px-4 py-2">{{ $generic_context->metrics }}</td>


                                    <td class="border px-4 py-2">
                                        <span wire:click="editGenericContext({{ $generic_context->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteGenericContext({{ $parent_id }},{{ $generic_context->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                        <div class="mb-4">
                            <label for="group"
                                class="block text-gray-700 text-sm font-bold mb-2">Knowledge Group</label>
                            <input type="text"
                                class="form-input w-full"
                                id="group" placeholder="Enter Knowledge Group" wire:model.lazy="generic_context_group">
                            @error('generic_context_group') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="context_name"
                                class="block text-gray-700 text-sm font-bold mb-2">Knowledge Context</label>
                            <input type="text"
                                class="form-input w-full"
                                id="context_name" placeholder="Enter Knowledge Context" wire:model.lazy="generic_context_context_name">
                            @error('generic_context_context_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="definition"
                                class="block text-gray-700 text-sm font-bold mb-2">Definition</label>
                            <textarea class="form-input w-full"
                                id="definition" placeholder="Enter Definition" wire:model.lazy="generic_context_definition"></textarea>
                            @error('generic_context_definition') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="goals"
                                class="block text-gray-700 text-sm font-bold mb-2">Goals</label>
                            <textarea class="form-input w-full"
                                id="goals" placeholder="Enter Goals" wire:model.lazy="generic_context_goals"></textarea>
                            @error('generic_context_goals') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="inputs"
                                class="block text-gray-700 text-sm font-bold mb-2">Inputs</label>
                            <textarea class="form-input w-full"
                                id="inputs" placeholder="Enter Inputs" wire:model.lazy="generic_context_inputs"></textarea>
                            @error('generic_context_inputs') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="suppliers"
                                class="block text-gray-700 text-sm font-bold mb-2">Suppliers</label>
                            <textarea class="form-input w-full"
                                id="suppliers" placeholder="Enter Suppliers" wire:model.lazy="generic_context_suppliers"></textarea>
                            @error('generic_context_suppliers') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="activities"
                                class="block text-gray-700 text-sm font-bold mb-2">Activities</label>
                            <textarea class="form-input w-full"
                                id="activities" placeholder="Enter Activities" wire:model.lazy="generic_context_activities"></textarea>
                            @error('generic_context_activities') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="participants"
                                class="block text-gray-700 text-sm font-bold mb-2">Participants</label>
                            <textarea class="form-input w-full"
                                id="participants" placeholder="Enter Participants" wire:model.lazy="generic_context_participants"></textarea>
                            @error('generic_context_participants') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="deliverables"
                                class="block text-gray-700 text-sm font-bold mb-2">Deliverables</label>
                            <textarea class="form-input w-full"
                                id="deliverables" placeholder="Enter Deliverables" wire:model.lazy="generic_context_deliverables"></textarea>
                            @error('generic_context_deliverables') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="consumers"
                                class="block text-gray-700 text-sm font-bold mb-2">Consumers</label>
                            <textarea class="form-input w-full"
                                id="consumers" placeholder="Enter Consumers" wire:model.lazy="generic_context_consumers"></textarea>
                            @error('generic_context_consumers') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="techniques"
                                class="block text-gray-700 text-sm font-bold mb-2">Techniques</label>
                            <textarea class="form-input w-full"
                                id="techniques" placeholder="Enter Techniques" wire:model.lazy="generic_context_techniques"></textarea>
                            @error('generic_context_techniques') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="tools"
                                class="block text-gray-700 text-sm font-bold mb-2">Tools</label>
                            <textarea class="form-input w-full"
                                id="tools" placeholder="Enter Tools" wire:model.lazy="generic_context_tools"></textarea>
                            @error('generic_context_tools') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="metrics"
                                class="block text-gray-700 text-sm font-bold mb-2">Metrics</label>
                            <textarea class="form-input w-full"
                                id="metrics" placeholder="Enter Metrics" wire:model.lazy="generic_context_metrics"></textarea>
                            @error('generic_context_metrics') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetGenericContextCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignGenericContext')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeGenericContextModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
