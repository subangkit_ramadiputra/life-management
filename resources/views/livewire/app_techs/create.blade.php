<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body_format"
                                class="block text-gray-700 text-sm font-bold mb-2">Body Format</label>
                            <input type="text"
                                class="form-input w-full"
                                id="body_format" placeholder="Enter Body Format" wire:model.lazy="body_format">
                            @error('body_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body"
                                class="block text-gray-700 text-sm font-bold mb-2">Detail</label>
                            <textarea class="form-input w-full"
                                id="body" placeholder="Enter Detail" wire:model.lazy="body"></textarea>
                            @error('body') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="techable"
                                class="block text-gray-700 text-sm font-bold mb-2">Technology</label>
                            <input type="text"
                                class="form-input w-full"
                                id="techable" placeholder="Enter Technology" wire:model.lazy="techable">
                            @error('techable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="technology_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Technology</label>
                            <input type="text"
                                class="form-input w-full"
                                id="technology_id" placeholder="Enter Technology" wire:model.lazy="technology_id">
                            @error('technology_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="local_path"
                                class="block text-gray-700 text-sm font-bold mb-2">Local Path</label>
                            <textarea class="form-input w-full"
                                id="local_path" placeholder="Enter Local Path" wire:model.lazy="local_path"></textarea>
                            @error('local_path') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="virtual_host"
                                class="block text-gray-700 text-sm font-bold mb-2">Virtual Host</label>
                            <textarea class="form-input w-full"
                                id="virtual_host" placeholder="Enter Virtual Host" wire:model.lazy="virtual_host"></textarea>
                            @error('virtual_host') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
