<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400" x-data="{editor:null, jsoneditor:null, testeditortemplate:null}">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full" style="min-width: 1000px;"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="framework_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Framework</label>
                            <select class="form-select" id="framework_id" wire:model.lazy="framework_id">
                                <option value="0">-</option>
                                @foreach($frameworks as $framework)
                                    <option value="{{$framework->id}}">{{$framework->Framework}}</option>
                                @endforeach
                            </select>
                            @error('framework') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="context_name"
                                class="block text-gray-700 text-sm font-bold mb-2">Context Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="context_name" placeholder="Enter Context Name" wire:model.lazy="context_name">
                            @error('context_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Definition Requirement</label>
                            <textarea class="form-input w-full hidden" id="requirement_existing_value" wire:model.lazy="requirement"></textarea>
                            <div id="requirement_editor" style="width: 100%; height: 400px;"></div>
                            <div wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('requirement'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('requirement');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                const options = {}
                                                                this.instance = true
                                                                jsoneditor = new JSONEditor(document.getElementById('requirement_editor'), options)

                                                                // set json
                                                                const value = document.getElementById('requirement_existing_value').value
                                                                console.log(value)
                                                                const initialJson = (value !== '') ? JSON.parse(value) : {attribute_name:'value'}
                                                                jsoneditor.set(initialJson)

                                                                window.clear = function(){
                                                                    jsoneditor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="requirement" placeholder="Enter Definition Requirement" wire:model.defer="requirement">
                                @error('requirement') <span class="text-red-500">{{ $message }}</span>@enderror
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="body" class="block text-gray-700 text-sm font-bold mb-2">Main Template</label>
                            <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('main_template'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('main_template');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                editor = ace.edit(document.getElementById('main_template'), {
                                                                    mode: 'ace/mode/{{ ($mode != '') ? $mode : 'html' }}',
                                                                                        theme: 'ace/theme/monokai',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                editor.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                editor.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    editor.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="main_template" placeholder="Enter Main Template" wire:model.defer="main_template"></textarea>
                            @error('main_template') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="mode"
                                class="block text-gray-700 text-sm font-bold mb-2">Mode</label>
                            <input type="text"
                                class="form-input w-full"
                                id="mode" placeholder="Enter Mode" wire:model.lazy="mode">
                            @error('mode') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="testing"
                                   class="block text-gray-700 text-sm font-bold mb-2">Test</label>
                        </div>
                        @foreach($generator_parameters as $parameter_name => $default_value)
                            <div class="mb-4">
                                <label for="mode"
                                       class="block text-gray-700 text-sm font-bold mb-2">{{ $parameter_name }}</label>
                                <input type="text"
                                       class="form-input w-full"
                                       id="generator_parameter_{{ $parameter_name }}" placeholder="Enter {{ $parameter_name }}" wire:model.lazy="generator_parameters.{{$parameter_name}}">
                            </div>
                        @endforeach
                        <div class="mb-4">
                            <button x-on:click="@this.set('main_template',editor.getValue());@this.set('requirement',JSON.stringify(jsoneditor.get()));@this.call('testTemplate')" type="button"
                                    class="inline-flex justify-center rounded-md border border-transparent px-4 py-2 bg-emerald-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-emerald-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                                Test
                            </button>
                            @if($testing_template != '')
                                <textarea wire:key="{{ rand() }}" x-data="{
                                                     value: @entangle('testing_template'),
                                                     instance: undefined,
                                                     updateObject: undefined,
                                                     updateInterval: 10000,
                                                     plugin(value) {
                                                        var that = this

                                                        if (typeof this.instance == 'undefined') {
                                                            let editorElement = document.getElementById('testing_template');

                                                            // If we have an editor element
                                                            if(editorElement){
                                                                // pass options to ace.edit
                                                                testeditortemplate = ace.edit(document.getElementById('testing_template'), {
                                                                    mode: 'ace/mode/{{ ($mode != '') ? $mode : 'html' }}',
                                                                                        theme: 'ace/theme/monokai',
                                                                                        maxLines: 50,
                                                                                        minLines: 10,
                                                                                        fontSize: 18
                                                                                        })
                                                                                        // use setOptions method to set several options at once
                                                                testeditortemplate.setOptions({
                                                                    autoScrollEditorIntoView: true,
                                                                    copyWithEmptySelection: true
                                                                });
                                                                testeditortemplate.on('change', function(e){
                                                                });

                                                                window.clear = function(){
                                                                    testeditortemplate.session.setValue('');
                                                                }
                                                            }
                                                        }
                                                     },
                                                     init() {
                                                         $watch('value', value => {
                                                            // Reinitialize
                                                            this.plugin(value)
                                                         });
                                                         // Initialize
                                                         this.plugin('')
                                                     }
                                                }" class="form-input w-full" rows="20" id="testing_template" placeholder="Press Test" wire:model.defer="testing_template" readonly></textarea>
                            @endif
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.set('main_template',editor.getValue());@this.set('requirement',JSON.stringify(jsoneditor.get()));@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
