<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Quadrant</th>
                                <th class="px-4 py-2">Title</th>
                                <th class="px-4 py-2">Summary</th>
                                <th class="px-4 py-2">Asignee</th>
                                <th class="px-4 py-2">Status</th>
                                <th class="px-4 py-2">Due Date</th>
                                <th class="px-4 py-2">Parent Task</th>


                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_tasks as $task)
                                <tr>
                                    <td class="border px-4 py-2">{{ $task->id }}</td>
                                    <td class="border px-4 py-2">{{ $task->quadrant_id }}</td>
                                    <td class="border px-4 py-2">{{ $task->title }}</td>
                                    <td class="border px-4 py-2">{{ $task->summary }}</td>
                                    <td class="border px-4 py-2">{{ $task->assignee_id }}</td>
                                    <td class="border px-4 py-2">{{ $task->status }}</td>
                                    <td class="border px-4 py-2">{{ $task->due_date }}</td>
                                    <td class="border px-4 py-2">{{ $task->parent_id }}</td>


                                    <td class="border px-4 py-2">
                                        <span wire:click="editTask({{ $task->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteTask({{ $parent_id }},{{ $task->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                                    <div class="mb-4">
                                        <label for="task_quadrant_id"
                                               class="block text-gray-700 text-sm font-bold mb-2">Quadrant</label>
                                        <select class="form-select" id="quadrant_id" placeholder="Enter Quadrant"
                                                wire:model.lazy="task_quadrant_id">
                                            <option value="0">-</option>
                                            @foreach($quadrans as $quadrant)
                                                <option value="{{$quadrant->id}}">{{$quadrant->title}}</option>
                                            @endforeach
                                        </select>
                                        @error('task_quadrant_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="task_parent_id"
                                               class="block text-gray-700 text-sm font-bold mb-2">Parent Task</label>
                                        <select class="form-select" id="task_parent_id" placeholder="Enter Parent Task"
                                                wire:model.lazy="task_parent_id">
                                            <option value="">-</option>
                                            @foreach($parent_tasks as $task)
                                                <option value="{{$task->id}}">{{$task->title}}</option>
                                            @endforeach
                                        </select>
                                        @error('task_parent_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="task_assignee_id"
                                               class="block text-gray-700 text-sm font-bold mb-2">Asignee</label>
                                        <select class="form-select" id="task_assignee_id" placeholder="Enter Asignee"
                                                wire:model.lazy="task_assignee_id">
                                            <option value="0">-</option>
                                            @foreach($employees as $employee)
                                                <option value="{{$employee->id}}">{{$employee->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('task_assignee_id') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="task_status"
                                               class="block text-gray-700 text-sm font-bold mb-2">Status</label>
                                        <select class="form-select" id="status" placeholder="Enter Status" wire:model.lazy="task_status">
                                            <option value="">-</option>
                                            @foreach($statuses as $index => $status)
                                                <option value="{{$index}}">{{$status}}</option>
                                            @endforeach
                                        </select>
                                        @error('task_status') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4" wire:key="{{ rand() }}">
                                        <label for="task_due_date"
                                               class="block text-gray-700 text-sm font-bold mb-2">Due Date</label>
                                        <!-- Datepicker built with flatpickr -->
                                        <div class="relative">
                                            <input
                                                x-data="{
                                                     value: @entangle($task_due_date),
                                                     instance: undefined,
                                                     init() {
                                                         $watch('value', value => this.instance.setDate(value, false));
                                                         this.instance = flatpickr(this.$refs.input, {{ json_encode((object)[
                                                                                                                            'dateFormat' => 'Y-m-d H:i:s',
                                                                                                                            'enableTime' => true,
                                                                                                                            'altFormat' =>  'j F Y H:i',
                                                                                                                            'altInput' => true
                                                                                                                        ]) }});
                                                     }
                                                }"
                                                x-ref="input"
                                                x-bind:value="value"
                                                wire:model.lazy="task_due_date"
                                                type="text"
                                                class="form-input pl-9 text-gray-500 hover:text-gray-600 font-medium focus:border-gray-300 w-60"
                                            />
                                            <div class="absolute inset-0 right-auto flex items-center pointer-events-none">
                                                <svg class="w-4 h-4 fill-current text-gray-500 ml-3" viewBox="0 0 16 16">
                                                    <path
                                                        d="M15 2h-2V0h-2v2H9V0H7v2H5V0H3v2H1a1 1 0 00-1 1v12a1 1 0 001 1h14a1 1 0 001-1V3a1 1 0 00-1-1zm-1 12H2V6h12v8z"/>
                                                </svg>
                                            </div>
                                        </div>
                                        @error('task_due_date') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="title"
                                            class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                                        <input type="text"
                                            class="form-input w-full"
                                            id="title" placeholder="Enter Title" wire:model.lazy="task_title">
                                        @error('task_title') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="summary"
                                            class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                                        <textarea class="form-input w-full"
                                            id="summary" placeholder="Enter Summary" wire:model.lazy="task_summary"></textarea>
                                        @error('task_summary') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="body"
                                            class="block text-gray-700 text-sm font-bold mb-2">Body</label>
                                        <textarea class="form-input w-full"
                                            id="body" placeholder="Enter Body" wire:model.lazy="task_body"></textarea>
                                        @error('task_body') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                    <div class="mb-4">
                                        <label for="body_format"
                                            class="block text-gray-700 text-sm font-bold mb-2">Body Format</label>
                                        <input type="text"
                                            class="form-input w-full"
                                            id="body_format" placeholder="Enter Body Format" wire:model.lazy="task_body_format">
                                        @error('task_body_format') <span class="text-red-500">{{ $message }}</span>@enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetTaskCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignTask')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeTaskModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
