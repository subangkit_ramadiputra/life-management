<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="quadrant_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Quadrant</label>
                            <select class="form-select" id="quadrant_id" placeholder="Enter Quadrant"
                                    wire:model.lazy="quadrant_id">
                                <option value="0">-</option>
                                @foreach($quadrans as $quadrant)
                                    <option value="{{$quadrant->id}}">{{$quadrant->title}}</option>
                                @endforeach
                            </select>
                            @error('quadrant_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="parent_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Parent Task</label>
                            <select class="form-select" id="parent_id" placeholder="Enter Parent Task"
                                    wire:model.lazy="parent_id">
                                <option value="">-</option>
                                @foreach($parent_tasks as $task)
                                    <option value="{{$task->id}}">{{$task->title}}</option>
                                @endforeach
                            </select>
                            @error('parent_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="assignee_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Asignee</label>
                            <select class="form-select" id="assignee_id" placeholder="Enter Asignee"
                                    wire:model.lazy="assignee_id">
                                <option value="0">-</option>
                                @foreach($employees as $employee)
                                    <option value="{{$employee->id}}">{{$employee->name}}</option>
                                @endforeach
                            </select>
                            @error('assignee_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="status"
                                   class="block text-gray-700 text-sm font-bold mb-2">Status</label>
                            <select class="form-select" id="status" placeholder="Enter Status" wire:model.lazy="status">
                                <option value="">-</option>
                                @foreach($statuses as $index => $status)
                                    <option value="{{$index}}">{{$status}}</option>
                                @endforeach
                            </select>
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4" wire:key="{{ rand() }}">
                            <label for="due_date"
                                   class="block text-gray-700 text-sm font-bold mb-2">Due Date</label>
                            <!-- Datepicker built with flatpickr -->
                            <div class="relative">
                                <input
                                    x-data="{
                                         value: @entangle($due_date),
                                         instance: undefined,
                                         init() {
                                             $watch('value', value => this.instance.setDate(value, false));
                                             this.instance = flatpickr(this.$refs.input, {{ json_encode((object)[
                                                                                                                'dateFormat' => 'Y-m-d H:i:s',
                                                                                                                'enableTime' => true,
                                                                                                                'altFormat' =>  'j F Y H:i',
                                                                                                                'altInput' => true
                                                                                                            ]) }});
                                         }
                                    }"
                                    x-ref="input"
                                    x-bind:value="value"
                                    wire:model.lazy="due_date"
                                    type="text"
                                    class="form-input pl-9 text-gray-500 hover:text-gray-600 font-medium focus:border-gray-300 w-60"
                                />
                                <div class="absolute inset-0 right-auto flex items-center pointer-events-none">
                                    <svg class="w-4 h-4 fill-current text-gray-500 ml-3" viewBox="0 0 16 16">
                                        <path
                                            d="M15 2h-2V0h-2v2H9V0H7v2H5V0H3v2H1a1 1 0 00-1 1v12a1 1 0 001 1h14a1 1 0 001-1V3a1 1 0 00-1-1zm-1 12H2V6h12v8z"/>
                                    </svg>
                                </div>
                            </div>
                            @error('due_date') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                   class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                   class="form-input w-full"
                                   id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                   class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea
                                class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body"
                                   class="block text-gray-700 text-sm font-bold mb-2">Body</label>
                            <textarea
                                class="form-input w-full"
                                id="body" placeholder="Enter Body" wire:model.lazy="body"></textarea>
                            @error('body') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body_format"
                                   class="block text-gray-700 text-sm font-bold mb-2">Body Format</label>
                            <input type="text"
                                   class="form-input w-full"
                                   id="body_format" placeholder="Enter Body Format" wire:model.lazy="body_format">
                            @error('body_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
