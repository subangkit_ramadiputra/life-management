<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div
            class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle max-w-4xl sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            @include('livewire/alert-message-render',['clearSession' => false])
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="py-3 flex-wrap">
                        <table class="table-fixed w-full">
                            <thead>
                            <tr class="bg-gray-100">
                                <th class="px-4 py-2 w-20">No.</th>
                                <th class="px-4 py-2">Stage</th>
                                <th class="px-4 py-2">Server Name</th>
                                <th class="px-4 py-2">IP Address</th>
                                <th class="px-4 py-2">Provider</th>
                                <th class="px-4 py-2">Summary</th>
                                <th class="px-4 py-2">Body</th>
                                <th class="px-4 py-2">SSH User</th>


                                <th class="px-4 py-2" width="100">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($recent_servers as $server)
                                <tr>
                                    <td class="border px-4 py-2">{{ $server->id }}</td>
                                    <td class="border px-4 py-2">{{ $server->stage }}</td>
                                    <td class="border px-4 py-2">{{ $server->title }}</td>
                                    <td class="border px-4 py-2">{{ $server->ip_address }}</td>
                                    <td class="border px-4 py-2">{{ $server->provider }}</td>
                                    <td class="border px-4 py-2">{{ $server->summary }}</td>
                                    <td class="border px-4 py-2">{{ $server->body }}</td>
                                    <td class="border px-4 py-2">{{ $server->user_access }}</td>


                                    <td class="border px-4 py-2">
                                        <span wire:click="editServer({{ $server->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-blue-500 shrink-0" viewBox="0 0 16 16">
                                                <path d="M11.7.3c-.4-.4-1-.4-1.4 0l-10 10c-.2.2-.3.4-.3.7v4c0 .6.4 1 1 1h4c.3 0 .5-.1.7-.3l10-10c.4-.4.4-1 0-1.4l-4-4zM4.6 14H2v-2.6l6-6L10.6 8l-6 6zM12 6.6L9.4 4 11 2.4 13.6 5 12 6.6z"></path>
                                            </svg>
                                        </span>
                                        <span wire:click="deleteServer({{ $parent_id }},{{ $server->id }})" class="btn border-gray-200 hover:border-gray-300 text-gray-600 cursor-pointer">
                                            <svg class="w-4 h-4 fill-current text-red-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="">
                        <div class="mb-4">
                            <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                <div class="">
                        <div class="mb-4">
                            <label for="stage"
                                class="block text-gray-700 text-sm font-bold mb-2">Stage</label>
                            <input type="text"
                                class="form-input w-full"
                                id="stage" placeholder="Enter Stage" wire:model.lazy="server_stage">
                            @error('server_stage') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Server Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Server Name" wire:model.lazy="server_title">
                            @error('server_title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="ip_address"
                                class="block text-gray-700 text-sm font-bold mb-2">IP Address</label>
                            <input type="text"
                                class="form-input w-full"
                                id="ip_address" placeholder="Enter IP Address" wire:model.lazy="server_ip_address">
                            @error('server_ip_address') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="provider"
                                class="block text-gray-700 text-sm font-bold mb-2">Provider</label>
                            <input type="text"
                                class="form-input w-full"
                                id="provider" placeholder="Enter Provider" wire:model.lazy="server_provider">
                            @error('server_provider') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="server_summary"></textarea>
                            @error('server_summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body"
                                class="block text-gray-700 text-sm font-bold mb-2">Body</label>
                            <textarea class="form-input w-full"
                                id="body" placeholder="Enter Body" wire:model.lazy="server_body"></textarea>
                            @error('server_body') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body_format"
                                class="block text-gray-700 text-sm font-bold mb-2">Format</label>
                            <input type="text"
                                class="form-input w-full"
                                id="body_format" placeholder="Enter Format" wire:model.lazy="server_body_format">
                            @error('server_body_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="user_access"
                                class="block text-gray-700 text-sm font-bold mb-2">SSH User</label>
                            <input type="text"
                                class="form-input w-full"
                                id="user_access" placeholder="Enter SSH User" wire:model.lazy="server_user_access">
                            @error('server_user_access') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <span wire:click="resetServerCreateForm()"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5 cursor-pointer">
                            Reset
                        </span>
                    </span>
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('assignServer')" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Save
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeServerModalPopover()" type="button"
                                class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
