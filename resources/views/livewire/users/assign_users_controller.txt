Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/users/assign_users_template.blade.php to resources/views/livewire/parent/assign_users.blade.php
or
cp resources/views/livewire/users/assign_users_template.blade.php resources/views/livewire/parents/assign_users.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isUserModalOpen)
            @include('livewire.parents.assign_users')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Users</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->users()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Users</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">User</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addUser({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        User
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\User;


    /**
     * Get all of the parents users.
     */
    public function users()
    {
        return $this->morphMany(User::class, 'userable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function users()
        {
            return $this->hasMany(User::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // User Section
    public $isUserModalOpen = 0;
    public $recent_users = [];
    public $users = [];
    public function addUser($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_users = $parent->users()->get();
        $this->resetUserCreateForm();
        $this->openUserModalPopover();
    }

    public function openUserModalPopover()
    {
        $this->isUserModalOpen = true;
    }
    public function closeUserModalPopover()
    {
        $this->isUserModalOpen = false;
    }

    public function editUser($user_id) {
        $user = User::findOrFail($user_id);
        $this->user_id = $user_id;
        $this->user_name = $user->name;
        $this->user_email = $user->email;
        $this->user_email_verified_at = $user->email_verified_at;
        $this->user_password = $user->password;
        $this->user_two_factor_secret = $user->two_factor_secret;
        $this->user_two_factor_recovery_codes = $user->two_factor_recovery_codes;
        $this->user_two_factor_confirmed_at = $user->two_factor_confirmed_at;
        $this->user_remember_token = $user->remember_token;
        $this->user_current_team_id = $user->current_team_id;
        $this->user_profile_photo_path = $user->profile_photo_path;

    }

    public function resetUserCreateForm(){
        $this->user_id = '';
        $this->user_name = '';
        $this->user_email = '';
        $this->user_email_verified_at = '';
        $this->user_password = '';
        $this->user_two_factor_secret = '';
        $this->user_two_factor_recovery_codes = '';
        $this->user_two_factor_confirmed_at = '';
        $this->user_remember_token = '';
        $this->user_current_team_id = '';
        $this->user_profile_photo_path = '';

    }

    public function assignUser() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->user_id == '') {
            $user = new User();
            $user->name = $this->user_name;
            $user->email = $this->user_email;
            $user->email_verified_at = $this->user_email_verified_at;
            $user->password = $this->user_password;
            $user->two_factor_secret = $this->user_two_factor_secret;
            $user->two_factor_recovery_codes = $this->user_two_factor_recovery_codes;
            $user->two_factor_confirmed_at = $this->user_two_factor_confirmed_at;
            $user->remember_token = $this->user_remember_token;
            $user->current_team_id = $this->user_current_team_id;
            $user->profile_photo_path = $this->user_profile_photo_path;

            $parent->users()->save($user);

            session()->flash('message', 'User updated.');
            //$this->closeUserModalPopover();
            $this->resetUserCreateForm();
        } else {
            User::updateOrCreate(['id' => $this->user_id], [
                'name' => $this->user_name,
                'email' => $this->user_email,
                'email_verified_at' => $this->user_email_verified_at,
                'password' => $this->user_password,
                'two_factor_secret' => $this->user_two_factor_secret,
                'two_factor_recovery_codes' => $this->user_two_factor_recovery_codes,
                'two_factor_confirmed_at' => $this->user_two_factor_confirmed_at,
                'remember_token' => $this->user_remember_token,
                'current_team_id' => $this->user_current_team_id,
                'profile_photo_path' => $this->user_profile_photo_path,

            ]);
        }

        $this->recent_users = $parent->users()->get();
    }

    public function deleteUser($parent_id, $user_id) {
        $parent = Parent::findOrFail($parent_id);

        $user = User::find($user_id);
        if ($user) {
            $parent->users()->where('id', '=', $user->id)->delete();
        }

        $this->recent_users = $parent->users()->get();

        session()->flash('message', 'User deleted.');
    }

    public function deleteExistingUser($user_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $user = User::find($user_id);
        if ($user) {
            $parent->users()->where('id', '=', $user->id)->delete();
        }

        $this->recent_users = $parent->users()->get();

        session()->flash('message', 'User deleted.');
    }
    // End of Section
