<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Full Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Full Name" wire:model.lazy="name">
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="email"
                                class="block text-gray-700 text-sm font-bold mb-2">Email</label>
                            <input type="text"
                                class="form-input w-full"
                                id="email" placeholder="Enter Email" wire:model.lazy="email">
                            @error('email') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="email_verified_at"
                                class="block text-gray-700 text-sm font-bold mb-2">Verified At</label>
                            <input type="text"
                                class="form-input w-full"
                                id="email_verified_at" placeholder="Enter Verified At" wire:model.lazy="email_verified_at">
                            @error('email_verified_at') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="password"
                                class="block text-gray-700 text-sm font-bold mb-2">Password</label>
                            <input type="text"
                                class="form-input w-full"
                                id="password" placeholder="Enter Password" wire:model.lazy="password">
                            @error('password') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="two_factor_secret"
                                class="block text-gray-700 text-sm font-bold mb-2">Two Factor Secret</label>
                            <textarea class="form-input w-full"
                                id="two_factor_secret" placeholder="Enter Two Factor Secret" wire:model.lazy="two_factor_secret"></textarea>
                            @error('two_factor_secret') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="two_factor_recovery_codes"
                                class="block text-gray-700 text-sm font-bold mb-2">Two Factor Recovery Codes</label>
                            <textarea class="form-input w-full"
                                id="two_factor_recovery_codes" placeholder="Enter Two Factor Recovery Codes" wire:model.lazy="two_factor_recovery_codes"></textarea>
                            @error('two_factor_recovery_codes') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="two_factor_confirmed_at"
                                class="block text-gray-700 text-sm font-bold mb-2">Two Factor Confirmed At</label>
                            <input type="text"
                                class="form-input w-full"
                                id="two_factor_confirmed_at" placeholder="Enter Two Factor Confirmed At" wire:model.lazy="two_factor_confirmed_at">
                            @error('two_factor_confirmed_at') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="remember_token"
                                class="block text-gray-700 text-sm font-bold mb-2">Remember Token</label>
                            <input type="text"
                                class="form-input w-full"
                                id="remember_token" placeholder="Enter Remember Token" wire:model.lazy="remember_token">
                            @error('remember_token') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="current_team_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Current Team</label>
                            <input type="text"
                                class="form-input w-full"
                                id="current_team_id" placeholder="Enter Current Team" wire:model.lazy="current_team_id">
                            @error('current_team_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="profile_photo_path"
                                class="block text-gray-700 text-sm font-bold mb-2">Profile Photo Path</label>
                            <input type="text"
                                class="form-input w-full"
                                id="profile_photo_path" placeholder="Enter Profile Photo Path" wire:model.lazy="profile_photo_path">
                            @error('profile_photo_path') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
