@if(Auth::user() != null)
<div
    x-data="{ page: @entangle('page'),
    @foreach($view_navigations as $group_navigations)
    @foreach($group_navigations['navigations'] as $navigation)
    @if(count($navigation['children']) > 0)
    @php
        $menu_array = "";
        foreach($navigation['children'] as $child) { $menu_array .= "'".$child['route']."',"; }
        $menu_array = substr($menu_array,0, strlen($menu_array) -1);
    @endphp
        menu_{{ $navigation['id'] }}: [{{ $menu_array }}],
    @endif
    @endforeach
    @endforeach

    }">
    <!-- Sidebar backdrop (mobile only) -->
    <div
        class="fixed inset-0 bg-gray-900 bg-opacity-30 z-40 lg:hidden lg:z-auto transition-opacity duration-200"
        :class="sidebarOpen ? 'opacity-100' : 'opacity-0 pointer-events-none'"
        aria-hidden="true"
        x-cloak
    ></div>

    <!-- Sidebar -->
    <div
        id="sidebar"
        class="flex flex-col absolute z-40 left-0 top-0 lg:static lg:left-auto lg:top-auto lg:translate-x-0 transform h-screen overflow-y-scroll lg:overflow-y-auto no-scrollbar w-64 lg:w-20 lg:sidebar-expanded:!w-64 2xl:!w-64 shrink-0 bg-gray-800 p-4 transition-all duration-200 ease-in-out"
        :class="sidebarOpen ? 'translate-x-0' : '-translate-x-64'"
        @click.outside="sidebarOpen = false"
        @keydown.escape.window="sidebarOpen = false"
        x-cloak="lg"
    >
        <!-- Sidebar header -->
        <div class="flex justify-between mb-10 pr-3 sm:px-2">
            <!-- Close button -->
            <button class="lg:hidden text-gray-500 hover:text-gray-400" @click.stop="sidebarOpen = !sidebarOpen" aria-controls="sidebar" :aria-expanded="sidebarOpen">
                <span class="sr-only">Close sidebar</span>
                <svg class="w-6 h-6 fill-current" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.7 18.7l1.4-1.4L7.8 13H20v-2H7.8l4.3-4.3-1.4-1.4L4 12z" />
                </svg>
            </button>
            <!-- Logo -->
            <a class="block" href="index.html">
                <svg width="32" height="32" viewBox="0 0 32 32">
                    <defs>
                        <linearGradient x1="28.538%" y1="20.229%" x2="100%" y2="108.156%" id="logo-a">
                            <stop stop-color="#A5B4FC" stop-opacity="0" offset="0%" />
                            <stop stop-color="#A5B4FC" offset="100%" />
                        </linearGradient>
                        <linearGradient x1="88.638%" y1="29.267%" x2="22.42%" y2="100%" id="logo-b">
                            <stop stop-color="#38BDF8" stop-opacity="0" offset="0%" />
                            <stop stop-color="#38BDF8" offset="100%" />
                        </linearGradient>
                    </defs>
                    <rect fill="#6366F1" width="32" height="32" rx="16" />
                    <path d="M18.277.16C26.035 1.267 32 7.938 32 16c0 8.837-7.163 16-16 16a15.937 15.937 0 01-10.426-3.863L18.277.161z" fill="#4F46E5" />
                    <path d="M7.404 2.503l18.339 26.19A15.93 15.93 0 0116 32C7.163 32 0 24.837 0 16 0 10.327 2.952 5.344 7.404 2.503z" fill="url(#logo-a)" />
                    <path d="M2.223 24.14L29.777 7.86A15.926 15.926 0 0132 16c0 8.837-7.163 16-16 16-5.864 0-10.991-3.154-13.777-7.86z" fill="url(#logo-b)" />
                </svg>
            </a>
        </div>

        <!-- Links -->
        <div class="space-y-8">
            <!-- Pages group -->
            @foreach($view_navigations as $group_navigations)
            <div>
                <h3 class="text-xs uppercase text-gray-500 font-semibold pl-3">
                    <span class="hidden lg:block lg:sidebar-expanded:hidden 2xl:hidden text-center w-6" aria-hidden="true">•••</span>
                    <span class="lg:hidden lg:sidebar-expanded:block 2xl:block">{{ $group_navigations['name'] }}</span>
                </h3>
                <ul class="mt-3">
                    @foreach($group_navigations['navigations'] as $navigation)
                        @if(count($navigation['children']) > 0)
                                <!-- Menu {{ $navigation['id'] }} -->
                                <li wire:key="{{ rand() }}" class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="{ 'bg-gray-900': menu_{{ $navigation['id'] }}.includes(page) }" x-data="{ open: false }" x-init="$nextTick(() => open = menu_{{ $navigation['id'] }}.includes(page))">
                                    <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="menu_{{ $navigation['id'] }}.includes(page) && 'hover:text-gray-200'" href="#0" @click.prevent="sidebarExpanded ? open = !open : sidebarExpanded = true">
                                        <div class="flex items-center justify-between">
                                            <div class="flex items-center">
                                                <svg class="shrink-0 h-6 w-6" viewBox="0 0 24 24">
                                                    <path class="fill-current text-gray-400" :class="menu_{{ $navigation['id'] }}.includes(page) && 'text-indigo-300'" d="M13 15l11-7L11.504.136a1 1 0 00-1.019.007L0 7l13 8z" />
                                                    <path class="fill-current text-gray-700" :class="menu_{{ $navigation['id'] }}.includes(page) && '!text-indigo-600'" d="M13 15L0 7v9c0 .355.189.685.496.864L13 24v-9z" />
                                                    <path class="fill-current text-gray-600" :class="menu_{{ $navigation['id'] }}.includes(page) && 'text-indigo-500'" d="M13 15.047V24l10.573-7.181A.999.999 0 0024 16V8l-11 7.047z" />
                                                </svg>
                                                <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $navigation['title'] }}</span>
                                            </div>
                                            <!-- Icon -->
                                            <div class="flex shrink-0 ml-2 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">
                                                <svg class="w-3 h-3 shrink-0 ml-1 fill-current text-gray-400" :class="open && 'transform rotate-180'" viewBox="0 0 12 12">
                                                    <path d="M5.9 11.4L.5 6l1.4-1.4 4 4 4-4L11.3 6z" />
                                                </svg>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="lg:hidden lg:sidebar-expanded:block 2xl:block">
                                        <ul class="pl-9 mt-1" :class="!open && 'hidden'" x-cloak>
                                            @foreach($navigation['children'] as $child)
                                            <li class="mb-1 last:mb-0">
                                                <a class="block text-gray-400 hover:text-gray-200 transition duration-150 truncate" :class="page === '{{ $child['route'] }}' && '!text-indigo-500'" href="/{{ $child['route'] }}">
                                                    <span class="text-sm font-medium lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $child['title'] }}</span>
                                                </a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                        @else
                        <li wire:key="{{ rand() }}" class="px-3 py-2 rounded-sm mb-0.5 last:mb-0" :class="page === '{{ $navigation['route'] }}' && 'bg-gray-900'">
                            <a class="block text-gray-200 hover:text-white truncate transition duration-150" :class="page === '{{ $navigation['route'] }}' && 'hover:text-gray-200'" href="/{{ $navigation['route'] }}">
                                <div class="flex items-center">
                                    {!! $navigation['svg'] !!}
                                    <span class="text-sm font-medium ml-3 lg:opacity-0 lg:sidebar-expanded:opacity-100 2xl:opacity-100 duration-200">{{ $navigation['title'] }}</span>
                                </div>
                            </a>
                        </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            @endforeach
        </div>
        <!-- Expand / collapse button -->
        <div class="pt-3 hidden lg:inline-flex 2xl:hidden justify-end mt-auto">
            <div class="px-3 py-2">
                <button @click="sidebarExpanded = !sidebarExpanded">
                    <span class="sr-only">Expand / collapse sidebar</span>
                    <svg class="w-6 h-6 fill-current sidebar-expanded:rotate-180" viewBox="0 0 24 24">
                        <path class="text-gray-400" d="M19.586 11l-5-5L16 4.586 23.414 12 16 19.414 14.586 18l5-5H7v-2z" />
                        <path class="text-gray-600" d="M3 23H1V1h2z" />
                    </svg>
                </button>
            </div>
        </div>

    </div>
</div>
@endif
