<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body_format"
                                class="block text-gray-700 text-sm font-bold mb-2">Format</label>
                            <input type="text"
                                class="form-input w-full"
                                id="body_format" placeholder="Enter Format" wire:model.lazy="body_format">
                            @error('body_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="body"
                                class="block text-gray-700 text-sm font-bold mb-2">Description</label>
                            <textarea class="form-input w-full"
                                id="body" placeholder="Enter Description" wire:model.lazy="body"></textarea>
                            @error('body') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="server_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Server</label>
                            <input type="text"
                                class="form-input w-full"
                                id="server_id" placeholder="Enter Server" wire:model.lazy="server_id">
                            @error('server_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="path"
                                class="block text-gray-700 text-sm font-bold mb-2">Path</label>
                            <textarea class="form-input w-full"
                                id="path" placeholder="Enter Path" wire:model.lazy="path"></textarea>
                            @error('path') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="git_branch"
                                class="block text-gray-700 text-sm font-bold mb-2">Git Branch</label>
                            <input type="text"
                                class="form-input w-full"
                                id="git_branch" placeholder="Enter Git Branch" wire:model.lazy="git_branch">
                            @error('git_branch') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="cicd_user_access"
                                class="block text-gray-700 text-sm font-bold mb-2">CICD User Access</label>
                            <input type="text"
                                class="form-input w-full"
                                id="cicd_user_access" placeholder="Enter CICD User Access" wire:model.lazy="cicd_user_access">
                            @error('cicd_user_access') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="stage"
                                class="block text-gray-700 text-sm font-bold mb-2">Stage</label>
                            <input type="text"
                                class="form-input w-full"
                                id="stage" placeholder="Enter Stage" wire:model.lazy="stage">
                            @error('stage') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="cicd_format"
                                class="block text-gray-700 text-sm font-bold mb-2">CICD Format</label>
                            <input type="text"
                                class="form-input w-full"
                                id="cicd_format" placeholder="Enter CICD Format" wire:model.lazy="cicd_format">
                            @error('cicd_format') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="stageable"
                                class="block text-gray-700 text-sm font-bold mb-2">Stageable</label>
                            <input type="text"
                                class="form-input w-full"
                                id="stageable" placeholder="Enter Stageable" wire:model.lazy="stageable">
                            @error('stageable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
