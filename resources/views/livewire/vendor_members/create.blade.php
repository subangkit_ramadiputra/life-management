<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="vendor_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">Vendor</label>
                            <select class="form-select" id="vendor_id"  placeholder="Enter Vendor" wire:model.lazy="vendor_id">
                                <option value="0">-</option>
                                @foreach($vendors as $vendor)
                                    <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                                @endforeach
                            </select>
                            @error('vendor_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="user_id"
                                   class="block text-gray-700 text-sm font-bold mb-2">User</label>
                            <select class="form-select" id="user_id"  placeholder="Enter User" wire:model.lazy="user_id">
                                <option value="0">-</option>
                                @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}} ({{$user->email}})</option>
                                @endforeach
                            </select>
                            @error('user_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="status"
                                class="block text-gray-700 text-sm font-bold mb-2">Member Status</label>
                            <input type="text"
                                class="form-input w-full"
                                id="status" placeholder="Enter Member Status" wire:model.lazy="status">
                            @error('status') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="is_banned"
                                class="block text-gray-700 text-sm font-bold mb-2">Banned</label>
                            <input type="text"
                                class="form-input w-full"
                                id="is_banned" placeholder="Enter Banned" wire:model.lazy="is_banned">
                            @error('is_banned') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="role"
                                class="block text-gray-700 text-sm font-bold mb-2">Role</label>
                            <input type="text"
                                class="form-input w-full"
                                id="role" placeholder="Enter Role" wire:model.lazy="role">
                            @error('role') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
