<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="field_name"
                                class="block text-gray-700 text-sm font-bold mb-2">Field Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="field_name" placeholder="Enter Field Name" wire:model.lazy="field_name">
                            @error('field_name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="nullable"
                                class="block text-gray-700 text-sm font-bold mb-2">Nullable ?</label>
                            <input type="text"
                                class="form-input w-full"
                                id="nullable" placeholder="Enter Nullable ?" wire:model.lazy="nullable">
                            @error('nullable') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="title"
                                class="block text-gray-700 text-sm font-bold mb-2">Title</label>
                            <input type="text"
                                class="form-input w-full"
                                id="title" placeholder="Enter Title" wire:model.lazy="title">
                            @error('title') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="type"
                                class="block text-gray-700 text-sm font-bold mb-2">Field Type</label>
                            <input type="text"
                                class="form-input w-full"
                                id="type" placeholder="Enter Field Type" wire:model.lazy="type">
                            @error('type') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="length"
                                class="block text-gray-700 text-sm font-bold mb-2">Data Length</label>
                            <input type="text"
                                class="form-input w-full"
                                id="length" placeholder="Enter Data Length" wire:model.lazy="length">
                            @error('length') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="precision"
                                class="block text-gray-700 text-sm font-bold mb-2">Data Precision (Number Type)</label>
                            <input type="text"
                                class="form-input w-full"
                                id="precision" placeholder="Enter Data Precision (Number Type)" wire:model.lazy="precision">
                            @error('precision') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="scale"
                                class="block text-gray-700 text-sm font-bold mb-2">Data Scale</label>
                            <input type="text"
                                class="form-input w-full"
                                id="scale" placeholder="Enter Data Scale" wire:model.lazy="scale">
                            @error('scale') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="digits"
                                class="block text-gray-700 text-sm font-bold mb-2">Number of Digits (Numeric Type)</label>
                            <input type="text"
                                class="form-input w-full"
                                id="digits" placeholder="Enter Number of Digits (Numeric Type)" wire:model.lazy="digits">
                            @error('digits') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="decimal"
                                class="block text-gray-700 text-sm font-bold mb-2">Decimal (Numeric Type)</label>
                            <input type="text"
                                class="form-input w-full"
                                id="decimal" placeholder="Enter Decimal (Numeric Type)" wire:model.lazy="decimal">
                            @error('decimal') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="default"
                                class="block text-gray-700 text-sm font-bold mb-2">Default Value</label>
                            <input type="text"
                                class="form-input w-full"
                                id="default" placeholder="Enter Default Value" wire:model.lazy="default">
                            @error('default') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="view_table"
                                class="block text-gray-700 text-sm font-bold mb-2">View on Table (Presentation)</label>
                            <input type="text"
                                class="form-input w-full"
                                id="view_table" placeholder="Enter View on Table (Presentation)" wire:model.lazy="view_table">
                            @error('view_table') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="view_create"
                                class="block text-gray-700 text-sm font-bold mb-2">View on Create Form (Presentation)</label>
                            <input type="text"
                                class="form-input w-full"
                                id="view_create" placeholder="Enter View on Create Form (Presentation)" wire:model.lazy="view_create">
                            @error('view_create') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
