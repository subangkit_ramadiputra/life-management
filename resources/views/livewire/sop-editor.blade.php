<div>
    <div class="overflow-visible">
        <table class="table-auto w-full">
            <!-- Table header -->
            <thead class="text-xs font-semibold uppercase text-gray-500 bg-gray-50 border-t border-b border-gray-200">
            <tr>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">No</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">Title</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">Summary</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">BpmnRoles</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">BpmnEvents</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">BpmnActivities</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">BpmnGateways</div>
                </th>
                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="font-semibold text-left">BpmnSquences</div>
                </th>

                <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <span class="sr-only">Menu</span>
                </th>
            </tr>
            </thead>
            <!-- Table body -->
            <tbody class="text-sm divide-y divide-gray-200">
            <!-- Row -->
            <tr>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="text-left">{{ $sop->id }}</div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="text-left">{{ $sop->title }}</div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="text-left">{{ $sop->summary }}</div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div class="text-left">{{ $sop->sopable }}</div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div>
                        @php
                            $countA = $sop->bpmn_roles()->count();
                        @endphp
                        @if($countA > 1)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnRoles</div>
                            </div>
                        @elseif ($countA > 0)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnRole</div>
                            </div>
                        @else
                        @endif
                        <button wire:click="addBpmnRole({{ $sop->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                            BpmnRole
                        </button>
                    </div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div>
                        @php
                            $countA = $sop->bpmn_events()->count();
                        @endphp
                        @if($countA > 1)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnEvents</div>
                            </div>
                        @elseif ($countA > 0)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnEvent</div>
                            </div>
                        @else
                        @endif
                        <button wire:click="addBpmnEvent({{ $sop->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                            BpmnEvent
                        </button>
                    </div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div>
                        @php
                            $countA = $sop->bpmn_activities()->count();
                        @endphp
                        @if($countA > 1)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnActivities</div>
                            </div>
                        @elseif ($countA > 0)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnActivity</div>
                            </div>
                        @else
                        @endif
                        <button wire:click="addBpmnActivity({{ $sop->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                            BpmnActivity
                        </button>
                    </div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div>
                        @php
                            $countA = $sop->bpmn_gateways()->count();
                        @endphp
                        @if($countA > 1)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnGateways</div>
                            </div>
                        @elseif ($countA > 0)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnGateway</div>
                            </div>
                        @else
                        @endif
                        <button wire:click="addBpmnGateway({{ $sop->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                            BpmnGateway
                        </button>
                    </div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                    <div>
                        @php
                            $countA = $sop->bpmn_squences()->count();
                        @endphp
                        @if($countA > 1)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnSquences</div>
                            </div>
                        @elseif ($countA > 0)
                            <div class="inline-flex">
                                <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                <div class="text-sm text-gray-500">BpmnSquence</div>
                            </div>
                        @else
                        @endif
                        <button wire:click="addBpmnSquence({{ $sop->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                            <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                            BpmnSquence
                        </button>
                    </div>
                </td>
                <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap w-px" style="min-width: 100px;">
                    <!-- Start -->
                    <div class="relative inline-flex" x-data="{ open: false }">
                        <button
                            class="text-gray-400 hover:text-gray-500 rounded-full"
                            :class="{ 'bg-gray-100 text-gray-500': open }"
                            aria-haspopup="true"
                            @click.prevent="open = !open"
                            :aria-expanded="open"
                        >
                            <span class="sr-only">Menu</span>
                            <svg class="w-8 h-8 fill-current" viewBox="0 0 32 32">
                                <circle cx="16" cy="16" r="2" />
                                <circle cx="10" cy="16" r="2" />
                                <circle cx="22" cy="16" r="2" />
                            </svg>
                        </button>
                        <div
                            class="origin-top-right z-10 absolute top-full left-0 min-w-36 bg-white border border-gray-200 py-1.5 rounded shadow-lg overflow-hidden mt-1"
                            @click.outside="open = false"
                            @keydown.escape.window="open = false"
                            x-show="open"
                            x-transition:enter="transition ease-out duration-200 transform"
                            x-transition:enter-start="opacity-0 -translate-y-2"
                            x-transition:enter-end="opacity-100 translate-y-0"
                            x-transition:leave="transition ease-out duration-200"
                            x-transition:leave-start="opacity-100"
                            x-transition:leave-end="opacity-0"
                            x-cloak
                        >
                            <ul>
                                <li>
                                    <a class="font-medium text-sm text-gray-600 hover:text-gray-800 flex py-1 px-3" wire:click="edit({{ $sop->id }})" @click="open = false" @focus="open = true" @focusout="open = false">Edit</a>
                                </li>
                                <li>
                                    <!-- Start -->
                                    <div x-data="{ modalOpen: false }">
                                        <a class="font-medium text-sm text-red-500 hover:text-red-600 flex py-1 px-3" @click.prevent="modalOpen = true" @focus="open = true" @focusout="open = false">Delete</a>
                                        <!-- Modal backdrop -->
                                        <div
                                            class="fixed inset-0 bg-gray-900 bg-opacity-30 z-50 transition-opacity"
                                            x-show="modalOpen"
                                            x-transition:enter="transition ease-out duration-200"
                                            x-transition:enter-start="opacity-0"
                                            x-transition:enter-end="opacity-100"
                                            x-transition:leave="transition ease-out duration-100"
                                            x-transition:leave-start="opacity-100"
                                            x-transition:leave-end="opacity-0"
                                            aria-hidden="true"
                                            x-cloak
                                        ></div>
                                        <!-- Modal dialog -->
                                        <div
                                            id="danger-modal"
                                            class="fixed inset-0 z-50 overflow-hidden flex items-center my-4 justify-center transform px-4 sm:px-6"
                                            role="dialog"
                                            aria-modal="true"
                                            x-show="modalOpen"
                                            x-transition:enter="transition ease-in-out duration-200"
                                            x-transition:enter-start="opacity-0 translate-y-4"
                                            x-transition:enter-end="opacity-100 translate-y-0"
                                            x-transition:leave="transition ease-in-out duration-200"
                                            x-transition:leave-start="opacity-100 translate-y-0"
                                            x-transition:leave-end="opacity-0 translate-y-4"
                                            x-cloak
                                        >
                                            <div class="bg-white rounded shadow-lg overflow-auto max-w-lg w-full max-h-full" @click.outside="modalOpen = false" @keydown.escape.window="modalOpen = false">
                                                <div class="p-5 flex space-x-4">
                                                    <!-- Icon -->
                                                    <div class="w-10 h-10 rounded-full flex items-center justify-center shrink-0 bg-red-100">
                                                        <svg class="w-4 h-4 shrink-0 fill-current text-red-500" viewBox="0 0 16 16">
                                                            <path d="M8 0C3.6 0 0 3.6 0 8s3.6 8 8 8 8-3.6 8-8-3.6-8-8-8zm0 12c-.6 0-1-.4-1-1s.4-1 1-1 1 .4 1 1-.4 1-1 1zm1-3H7V4h2v5z" />
                                                        </svg>
                                                    </div>
                                                    <!-- Content -->
                                                    <div class="w-full">
                                                        <!-- Modal header -->
                                                        <div class="mb-2">
                                                            <div class="text-lg font-semibold text-gray-800">Delete 1 sop?</div>
                                                        </div>
                                                        <!-- Modal content -->
                                                        <div class="text-sm mb-10">
                                                            <div class="space-y-2">
                                                                <p class="w-full">{{ $sop->title }}</p>
                                                            </div>
                                                        </div>
                                                        <!-- Modal footer -->
                                                        <div class="flex flex-wrap justify-end space-x-2">
                                                            <button class="btn-sm border-gray-200 hover:border-gray-300 text-gray-600" @click="modalOpen = false; open = false">Cancel</button>
                                                            <button class="btn-sm bg-red-500 hover:bg-red-600 text-white" wire:click="delete({{ $sop->id }})" @click="modalOpen = false; open = false">Yes, Delete it</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End -->
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End -->
                </td>

            </tr>
            </tbody>
        </table>

        {{ json_encode($definitionArray) }}
        {{ $serializer }}

        <div class="content" id="js-drop-zone">

            <div class="message intro">
                <div class="note">
                    Drop BPMN diagram from your desktop or <a id="js-create-diagram" href>create a new diagram</a> to get started.
                </div>
            </div>

            <div class="message error">
                <div class="note">
                    <p>Ooops, we could not display the BPMN 2.0 diagram.</p>

                    <div class="details">
                        <span>cause of the problem</span>
                        <pre></pre>
                    </div>
                </div>
            </div>

            <div class="canvas" id="js-canvas"></div>
        </div>

        <ul class="buttons">
            <li>
                download
            </li>
            <li>
                <a id="js-download-diagram" href title="download BPMN diagram">
                    BPMN diagram
                </a>
            </li>
            <li>
                <a id="js-download-svg" href title="download as SVG image">
                    SVG image
                </a>
            </li>
        </ul>

    </div>
</div>
