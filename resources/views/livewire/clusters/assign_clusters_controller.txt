Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/clusters/assign_clusters_template.blade.php to resources/views/livewire/parent/assign_clusters.blade.php
or
cp resources/views/livewire/clusters/assign_clusters_template.blade.php resources/views/livewire/parents/assign_clusters.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isClusterModalOpen)
            @include('livewire.parents.assign_clusters')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Clusters</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->clusters()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Clusters</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Cluster</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addCluster({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        Cluster
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\Cluster;


    /**
     * Get all of the parents clusters.
     */
    public function clusters()
    {
        return $this->morphMany(Cluster::class, 'clusterable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function clusters()
        {
            return $this->hasMany(Cluster::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // Cluster Section
    public $isClusterModalOpen = 0;
    public $recent_clusters = [];
    public $clusters = [];
    public function addCluster($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_clusters = $parent->clusters()->get();
        $this->resetClusterCreateForm();
        $this->openClusterModalPopover();
    }

    public function openClusterModalPopover()
    {
        $this->isClusterModalOpen = true;
    }
    public function closeClusterModalPopover()
    {
        $this->isClusterModalOpen = false;
    }

    public function editCluster($cluster_id) {
        $cluster = Cluster::findOrFail($cluster_id);
        $this->cluster_id = $cluster_id;
        $this->cluster_name = $cluster->name;
        $this->cluster_summary = $cluster->summary;
        $this->cluster_body_format = $cluster->body_format;
        $this->cluster_body = $cluster->body;
        $this->cluster_clusterable = $cluster->clusterable;

    }

    public function resetClusterCreateForm(){
        $this->cluster_id = '';
        $this->cluster_name = '';
        $this->cluster_summary = '';
        $this->cluster_body_format = 'markdown';
        $this->cluster_body = '';
        $this->cluster_clusterable = '';

    }

    public function assignCluster() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->cluster_id == '') {
            $cluster = new Cluster();
            $cluster->name = $this->cluster_name;
            $cluster->summary = $this->cluster_summary;
            $cluster->body_format = $this->cluster_body_format;
            $cluster->body = $this->cluster_body;

            $parent->clusters()->save($cluster);

            $this->alertSuccessMessage('Cluster created.');
            //$this->closeClusterModalPopover();
            $this->resetClusterCreateForm();
        } else {
            Cluster::updateOrCreate(['id' => $this->cluster_id], [
                'name' => $this->cluster_name,
                'summary' => $this->cluster_summary,
                'body_format' => $this->cluster_body_format,
                'body' => $this->cluster_body,

            ]);
            $this->alertSuccessMessage('Cluster updated.');
        }

        $this->recent_clusters = $parent->clusters()->get();
    }

    public function deleteCluster($parent_id, $cluster_id) {
        $parent = Parent::findOrFail($parent_id);

        $cluster = Cluster::find($cluster_id);
        if ($cluster) {
            $parent->clusters()->where('id', '=', $cluster->id)->delete();
        }

        $this->recent_clusters = $parent->clusters()->get();

        $this->alertSuccessMessage('Cluster deleted.');
    }

    public function deleteExistingCluster($cluster_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $cluster = Cluster::find($cluster_id);
        if ($cluster) {
            $parent->clusters()->where('id', '=', $cluster->id)->delete();
        }

        $this->recent_clusters = $parent->clusters()->get();

        $this->alertSuccessMessage('Cluster deleted.');
    }
    // End of Section
