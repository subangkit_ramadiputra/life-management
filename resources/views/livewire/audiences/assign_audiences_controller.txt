Note:
Change Parent -> Model Parent

1. Copy resources/views/livewire/audiences/assign_audiences_template.blade.php to resources/views/livewire/parent/assign_audiences.blade.php
or
cp resources/views/livewire/audiences/assign_audiences_template.blade.php resources/views/livewire/parents/assign_audiences.blade.php

1.a Then change
$parent
2. Add View to Parent View
        @if($isAudienceModalOpen)
            @include('livewire.parents.assign_audiences')
        @endif
3. Add Column for New Assign
                        <th class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                            <div class="font-semibold text-left">Audiences</div>
                        </th>
4. Add Column in Row Level
                            <td class="px-2 first:pl-5 last:pr-5 py-3 whitespace-nowrap">
                                <div>
                                    @php
                                        $countA = $parent->audiences()->count();
                                    @endphp
                                    @if($countA > 1)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Audiences</div>
                                        </div>
                                    @elseif ($countA > 0)
                                        <div class="inline-flex">
                                            <div class="text-3xl font-bold text-gray-800 mr-2">{{ $countA }}</div>
                                            <div class="text-sm text-gray-500">Audience</div>
                                        </div>
                                    @else
                                    @endif
                                    <button wire:click="addAudience({{ $parent->id }})" class="inline-flex btn border-gray-200 hover:border-gray-300 bg-blue-100 mt-1 p-1">
                                        <span><svg class="w-4 h-4 fill-current text-blue-500 shrink-0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6"></path></svg></span>
                                        Audience
                                    </button>
                                </div>
                            </td>
4.a Change $parent to parent variable
5. Add relations to Parent
use App\Models\Audience;


    /**
     * Get all of the parents audiences.
     */
    public function audiences()
    {
        return $this->morphMany(Audience::class, 'audienceable');
    }

        /**
         * Get the applicatoins for the concept.
         */
        public function audiences()
        {
            return $this->hasMany(Audience::class,'parent_id');
        }
6. Run php artisan migrate
7. Add To Parent Controller
7.a replace parent -> parent variable
7.b replace Parent -> Parent variable

    // Audience Section
    public $isAudienceModalOpen = 0;
    public $recent_audiences = [];
    public $audiences = [];
    public function addAudience($id) {
        $this->parent_id = $id;
        $parent = Parent::findOrFail($this->parent_id);
        $this->recent_audiences = $parent->audiences()->get();
        $this->resetAudienceCreateForm();
        $this->openAudienceModalPopover();
    }

    public function openAudienceModalPopover()
    {
        $this->isAudienceModalOpen = true;
    }
    public function closeAudienceModalPopover()
    {
        $this->isAudienceModalOpen = false;
    }

    public function editAudience($audience_id) {
        $audience = Audience::findOrFail($audience_id);
        $this->audience_id = $audience_id;
        $this->audience_name = $audience->name;
        $this->audience_summary = $audience->summary;
        $this->audience_detail = $audience->detail;
        $this->audience_min_age = $audience->min_age;
        $this->audience_max_age = $audience->max_age;
        $this->audience_priority = $audience->priority;
        $this->audience_highlight_words = $audience->highlight_words;
        $this->audience_application_id = $audience->application_id;

    }

    public function resetAudienceCreateForm(){
        $this->audience_id = '';
        $this->audience_name = '';
        $this->audience_summary = '';
        $this->audience_detail = '';
        $this->audience_min_age = '0';
        $this->audience_max_age = '100';
        $this->audience_priority = 'Normal';
        $this->audience_highlight_words = '';
        $this->audience_application_id = '';

    }

    public function assignAudience() {
        $parent = Parent::findOrFail($this->parent_id);
        if ($this->audience_id == '') {
            $audience = new Audience();
            $audience->name = $this->audience_name;
            $audience->summary = $this->audience_summary;
            $audience->detail = $this->audience_detail;
            $audience->min_age = $this->audience_min_age;
            $audience->max_age = $this->audience_max_age;
            $audience->priority = $this->audience_priority;
            $audience->highlight_words = $this->audience_highlight_words;
            $audience->application_id = $this->audience_application_id;

            $parent->audiences()->save($audience);

            $this->alertSuccessMessage('Audience created.');
            //$this->closeAudienceModalPopover();
            $this->resetAudienceCreateForm();
        } else {
            Audience::updateOrCreate(['id' => $this->audience_id], [
                'name' => $this->audience_name,
                'summary' => $this->audience_summary,
                'detail' => $this->audience_detail,
                'min_age' => $this->audience_min_age,
                'max_age' => $this->audience_max_age,
                'priority' => $this->audience_priority,
                'highlight_words' => $this->audience_highlight_words,
                'application_id' => $this->audience_application_id,

            ]);
            $this->alertSuccessMessage('Audience updated.');
        }

        $this->recent_audiences = $parent->audiences()->get();
    }

    public function deleteAudience($parent_id, $audience_id) {
        $parent = Parent::findOrFail($parent_id);

        $audience = Audience::find($audience_id);
        if ($audience) {
            $parent->audiences()->where('id', '=', $audience->id)->delete();
        }

        $this->recent_audiences = $parent->audiences()->get();

        $this->alertSuccessMessage('Audience deleted.');
    }

    public function deleteExistingAudience($audience_id) {
        $parent = Parent::findOrFail($this->parent_id);

        $audience = Audience::find($audience_id);
        if ($audience) {
            $parent->audiences()->where('id', '=', $audience->id)->delete();
        }

        $this->recent_audiences = $parent->audiences()->get();

        $this->alertSuccessMessage('Audience deleted.');
    }
    // End of Section
