<div class="fixed z-10 inset-0 overflow-y-auto ease-out duration-400">
    <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
        <div class="fixed inset-0 transition-opacity">
            <div class="absolute inset-0 bg-gray-500 opacity-75"></div>
        </div>
        <div class="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full"
            role="dialog" aria-modal="true" aria-labelledby="modal-headline">
            <form>
                <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                    <div class="">
                        <div class="mb-4">
                            <label for="name"
                                class="block text-gray-700 text-sm font-bold mb-2">Audience Group Name</label>
                            <input type="text"
                                class="form-input w-full"
                                id="name" placeholder="Enter Audience Group Name" wire:model.lazy="name">
                            @error('name') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="summary"
                                class="block text-gray-700 text-sm font-bold mb-2">Summary</label>
                            <textarea class="form-input w-full"
                                id="summary" placeholder="Enter Summary" wire:model.lazy="summary"></textarea>
                            @error('summary') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="detail"
                                class="block text-gray-700 text-sm font-bold mb-2">Detail</label>
                            <textarea class="form-input w-full"
                                id="detail" placeholder="Enter Detail" wire:model.lazy="detail"></textarea>
                            @error('detail') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="min_age"
                                class="block text-gray-700 text-sm font-bold mb-2">Minimum Age</label>
                            <input type="text"
                                class="form-input w-full"
                                id="min_age" placeholder="Enter Minimum Age" wire:model.lazy="min_age">
                            @error('min_age') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="max_age"
                                class="block text-gray-700 text-sm font-bold mb-2">Maximum Age</label>
                            <input type="text"
                                class="form-input w-full"
                                id="max_age" placeholder="Enter Maximum Age" wire:model.lazy="max_age">
                            @error('max_age') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="priority"
                                class="block text-gray-700 text-sm font-bold mb-2">Priority ( Normal | Medium | High )</label>
                            <input type="text"
                                class="form-input w-full"
                                id="priority" placeholder="Enter Priority ( Normal | Medium | High )" wire:model.lazy="priority">
                            @error('priority') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="highlight_words"
                                class="block text-gray-700 text-sm font-bold mb-2">Highlighted Words</label>
                            <textarea class="form-input w-full"
                                id="highlight_words" placeholder="Enter Highlighted Words" wire:model.lazy="highlight_words"></textarea>
                            @error('highlight_words') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>
                        <div class="mb-4">
                            <label for="application_id"
                                class="block text-gray-700 text-sm font-bold mb-2">Application</label>
                            <input type="text"
                                class="form-input w-full"
                                id="application_id" placeholder="Enter Application" wire:model.lazy="application_id">
                            @error('application_id') <span class="text-red-500">{{ $message }}</span>@enderror
                        </div>

                    </div>
                </div>
                <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <span class="flex w-full rounded-md shadow-sm sm:ml-3 sm:w-auto">
                        <button x-on:click="@this.call('store')" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-transparent px-4 py-2 bg-red-600 text-base leading-6 font-bold text-white shadow-sm hover:bg-red-700 focus:outline-none focus:border-green-700 focus:shadow-outline-green transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Store
                        </button>
                    </span>
                    <span class="mt-3 flex w-full rounded-md shadow-sm sm:mt-0 sm:w-auto">
                        <button wire:click="closeModalPopover()" type="button"
                            class="inline-flex justify-center w-full rounded-md border border-gray-300 px-4 py-2 bg-white text-base leading-6 font-bold text-gray-700 shadow-sm hover:text-gray-700 focus:outline-none focus:border-blue-300 focus:shadow-outline-blue transition ease-in-out duration-150 sm:text-sm sm:leading-5">
                            Close
                        </button>
                    </span>
                </div>
            </form>
        </div>
    </div>
</div>
