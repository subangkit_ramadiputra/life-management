<div class="px-4 sm:px-6 lg:px-8 py-8 w-full">

    <!-- Page content -->
    <div class="max-w-5xl mx-auto flex flex-col lg:flex-row lg:space-x-8 xl:space-x-16">

        <!-- Cart items -->
        <div class="mb-6 lg:mb-0 flex-initial lg:w-2/3 md:w-1/2">
            <header class="mb-2">
                <!-- Title -->
                <h1 class="text-2xl md:text-3xl text-gray-800 font-bold">Data</h1>
            </header>
            <div class="break-words text-teal-600 text-sm">
            {{ json_encode($values)  }}
            </div>

            <header class="mb-2">
                <!-- Title -->
                <h1 class="text-2xl md:text-3xl text-gray-800 font-bold">Output</h1>
            </header>
            <p style="white-space: pre-wrap;" class="w-full">{!! htmlentities($output) !!} </p>
        </div>

        <!-- Sidebar -->
        <div class="flex-initial lg:w-1/3 md:w-1/2 max-w-sm mx-auto lg:max-w-none">
            <div class="bg-white p-5 shadow-lg rounded-sm border border-gray-200 lg:w-72 xl:w-80">
                <div class="text-gray-800 font-semibold mb-2">Values</div>
                <!-- Promo box -->
                @foreach($attributes as $attribute)
                    @php
                        $view = 'default';
                        switch($attribute->type) {
                            case 'text' :
                            case 'longText' :
                                $view = 'text';
                            break;
                            case 'string' :
                                $view = 'string';
                            break;
                            default:
                                $view = 'default';
                        }
                    @endphp
                    @include('livewire.attribute_fields.'.$view)
                @endforeach
                <div class="mb-4">
                    <button x-on:click="@this.call('run')" class="btn w-full bg-indigo-500 hover:bg-indigo-600 text-white">Run</button>
                </div>
                <div class="text-xs text-gray-500 italic text-center">Run to show text with format</div>
            </div>
        </div>

    </div>

</div>
