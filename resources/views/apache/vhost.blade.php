# {{ $web_root_folder }}
<VirtualHost *:80>
    DocumentRoot "{{ $web_root_folder }}"
    ServerName {{ $domain }}
    ErrorLog "/private/var/log/apache2/{{ $domain }}.local-error_log"
    CustomLog "/private/var/log/apache2/{{ $domain }}.local-access_log" common
    <Directory "{{ $web_root_folder }}">
    AllowOverride All
    Require all granted
    </Directory>
</VirtualHost>
