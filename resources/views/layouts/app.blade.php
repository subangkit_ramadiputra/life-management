<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap">
        <link href="./jsoneditor/dist/jsoneditor.min.css" rel="stylesheet" type="text/css">

        <!-- Scripts -->
        <script src="./ace/ace.js"></script>

        <script src="https://cdn.tiny.cloud/1/5ec3dvn474436be0z88nx040lias88ds1rv4e5cns5ao9knh/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
        <script src="./jsoneditor/dist/jsoneditor.min.js"></script>
        @vite(['resources/css/style.scss', 'resources/js/jquery-3.6.1.min.js', 'resources/js/app.js'])

        <!-- Styles -->
        @livewireStyles

    </head>
    <body class="font-inter antialiased bg-gray-100 text-gray-600 sidebar-expanded"
          :class="{ 'sidebar-expanded': sidebarExpanded }"
          x-data="{ page: 'dashboard', sidebarOpen: false, sidebarExpanded: localStorage.getItem('sidebar-expanded') == 'true' }"
          x-init="$watch('sidebarExpanded', value => localStorage.setItem('sidebar-expanded', value))">

    <script>
        if (localStorage.getItem('sidebar-expanded') == 'true') {
            document.querySelector('body').classList.add('sidebar-expanded');
        } else {
            document.querySelector('body').classList.remove('sidebar-expanded');
        }
    </script>

        <!-- Page wrapper -->
        <div class="flex h-screen overflow-hidden">
            <!-- Sidebar -->
            @livewire('sidebar')

            <!-- Content area -->
            <div class="relative flex flex-col flex-1 overflow-y-auto overflow-x-scroll">

                @livewire('navigation-menu')

                <!-- Page Content -->
                <main>
                    {{ $slot }}
                </main>
            </div>
        </div>

        @stack('modals')

        @livewireScripts
        @stack('scripts')
    </body>
</html>
