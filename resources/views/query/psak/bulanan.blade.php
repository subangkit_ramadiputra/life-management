@foreach($list_tahun as $tahun)

@if ($tahun == $list_tahun[0])
    IF OBJECT_ID(N'dbo.{{ $table }}', N'U') IS NOT NULL
        DROP TABLE {{ $table }};
@endif
-- JANUARI
@if ($tahun != $list_tahun[0])
INSERT INTO {{ $table }}
@endif
SELECT
-- SEGMEN
CASE WHEN COALESCE(PEL.segmen, KAP.kelompok, NULL) IS NULL OR COALESCE(PEL.segmen, KAP.kelompok, NULL) = '' THEN 'Tidak Diketahui' ELSE COALESCE(PEL.segmen, KAP.kelompok, NULL) END SEGMEN,
TAHUN,BULAN,
-- SALDO AKHIR BULAN -1 , JANUARI = 0
CAST(0 AS NUMERIC(38,2)) SALDO_AWAL,
-- TAMBAH = DEBET
SUM(COALESCE(PEL.byversjp, KAP.biaya, 0)) TAMBAH,
-- KURANG = KREDIT
CAST(0 AS NUMERIC(38,2)) KURANG,
-- SALDO AKHIR
SUM(COALESCE(PEL.byversjp, KAP.biaya, 0)) SALDO_AKHIR
@if ($tahun == $list_tahun[0])
INTO {{ $table }}
@endif
FROM (
SELECT TAHUN, BULAN, NOREG, KAPNOREGBLN, DK, COUNT(1) JML, SUM(NILAI) NILAI FROM bbth_psak_all
WHERE @if ($report == 'beban') KDJNSTRSKEU = 'TMB' @else KDJNSTRSKEU IN ('TBK', 'TBD','TKK', 'TKD') @endif AND KDAKUN = '{{ $akun }}' AND TAHUN = {{ $tahun }} AND BULAN = 1
GROUP BY TAHUN, BULAN, NOREG, KAPNOREGBLN, DK
) BBTH
LEFT JOIN dbAkrual.dbo.PSAK PEL ON BBTH.NOREG = PEL.noreg
LEFT JOIN dbo.pembayaran_kapitasi_persegmen KAP ON BBTH.KAPNOREGBLN = KAP.kapnoregbln
GROUP BY
CASE WHEN COALESCE(PEL.segmen, KAP.kelompok, NULL) IS NULL OR COALESCE(PEL.segmen, KAP.kelompok, NULL) = '' THEN 'Tidak Diketahui' ELSE COALESCE(PEL.segmen, KAP.kelompok, NULL) END,
TAHUN, BULAN;

@for ($bulan = 2; $bulan <= 12; $bulan++)

    -- {{ \Carbon\Carbon::parse($tahun.'-'.$bulan.'-01')->format('F') }}
    WITH BULAN_A AS (
    SELECT
    -- SEGMEN
    CASE WHEN COALESCE(PEL.segmen, KAP.kelompok, NULL) IS NULL OR COALESCE(PEL.segmen, KAP.kelompok, NULL) = '' THEN 'Tidak Diketahui' ELSE COALESCE(PEL.segmen, KAP.kelompok, NULL) END SEGMEN,
    BBTH.TAHUN, BBTH.BULAN,
    -- TAMBAH = DEBET
    SUM(COALESCE(PEL.byversjp, KAP.biaya, 0)) TAMBAH,
    -- KURANG = KREDIT
    CAST(0 AS NUMERIC(38,2)) KURANG
    FROM (
    SELECT TAHUN, BULAN, NOREG, KAPNOREGBLN, DK, COUNT(1) JML, SUM(NILAI) NILAI FROM bbth_psak_all
    WHERE @if ($report == 'beban') KDJNSTRSKEU = 'TMB' @else KDJNSTRSKEU IN ('TBK', 'TBD','TKK', 'TKD') @endif AND  KDAKUN = '{{ $akun }}' AND TAHUN = {{ $tahun }} AND BULAN = {{ $bulan }}
    GROUP BY TAHUN, BULAN, NOREG, KAPNOREGBLN, DK
    ) BBTH
    LEFT JOIN dbAkrual.dbo.PSAK PEL ON BBTH.NOREG = PEL.noreg
    LEFT JOIN dbo.pembayaran_kapitasi_persegmen KAP ON BBTH.KAPNOREGBLN = KAP.kapnoregbln
    GROUP BY
    CASE WHEN COALESCE(PEL.segmen, KAP.kelompok, NULL) IS NULL OR COALESCE(PEL.segmen, KAP.kelompok, NULL) = '' THEN 'Tidak Diketahui' ELSE COALESCE(PEL.segmen, KAP.kelompok, NULL) END,
    BBTH.TAHUN, BBTH.BULAN
    )
    INSERT INTO {{ $table }}
    SELECT
    BULAN_A.SEGMEN, BULAN_A.TAHUN, BULAN_A.BULAN, COALESCE({{ $table }}.SALDO_AKHIR,0) SALDO_AWAL, BULAN_A.TAMBAH, BULAN_A.KURANG,
    -- SALDO AKHIR
    COALESCE({{ $table }}.SALDO_AKHIR,0) + BULAN_A.TAMBAH - BULAN_A.KURANG SALDO_AKHIR
    FROM BULAN_A
    LEFT JOIN {{ $table }} ON {{ $table }}.TAHUN = {{ $tahun }} AND {{ $table }}.BULAN = {{ $bulan - 1 }} AND {{ $table }}.SEGMEN = BULAN_A.SEGMEN
    ;
@endfor
@endforeach

SELECT * FROM {{ $table }} ORDER BY SEGMEN,TAHUN,BULAN
