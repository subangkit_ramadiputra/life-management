@php
    \Carbon\Carbon::setLocale('id');
@endphp
@foreach($list_tahun as $tahun)
    @if( $tahun == $list_tahun[0])
        IF OBJECT_ID(N'dbo.{{ $table_tahunan }}', N'U') IS NOT NULL
        DROP TABLE {{ $table_tahunan }};
    @endif
-- {{ $tahun }}
WITH SEGMEN_T AS (
SELECT SEGMEN,
@for ($bulan = 1; $bulan <= 12; $bulan++)
    SUM([{{$bulan}}]) TAMBAH_{{$bulan}}@if($bulan != 12),@endif
@endfor
  FROM (
SELECT *
FROM {{ $table }}
PIVOT
(
SUM(TAMBAH) FOR [BULAN] IN (
@for ($bulan = 1; $bulan <= 12; $bulan++)
    [{{$bulan}}]@if($bulan != 12),@endif
@endfor
)
) AS p
WHERE TAHUN = {{ $tahun }}
) PIVOTAL
GROUP BY SEGMEN
), SEGMEN_K AS (
SELECT SEGMEN,
@for ($bulan = 1; $bulan <= 12; $bulan++)
    SUM([{{$bulan}}]) KURANG_{{$bulan}}@if($bulan != 12),@endif
@endfor
  FROM (
SELECT *
FROM {{ $table }}
PIVOT
(
SUM(KURANG) FOR [BULAN] IN (
@for ($bulan = 1; $bulan <= 12; $bulan++)
    [{{$bulan}}]@if($bulan != 12),@endif
@endfor
)
) AS p
WHERE TAHUN = {{ $tahun }}
) PIVOTAL
GROUP BY SEGMEN
), TOTAL AS (SELECT SEGMEN, SUM(TAMBAH-KURANG) TOTAL FROM {{ $table }} WHERE TAHUN = {{ $tahun }} GROUP BY SEGMEN)
@if( $tahun != $list_tahun[0])
    INSERT INTO {{ $table_tahunan }}
@endif
SELECT {{ $tahun }} TAHUN, A.SEGMEN,
@for ($bulan = 1; $bulan <= 12; $bulan++)
COALESCE(TAMBAH_{{$bulan}},0)-COALESCE(KURANG_{{$bulan}},0) [{{\Carbon\Carbon::parse($tahun.'-'.$bulan.'-01')->format('F')}}]@if($bulan != 12),@endif
@endfor
,C.TOTAL
@if( $tahun == $list_tahun[0])
    INTO {{ $table_tahunan }}
@endif
FROM SEGMEN_T A
LEFT JOIN SEGMEN_K B ON A.SEGMEN = B.SEGMEN
LEFT JOIN TOTAL C ON A.SEGMEN = C.SEGMEN;
@endforeach
