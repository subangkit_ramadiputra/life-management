<?php

namespace Tests\Garudaku;

use App\Http\Livewire\DashboardGaruda;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InitialTest extends TestCase
{
    /** @test  */
    function dashboard_garudaku_contains_livewire_component()
    {
        $this->actingAs(User::factory()->create());
        $this->get('/garudaku')->assertSeeLivewire(DashboardGaruda::class);
    }
}
