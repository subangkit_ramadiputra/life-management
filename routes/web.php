<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', \App\Http\Livewire\Welcome::class);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    /*Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');*/

    Route::get('/garudaku', \App\Http\Livewire\DashboardGaruda::class);
    Route::get('/dashboard', \App\Http\Livewire\DashboardLife::class);
    Route::get('/task-quadrant', \App\Http\Livewire\TaskQuadrant::class);
    Route::get('/concepts', \App\Http\Livewire\Concepts::class);
    Route::get('/concept/{concept_id}', \App\Http\Livewire\ConceptDetail::class);
    Route::get('/format/{format_id}/run', \App\Http\Livewire\FormatRun::class);
    Route::get('/sop/{sop_id}/editor', \App\Http\Livewire\SopEditor::class);
    Route::get('/tags', \App\Http\Livewire\Tags::class);
    Route::get('/uploads', \App\Http\Livewire\Uploads::class);

    // Infrastructure Management
    Route::get('/servers', \App\Http\Livewire\Servers::class);

    // Application Management
    Route::get('/applications', \App\Http\Livewire\Applications::class);
    Route::get('/features', \App\Http\Livewire\Features::class);
    Route::get('/entities', \App\Http\Livewire\Entities::class);
    Route::get('/attributes', \App\Http\Livewire\Attributes::class);

    // Data Management
    Route::get('/etls', \App\Http\Livewire\Etls::class);

    // Vendor Management
    Route::get('/vendor_categories', \App\Http\Livewire\VendorCategories::class);
    Route::get('/vendors', \App\Http\Livewire\Vendors::class);
    Route::get('/vendor_members', \App\Http\Livewire\VendorMembers::class);

    // Navigation Management
    Route::get('/navigation_groups', \App\Http\Livewire\NavigationGroups::class);
    Route::get('/navigations', \App\Http\Livewire\Navigations::class);

    // Auth Management
    Route::get('/users', \App\Http\Livewire\Users::class);
    Route::get('/teams', \App\Http\Livewire\Teams::class);
    Route::get('/team_invitations', \App\Http\Livewire\TeamInvitations::class);

    // Components
    Route::get('/com/dropdown', \App\Http\Livewire\ComponentDropdown::class);

    // Technologies Entity 91
    Route::get('/technologies', \App\Http\Livewire\Technologies::class);

    // App Techs Entity 92
    Route::get('/app_techs', \App\Http\Livewire\AppTechs::class);

    // Roles Entity 93
    Route::get('/roles', \App\Http\Livewire\Roles::class);

    // Permissions Entity 94
    Route::get('/permissions', \App\Http\Livewire\Permissions::class);

    // Role Permissions Entity 95
    Route::get('/role_permissions', \App\Http\Livewire\RolePermissions::class);

    // User Roles Entity 96
    Route::get('/user_roles', \App\Http\Livewire\UserRoles::class);

    // Entity Actions Entity 102
    Route::get('/entity_actions', \App\Http\Livewire\EntityActions::class);

    // Quadrant Entity 109
    Route::get('/quadrants', \App\Http\Livewire\Quadrants::class);

    // Tasks Entity 108
    Route::get('/tasks', \App\Http\Livewire\Tasks::class);

    // SOPs Entity 63
    Route::get('/sops', \App\Http\Livewire\Sops::class);

    // Notes Entity 110
    Route::get('/notes', \App\Http\Livewire\Notes::class);

    // Format Entity 111
    Route::get('/formats', \App\Http\Livewire\Formats::class);

    // Attributes Entity 112
    Route::get('/attributes', \App\Http\Livewire\Attributes::class);

    // BPMN Event Types Entity 114
    Route::get('/bpmn_event_types', \App\Http\Livewire\BpmnEventTypes::class);

    // BPMN Activity Types Entity 116
    Route::get('/bpmn_activity_types', \App\Http\Livewire\BpmnActivityTypes::class);

    // BPMN Gateway Types Entity 118
    Route::get('/bpmn_gateway_types', \App\Http\Livewire\BpmnGatewayTypes::class);

    // BPMN Roles Entity 113
    Route::get('/bpmn_roles', \App\Http\Livewire\BpmnRoles::class);

    // BPMN Events Entity 115
    Route::get('/bpmn_events', \App\Http\Livewire\BpmnEvents::class);

    // BPMN Activities Entity 117
    Route::get('/bpmn_activities', \App\Http\Livewire\BpmnActivities::class);

    // BPMN Gateways Entity 119
    Route::get('/bpmn_gateways', \App\Http\Livewire\BpmnGateways::class);

    // BPMN Squences Entity 120
    Route::get('/bpmn_squences', \App\Http\Livewire\BpmnSquences::class);

    // User Access Entity 122
    Route::get('/user_accesses', \App\Http\Livewire\UserAccesses::class);

    // Database Type Entity 124
    Route::get('/database_types', \App\Http\Livewire\DatabaseTypes::class);

    // Database Server Entity 123
    Route::get('/database_servers', \App\Http\Livewire\DatabaseServers::class);

    // Hostable Entity 125
    Route::get('/hostables', \App\Http\Livewire\Hostables::class);

    // Clusters Entity 15
    Route::get('/clusters', \App\Http\Livewire\Clusters::class);

    // Links Entity 62
    Route::get('/links', \App\Http\Livewire\Links::class);

    // Application Navigation Group Entity 136
    Route::get('/application_navigation_groups', \App\Http\Livewire\ApplicationNavigationGroups::class);

    // Application Navigation Entity 126
    Route::get('/application_navigations', \App\Http\Livewire\ApplicationNavigations::class);

    // Application Active Font Entity 127
    Route::get('/application_active_fonts', \App\Http\Livewire\ApplicationActiveFonts::class);

    // Application Styles Entity 128
    Route::get('/application_styles', \App\Http\Livewire\ApplicationStyles::class);

    // Color Palette Entity 129
    Route::get('/color_palettes', \App\Http\Livewire\ColorPalettes::class);

    // Colors Entity 137
    Route::get('/colors', \App\Http\Livewire\Colors::class);

    // Audiences Entity 130
    Route::get('/audiences', \App\Http\Livewire\Audiences::class);

    // Application Value Entity 131
    Route::get('/application_values', \App\Http\Livewire\ApplicationValues::class);

    // Application Goal Entity 132
    Route::get('/application_goals', \App\Http\Livewire\ApplicationGoals::class);

    // Application Contents Entity 133
    Route::get('/application_contents', \App\Http\Livewire\ApplicationContents::class);

    // Application Components Entity 134
    Route::get('/application_components', \App\Http\Livewire\ApplicationComponents::class);

    // Application API Entity 135
    Route::get('/application_apis', \App\Http\Livewire\ApplicationApis::class);

    // Application Concept Entity 138
    Route::get('/application_concepts', \App\Http\Livewire\ApplicationConcepts::class);

    // Generic Context Entity 139
    Route::get('/generic_contexts', \App\Http\Livewire\GenericContexts::class);

    // Framework Entity 140
    Route::get('/frameworks', \App\Http\Livewire\Frameworks::class);

    // Field Format Entity 141
    Route::get('/field_formats', \App\Http\Livewire\FieldFormats::class);

    // Context Definition Entity 143
    Route::get('/context_definitions', \App\Http\Livewire\ContextDefinitions::class);

    // Context Generation Entity 144
    Route::get('/context_generations', \App\Http\Livewire\ContextGenerations::class);

    // Row Formatter Entity 145
    Route::get('/row_formatters', \App\Http\Livewire\RowFormatters::class);

    // Files Entity 146
    Route::get('/files', \App\Http\Livewire\Files::class);

    // Context Monitoring Entity 147
    Route::get('/context_monitorings', \App\Http\Livewire\ContextMonitorings::class);

    // Minute of Meeting Entity 149
    Route::get('/minute_of_meetings', \App\Http\Livewire\MinuteOfMeetings::class);

    // Key Performance Index Entity 150
    Route::get('/key_performance_indices', \App\Http\Livewire\KeyPerformanceIndices::class);

    // Stub Formatter Entity 151
    Route::get('/stub_formatters', \App\Http\Livewire\StubFormatters::class);

// ANCHOR ROUTE, Don't delete this line
});


