<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bpmn_squences', function (Blueprint $table) {
            $table->id();
            $table->string('code',200);
            $table->morphs('source');
            $table->morphs('destination');
            $table->text('summary')->nullable();
            $table->morphs('sequenceable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bpmn_squences');
    }
};
