<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('context_generations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('context_definition_id');
            $table->string('scope_generation',100)->default('body');
            $table->bigInteger('framework_id');
            $table->text('template');
            $table->text('custom_data');
            $table->string('mode',100)->default('javascript');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('context_generations');
    }
};
