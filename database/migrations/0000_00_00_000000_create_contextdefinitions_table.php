<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('context_definitions', function (Blueprint $table) {
            $table->id();
            $table->string('context_name',100)->default('Component');
            $table->text('requirement')->nullable();
            $table->text('main_template');
            $table->bigInteger('framework_id');
            $table->string('mode',100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('context_definitions');
    }
};
