<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('row_formatters', function (Blueprint $table) {
            $table->id();
            $table->string('title',200);
            $table->text('summary')->nullable();
            $table->text('query');
            $table->text('row_template');
            $table->string('connection',100)->default('mysql');
            $table->text('parameters');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('row_formatters');
    }
};
