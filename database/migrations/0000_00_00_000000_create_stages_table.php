<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stages', function (Blueprint $table) {
            $table->id();
            $table->string('title',100);
            $table->text('summary')->nullable();
            $table->string('body_format',100)->default('markdown');
            $table->text('body');
            $table->bigInteger('server_id');
            $table->text('path');
            $table->string('git_branch',100)->nullable();
            $table->string('cicd_user_access',100)->nullable();
            $table->string('stage',100)->default('development');
            $table->string('cicd_format',100)->default('lumen');
            $table->morphs('stageable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stages');
    }
};
