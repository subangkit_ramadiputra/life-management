<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entity_actions', function (Blueprint $table) {
            $table->id();
            $table->string('title',100);
            $table->text('summary')->nullable();
            $table->string('body_format',100)->default('markdown');
            $table->text('body');
            $table->morphs('actionable');
            $table->text('parameters')->default('username|path');
            $table->string('method',10)->default('POST');
            $table->string('tags',100);
            $table->text('responses')->default('200|Success,400|Has Error');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entity_actions');
    }
};
