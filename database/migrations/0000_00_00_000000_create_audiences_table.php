<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('audiences', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->text('summary')->nullable();
            $table->text('detail');
            $table->integer('min_age')->nullable()->default('0');
            $table->integer('max_age')->default('100');
            $table->string('priority',20)->default('Normal');
            $table->text('highlight_words');
            $table->bigInteger('application_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('audiences');
    }
};
