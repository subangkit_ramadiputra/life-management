<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bpmn_events', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('type_id');
            $table->string('code',100);
            $table->string('name',200);
            $table->text('summary')->nullable();
            $table->morphs('eventable');
            $table->bigInteger('role_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bpmn_events');
    }
};
