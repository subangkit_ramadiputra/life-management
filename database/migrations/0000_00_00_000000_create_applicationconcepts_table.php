<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_concepts', function (Blueprint $table) {
            $table->id();
            $table->string('name',200);
            $table->text('summary');
            $table->morphs('conceptable');
            $table->text('project_path')->nullable();
            $table->bigInteger('framework')->nullable();
            $table->text('config')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_concepts');
    }
};
