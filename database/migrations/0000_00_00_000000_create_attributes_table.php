<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->id();
            $table->string('field_name',200);
            $table->smallInteger('nullable')->default('0');
            $table->string('title',200);
            $table->string('type',100)->default('string');
            $table->string('length',100);
            $table->integer('precision')->default('0');
            $table->integer('scale')->default('0');
            $table->integer('digits')->default('0');
            $table->integer('decimal')->default('0');
            $table->string('default',200);
            $table->smallInteger('view_table')->default('1');
            $table->smallInteger('view_create')->default('1');
            $table->morphs('attributable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
};
