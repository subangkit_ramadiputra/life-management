<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concepts', function (Blueprint $table) {
            $table->id();
            $table->string('title',100);
            $table->text('summary')->nullable();
            $table->longText('body')->nullable();
            $table->string('body_format',100)->default('markdown');
            $table->string('source_url',1000)->nullable();
            $table->bigInteger('owner_id');
            $table->integer('weight')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concepts');
    }
};
