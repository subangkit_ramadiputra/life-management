<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostables', function (Blueprint $table) {
            $table->id();
            $table->morphs('hostable');
            $table->string('host',250);
            $table->string('port',10)->nullable();
            $table->string('instance',100)->nullable();
            $table->string('environment',100)->default('Production');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostables');
    }
};
