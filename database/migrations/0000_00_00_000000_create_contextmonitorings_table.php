<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('context_monitorings', function (Blueprint $table) {
            $table->id();
            $table->string('title',250);
            $table->text('summary');
            $table->decimal('progress')->default('0.0');
            $table->text('strategy')->nullable();
            $table->bigInteger('parent_id')->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('context_monitorings');
    }
};
