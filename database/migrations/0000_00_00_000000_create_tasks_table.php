<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('quadrant_id');
            $table->string('title',200);
            $table->text('summary');
            $table->text('body');
            $table->string('body_format',100)->default('markdown');
            $table->bigInteger('assignee_id')->nullable();
            $table->string('status',100)->default('todo');
            $table->dateTime('due_date');
            $table->morphs('taskable');
            $table->bigInteger('parent_id')->nullable()->default('0');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
};
