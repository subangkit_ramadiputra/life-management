<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_components', function (Blueprint $table) {
            $table->id();
            $table->integer('column_num')->default('1');
            $table->integer('row_num')->default('1');
            $table->text('cell_composition');
            $table->morphs('componentable');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_components');
    }
};
