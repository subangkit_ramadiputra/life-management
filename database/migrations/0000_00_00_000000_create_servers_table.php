<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->id();
            $table->string('stage',100)->default('production');
            $table->string('title',150);
            $table->string('ip_address',100)->nullable();
            $table->string('provider',100);
            $table->text('summary')->nullable();
            $table->text('body')->nullable();
            $table->string('body_format',100)->default('markdown');
            $table->string('user_access',100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
};
