<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('etls', function (Blueprint $table) {
            $table->id();
            $table->string('title',100);
            $table->text('summary')->nullable();
            $table->string('schedule',200);
            $table->string('request_by',200);
            $table->text('source_data')->nullable();
            $table->text('transform_summary')->nullable();
            $table->text('target_data')->nullable();
            $table->text('consumed_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('etls');
    }
};
