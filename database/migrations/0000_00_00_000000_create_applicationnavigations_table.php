<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_navigations', function (Blueprint $table) {
            $table->id();
            $table->string('name',40);
            $table->string('title_format',100);
            $table->string('seo_url_format',255);
            $table->string('page_type',100)->default('page');
            $table->bigInteger('navigation_group_id');
            $table->string('navigation_back_schema',100)->default('stack');
            $table->string('reset_navigation_back',100);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_navigations');
    }
};
