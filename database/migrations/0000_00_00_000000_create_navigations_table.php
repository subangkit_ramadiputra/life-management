<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('navigations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('vendor_id')->nullable();
            $table->string('group',200)->default('Main');
            $table->string('title',200);
            $table->string('controller',300)->default('\App\Http\Livewire\Pages');
            $table->string('function',150)->nullable();
            $table->string('route',200);
            $table->bigInteger('parent_id')->default('0');
            $table->tinyInteger('is_show')->default('1');
            $table->tinyInteger('is_enable')->default('1');
            $table->bigInteger('application_id');
            $table->string('class',100)->nullable();
            $table->text('svg')->nullable();
            $table->integer('weight')->nullable();
            $table->string('nav_sort',100)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('navigations');
    }
};
