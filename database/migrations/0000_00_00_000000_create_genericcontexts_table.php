<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generic_contexts', function (Blueprint $table) {
            $table->id();
            $table->string('group',200);
            $table->string('context_name',200);
            $table->text('definition');
            $table->text('goals');
            $table->text('inputs')->nullable();
            $table->text('suppliers')->nullable();
            $table->text('activities')->nullable();
            $table->text('participants')->nullable();
            $table->text('deliverables')->nullable();
            $table->text('consumers')->nullable();
            $table->text('techniques')->nullable();
            $table->text('tools')->nullable();
            $table->text('metrics')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generic_contexts');
    }
};
