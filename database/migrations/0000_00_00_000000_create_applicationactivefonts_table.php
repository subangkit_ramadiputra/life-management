<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('application_active_fonts', function (Blueprint $table) {
            $table->id();
            $table->string('name',100);
            $table->text('purpose')->nullable();
            $table->string('type',100)->default('main');
            $table->text('font_url')->nullable();
            $table->bigInteger('application_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_active_fonts');
    }
};
