<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body',
        'body_format',
        'entitiable_type',
        'entitiable_id',

    ];

    /**
     * Get all of the owning uploadable models.
     */
    public function entitiable()
    {
        return $this->morphTo();
    }

    /**
     * Get all of the entities files.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    /**
     * Get all of the entities places.
     */
    public function places()
    {
        return $this->morphMany(Place::class, 'placeable');
    }

    /**
     * Get all of the entities transitions.
     */
    public function transitions()
    {
        return $this->morphMany(Transition::class, 'transitionable');
    }

    /**
     * Get all of the entities attributes.
     */
    public function attributes()
    {
        return $this->morphMany(Attribute::class, 'attributable');
    }

    /**
     * Get all of the entity actions.
     */
    public function entity_actions()
    {
        return $this->morphMany(EntityAction::class, 'actionable');
    }

    /**
     * Get all of the entity permissions.
     */
    public function permissions()
    {
        return $this->morphMany(Permission::class, 'permitable');
    }

    /**
     * Get all of the entity sops.
     */
    public function sops()
    {
        return $this->morphMany(Sop::class, 'sopable');
    }
}
