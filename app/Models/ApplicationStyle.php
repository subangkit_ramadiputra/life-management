<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationStyle extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'application_id',

    ];
}
