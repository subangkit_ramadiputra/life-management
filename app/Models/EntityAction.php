<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntityAction extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body_format',
        'body',
        'actionable',
        'parameters',
        'method',
        'tags',
        'responses',

    ];
}
