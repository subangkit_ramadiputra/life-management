<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Concept extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body',
        'body_format',
        'source_url',
        'owner_id',
        'weight',

    ];

    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Get all of the concept's files.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    /**
     * Get the applicatoins for the concept.
     */
    public function applications()
    {
        return $this->hasMany(Application::class,'concept_id');
    }

    /**
     * Get all of the concepts links.
     */
    public function links()
    {
        return $this->morphMany(Link::class, 'linkable');
    }

    /**
     * Get all of the concept tasks.
     */
    public function tasks()
    {
        return $this->morphMany(Task::class, 'taskable');
    }

    // taskable Section
    public function getTaskableNameAttribute() {
        return "Concept :: ".$this->getAttribute('title');
    }

    public function getTaskableUrlAttribute() {
        return url('concepts');
    }

    /**
     * Get all of the concept notes.
     */
    public function notes()
    {
        return $this->morphMany(Note::class, 'noteable');
    }

    /**
     * Get all of the concept sops.
     */
    public function sops()
    {
        return $this->morphMany(Sop::class, 'sopable');
    }

    /**
     * Get all of the concepts user_accesses.
     */
    public function user_accesses()
    {
        return $this->morphMany(UserAccess::class, 'accessable');
    }

    /**
     * Get all of the concept clusters.
     */
    public function clusters()
    {
        return $this->morphMany(Cluster::class, 'clusterable');
    }

    /**
     * Get all of the parents files.
     */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
