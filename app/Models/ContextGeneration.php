<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContextGeneration extends Model
{
    use HasFactory;

    protected $fillable = [
        'context_definition_id',
        'scope_generation',
        'framework_id',
        'template',
        'custom_data',
        'mode',

    ];

    /**
     * Get the application for the concept.
     */
    public function context_definition()
    {
        return $this->belongsTo(ContextDefinition::class,'context_definition_id');
    }
}
