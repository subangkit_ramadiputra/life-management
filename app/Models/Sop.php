<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sop extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body_format',
        'body',
        'sopable_type',
        'sopable_id',
    ];

    /**
     * Get all of the parents bpmn_roles.
     */
    public function bpmn_roles()
    {
        return $this->morphMany(BpmnRole::class, 'roleable');
    }
    /**
     * Get all of the parents bpmn_events.
     */
    public function bpmn_events()
    {
        return $this->morphMany(BpmnEvent::class, 'eventable');
    }
    /**
     * Get all of the parents bpmn_activities.
     */
    public function bpmn_activities()
    {
        return $this->morphMany(BpmnActivity::class, 'activitiable');
    }
    /**
     * Get all of the parents bpmn_gateways.
     */
    public function bpmn_gateways()
    {
        return $this->morphMany(BpmnGateway::class, 'gatewayable');
    }
    /**
     * Get all of the parents bpmn_squences.
     */
    public function bpmn_squences()
    {
        return $this->morphMany(BpmnSquence::class, 'sequenceable');
    }
}
