<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_id',
        'group',
        'title',
        'controller',
        'function',
        'route',
        'parent_id',
        'is_show',
        'is_enable',
        'application_id',
        'class',
        'svg',
        'weight',
        'nav_sort',

    ];
}
