<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RowFormatter extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'query',
        'row_template',
        'connection',
        'parameters',

    ];
}
