<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationGoal extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'detail',
        'application_id',

    ];
}
