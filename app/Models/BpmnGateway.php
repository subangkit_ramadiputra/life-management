<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BpmnGateway extends Model
{
    use HasFactory;

    protected $fillable = [
        'type_id',
        'code',
        'name',
        'summary',
        'gatewayable',
        'role_id',
        'default_squence_id',

    ];

    public function type()
    {
        return $this->belongsTo(BpmnGatewayType::class,'type_id');
    }

    public function role()
    {
        return $this->belongsTo(BpmnRole::class,'role_id');
    }

    public function default_squence()
    {
        return $this->belongsTo(BpmnSquence::class,'default_squence_id');
    }
}
