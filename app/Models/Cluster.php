<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cluster extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'summary',
        'body_format',
        'body',
        'clusterable',

    ];

    /**
     * Get all of the parents database_servers.
     */
    public function database_servers()
    {
        return $this->morphMany(DatabaseServer::class, 'databaseable');
    }
}
