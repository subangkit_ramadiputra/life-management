<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContextDefinition extends Model
{
    use HasFactory;

    protected $fillable = [
        'context_name',
        'requirement',
        'main_template',
        'framework_id',
        'mode',

    ];

    /**
     * Get the framework
     */
    public function framework()
    {
        return $this->belongsTo(Framework::class,'framework_id');
    }
}
