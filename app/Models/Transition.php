<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transition extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body',
        'body_format',
        'from_place_id',
        'to_place_id',

    ];

    /**
     * Get the place that owns the transition.
     */
    public function from_place()
    {
        return $this->belongsTo(Place::class,'from_place_id');
    }

    /**
     * Get the place that owns the transition.
     */
    public function to_place()
    {
        return $this->belongsTo(Place::class,'to_place_id');
    }

    /**
     * Get the place that owns the transition.
     */
    public function from_entity()
    {
        return $this->belongsTo(Entity::class,'from_place_id');
    }

    /**
     * Get the place that owns the transition.
     */
    public function to_entity()
    {
        return $this->belongsTo(Entity::class,'to_place_id');
    }
}
