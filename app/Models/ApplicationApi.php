<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationApi extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'method',
        'url_format',
        'query_string_sample',
        'post_sample',
        'application_id',

    ];
}
