<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'summary',
        'concept_id',
        'programming_language',
    ];

    // featureable Section
    public function getFeatureableNameAttribute() {
        return (($this->concept) ? $this->concept->title : '').' :: '."Application :: ".$this->getAttribute('name');
    }

    public function getFeatureableUrlAttribute() {
        return url('applications');
    }
    // End of Section

    // techable Section
    public function getTechableNameAttribute() {
        return "Application :: ".$this->getAttribute('name');
    }

    public function getTechableUrlAttribute() {
        return url('applications').'?'.\Illuminate\Support\Arr::query([
            'filter_concept_id' => $this->concept_id
        ]);
    }
    // End of Section

    /**
     * Get the concept that owns the application.
     */
    public function concept()
    {
        return $this->belongsTo(Concept::class);
    }

    /**
     * Get all of the application's features.
     */
    public function features()
    {
        return $this->morphMany(Feature::class, 'featureable');
    }

    /**
     * Get all of the applications stages.
     */
    public function stages()
    {
        return $this->morphMany(Stage::class, 'stageable');
    }

    /**
     * Get all of the application roles.
     */
    public function roles()
    {
        return $this->morphMany(Role::class, 'roleable');
    }

    /**
     * Get all of the concept tasks.
     */
    public function tasks()
    {
        return $this->morphMany(Task::class, 'taskable');
    }

    // taskable Section
    public function getTaskableNameAttribute() {
        return "Application :: ".$this->getAttribute('title');
    }

    public function getTaskableUrlAttribute() {
        return url('applications');
    }

    /**
     * Get all of the application app_techs.
     */
    public function app_techs()
    {
        return $this->morphMany(AppTech::class, 'techable');
    }

    /**
     * Get all of the application links.
     */
    public function links()
    {
        return $this->morphMany(Link::class, 'linkable');
    }

    /**
     * Get all of the application notes.
     */
    public function notes()
    {
        return $this->morphMany(Note::class, 'noteable');
    }

    /**
     * Get all of the parents application_concepts.
     */
    public function application_concepts()
    {
        return $this->morphMany(ApplicationConcept::class, 'conceptable');
    }

    // Object Section
    public function getConceptableNameAttribute() {
        return "Application :: ".$this->getAttribute('name');
    }

    public function getConceptableUrlAttribute() {
        return url('applications');
    }

}
