<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BpmnActivity extends Model
{
    use HasFactory;

    protected $fillable = [
        'type_id',
        'code',
        'name',
        'summary',
        'activitiable',
        'role_id',
    ];

    public function type()
    {
        return $this->belongsTo(BpmnActivityType::class,'type_id');
    }

    public function role()
    {
        return $this->belongsTo(BpmnRole::class,'role_id');
    }
}
