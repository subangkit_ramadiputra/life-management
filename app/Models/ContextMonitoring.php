<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContextMonitoring extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'progress',
        'strategy',
        'parent_id',

    ];
}
