<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeyPerformanceIndex extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'indicator',
        'target',

    ];
}
