<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationNavigationGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'Summary',
        'direction',
        'style',
        'application_id',

    ];
}
