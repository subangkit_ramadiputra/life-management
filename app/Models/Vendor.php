<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'short_name',
        'address',
        'short_address',
        'phone',
        'wa',
        'category_id',

    ];

    /**
     * Get the category that owns the vendor.
     */
    public function category()
    {
        return $this->belongsTo(VendorCategory::class);
    }
}
