<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GenericContext extends Model
{
    use HasFactory;

    protected $fillable = [
        'group',
        'context_name',
        'definition',
        'goals',
        'inputs',
        'suppliers',
        'activities',
        'participants',
        'deliverables',
        'consumers',
        'techniques',
        'tools',
        'metrics',

    ];
}
