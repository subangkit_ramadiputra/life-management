<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationComponent extends Model
{
    use HasFactory;

    protected $fillable = [
        'column_num',
        'row_num',
        'cell_composition',
        'componentable',

    ];
}
