<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body_format',
        'body',
        'server_id',
        'path',
        'git_branch',
        'cicd_user_access',
        'cicd_private_key',
        'stage',
        'cicd_format',
        'stageable',

    ];

    /**
     * Get the server that owns the stage.
     */
    public function server()
    {
        return $this->belongsTo(Server::class);
    }
}
