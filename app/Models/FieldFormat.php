<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FieldFormat extends Model
{
    use HasFactory;

    protected $fillable = [
        'format_name',
        'summary',
        'input_requirement',

    ];
}
