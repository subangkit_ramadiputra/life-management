<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatabaseServer extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'type_id',
        'databaseable',

    ];

    /**
     * Get the type for the database server.
     */
    public function type()
    {
        return $this->belongsTo(DatabaseType::class,'type_id');
    }

    /**
     * Get all of the database server hostables.
     */
    public function hostables()
    {
        return $this->morphMany(Hostable::class, 'hostable');
    }
}
