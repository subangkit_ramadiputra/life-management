<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AppTech extends Model
{
    use HasFactory;

    protected $table = 'app_techs';

    protected $fillable = [
        'title',
        'summary',
        'body_format',
        'body',
        'techable',
        'technology_id',
        'local_path',
        'virtual_host',

    ];

    /**
     * Get all of the owning techable models.
     */
    public function techable()
    {
        return $this->morphTo();
    }
}
