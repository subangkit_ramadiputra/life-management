<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'field_name',
        'nullable',
        'title',
        'type',
        'length',
        'precision',
        'scale',
        'digits',
        'decimal',
        'default',
        'view_table',
        'view_create',

    ];
}
