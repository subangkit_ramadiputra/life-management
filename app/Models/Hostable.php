<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hostable extends Model
{
    use HasFactory;

    protected $fillable = [
        'hostable',
        'host',
        'port',
        'instance',
        'environment',

    ];
}
