<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    use HasFactory;

    protected $fillable = [
        'stage',
        'title',
        'ip_address',
        'provider',
        'summary',
        'body',
        'body_format',
        'user_access',

    ];
}
