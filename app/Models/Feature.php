<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'body',
        'body_format',
        'featureable_type',
        'featureable_id',
    ];

    // entitiable Section
    public function getEntitiableNameAttribute() {
        return "Feature :: ".$this->getAttribute('title');
    }

    public function getEntitiableUrlAttribute() {
        return url('features');
    }
    // End of Section

    /**
     * Get all of the owning uploadable models.
     */
    public function featureable()
    {
        return $this->morphTo();
    }

    public function application() {
        $this->featureable();
    }

    /**
     * Get all of the feature's files.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    /**
     * Get all of the features entities.
     */
    public function entities()
    {
        return $this->morphMany(Entity::class, 'entitiable');
    }

    /**
     * Get all of the features transitions.
     */
    public function transitions()
    {
        return $this->morphMany(Transition::class, 'transitionable');
    }

    /**
     * Get all of the parents sops.
     */
    public function sops()
    {
        return $this->morphMany(Sop::class, 'sopable');
    }

    /**
     * Get all of the features links.
     */
    public function links()
    {
        return $this->morphMany(Link::class, 'linkable');
    }
}
