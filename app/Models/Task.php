<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'quadrant_id',
        'title',
        'summary',
        'body',
        'body_format',
        'assignee_id',
        'status',
        'due_date',
        'taskable',
        'parent_id',

    ];

    /**
     * Get all of the owning uploadable models.
     */
    public function taskable()
    {
        return $this->morphTo();
    }

    /**
     * Get the quadrant that owns the task.
     */
    public function quadrant()
    {
        return $this->belongsTo(Quadrant::class);
    }

    /**
     * Get the quadrant that owns the task.
     */
    public function parent()
    {
        return $this->belongsTo(Task::class,'parent_id');
    }
}
