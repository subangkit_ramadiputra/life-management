<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationConcept extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'summary',
        'conceptable',
        'project_path',
        'framework',
        'config',

    ];

    /**
     * Get all of the owning uploadable models.
     */
    public function conceptable()
    {
        return $this->morphTo();
    }


    /**
     * Get the applications for the concept.
     */
    public function application_navigation_groups()
    {
        return $this->hasMany(ApplicationNavigationGroup::class,'application_id');
    }
}
