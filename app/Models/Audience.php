<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Audience extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'summary',
        'detail',
        'min_age',
        'max_age',
        'priority',
        'highlight_words',
        'application_id',

    ];
}
