<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicationNavigation extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'title_format',
        'seo_url_format',
        'page_type',
        'navigation_group_id',
        'navigation_back_schema',
        'reset_navigation_back',

    ];
}
