<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorMember extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_id',
        'user_id',
        'status',
        'is_banned',
        'role',

    ];

    /**
     * Get the vendor that owns the member.
     */
    public function vendor()
    {
        return $this->belongsTo(Vendor::class);
    }

    /**
     * Get the user that owns the member.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
