<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BpmnGatewayType extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'name',

    ];
}
