<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NavigationGroup extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'summary',
        'weight',

    ];
}
