<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etl extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'schedule',
        'request_by',
        'source_data',
        'transform_summary',
        'target_data',
        'consumed_by',

    ];

    /**
     * Get all of the parent's files.
     */
    public function uploads()
    {
        return $this->morphMany(Upload::class, 'uploadable');
    }

    /*
    * Get all of the etl notes.
    */
    public function notes()
    {
        return $this->morphMany(Note::class, 'noteable');
    }
}
