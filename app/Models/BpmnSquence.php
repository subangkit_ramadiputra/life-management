<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BpmnSquence extends Model
{
    use HasFactory;

    protected $fillable = [
        'code',
        'source',
        'destination',
        'summary',
        'sequenceable',
    ];

    public function source()
    {
        return $this->morphTo();
    }

    public function destination()
    {
        return $this->morphTo();
    }
}
