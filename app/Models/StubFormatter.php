<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StubFormatter extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'summary',
        'stub',
        'mode',

    ];
}
