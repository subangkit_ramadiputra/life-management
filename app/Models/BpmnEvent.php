<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BpmnEvent extends Model
{
    use HasFactory;

    protected $fillable = [
        'type_id',
        'code',
        'name',
        'summary',
        'eventable',
        'role_id',

    ];

    public function type()
    {
        return $this->belongsTo(BpmnEventType::class,'type_id');
    }

    public function role()
    {
        return $this->belongsTo(BpmnRole::class,'role_id');
    }
}
