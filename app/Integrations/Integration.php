<?php


namespace App\Integrations;


class Integration
{
    public $object;
    public $name;
    public function __construct($object)
    {
        $this->object = $object;
        $this->name = $object['name'];
    }
}
