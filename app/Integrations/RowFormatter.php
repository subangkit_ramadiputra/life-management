<?php


namespace App\Integrations;


use App\Traits\GenerateTraits;
use Illuminate\Support\Facades\DB;
use function SpomkyLabs\Pki\X509\CertificationPath\Policy\parent;

class RowFormatter extends Integration
{
    use GenerateTraits;

    public function __construct($object)
    {
        parent::__construct($object);
    }

    public function isValidRowFormatterObject($object) {
        if (!isset($object['formatter_name']))
            return false;

        return true;
    }

    public function getRowFormatterName($object) {
        return $object['formatter_name'];
    }
    public function getRowFormatterObject($formatter_name) {
        return \App\Models\RowFormatter::where('name', $formatter_name)->first();
    }

    public function getOutput() {
        if (!$this->isValidRowFormatterObject($this->object))
            return '';

        $formatter = $this->getRowFormatterObject($this->getRowFormatterName($this->object));
        // TODO Get Component Attribute
        $query_generate = $formatter->query;
        $rows = DB::connection($formatter->connection)->select(DB::raw($query_generate));
        $results = '';
        foreach ($rows as $row) {
            $results .= $this->getStubContents($formatter->template, (array) $row)."\n";
        }
        return $results;
    }
}
