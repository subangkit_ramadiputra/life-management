<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function garudaku(Request $request) {
        $player_stats = DB::connection('garudaku')->select(DB::raw("SELECT
               COUNT(1) TOTAL_ROW_PLAYER,
               SUM(CASE WHEN verified = 1 THEN 1 ELSE 0 END) VERIFIED,
               SUM(CASE WHEN username IS NOT NULL THEN 1 ELSE 0 END) ADDPROFILE,
               SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END) MALE,
               SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END) FEMALE,
               SUM(CASE WHEN gender IS NULL THEN 1 ELSE 0 END) JKNULL,
               SUM(otp_attemp) TOTAL_SMS_SENT
        FROM players"));

        var_dump($player_stats); exit;

        return view('dashboard.garudaku', compact('player_stats'));
    }
}
