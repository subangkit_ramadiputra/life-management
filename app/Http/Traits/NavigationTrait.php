<?php


namespace App\Http\Traits;


use App\Models\Navigation;
use App\Models\NavigationGroup;
use App\Traits\CacheNavigationTrait;

trait NavigationTrait
{
    use CacheNavigationTrait;

    public $view_navigations = [];
    protected function getNavigation($parent_id = 0, $depth=2) {
        // Use Global Navigation
        if (env('APP_DEBUG')) {
            $this->forgetNavigationCache();
        }
        $this->view_navigations = $this->rememberNavigationCache(function() use ($parent_id, $depth) {
            return $this->getGroupNavigation($parent_id, $depth);
        });
    }

    protected function getGroupNavigation($parent_id = 0, $depth, $group_ids = []) {
        if (count($group_ids) == 0) {
            $groups = NavigationGroup::orderBy('weight')->get()->toArray();
        } else {
            $groups = NavigationGroup::whereIn('id', $group_ids)->orderBy('weight')->get()->toArray();
        }
        foreach ($groups as $index => $group) {
            $groups[$index]['navigations'] = $this->getStructuredNavigation($group['id'],$parent_id, $depth);
        }

        return $groups;
    }

    protected function getStructuredNavigation($group_id, $parent_id, $depth) {
        if ($depth > 0) {
            $navs = Navigation::where('group', $group_id)->where('parent_id',$parent_id)->orderBy('nav_sort','asc')->get()->toArray();
            if ($navs != null) {
                foreach ($navs as $index => $nav) {
                    $navs[$index]['children'] = $this->getStructuredNavigation($group_id, $nav['id'], $depth - 1);
                }
                return $navs;
            } else {
                return [];
            }
        }

        return [];
    }

    protected function getNavigationCode($navigation_id, $new_parent_id = '') {
        $navigation = Navigation::find($navigation_id);
        $weight = (int) $navigation->weight;
        if ($navigation) {
            do {
                if ($navigation->parent_id !== 0) {
                    $navigation = Navigation::find($navigation->parent_id);
                    $weight += (int) $navigation->weight;
                } elseif ($new_parent_id != '') {
                    $navigation = Navigation::find($new_parent_id);
                    $weight += (int) $navigation->weight;
                    $new_parent_id = '';
                }
            } while($navigation->parent_id != 0);
        }

        return $weight;
    }
}
