<?php


namespace App\Http\Traits;


use Illuminate\Support\Pluralizer;

trait Generator
{
    public function getWording($entity) {
        return [
            'singular' => str_replace(' ','',$this->getSingularClassName($entity)),
            'singular_lowercase' => str_replace(' ','_',strtolower($this->getSingularClassName($entity))),
            'plural' => str_replace(' ','',$this->getPluralClassName($entity)),
            'plural_lowercase' => str_replace(' ','_',strtolower($this->getPluralClassName($entity))),
        ];
    }

    /**
     * Return the Singular Capitalize Name
     * @param $name
     * @return string
     */
    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }

    /**
     * Return the Singular Capitalize Name
     * @param $name
     * @return string
     */
    public function getPluralClassName($name)
    {
        return ucwords(Pluralizer::plural($name));
    }
}
