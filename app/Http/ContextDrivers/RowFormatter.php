<?php

namespace App\Http\ContextDrivers;

use App\Traits\GenerateTraits;
use Illuminate\Support\Facades\DB;

class RowFormatter extends ContextDriver
{
    use GenerateTraits;
    public function handle($data) {
        $name = $data['formatter_name'];

        $formatter = $this->getRowFormatterObject($name);
        return $this->runRowFormatter($formatter, $data);
    }

    public function getQuery($formatter, $parameters) {
        $query = $formatter->query;
        foreach ($parameters as $parameter_name => $value) {
            if (!is_object($value))
                $query = str_replace('$$'.$parameter_name.'$$',"'".$value."'",$query);
        }
        return $query;
    }

    public function runRowFormatter($formatter,$data) {
        $requiredParameters = json_decode($formatter->parameters);
        foreach ($requiredParameters as $parameter_name => $default_value) {
            if (!isset($data[$parameter_name]))
                throw new \Exception($formatter->title.' require '.$parameter_name.' parameter');
        }

        $query = $this->getQuery($formatter, $data);
        return $this->runRowFormatterTemplate($formatter->connection, $query, $formatter->row_template);
    }

    public function getRowFormatterObject($name) {
        $formatter = \App\Models\RowFormatter::where('title',$name)->first();
        if ($formatter == null)
            throw new \Exception('RowFormatter not found');

        return $formatter;
    }
}
