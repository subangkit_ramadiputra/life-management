<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Color;

class Colors extends Component
{
    use AlertMessageTrait;

    public $colors, $color_id;
    public $name;
    public $hex_code;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->colors = Color::all();
        $this->total_rows = Color::count();
        return view('livewire.colors');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->color_id = '';
        $this->name = '';
        $this->hex_code = '#000000';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'hex_code' => 'required',

        ]);

        Color::updateOrCreate(['id' => $this->color_id], [
            'name' => $this->name,
            'hex_code' => $this->hex_code,

        ]);

        $this->alertSuccessMessage($this->color_id ? 'Color updated.' : 'Color created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $color = Color::findOrFail($id);
        $this->color_id = $id;
        $this->name = $color->name;
        $this->hex_code = $color->hex_code;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Color::find($id)->delete();
        $this->alertSuccessMessage('Color deleted.');
    }
}
