<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationApi;

class ApplicationApis extends Component
{
    use AlertMessageTrait;

    public $application_apis, $application_api_id;
    public $name;
    public $description;
    public $method;
    public $url_format;
    public $query_string_sample;
    public $post_sample;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_apis = ApplicationApi::all();
        $this->total_rows = ApplicationApi::count();
        return view('livewire.application_apis');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_api_id = '';
        $this->name = '';
        $this->description = '';
        $this->method = 'GET';
        $this->url_format = '';
        $this->query_string_sample = '';
        $this->post_sample = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'description' => 'required',
            'method' => 'required',
            'url_format' => 'required',
            'post_sample' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationApi::updateOrCreate(['id' => $this->application_api_id], [
            'name' => $this->name,
            'description' => $this->description,
            'method' => $this->method,
            'url_format' => $this->url_format,
            'query_string_sample' => $this->query_string_sample,
            'post_sample' => $this->post_sample,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_api_id ? 'ApplicationApi updated.' : 'ApplicationApi created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_api = ApplicationApi::findOrFail($id);
        $this->application_api_id = $id;
        $this->name = $application_api->name;
        $this->description = $application_api->description;
        $this->method = $application_api->method;
        $this->url_format = $application_api->url_format;
        $this->query_string_sample = $application_api->query_string_sample;
        $this->post_sample = $application_api->post_sample;
        $this->application_id = $application_api->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationApi::find($id)->delete();
        $this->alertSuccessMessage('ApplicationApi deleted.');
    }
}
