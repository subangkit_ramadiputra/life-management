<?php

namespace App\Http\Livewire;

use App\Models\Format;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;

class FormatRun extends Component
{
    public $format_id;
    public $format;
    public $values = [];

    public function mount($format_id)
    {
        $this->format = Format::findOrFail($format_id);
        $this->format_id = $format_id;
        $this->attributes = $this->format->attributes()->get();
        $this->setDefaultVariables();
    }

    public $attributes = [];
    public function render()
    {
        return view('livewire.format-run');
    }

    public function setDefaultVariables() {
        foreach ($this->attributes as $attribute) {
            $this->values[$attribute->field_name] = $attribute->default;
        }
    }

    public $output = '';
    public function run() {
        $file = new Filesystem;
        $file->cleanDirectory(Storage::disk('twig')->path('/cache'));

        $loader = new \Twig\Loader\FilesystemLoader(Storage::disk('twig')->path('/'));
        $twig = new \Twig\Environment($loader, [
            'cache' => Storage::disk('twig')->path('/cache'),
        ]);

        $template = $twig->load('format_'.$this->format_id.'.twig');
        $this->output = $template->render($this->values);
    }
}
