<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\StubFormatter;

class StubFormatters extends Component
{
    use AlertMessageTrait;

    public $stub_formatters, $stub_formatter_id;
    public $title;
    public $summary;
    public $stub;
    public $mode;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->stub_formatters = StubFormatter::all();
        $this->total_rows = StubFormatter::count();
        return view('livewire.stub_formatters');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->stub_formatter_id = '';
        $this->title = '';
        $this->summary = '';
        $this->stub = '';
        $this->mode = 'text';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'stub' => 'required',
            'mode' => 'required',

        ]);

        StubFormatter::updateOrCreate(['id' => $this->stub_formatter_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'stub' => $this->stub,
            'mode' => $this->mode,

        ]);

        $this->alertSuccessMessage($this->stub_formatter_id ? 'StubFormatter updated.' : 'StubFormatter created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $stub_formatter = StubFormatter::findOrFail($id);
        $this->stub_formatter_id = $id;
        $this->title = $stub_formatter->title;
        $this->summary = $stub_formatter->summary;
        $this->stub = $stub_formatter->stub;
        $this->mode = $stub_formatter->mode;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        StubFormatter::find($id)->delete();
        $this->alertSuccessMessage('StubFormatter deleted.');
    }
}
