<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationNavigationGroup;

class ApplicationNavigationGroups extends Component
{
    use AlertMessageTrait;

    public $application_navigation_groups, $application_navigation_group_id;
    public $name;
    public $Summary;
    public $direction;
    public $style;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_navigation_groups = ApplicationNavigationGroup::all();
        $this->total_rows = ApplicationNavigationGroup::count();
        return view('livewire.application_navigation_groups');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_navigation_group_id = '';
        $this->name = '';
        $this->Summary = '';
        $this->direction = 'Horizontal';
        $this->style = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'direction' => 'required',
            'style' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationNavigationGroup::updateOrCreate(['id' => $this->application_navigation_group_id], [
            'name' => $this->name,
            'Summary' => $this->Summary,
            'direction' => $this->direction,
            'style' => $this->style,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_navigation_group_id ? 'ApplicationNavigationGroup updated.' : 'ApplicationNavigationGroup created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_navigation_group = ApplicationNavigationGroup::findOrFail($id);
        $this->application_navigation_group_id = $id;
        $this->name = $application_navigation_group->name;
        $this->Summary = $application_navigation_group->Summary;
        $this->direction = $application_navigation_group->direction;
        $this->style = $application_navigation_group->style;
        $this->application_id = $application_navigation_group->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationNavigationGroup::find($id)->delete();
        $this->alertSuccessMessage('ApplicationNavigationGroup deleted.');
    }
}
