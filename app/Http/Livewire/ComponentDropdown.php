<?php

namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;

use Livewire\Component;

class ComponentDropdown extends Component
{
    use AlertMessageTrait;

    public function render()
    {
        return view('livewire.component-dropdown');
    }
}
