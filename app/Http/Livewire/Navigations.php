<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Http\Traits\NavigationTrait;
use App\Models\Application;
use App\Models\NavigationGroup;
use App\Models\Vendor;
use Livewire\Component;
use App\Models\Navigation;
use Storage;
use DB;

class Navigations extends Component
{
    use AlertMessageTrait;

    use NavigationTrait;

    public $navigations, $navigation_id;
    public $vendor_id;
    public $group;
    public $title;
    public $controller;
    public $function;
    public $route;
    public $parent_id;
    public $is_show;
    public $is_enable;
    public $application_id;
    public $class;
    public $svg;
    public $weight;
    public $nav_sort;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->navigations = Navigation::orderBy('nav_sort')->get();
        $this->total_rows = Navigation::count();

        $this->nav_parents = Navigation::all();
        $this->vendors = Vendor::all();
        $this->groups = NavigationGroup::all();
        $this->applications = Application::all();
        $this->controllers = $this->getListControllers();
        return view('livewire.navigations');
    }
    protected function getListControllers() {
        $files = Storage::disk('laravel')->files('app/Http/Livewire');
        foreach ($files as $index => $file) {
            $file = str_replace('.php','',$file);
            $file = str_replace('app/Http/Livewire/','',$file);
            $files[$index] = $file;
        }

        return $files;
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->navigation_id = '';
        $this->vendor_id = 0;
        $this->group = 0;
        $this->title = '';
        $this->controller = '\App\Http\Livewire\Pages';
        $this->function = '';
        $this->route = '';
        $this->parent_id = '0';
        $this->is_show = '1';
        $this->is_enable = '1';
        $this->application_id = '';
        $this->class = '';
        $this->svg = '';
        $this->weight = '0';
        $this->nav_sort = '0';

    }

    public function store()
    {
        $this->validate([
            'group' => 'required',
            'title' => 'required',
            'controller' => 'required',
            'route' => 'required',
            'parent_id' => 'required',
            'is_show' => 'required',
            'is_enable' => 'required',
            'application_id' => 'required',

        ]);

        if ($this->parent_id != $this->navigation_id) {

            Navigation::updateOrCreate(['id' => $this->navigation_id], [
                'vendor_id' => $this->vendor_id,
                'group' => $this->group,
                'title' => $this->title,
                'controller' => $this->controller,
                'function' => $this->function,
                'route' => $this->route,
                'parent_id' => $this->parent_id,
                'is_show' => $this->is_show,
                'is_enable' => $this->is_enable,
                'application_id' => $this->application_id,
                'class' => $this->class,
                'svg' => $this->svg,
                'weight' => $this->weight,
                'nav_sort' => $this->nav_sort,
            ]);

            if ($this->navigation_id == '') {
                $nav_id = DB::getPdo()->lastInsertId();
            } else {
                $nav_id = $this->navigation_id;
            }

            $nav = Navigation::find($nav_id);
            if ($nav) {
                $nav->nav_sort = $this->getNavigationCode($nav_id);
                $nav->save();
            }

            $this->alertSuccessMessage($this->navigation_id ? 'Navigation updated.' : 'Navigation created.');
            $this->closeModalPopover();
            $this->resetCreateForm();
        } else {
            $this->alertErrorMessage('Unable to update navigation ( Parent Self Reference).');
        }
    }
    public function edit($id)
    {
        $navigation = Navigation::findOrFail($id);
        $this->navigation_id = $id;
        $this->vendor_id = $navigation->vendor_id;
        $this->group = $navigation->group;
        $this->title = $navigation->title;
        $this->controller = $navigation->controller;
        $this->function = $navigation->function;
        $this->route = $navigation->route;
        $this->parent_id = $navigation->parent_id;
        $this->is_show = $navigation->is_show;
        $this->is_enable = $navigation->is_enable;
        $this->application_id = $navigation->application_id;
        $this->class = $navigation->class;
        $this->svg = $navigation->svg;
        $this->weight = $navigation->weight;
        $this->nav_sort = $navigation->nav_sort;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Navigation::find($id)->delete();
        $this->alertSuccessMessage('Navigation deleted.');
    }
}
