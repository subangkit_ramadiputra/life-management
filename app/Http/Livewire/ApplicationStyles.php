<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationStyle;

class ApplicationStyles extends Component
{
    use AlertMessageTrait;

    public $application_styles, $application_style_id;
    public $title;
    public $summary;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_styles = ApplicationStyle::all();
        $this->total_rows = ApplicationStyle::count();
        return view('livewire.application_styles');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_style_id = '';
        $this->title = '';
        $this->summary = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationStyle::updateOrCreate(['id' => $this->application_style_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_style_id ? 'ApplicationStyle updated.' : 'ApplicationStyle created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_style = ApplicationStyle::findOrFail($id);
        $this->application_style_id = $id;
        $this->title = $application_style->title;
        $this->summary = $application_style->summary;
        $this->application_id = $application_style->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationStyle::find($id)->delete();
        $this->alertSuccessMessage('ApplicationStyle deleted.');
    }
}
