<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnActivityType;

class BpmnActivityTypes extends Component
{
    use AlertMessageTrait;

    public $bpmn_activity_types, $bpmn_activity_type_id;
    public $code;
    public $name;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_activity_types = BpmnActivityType::all();
        $this->total_rows = BpmnActivityType::count();
        return view('livewire.bpmn_activity_types');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_activity_type_id = '';
        $this->code = '';
        $this->name = '';

    }

    public function store()
    {
        $this->validate([
            'code' => 'required',
            'name' => 'required',

        ]);

        BpmnActivityType::updateOrCreate(['id' => $this->bpmn_activity_type_id], [
            'code' => $this->code,
            'name' => $this->name,

        ]);

        $this->alertSuccessMessage($this->bpmn_activity_type_id ? 'BpmnActivityType updated.' : 'BpmnActivityType created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_activity_type = BpmnActivityType::findOrFail($id);
        $this->bpmn_activity_type_id = $id;
        $this->code = $bpmn_activity_type->code;
        $this->name = $bpmn_activity_type->name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnActivityType::find($id)->delete();
        $this->alertSuccessMessage('BpmnActivityType deleted.');
    }
}
