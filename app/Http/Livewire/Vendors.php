<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Models\VendorCategory;
use Livewire\Component;
use App\Models\Vendor;

class Vendors extends Component
{
    use AlertMessageTrait;

    public $vendors, $vendor_id;
    public $name;
    public $short_name;
    public $address;
    public $short_address;
    public $phone;
    public $wa;
    public $category_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    // Relation
    public $vendor_categories = [];

    public function render()
    {
        $this->vendors = Vendor::all();
        $this->total_rows = Vendor::count();

        // Relation
        $this->vendor_categories = VendorCategory::all();

        return view('livewire.vendors');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->vendor_id = '';
        $this->name = '';
        $this->short_name = '';
        $this->address = '';
        $this->short_address = '';
        $this->phone = '';
        $this->wa = '';
        $this->category_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'short_name' => 'required',
            'short_address' => 'required',
            'wa' => 'required',
            'category_id' => 'required',

        ]);

        Vendor::updateOrCreate(['id' => $this->vendor_id], [
            'name' => $this->name,
            'short_name' => $this->short_name,
            'address' => $this->address,
            'short_address' => $this->short_address,
            'phone' => $this->phone,
            'wa' => $this->wa,
            'category_id' => $this->category_id,

        ]);
        $this->alertSuccessMessage($this->vendor_id ? 'Vendor updated.' : 'Vendor created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $vendor = Vendor::findOrFail($id);
        $this->vendor_id = $id;
        $this->name = $vendor->name;
        $this->short_name = $vendor->short_name;
        $this->address = $vendor->address;
        $this->short_address = $vendor->short_address;
        $this->phone = $vendor->phone;
        $this->wa = $vendor->wa;
        $this->category_id = $vendor->category_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Vendor::find($id)->delete();
        $this->alertSuccessMessage('Vendor deleted.');
    }
}
