<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Upload;

class Uploads extends Component
{
    use AlertMessageTrait;

    public $uploads, $upload_id;
    public $file_name;
    public $file_size;
    public $file_extension;
    public $path;
    public $url;
    public $total_rows = 0;

    public $isModalOpen = 0;
    public function render()
    {
        $this->uploads = Upload::all();
        $this->total_rows = Upload::count();
        return view('livewire.uploads');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->upload_id = '';
        $this->file_name = '';
        $this->file_size = '';
        $this->file_extension = '';
        $this->path = '';
        $this->url = '';

    }

    public function store()
    {
        $this->validate([
            'file_name' => 'required',
            'file_size' => 'required',
            'file_extension' => 'required',
            'path' => 'required',
            'url' => 'required',

        ]);

        Upload::updateOrCreate(['id' => $this->upload_id], [
            'file_name' => $this->file_name,
            'file_size' => $this->file_size,
            'file_extension' => $this->file_extension,
            'path' => $this->path,
            'url' => $this->url,

        ]);
        $this->alertSuccessMessage($this->upload_id ? 'Upload updated.' : 'Upload created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $upload = Upload::findOrFail($id);
        $this->upload_id = $id;
        $this->file_name = $upload->file_name;
        $this->file_size = $upload->file_size;
        $this->file_extension = $upload->file_extension;
        $this->path = $upload->path;
        $this->url = $upload->url;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Upload::find($id)->delete();
        $this->alertSuccessMessage('Upload deleted.');
    }
}
