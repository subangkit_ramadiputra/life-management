<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Stage;

class Stages extends Component
{
    use AlertMessageTrait;

    public $stages, $stage_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;
    public $server_id;
    public $path;
    public $git_branch;
    public $cicd_user_access;
    public $stage;
    public $cicd_format;
    public $stageable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->stages = Stage::all();
        $this->total_rows = Stage::count();
        return view('livewire.stages');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->stage_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';
        $this->server_id = '';
        $this->path = '';
        $this->git_branch = '';
        $this->cicd_user_access = '';
        $this->stage = 'development';
        $this->cicd_format = 'lumen';
        $this->stageable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',
            'server_id' => 'required',
            'path' => 'required',
            'stage' => 'required',
            'cicd_format' => 'required',
            'stageable' => 'required',

        ]);

        Stage::updateOrCreate(['id' => $this->stage_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'server_id' => $this->server_id,
            'path' => $this->path,
            'git_branch' => $this->git_branch,
            'cicd_user_access' => $this->cicd_user_access,
            'stage' => $this->stage,
            'cicd_format' => $this->cicd_format,
            'stageable' => $this->stageable,

        ]);
        $this->alertSuccessMessage($this->stage_id ? 'Stage updated.' : 'Stage created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $stage = Stage::findOrFail($id);
        $this->stage_id = $id;
        $this->title = $stage->title;
        $this->summary = $stage->summary;
        $this->body_format = $stage->body_format;
        $this->body = $stage->body;
        $this->server_id = $stage->server_id;
        $this->path = $stage->path;
        $this->git_branch = $stage->git_branch;
        $this->cicd_user_access = $stage->cicd_user_access;
        $this->stage = $stage->stage;
        $this->cicd_format = $stage->cicd_format;
        $this->stageable = $stage->stageable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Stage::find($id)->delete();
        $this->alertSuccessMessage('Stage deleted.');
    }
}
