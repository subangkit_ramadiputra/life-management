<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Audience;

class Audiences extends Component
{
    use AlertMessageTrait;

    public $audiences, $audience_id;
    public $name;
    public $summary;
    public $detail;
    public $min_age;
    public $max_age;
    public $priority;
    public $highlight_words;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->audiences = Audience::all();
        $this->total_rows = Audience::count();
        return view('livewire.audiences');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->audience_id = '';
        $this->name = '';
        $this->summary = '';
        $this->detail = '';
        $this->min_age = '0';
        $this->max_age = '100';
        $this->priority = 'Normal';
        $this->highlight_words = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'detail' => 'required',
            'max_age' => 'required',
            'priority' => 'required',
            'highlight_words' => 'required',
            'application_id' => 'required',

        ]);

        Audience::updateOrCreate(['id' => $this->audience_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'detail' => $this->detail,
            'min_age' => $this->min_age,
            'max_age' => $this->max_age,
            'priority' => $this->priority,
            'highlight_words' => $this->highlight_words,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->audience_id ? 'Audience updated.' : 'Audience created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $audience = Audience::findOrFail($id);
        $this->audience_id = $id;
        $this->name = $audience->name;
        $this->summary = $audience->summary;
        $this->detail = $audience->detail;
        $this->min_age = $audience->min_age;
        $this->max_age = $audience->max_age;
        $this->priority = $audience->priority;
        $this->highlight_words = $audience->highlight_words;
        $this->application_id = $audience->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Audience::find($id)->delete();
        $this->alertSuccessMessage('Audience deleted.');
    }
}
