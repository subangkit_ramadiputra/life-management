<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Note;

class Notes extends Component
{
    use AlertMessageTrait;

    public $notes, $note_id;
    public $title;
    public $body;
    public $noteable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->notes = Note::all();
        $this->total_rows = Note::count();
        return view('livewire.notes');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->note_id = '';
        $this->title = '';
        $this->body = '';
        $this->noteable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body' => 'required',
            'noteable' => 'required',

        ]);

        Note::updateOrCreate(['id' => $this->note_id], [
            'title' => $this->title,
            'body' => $this->body,
            'noteable' => $this->noteable,

        ]);

        $this->alertSuccessMessage($this->note_id ? 'Note updated.' : 'Note created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $note = Note::findOrFail($id);
        $this->note_id = $id;
        $this->title = $note->title;
        $this->body = $note->body;
        $this->noteable = $note->noteable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Note::find($id)->delete();
        $this->alertSuccessMessage('Note deleted.');
    }
}
