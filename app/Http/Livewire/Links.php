<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Link;

class Links extends Component
{
    use AlertMessageTrait;

    public $links, $link_id;
    public $title;
    public $url;
    public $summary;
    public $linkable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->links = Link::all();
        $this->total_rows = Link::count();
        return view('livewire.links');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->link_id = '';
        $this->title = '';
        $this->url = '';
        $this->summary = '';
        $this->linkable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'url' => 'required',
            'linkable' => 'required',

        ]);

        Link::updateOrCreate(['id' => $this->link_id], [
            'title' => $this->title,
            'url' => $this->url,
            'summary' => $this->summary,
            'linkable' => $this->linkable,

        ]);

        $this->alertSuccessMessage($this->link_id ? 'Link updated.' : 'Link created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $link = Link::findOrFail($id);
        $this->link_id = $id;
        $this->title = $link->title;
        $this->url = $link->url;
        $this->summary = $link->summary;
        $this->linkable = $link->linkable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Link::find($id)->delete();
        $this->alertSuccessMessage('Link deleted.');
    }
}
