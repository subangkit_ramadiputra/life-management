<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\DatabaseType;

class DatabaseTypes extends Component
{
    use AlertMessageTrait;

    public $database_types, $database_type_id;
    public $title;
    public $summary;
    public $slug;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->database_types = DatabaseType::all();
        $this->total_rows = DatabaseType::count();
        return view('livewire.database_types');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->database_type_id = '';
        $this->title = '';
        $this->summary = '';
        $this->slug = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'slug' => 'required',

        ]);

        DatabaseType::updateOrCreate(['id' => $this->database_type_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'slug' => $this->slug,

        ]);

        $this->alertSuccessMessage($this->database_type_id ? 'DatabaseType updated.' : 'DatabaseType created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $database_type = DatabaseType::findOrFail($id);
        $this->database_type_id = $id;
        $this->title = $database_type->title;
        $this->summary = $database_type->summary;
        $this->slug = $database_type->slug;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        DatabaseType::find($id)->delete();
        $this->alertSuccessMessage('DatabaseType deleted.');
    }
}
