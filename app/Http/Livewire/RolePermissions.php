<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\RolePermission;

class RolePermissions extends Component
{
    use AlertMessageTrait;

    public $role_permissions, $role_permission_id;
    public $role_id;
    public $permission_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->role_permissions = RolePermission::all();
        $this->total_rows = RolePermission::count();
        return view('livewire.role_permissions');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->role_permission_id = '';
        $this->role_id = '';
        $this->permission_id = '';

    }

    public function store()
    {
        $this->validate([
            'role_id' => 'required',
            'permission_id' => 'required',

        ]);

        RolePermission::updateOrCreate(['id' => $this->role_permission_id], [
            'role_id' => $this->role_id,
            'permission_id' => $this->permission_id,

        ]);
        $this->alertSuccessMessage($this->role_permission_id ? 'RolePermission updated.' : 'RolePermission created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $role_permission = RolePermission::findOrFail($id);
        $this->role_permission_id = $id;
        $this->role_id = $role_permission->role_id;
        $this->permission_id = $role_permission->permission_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        RolePermission::find($id)->delete();
        $this->alertSuccessMessage('RolePermission deleted.');
    }
}
