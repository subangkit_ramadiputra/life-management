<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Permission;

class Permissions extends Component
{
    use AlertMessageTrait;

    public $permissions, $permission_id;
    public $name;
    public $slug;
    public $description;
    public $permitable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->permissions = Permission::all();
        $this->total_rows = Permission::count();
        return view('livewire.permissions');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->permission_id = '';
        $this->name = '';
        $this->slug = '';
        $this->description = '';
        $this->permitable = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'permitable' => 'required',

        ]);

        Permission::updateOrCreate(['id' => $this->permission_id], [
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'permitable' => $this->permitable,

        ]);
        $this->alertSuccessMessage($this->permission_id ? 'Permission updated.' : 'Permission created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $permission = Permission::findOrFail($id);
        $this->permission_id = $id;
        $this->name = $permission->name;
        $this->slug = $permission->slug;
        $this->description = $permission->description;
        $this->permitable = $permission->permitable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Permission::find($id)->delete();
        $this->alertSuccessMessage('Permission deleted.');
    }
}
