<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnGatewayType;

class BpmnGatewayTypes extends Component
{
    use AlertMessageTrait;

    public $bpmn_gateway_types, $bpmn_gateway_type_id;
    public $code;
    public $name;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_gateway_types = BpmnGatewayType::all();
        $this->total_rows = BpmnGatewayType::count();
        return view('livewire.bpmn_gateway_types');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_gateway_type_id = '';
        $this->code = '';
        $this->name = '';

    }

    public function store()
    {
        $this->validate([
            'code' => 'required',
            'name' => 'required',

        ]);

        BpmnGatewayType::updateOrCreate(['id' => $this->bpmn_gateway_type_id], [
            'code' => $this->code,
            'name' => $this->name,

        ]);

        $this->alertSuccessMessage($this->bpmn_gateway_type_id ? 'BpmnGatewayType updated.' : 'BpmnGatewayType created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_gateway_type = BpmnGatewayType::findOrFail($id);
        $this->bpmn_gateway_type_id = $id;
        $this->code = $bpmn_gateway_type->code;
        $this->name = $bpmn_gateway_type->name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnGatewayType::find($id)->delete();
        $this->alertSuccessMessage('BpmnGatewayType deleted.');
    }
}
