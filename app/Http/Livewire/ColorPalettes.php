<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ColorPalette;

class ColorPalettes extends Component
{
    use AlertMessageTrait;

    public $color_palettes, $color_palette_id;
    public $name;
    public $summary;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->color_palettes = ColorPalette::all();
        $this->total_rows = ColorPalette::count();
        return view('livewire.color_palettes');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->color_palette_id = '';
        $this->name = '';
        $this->summary = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'application_id' => 'required',

        ]);

        ColorPalette::updateOrCreate(['id' => $this->color_palette_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->color_palette_id ? 'ColorPalette updated.' : 'ColorPalette created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $color_palette = ColorPalette::findOrFail($id);
        $this->color_palette_id = $id;
        $this->name = $color_palette->name;
        $this->summary = $color_palette->summary;
        $this->application_id = $color_palette->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ColorPalette::find($id)->delete();
        $this->alertSuccessMessage('ColorPalette deleted.');
    }
}
