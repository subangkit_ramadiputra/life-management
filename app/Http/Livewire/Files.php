<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\File;

class Files extends Component
{
    use AlertMessageTrait;

    public $files, $file_id;
    public $name;
    public $summary;
    public $file_path;
    public $Mode;
    public $fileable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->files = File::all();
        $this->total_rows = File::count();
        return view('livewire.files');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->file_id = '';
        $this->name = '';
        $this->summary = '';
        $this->file_path = '';
        $this->Mode = 'html';
        $this->fileable = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'file_path' => 'required',
            'Mode' => 'required',
            'fileable' => 'required',

        ]);

        File::updateOrCreate(['id' => $this->file_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'file_path' => $this->file_path,
            'Mode' => $this->Mode,
            'fileable' => $this->fileable,

        ]);

        $this->alertSuccessMessage($this->file_id ? 'File updated.' : 'File created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $file = File::findOrFail($id);
        $this->file_id = $id;
        $this->name = $file->name;
        $this->summary = $file->summary;
        $this->file_path = $file->file_path;
        $this->Mode = $file->Mode;
        $this->fileable = $file->fileable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        File::find($id)->delete();
        $this->alertSuccessMessage('File deleted.');
    }
}
