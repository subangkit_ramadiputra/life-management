<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Traits\GenerateTraits;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use App\Models\RowFormatter;

class RowFormatters extends Component
{
    use AlertMessageTrait;
    use GenerateTraits;

    public $row_formatters, $row_formatter_id;
    public $title;
    public $summary;
    public $query;
    public $row_template;
    public $connections = [], $connection;
    public $query_results = '';
    public $template_result = '';
    public $parameters;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->row_formatters = RowFormatter::all();
        $connections = [];
        foreach (\Config::get('database.connections') as $connection_name => $connection_options) {
            $connections[] = (object) [
                'name' => $connection_name,
                'driver' => strtoupper($connection_options['driver'])
            ];
        }
        $this->connections = $connections;
        $this->total_rows = RowFormatter::count();
        return view('livewire.row_formatters');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->row_formatter_id = '';
        $this->title = '';
        $this->summary = '';
        $this->query = '';
        $this->row_template = '';
        $this->connection = 'mysql';
        $this->query_results = '';
        $this->template_result = '';
        $this->parameters = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'query' => 'required',
            'row_template' => 'required',
            'connection' => 'required',
            'parameters' => 'required',
        ]);

        RowFormatter::updateOrCreate(['id' => $this->row_formatter_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'query' => $this->query,
            'row_template' => $this->row_template,
            'connection' => $this->connection,
            'parameters' => $this->parameters,
        ]);

        $this->alertSuccessMessage($this->row_formatter_id ? 'RowFormatter updated.' : 'RowFormatter created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $row_formatter = RowFormatter::findOrFail($id);
        $this->row_formatter_id = $id;
        $this->title = $row_formatter->title;
        $this->summary = $row_formatter->summary;
        $this->query = $row_formatter->query;
        $this->row_template = $row_formatter->row_template;
        $this->connection = $row_formatter->connection;
        $this->parameters = $row_formatter->parameters;


        $this->openModalPopover();
    }
    public function duplicate($id)
    {
        $row_formatter = RowFormatter::findOrFail($id);

        RowFormatter::updateOrCreate(['id' => null], [
            'title' => $row_formatter->title,
            'summary' => $row_formatter->summary,
            'query' => $row_formatter->query,
            'row_template' => $row_formatter->row_template,
            'connection' => $row_formatter->connection,
            'parameters' => $row_formatter->parameters,
        ]);

        $this->render();
    }

    public function delete($id)
    {
        RowFormatter::find($id)->delete();
        $this->alertSuccessMessage('RowFormatter deleted.');
    }

    public function getParameters() {
        $parameters = json_decode($this->parameters);
        return $parameters;
    }

    public function getQuery() {
        $query = $this->query;
        foreach ($this->getParameters() as $parameter => $default_value) {
            $query = str_replace('$$'.$parameter.'$$',"'".$default_value."'",$query);
        }
        return $query;
    }

    public function testQuery() {
        $this->openModalPopover();

        if ($this->query != '') {
            $this->query_results = json_encode(DB::connection($this->connection)->select(DB::raw($this->getQuery())),JSON_PRETTY_PRINT);
        } else {
            $this->query_results = 'Empty Query';
        }
    }

    public function testTemplate() {
        $this->openModalPopover();

        if ($this->row_template != '') {
            $this->template_result = $this->runRowFormatterTemplate($this->connection, $this->getQuery(), $this->row_template);
        } else {
            $this->template_result = 'Empty Template';
        }
    }
}
