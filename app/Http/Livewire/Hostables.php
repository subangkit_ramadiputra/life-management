<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Hostable;

class Hostables extends Component
{
    use AlertMessageTrait;

    public $hostables, $hostable_id;
    public $hostable;
    public $host;
    public $port;
    public $instance;
    public $environment;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->hostables = Hostable::all();
        $this->total_rows = Hostable::count();
        return view('livewire.hostables');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->hostable_id = '';
        $this->hostable = '';
        $this->host = '';
        $this->port = '';
        $this->instance = '';
        $this->environment = 'Production';

    }

    public function store()
    {
        $this->validate([
            'hostable' => 'required',
            'host' => 'required',
            'environment' => 'required',

        ]);

        Hostable::updateOrCreate(['id' => $this->hostable_id], [
            'hostable' => $this->hostable,
            'host' => $this->host,
            'port' => $this->port,
            'instance' => $this->instance,
            'environment' => $this->environment,

        ]);

        $this->alertSuccessMessage($this->hostable_id ? 'Hostable updated.' : 'Hostable created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $hostable = Hostable::findOrFail($id);
        $this->hostable_id = $id;
        $this->hostable = $hostable->hostable;
        $this->host = $hostable->host;
        $this->port = $hostable->port;
        $this->instance = $hostable->instance;
        $this->environment = $hostable->environment;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Hostable::find($id)->delete();
        $this->alertSuccessMessage('Hostable deleted.');
    }
}
