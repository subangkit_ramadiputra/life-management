<?php

namespace App\Http\Livewire\Inputs;

use Livewire\Component;

class Date extends Component
{
    public $options = [];
    public function render()
    {
        $this->options = array_merge([
            'dateFormat' => 'Y-m-d',
            'enableTime' => false,
            'altFormat' =>  'j F Y',
            'altInput' => true
        ], $this->options);
        return view('livewire.inputs.date');
    }
}
