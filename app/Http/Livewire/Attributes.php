<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Attribute;

class Attributes extends Component
{
    use AlertMessageTrait;

    public $attributes, $attribute_id;
    public $field_name;
    public $nullable;
    public $title;
    public $type;
    public $length;
    public $precision;
    public $scale;
    public $digits;
    public $decimal;
    public $default;
    public $view_table;
    public $view_create;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->attributes = Attribute::all();
        $this->total_rows = Attribute::count();
        return view('livewire.attributes');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->attribute_id = '';
        $this->field_name = '';
        $this->nullable = '0';
        $this->title = '';
        $this->type = 'string';
        $this->length = '100';
        $this->precision = '0';
        $this->scale = '0';
        $this->digits = '0';
        $this->decimal = '0';
        $this->default = '';
        $this->view_table = '1';
        $this->view_create = '1';

    }

    public function store()
    {
        $this->validate([
            'field_name' => 'required',
            'nullable' => 'required',
            'title' => 'required',
            'type' => 'required',
            'length' => 'required',
            'precision' => 'required',
            'scale' => 'required',
            'digits' => 'required',
            'decimal' => 'required',
            'default' => 'required',
            'view_table' => 'required',
            'view_create' => 'required',

        ]);

        Attribute::updateOrCreate(['id' => $this->attribute_id], [
            'field_name' => $this->field_name,
            'nullable' => $this->nullable,
            'title' => $this->title,
            'type' => $this->type,
            'length' => $this->length,
            'precision' => $this->precision,
            'scale' => $this->scale,
            'digits' => $this->digits,
            'decimal' => $this->decimal,
            'default' => $this->default,
            'view_table' => $this->view_table,
            'view_create' => $this->view_create,

        ]);

        $this->alertSuccessMessage($this->attribute_id ? 'Attribute updated.' : 'Attribute created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $attribute = Attribute::findOrFail($id);
        $this->attribute_id = $id;
        $this->field_name = $attribute->field_name;
        $this->nullable = $attribute->nullable;
        $this->title = $attribute->title;
        $this->type = $attribute->type;
        $this->length = $attribute->length;
        $this->precision = $attribute->precision;
        $this->scale = $attribute->scale;
        $this->digits = $attribute->digits;
        $this->decimal = $attribute->decimal;
        $this->default = $attribute->default;
        $this->view_table = $attribute->view_table;
        $this->view_create = $attribute->view_create;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Attribute::find($id)->delete();
        $this->alertSuccessMessage('Attribute deleted.');
    }
}
