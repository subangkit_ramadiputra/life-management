<?php

namespace App\Http\Livewire;

use App\Models\BpmnEventType;
use App\Models\Sop;
use App\Traits\SopBpmnTrait;
use Livewire\Component;
use PHPMentors\Workflower\Definition\Bpmn2File;
use PHPMentors\Workflower\Definition\ProcessDefinitionRepository;
use PHPMentors\Workflower\Persistence\Base64PhpWorkflowSerializer;
use PHPMentors\Workflower\Persistence\PhpWorkflowSerializer;
use PHPMentors\Workflower\Workflow\ProcessDefinition;

class SopEditor extends Component
{
    use SopBpmnTrait;

    public $sop_id;
    public $sop;

    public function mount($sop_id)
    {
        $this->sop = Sop::findOrFail($sop_id);
        $this->sop_id = $sop_id;
    }

    public function render()
    {
        $this->getProcess();
        return view('livewire.sop-editor');
    }

    /**
     * @var array
     */
    protected $workflows = [];
    protected $definitions;
    public $definitionArray = [];
    public $serializer = '';
    public function getProcess() {
        $definitionArray = [];
        $definitionArray['id'] = $this->codefication($this->sop->title);
        $definitionArray['name'] = $this->sop->title;

        $role = $this->sop->bpmn_roles()->first();
        foreach ( $this->sop->bpmn_roles()->get() as $role) {
            $definitionArray['roles'][] = [
                'id' => $role->code,
                'name' => $role->name
            ];
        }

        $start = BpmnEventType::where('code','Start')->first();
        foreach ( $this->sop->bpmn_events()->where('type_id',$start->id)->get() as $event) {
            $definitionArray['startEvents'][] = [
                'id' => $event->code,
                'roleId' => $event->role->code
            ];
        }

        $end = BpmnEventType::where('code','End')->first();
        foreach ( $this->sop->bpmn_events()->where('type_id',$end->id)->get() as $event) {
            $definitionArray['endEvents'][] = [
                'id' => $event->code,
                'roleId' => $event->role->code
            ];
        }

        foreach ( $this->sop->bpmn_activities()->get() as $activity) {
            switch ($activity->type->code) {
                case 'Regular' :
                    $definitionArray['tasks'][] = [
                        'id' => $activity->code,
                        'name' => $activity->name,
                        'roleId' => $activity->role->code
                    ];
                    break;
            }
        }

        foreach ( $this->sop->bpmn_gateways()->get() as $gateway) {
            switch ($gateway->type->code) {
                case 'Exclusive' :
                    $definitionArray['exclusiveGateways'][] = [
                        'id' => $activity->code,
                        'name' => $activity->name,
                        'roleId' => $gateway->role->code,
                        'defaultSquenceFlowId' => $gateway->default_squence->code
                    ];
                    break;
                case 'Inclusive' :
                    $definitionArray['inclusiveGateways'][] = [
                        'id' => $activity->code,
                        'name' => $activity->name,
                        'roleId' => $gateway->role->code,
                        'defaultSquenceFlowId' => $gateway->default_squence->code
                    ];
                    break;
                case 'Parallel' :
                    $definitionArray['parallelGateways'][] = [
                        'id' => $activity->code,
                        'name' => $activity->name,
                        'roleId' => $gateway->role->code,
                        'defaultSquenceFlowId' => $gateway->default_squence->code
                    ];
                    break;
            }
        }

        foreach ( $this->sop->bpmn_squences()->get() as $squence) {
            $source = $squence->source()->first();
            $destination = $squence->destination()->first();
            $definitionArray['sequenceFlows'][] = [
                'id' => $squence->code,
                'source' => $source->name,
                'destination' => $destination->code
            ];
        }

        $this->definitionArray = $definitionArray;

        $processDefinitions = new ProcessDefinition($definitionArray);
        $this->definitions = new ProcessDefinitionRepository();
        $this->definitions->add($processDefinitions);

        $processInstance = $processDefinitions->createProcessInstance();
        $base64Serializer = new PhpWorkflowSerializer();
        $this->serializer = $base64Serializer->serialize($processInstance);

    }
}
