<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\EntityAction;

class EntityActions extends Component
{
    use AlertMessageTrait;

    public $entity_actions, $entity_action_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;
    public $actionable;
    public $parameters;
    public $method;
    public $tags;
    public $responses;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->entity_actions = EntityAction::all();
        $this->total_rows = EntityAction::count();
        return view('livewire.entity_actions');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->entity_action_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';
        $this->actionable = '';
        $this->parameters = 'username|path';
        $this->method = 'POST';
        $this->tags = '';
        $this->responses = '200|Success,400|Has Error';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',
            'actionable' => 'required',
            'parameters' => 'required',
            'method' => 'required',
            'tags' => 'required',
            'responses' => 'required',

        ]);

        EntityAction::updateOrCreate(['id' => $this->entity_action_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'actionable' => $this->actionable,
            'parameters' => $this->parameters,
            'method' => $this->method,
            'tags' => $this->tags,
            'responses' => $this->responses,

        ]);

        $this->alertSuccessMessage($this->entity_action_id ? 'EntityAction updated.' : 'EntityAction created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $entity_action = EntityAction::findOrFail($id);
        $this->entity_action_id = $id;
        $this->title = $entity_action->title;
        $this->summary = $entity_action->summary;
        $this->body_format = $entity_action->body_format;
        $this->body = $entity_action->body;
        $this->actionable = $entity_action->actionable;
        $this->parameters = $entity_action->parameters;
        $this->method = $entity_action->method;
        $this->tags = $entity_action->tags;
        $this->responses = $entity_action->responses;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        EntityAction::find($id)->delete();
        $this->alertSuccessMessage('EntityAction deleted.');
    }
}
