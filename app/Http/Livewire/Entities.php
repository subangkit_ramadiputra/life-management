<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Http\Traits\Generator;
use App\Models\Application;
use App\Models\Attribute;
use App\Models\Concept;
use App\Models\Feature;
use App\Models\Navigation;
use App\Models\NavigationGroup;
use App\Models\Permission;
use App\Models\Place;
use App\Models\Transition;
use App\Models\Upload;
use App\Models\EntityAction;
use App\Models\Sop;
use App\Models\AppTech;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Livewire\Component;
use App\Models\Entity;
use Livewire\WithFileUploads;
use Symfony\Component\Process\Process;
use Symfony\Component\Workflow\DefinitionBuilder;
use Storage,DB;
use Symfony\Component\Workflow\Dumper\GraphvizDumper;

class Entities extends Component
{
    use AlertMessageTrait;

    protected $queryString = ['filter_concept_id','filter_application_id','filter_feature_id'];
    public $entities = [];
    public $entity_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $total_rows = 0;

    public $filter_concept_id = 0;
    public $filter_application_id = 0;
    public $filter_feature_id = 0;
    public $filter_primary = 'concept';


    public $isModalOpen = 0;
    public function render()
    {
        $this->changeFilter();
        $this->concepts = Concept::all();

        return view('livewire.entities');
    }

    // Filter Section
    public function changeFilter() {
        if ($this->filter_concept_id != 0) {
            $this->applications = Application::where('concept_id', $this->filter_concept_id)->get();
            $this->features = Feature::whereHasMorph(
                'featureable',
                [Application::class],
                function ($query) {
                    $query->where('concept_id', '=', $this->filter_concept_id);
                }
            )->get();
            if ($this->filter_application_id != 0) {
                $this->features = Feature::whereHasMorph(
                    'featureable',
                    [Application::class],
                    function ($query) {
                        $query->where('id', '=', $this->filter_application_id);
                    }
                )->get();
                if ($this->filter_feature_id != 0) {
                    if($this->filter_application_id == -1) {
                        $f = Feature::findOrFail($this->filter_feature_id);
                        if ($f) {
                            $this->filter_concept_id = $f->featureable->concept_id;
                            $this->filter_application_id = $f->featureable->id;
                        }
                    }
                    $query = Entity::whereHasMorph(
                        'entitiable',
                        [Feature::class],
                        function ($query) {
                            $query->where('id',$this->filter_feature_id);
                        }
                    );
                    $this->entities = $query->get();
                    $this->total_rows = $query->count();
                } else {
                    $query = Entity::whereHasMorph(
                        'entitiable',
                        [Feature::class],
                        function ($query) {
                            $query->whereHasMorph(
                                'featureable',
                                [Application::class],
                                function ($query2) {
                                    $query2->where('id', '=', $this->filter_application_id);
                                }
                            );
                        }
                    );
                    $this->entities = $query->get();
                    $this->total_rows = $query->count();
                }
            } else {
                $query = Entity::whereHasMorph(
                    'entitiable',
                    [Feature::class],
                    function ($query) {
                        $query->whereHasMorph(
                            'featureable',
                            [Application::class],
                            function ($query2) {
                                $query2->where('concept_id', '=', $this->filter_concept_id);
                            }
                        );
                    }
                );
                $this->entities = $query->get();
                $this->total_rows = $query->count();
            }
        } else {
            $this->entities = Entity::all();
            $this->total_rows = Entity::count();
            $this->applications = [];
            $this->features = [];
        }
    }
    // End of Section
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->entity_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',

        ]);

        $entitiable_type = '';
        $entitiable_id = '';

        if ($this->filter_feature_id != 0) {
            $entitiable_type = "App\\Models\\Feature";
            $entitiable_id = $this->filter_feature_id;
        }

        Entity::updateOrCreate(['id' => $this->entity_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'entitiable_type' => $entitiable_type,
            'entitiable_id' => $entitiable_id,
        ]);

        if ($this->entity_id == '') {
            $this->entity_id = DB::getPdo()->lastInsertId();
        }

        $this->generateEntityPermission($this->entity_id);

        $this->alertSuccessMessage($this->entity_id ? 'Entity updated.' : 'Entity created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    protected function generateEntityPermission($entity_id) {
        $entity = Entity::find($entity_id);
        if ($entity) {
            $default_permissions = [
                'list' => [
                    'method' => 'GET',
                    'permission' => 'list',
                    'name' => 'View Data'
                ],
                'create' => [
                    'method' => 'POST',
                    'permission' => 'create',
                    'name' => 'Create'
                ],
                'update' => [
                    'method' => 'PUT',
                    'permission' => 'update',
                    'name' => 'Update'
                ],
                'delete' => [
                    'method' => 'DELETE',
                    'permission' => 'delete',
                    'name' => 'Delete'
                ],
            ];

            foreach ($default_permissions as $type => $entity_permission) {
                $permission_name = $entity->title.' '.$entity_permission['name'];
                $permission_slug = Str::slug($permission_name);
                $check = $entity->permissions()->where('slug', $permission_slug)->first();
                if ($check == null) {
                    $permission = new Permission();
                    $permission->name = $permission_name;
                    $permission->slug = $permission_slug;
                    $permission->description = $permission_name;

                    $entity->permissions()->save($permission);
                }
            }
        }
    }
    public function edit($id)
    {
        $entity = Entity::findOrFail($id);
        $this->entity_id = $id;
        $this->title = $entity->title;
        $this->summary = $entity->summary;
        $this->body = $entity->body;
        $this->body_format = $entity->body_format;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Entity::find($id)->delete();
        $this->alertSuccessMessage('Entity deleted.');
    }

    // Place Section
    public $isPlaceModalOpen = 0;
    public $recent_places = [];
    public $places = [];
    public function addPlace($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_places = $entity->places()->get();
        $this->resetPlaceCreateForm();
        $this->openPlaceModalPopover();
    }

    public function openPlaceModalPopover()
    {
        $this->isPlaceModalOpen = true;
    }
    public function closePlaceModalPopover()
    {
        $this->isPlaceModalOpen = false;
    }

    public function editPlace($place_id) {
        $place = Place::findOrFail($place_id);
        $this->place_id = $place_id;
        $this->place_title = $place->title;
        $this->place_summary = $place->summary;
        $this->place_body = $place->body;
        $this->place_body_format = $place->body_format;
        $this->place_weight = $place->weight;
    }

    public function resetPlaceCreateForm(){
        $this->place_id = '';
        $this->place_title = '';
        $this->place_summary = '';
        $this->place_body = '';
        $this->place_body_format = 'markdown';
        $this->place_weight = 0;
    }

    public function assignPlace() {
        $entity = Entity::findOrFail($this->entity_id);
        if ($this->place_id == '') {
            $place = new Place();
            $place->title = $this->place_title;
            $place->summary = $this->place_summary;
            $place->body = $this->place_body;
            $place->body_format = $this->place_body_format;
            $place->weight = $this->place_weight;
            $entity->places()->save($place);

            $this->alertSuccessMessage('Place created.');
            //$this->closePlaceModalPopover();
            $this->resetPlaceCreateForm();
        } else {
            Place::updateOrCreate(['id' => $this->place_id], [
                'title' => $this->place_title,
                'summary' => $this->place_summary,
                'body' => $this->place_body,
                'body_format' => $this->place_body_format,
                'weight' => $this->place_weight,
            ]);

            $this->alertSuccessMessage('Place updated.');
        }

        $this->recent_places = $entity->places()->get();
    }

    public function deletePlace($entity_id, $place_id) {
        $entity = Entity::findOrFail($entity_id);

        $place = Place::find($place_id);
        if ($place) {
            $entity->places()->where('id', '=', $place->id)->delete();
        }

        $this->recent_places = $entity->places()->get();

        $this->alertSuccessMessage('Place deleted.');
    }

    public function deleteExistingPlace($place_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $place = Place::find($place_id);
        if ($place) {
            $entity->places()->where('id', '=', $place->id)->delete();
        }

        $this->recent_places = $entity->places()->get();

        $this->alertSuccessMessage('Place deleted.');
    }
    // End of Section

    // Transition Section
    public $isTransitionModalOpen = 0;
    public $recent_transitions = [];
    public $transitions = [];
    public $from_places;
    public $to_places;
    public $transition_id;
    public $transition_title;
    public $transition_summary;
    public $transition_body;
    public $transition_body_format;
    public $transition_from_place_id;
    public $transition_to_place_id;

    public function changeFromPlace() {
        $entity = Entity::findOrFail($this->entity_id);
        $this->to_places = $entity->places()->where('id','!=',$this->transition_from_place_id)->get();
    }

    public function changeToPlace() {
        $entity = Entity::findOrFail($this->entity_id);
        $this->from_places = $entity->places()->where('id','!=',$this->transition_to_place_id)->get();
    }

    public function addTransition($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_transitions = $entity->transitions()->get();
        $this->resetTransitionCreateForm();
        $this->openTransitionModalPopover();
        $this->from_places = $entity->places()->get();
        $this->to_places = $entity->places()->get();
    }

    public function openTransitionModalPopover()
    {
        $this->isTransitionModalOpen = true;
    }
    public function closeTransitionModalPopover()
    {
        $this->isTransitionModalOpen = false;
    }

    public function editTransition($transition_id) {
        $transition = Transition::findOrFail($transition_id);
        $this->transition_id = $transition_id;
        $this->transition_title = $transition->title;
        $this->transition_summary = $transition->summary;
        $this->transition_body = $transition->body;
        $this->transition_body_format = $transition->body_format;
        $this->transition_from_place_id = $transition->from_place_id;
        $this->transition_to_place_id = $transition->to_place_id;
    }

    public function resetTransitionCreateForm(){
        $this->transition_id = '';
        $this->transition_title = '';
        $this->transition_summary = '';
        $this->transition_body = '';
        $this->transition_body_format = 'markdown';
        $this->from_place_id = 0;
        $this->to_place_id = 0;
    }

    public function assignTransition() {
        $entity = Entity::findOrFail($this->entity_id);
        if ($this->transition_id == '') {
            $transition = new Transition();
            $transition->title = $this->transition_title;
            $transition->summary = $this->transition_summary;
            $transition->body = $this->transition_body;
            $transition->body_format = $this->transition_body_format;
            $transition->from_place_id = $this->transition_from_place_id;
            $transition->to_place_id = $this->transition_to_place_id;

            $entity->transitions()->save($transition);

            $this->alertSuccessMessage('Transition created.');
            //$this->closeTransitionModalPopover();
            $this->resetTransitionCreateForm();
        } else {
            Transition::updateOrCreate(['id' => $this->transition_id], [
                'title' => $this->transition_title,
                'summary' => $this->transition_summary,
                'body' => $this->transition_body,
                'body_format' => $this->transition_body_format,
                'from_place_id' => $this->transition_from_place_id,
                'to_place_id' => $this->transition_to_place_id,
            ]);

            $this->alertSuccessMessage('Transition updated.');
        }

        $this->recent_transitions = $entity->transitions()->get();
        $this->from_places = $entity->places()->get();
        $this->to_places = $entity->places()->get();
    }

    public function deleteTransition($entity_id, $transition_id) {
        $entity = Entity::findOrFail($entity_id);

        $transition = Transition::find($transition_id);
        if ($transition) {
            $entity->transitions()->where('id', '=', $transition->id)->delete();
        }

        $this->recent_transitions = $entity->transitions()->get();
        $this->from_places = $entity->places()->get();
        $this->to_places = $entity->places()->get();

        $this->alertSuccessMessage('Transition deleted.');
    }

    public function deleteExistingTransition($transition_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $transition = Transition::find($transition_id);
        if ($transition) {
            $entity->transitions()->where('id', '=', $transition->id)->delete();
        }

        $this->recent_transitions = $entity->transitions()->get();
        $this->from_places = $entity->places()->get();
        $this->to_places = $entity->places()->get();

        $this->alertSuccessMessage('Transition deleted.');
    }
    // End of Section

    // Workflow Section
    public function generateWorkflow($entity_id) {
        $this->entity_id = $entity_id;
        $entity = Entity::findOrFail($this->entity_id);

        // Workflow Section
        $definitionBuilder = new DefinitionBuilder();
        $places = $entity->places()->get()->pluck('title')->toArray();
        $definition = $definitionBuilder->addPlaces($places);
        foreach($entity->transitions()->get() as $transition) {
            // Transitions are defined with a unique name, an origin place and a destination place
            $definition->addTransition(new \Symfony\Component\Workflow\Transition($transition->title, $transition->from_place->title, $transition->to_place->title));
        }
        $definition = $definition->build();

        Storage::disk('public')->makeDirectory('/workflows');
        $path = Storage::disk('public')->path('/workflows');

        $dumper = new GraphvizDumper();
        $format = 'png';
        $workflowName = $entity->title.'_'.date('YmdHis');
        $dotCommand = ['dot', "-T${format}", '-o', "${workflowName}.${format}"];

        $process = new Process($dotCommand);
        $process->setWorkingDirectory($path);
        $process->setInput($dumper->dump($definition));
        $process->mustRun();

        $upload = new Upload();
        $upload->file_name = $workflowName.'.'.$format;
        $upload->file_size = Storage::disk('public')->size('workflows/'.$workflowName.'.'.$format);
        $upload->file_extension = $format;
        $upload->path = $path;
        $upload->url = Storage::disk('public')->url('workflows/'.$workflowName.'.'.$format);
        $entity->uploads()->save($upload);
    }
    // End of Section

    // Attribute Section
    public $isAttributeModalOpen = 0;
    public $recent_attributes = [];
    public $attributes = [];
    public function addAttribute($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_attributes = $entity->attributes()->get();
        $this->resetAttributeCreateForm();
        $this->openAttributeModalPopover();
    }

    public function resetTypeOptions() {
        $this->attribute_length = '';
        $this->attribute_precision = '0';
        $this->attribute_scale = '0';
        $this->attribute_digits = '0';
        $this->attribute_decimal = '0';
        $this->attribute_default = '';
    }

    public function changeType() {
        $this->resetTypeOptions();
        switch($this->attribute_type) {
            case 'string' :
                $this->attribute_length = 100;
                break;
        }
    }

    public function openAttributeModalPopover()
    {
        $this->isAttributeModalOpen = true;
    }
    public function closeAttributeModalPopover()
    {
        $this->isAttributeModalOpen = false;
    }

    public function editAttribute($attribute_id) {
        $attribute = Attribute::findOrFail($attribute_id);
        $this->attribute_id = $attribute_id;
        $this->attribute_field_name = $attribute->field_name;
        $this->attribute_nullable = $attribute->nullable == '1';
        $this->attribute_title = $attribute->title;
        $this->attribute_type = $attribute->type;
        $this->attribute_length = $attribute->length;
        $this->attribute_precision = $attribute->precision;
        $this->attribute_scale = $attribute->scale;
        $this->attribute_digits = $attribute->digits;
        $this->attribute_decimal = $attribute->decimal;
        $this->attribute_default = $attribute->default;
        $this->attribute_view_table = $attribute->view_table == '1';
        $this->attribute_view_create = $attribute->view_create == '1';
    }

    public function resetAttributeCreateForm(){
        $this->attribute_id = '';
        $this->attribute_field_name = '';
        $this->attribute_nullable = false;
        $this->attribute_title = '';
        $this->attribute_type = 'string';
        $this->attribute_length = '100';
        $this->attribute_precision = '0';
        $this->attribute_scale = '0';
        $this->attribute_digits = '0';
        $this->attribute_decimal = '0';
        $this->attribute_default = '';
        $this->attribute_view_table = true;
        $this->attribute_view_create = true;

        $this->changeType();
    }

    public function assignAttribute() {
        $entity = Entity::findOrFail($this->entity_id);
        if ($this->attribute_id == '') {
            $attribute = new Attribute();
            $attribute->field_name = $this->attribute_field_name;
            $attribute->nullable = $this->attribute_nullable ? '1' : '0';
            $attribute->title = $this->attribute_title;
            $attribute->type = $this->attribute_type;
            $attribute->length = $this->attribute_length;
            $attribute->precision = $this->attribute_precision;
            $attribute->scale = $this->attribute_scale;
            $attribute->digits = $this->attribute_digits;
            $attribute->decimal = $this->attribute_decimal;
            $attribute->default = $this->attribute_default;
            $attribute->view_table = $this->attribute_view_table ? '1' : '0';
            $attribute->view_create = $this->attribute_view_create ? '1' : '0';
            $entity->attributes()->save($attribute);

            $this->alertSuccessMessage('Attribute created.');
            //$this->closeAttributeModalPopover();
            $this->resetAttributeCreateForm();
        } else {
            Attribute::updateOrCreate(['id' => $this->attribute_id], [
                'field_name' => $this->attribute_field_name,
                'nullable' => $this->attribute_nullable ? '1' : '0',
                'title' => $this->attribute_title,
                'type' => $this->attribute_type,
                'length' => $this->attribute_length,
                'precision' => $this->attribute_precision,
                'scale' => $this->attribute_scale,
                'digits' => $this->attribute_digits,
                'decimal' => $this->attribute_decimal,
                'default' => $this->attribute_default,
                'view_table' => $this->attribute_view_table ? '1' : '0',
                'view_create' => $this->attribute_view_create ? '1' : '0',
            ]);

            $this->alertSuccessMessage('Attribute updated.');
        }

        $this->recent_attributes = $entity->attributes()->get();
    }

    public function deleteAttribute($entity_id, $attribute_id) {
        $entity = Entity::findOrFail($entity_id);

        $attribute = Attribute::find($attribute_id);
        if ($attribute) {
            $entity->attributes()->where('id', '=', $attribute->id)->delete();
        }

        $this->recent_attributes = $entity->attributes()->get();

        $this->alertSuccessMessage('Attribute deleted.');
    }

    public function deleteExistingAttribute($attribute_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $attribute = Attribute::find($attribute_id);
        if ($attribute) {
            $entity->attributes()->where('id', '=', $attribute->id)->delete();
        }

        $this->recent_attributes = $entity->attributes()->get();

        $this->alertSuccessMessage('Attribute deleted.');
    }
    // End of Section

    // Upload Section
    use WithFileUploads;
    public $isUploadModalOpen = 0;
    public $files = [];
    public function addUpload($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_uploads = $entity->uploads()->get();
        $this->resetUploadCreateForm();
        $this->openUploadModalPopover();
    }

    public function openUploadModalPopover()
    {
        $this->isUploadModalOpen = true;
    }
    public function closeUploadModalPopover()
    {
        $this->isUploadModalOpen = false;
    }
    private function resetUploadCreateForm(){
        $this->files = [];
    }

    public function assignUpload() {
        $this->validate([
            'files.*' => 'max:309600', // 1MB Max
        ]);

        $entity = Entity::findOrFail($this->entity_id);

        foreach ($this->files as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload();
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $entity->uploads()->save($upload);
        }

        $this->recent_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        // $this->closeUploadModalPopover();
        $this->resetUploadCreateForm();
    }

    public function deleteUpload($entity_id, $upload_id) {
        $entity = Entity::findOrFail($entity_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $entity->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingUpload($upload_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $entity->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // GenerateCRUD Section
    use Generator;
    public function generateCRUD($entity_id) {
        $entity = Entity::findOrFail($entity_id);
        if ($entity) {
            // Add Navigation
            $feature = $entity->entitiable;
            $application = $feature->featureable;
            $entity_properties = $this->getWording(strtolower($entity->title));

            $group = NavigationGroup::where('name','Management')->first();
            if ($group == null) {
                $group = new NavigationGroup();
                $group->name = 'Management';
                $group->summary = '';
                $group->weight = '1000000';
                $group->save();
            }

            Navigation::updateOrCreate(['id' => ''], [
                'vendor_id' => 0,
                'group' => $group->id,
                'title' => $entity->title,
                'controller' => '\\App\\Http\\Livewire\\'.$entity_properties['plural'],
                'function' => '',
                'route' => $entity_properties['plural_lowercase'],
                'parent_id' => 0,
                'is_show' => 1,
                'is_enable' => 1,
                'application_id' => $application->id,

            ]);

            // Generate CRUD
            $attributes = $entity->attributes()->select('field_name')->get()->pluck('field_name')->toArray();
            $fields = implode(',',$attributes);

            $attributes = $entity->attributes()->get();
            foreach ($attributes as $attribute) {
                $field_attributes[$attribute->field_name] = $attribute->toArray();
            }

            Artisan::call('generate:crud',[
                '--no-ask' => true,
                '--entity' => strtolower($entity->title),
                '--entity_id' => $entity->id,
                '--name' => $entity->title,
                '--fields' => $fields,
                '--field_properties' => json_encode($field_attributes)
            ]);
            $this->alertSuccessMessage(Artisan::output());
        }
    }
    // End of Section

    // Generate Permission Section
    public function generatePermission($entity_id) {
        $entity = Entity::findOrFail($entity_id);
        if ($entity) {
            $this->generateEntityPermission($entity_id);
            $this->alertSuccessMessage('Permission '.$entity->title.' updated.');
        }
    }
    // End of Section


    // EntityAction Section
    public $isEntityActionModalOpen = 0;
    public $recent_entity_actions = [];
    public $entity_actions = [];
    public function addEntityAction($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_entity_actions = $entity->entity_actions()->get();
        $this->resetEntityActionCreateForm();
        $this->openEntityActionModalPopover();
    }

    public function openEntityActionModalPopover()
    {
        $this->isEntityActionModalOpen = true;
    }
    public function closeEntityActionModalPopover()
    {
        $this->isEntityActionModalOpen = false;
    }

    public function editEntityAction($entity_action_id) {
        $entity_action = EntityAction::findOrFail($entity_action_id);
        $this->entity_action_id = $entity_action_id;
        $this->entity_action_title = $entity_action->title;
        $this->entity_action_summary = $entity_action->summary;
        $this->entity_action_body_format = $entity_action->body_format;
        $this->entity_action_body = $entity_action->body;
        $this->entity_action_actionable = $entity_action->actionable;
        $this->entity_action_parameters = $entity_action->parameters;
        $this->entity_action_method = $entity_action->method;
        $this->entity_action_tags = $entity_action->tags;
        $this->entity_action_responses = $entity_action->responses;

    }

    public function resetEntityActionCreateForm(){
        $this->entity_action_id = '';
        $this->entity_action_title = '';
        $this->entity_action_summary = '';
        $this->entity_action_body_format = 'markdown';
        $this->entity_action_body = '';
        $this->entity_action_actionable = '';
        $this->entity_action_parameters = 'username|path';
        $this->entity_action_method = 'POST';
        $this->entity_action_tags = '';
        $this->entity_action_responses = '200|Success,400|Has Error';

    }

    public function assignEntityAction() {
        $entity = Entity::findOrFail($this->entity_id);
        if ($this->entity_action_id == '') {
            $entity_action = new EntityAction();
            $entity_action->title = $this->entity_action_title;
            $entity_action->summary = $this->entity_action_summary;
            $entity_action->body_format = $this->entity_action_body_format;
            $entity_action->body = $this->entity_action_body;
            $entity_action->parameters = $this->entity_action_parameters;
            $entity_action->method = $this->entity_action_method;
            $entity_action->tags = $this->entity_action_tags;
            $entity_action->responses = $this->entity_action_responses;

            $entity->entity_actions()->save($entity_action);

            $this->alertSuccessMessage('EntityAction created.');
            //$this->closeEntityActionModalPopover();
            $this->resetEntityActionCreateForm();
        } else {
            EntityAction::updateOrCreate(['id' => $this->entity_action_id], [
                'title' => $this->entity_action_title,
                'summary' => $this->entity_action_summary,
                'body_format' => $this->entity_action_body_format,
                'body' => $this->entity_action_body,
                'parameters' => $this->entity_action_parameters,
                'method' => $this->entity_action_method,
                'tags' => $this->entity_action_tags,
                'responses' => $this->entity_action_responses,

            ]);

            $this->alertSuccessMessage('EntityAction updated.');
        }

        $this->recent_entity_actions = $entity->entity_actions()->get();
    }

    public function deleteEntityAction($entity_id, $entity_action_id) {
        $entity = Entity::findOrFail($entity_id);

        $entity_action = EntityAction::find($entity_action_id);
        if ($entity_action) {
            $entity->entity_actions()->where('id', '=', $entity_action->id)->delete();
        }

        $this->recent_entity_actions = $entity->entity_actions()->get();

        $this->alertSuccessMessage('EntityAction deleted.');
    }

    public function deleteExistingEntityAction($entity_action_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $entity_action = EntityAction::find($entity_action_id);
        if ($entity_action) {
            $entity->entity_actions()->where('id', '=', $entity_action->id)->delete();
        }

        $this->recent_entity_actions = $entity->entity_actions()->get();

        $this->alertSuccessMessage('EntityAction deleted.');
    }
    // End of Section

    // Sop Section
    public $isSopModalOpen = 0;
    public $recent_sops = [];
    public $sops = [];
    public function addSop($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_sops = $entity->sops()->get();
        $this->resetSopCreateForm();
        $this->openSopModalPopover();
    }

    public function openSopModalPopover()
    {
        $this->isSopModalOpen = true;
    }
    public function closeSopModalPopover()
    {
        $this->isSopModalOpen = false;
    }

    public function editSop($sop_id) {
        $sop = Sop::findOrFail($sop_id);
        $this->sop_id = $sop_id;
        $this->sop_title = $sop->title;
        $this->sop_summary = $sop->summary;
        $this->sop_body_format = $sop->body_format;
        $this->sop_body = $sop->body;
        $this->sop_sopable = $sop->sopable;

    }

    public function resetSopCreateForm(){
        $this->sop_id = '';
        $this->sop_title = '';
        $this->sop_summary = '';
        $this->sop_body_format = 'markdown';
        $this->sop_body = '';
        $this->sop_sopable = '';

    }

    public function assignSop() {
        $entity = Entity::findOrFail($this->entity_id);
        if ($this->sop_id == '') {
            $sop = new Sop();
            $sop->title = $this->sop_title;
            $sop->summary = $this->sop_summary;
            $sop->body_format = $this->sop_body_format;
            $sop->body = $this->sop_body;

            $entity->sops()->save($sop);

            $this->alertSuccessMessage('Sop created.');
            //$this->closeSopModalPopover();
            $this->resetSopCreateForm();
        } else {
            Sop::updateOrCreate(['id' => $this->sop_id], [
                'title' => $this->sop_title,
                'summary' => $this->sop_summary,
                'body_format' => $this->sop_body_format,
                'body' => $this->sop_body,

            ]);

            $this->alertSuccessMessage('Sop updated.');

            $c = $this->sop_body;
            $this->sop_body = "";
            $this->sop_body = $c;
        }

        $this->recent_sops = $entity->sops()->get();
    }

    public function deleteSop($entity_id, $sop_id) {
        $entity = Entity::findOrFail($entity_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $entity->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $entity->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }

    public function deleteExistingSop($sop_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $entity->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $entity->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }
    // End of Section
}
