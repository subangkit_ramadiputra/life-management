<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationGoal;

class ApplicationGoals extends Component
{
    use AlertMessageTrait;

    public $application_goals, $application_goal_id;
    public $title;
    public $summary;
    public $detail;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_goals = ApplicationGoal::all();
        $this->total_rows = ApplicationGoal::count();
        return view('livewire.application_goals');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_goal_id = '';
        $this->title = '';
        $this->summary = '';
        $this->detail = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'detail' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationGoal::updateOrCreate(['id' => $this->application_goal_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'detail' => $this->detail,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_goal_id ? 'ApplicationGoal updated.' : 'ApplicationGoal created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_goal = ApplicationGoal::findOrFail($id);
        $this->application_goal_id = $id;
        $this->title = $application_goal->title;
        $this->summary = $application_goal->summary;
        $this->detail = $application_goal->detail;
        $this->application_id = $application_goal->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationGoal::find($id)->delete();
        $this->alertSuccessMessage('ApplicationGoal deleted.');
    }
}
