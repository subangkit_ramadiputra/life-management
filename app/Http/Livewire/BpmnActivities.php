<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnActivity;

class BpmnActivities extends Component
{
    use AlertMessageTrait;

    public $bpmn_activities, $bpmn_activity_id;
    public $type_id;
    public $code;
    public $name;
    public $summary;
    public $activitiable;
    public $role_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_activities = BpmnActivity::all();
        $this->total_rows = BpmnActivity::count();
        return view('livewire.bpmn_activities');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_activity_id = '';
        $this->type_id = '';
        $this->code = '';
        $this->name = '';
        $this->summary = '';
        $this->activitiable = '';
        $this->role_id = '';

    }

    public function store()
    {
        $this->validate([
            'type_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'activitiable' => 'required',
            'role_id' => 'required',

        ]);

        BpmnActivity::updateOrCreate(['id' => $this->bpmn_activity_id], [
            'type_id' => $this->type_id,
            'code' => $this->code,
            'name' => $this->name,
            'summary' => $this->summary,
            'activitiable' => $this->activitiable,
            'role_id' => $this->role_id,

        ]);

        $this->alertSuccessMessage($this->bpmn_activity_id ? 'BpmnActivity updated.' : 'BpmnActivity created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_activity = BpmnActivity::findOrFail($id);
        $this->bpmn_activity_id = $id;
        $this->type_id = $bpmn_activity->type_id;
        $this->code = $bpmn_activity->code;
        $this->name = $bpmn_activity->name;
        $this->summary = $bpmn_activity->summary;
        $this->activitiable = $bpmn_activity->activitiable;
        $this->role_id = $bpmn_activity->role_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnActivity::find($id)->delete();
        $this->alertSuccessMessage('BpmnActivity deleted.');
    }
}
