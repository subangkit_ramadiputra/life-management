<?php

namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;

use Illuminate\Support\Facades\URL;
use Livewire\Component;

class Sidebar extends Component
{
    use AlertMessageTrait;

    public $segments;
    public $page;
    public function render()
    {
        $url = parse_url(URL::current());
        try {
            $path = $url['path'];
        } catch (\Exception $e) {
            $path = '/';
        }
        $this->segments = explode("/",$path);
        $this->page = $this->segments[1];
        return view('livewire.sidebar');
    }
}
