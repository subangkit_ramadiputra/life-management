<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Page;

class Pages extends Component
{
    use AlertMessageTrait;

    public $pages, $page_id;
    public $title;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->pages = Page::all();
        $this->total_rows = Page::count();
        return view('livewire.pages');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->page_id = '';
        $this->title = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',

        ]);

        Page::updateOrCreate(['id' => $this->page_id], [
            'title' => $this->title,

        ]);
        $this->alertSuccessMessage($this->page_id ? 'Page updated.' : 'Page created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $this->page_id = $id;
        $this->title = $page->title;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Page::find($id)->delete();
        $this->alertSuccessMessage('Page deleted.');
    }
}
