<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Server;

class Servers extends Component
{
    use AlertMessageTrait;

    public $servers, $server_id;
    public $stage;
    public $title;
    public $ip_address;
    public $provider;
    public $summary;
    public $body;
    public $body_format;
    public $user_access;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->servers = Server::all();
        $this->total_rows = Server::count();
        return view('livewire.servers');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->server_id = '';
        $this->stage = 'production';
        $this->title = '';
        $this->ip_address = '';
        $this->provider = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';
        $this->user_access = '';

    }

    public function store()
    {
        $this->validate([
            'stage' => 'required',
            'title' => 'required',
            'provider' => 'required',
            'body_format' => 'required',
            'user_access' => 'required',

        ]);

        Server::updateOrCreate(['id' => $this->server_id], [
            'stage' => $this->stage,
            'title' => $this->title,
            'ip_address' => $this->ip_address,
            'provider' => $this->provider,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'user_access' => $this->user_access,

        ]);
        $this->alertSuccessMessage($this->server_id ? 'Server updated.' : 'Server created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $server = Server::findOrFail($id);
        $this->server_id = $id;
        $this->stage = $server->stage;
        $this->title = $server->title;
        $this->ip_address = $server->ip_address;
        $this->provider = $server->provider;
        $this->summary = $server->summary;
        $this->body = $server->body;
        $this->body_format = $server->body_format;
        $this->user_access = $server->user_access;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Server::find($id)->delete();
        $this->alertSuccessMessage('Server deleted.');
    }
}
