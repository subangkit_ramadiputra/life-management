<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\KeyPerformanceIndex;

class KeyPerformanceIndices extends Component
{
    use AlertMessageTrait;

    public $key_performance_indices, $key_performance_index_id;
    public $title;
    public $summary;
    public $indicator;
    public $target;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->key_performance_indices = KeyPerformanceIndex::all();
        $this->total_rows = KeyPerformanceIndex::count();
        return view('livewire.key_performance_indices');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->key_performance_index_id = '';
        $this->title = '';
        $this->summary = '';
        $this->indicator = '';
        $this->target = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'indicator' => 'required',
            'target' => 'required',

        ]);

        KeyPerformanceIndex::updateOrCreate(['id' => $this->key_performance_index_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'indicator' => $this->indicator,
            'target' => $this->target,

        ]);

        $this->alertSuccessMessage($this->key_performance_index_id ? 'KeyPerformanceIndex updated.' : 'KeyPerformanceIndex created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $key_performance_index = KeyPerformanceIndex::findOrFail($id);
        $this->key_performance_index_id = $id;
        $this->title = $key_performance_index->title;
        $this->summary = $key_performance_index->summary;
        $this->indicator = $key_performance_index->indicator;
        $this->target = $key_performance_index->target;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        KeyPerformanceIndex::find($id)->delete();
        $this->alertSuccessMessage('KeyPerformanceIndex deleted.');
    }
}
