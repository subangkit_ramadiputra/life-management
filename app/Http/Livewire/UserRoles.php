<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\UserRole;

class UserRoles extends Component
{
    use AlertMessageTrait;

    public $user_roles, $user_role_id;
    public $user_id;
    public $role_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->user_roles = UserRole::all();
        $this->total_rows = UserRole::count();
        return view('livewire.user_roles');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->user_role_id = '';
        $this->user_id = '';
        $this->role_id = '';

    }

    public function store()
    {
        $this->validate([
            'user_id' => 'required',
            'role_id' => 'required',

        ]);

        UserRole::updateOrCreate(['id' => $this->user_role_id], [
            'user_id' => $this->user_id,
            'role_id' => $this->role_id,

        ]);
        $this->alertSuccessMessage($this->user_role_id ? 'UserRole updated.' : 'UserRole created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $user_role = UserRole::findOrFail($id);
        $this->user_role_id = $id;
        $this->user_id = $user_role->user_id;
        $this->role_id = $user_role->role_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        UserRole::find($id)->delete();
        $this->alertSuccessMessage('UserRole deleted.');
    }
}
