<?php

namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;

use Livewire\Component;

class Welcome extends Component
{
    use AlertMessageTrait;

    public function render()
    {
        return view('livewire.welcome')->layout('layouts.app_portal');
    }
}
