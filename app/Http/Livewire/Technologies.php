<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Technology;

class Technologies extends Component
{
    use AlertMessageTrait;

    public $technologies, $technology_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;
    public $git_template;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->technologies = Technology::all();
        $this->total_rows = Technology::count();
        return view('livewire.technologies');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->technology_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';
        $this->git_template = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',

        ]);

        Technology::updateOrCreate(['id' => $this->technology_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'git_template' => $this->git_template,

        ]);

        $this->alertSuccessMessage($this->technology_id ? 'Technology updated.' : 'Technology created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $technology = Technology::findOrFail($id);
        $this->technology_id = $id;
        $this->title = $technology->title;
        $this->summary = $technology->summary;
        $this->body_format = $technology->body_format;
        $this->body = $technology->body;
        $this->git_template = $technology->git_template;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Technology::find($id)->delete();
        $this->alertSuccessMessage('Technology deleted.');
    }
}
