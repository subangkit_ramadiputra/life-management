<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Quadrant;

class Quadrants extends Component
{
    use AlertMessageTrait;

    public $quadrants, $quadrant_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->quadrants = Quadrant::all();
        $this->total_rows = Quadrant::count();
        return view('livewire.quadrants');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->quadrant_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',

        ]);

        Quadrant::updateOrCreate(['id' => $this->quadrant_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,

        ]);

        $this->alertSuccessMessage($this->quadrant_id ? 'Quadrant updated.' : 'Quadrant created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $quadrant = Quadrant::findOrFail($id);
        $this->quadrant_id = $id;
        $this->title = $quadrant->title;
        $this->summary = $quadrant->summary;
        $this->body_format = $quadrant->body_format;
        $this->body = $quadrant->body;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Quadrant::find($id)->delete();
        $this->alertSuccessMessage('Quadrant deleted.');
    }
}
