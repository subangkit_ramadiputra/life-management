<?php
namespace App\Http\Livewire;
use App\Models\BpmnActivityType;
use App\Models\BpmnEventType;
use App\Models\BpmnGatewayType;
use App\Traits\AlertMessageTrait;
use App\Traits\SopBpmnTrait;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use App\Models\Sop;
use App\Models\BpmnRole;
use App\Models\BpmnEvent;
use App\Models\BpmnActivity;
use App\Models\BpmnGateway;
use App\Models\BpmnSquence;

class Sops extends Component
{
    use AlertMessageTrait, SopBpmnTrait;

    protected $queryString = ['filter_polymorph_id','filter_polymorph_object'];

    public $sops, $sop_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;


    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->filterResult();
        return view('livewire.sops');
    }

    public $filter_polymorph_id = 0;
    public $filter_polymorph_object = '';
    public $sopable_type = '';
    public $sopable_id = 0;
    public function filterResult() {
        $query = Sop::select('*');
        if ($this->filter_polymorph_object != '') {
            $this->sopable_type = 'App\\Models\\'.$this->filter_polymorph_object;
            $this->sopable_id = $this->filter_polymorph_id;
            $query->where('sopable_type','App\\Models\\'.$this->filter_polymorph_object);
            $query->where('sopable_id', $this->filter_polymorph_id);
        }

        $this->sops = $query->get();
        $this->total_rows = $query->count();
    }

    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->sop_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',

        ]);

        Sop::updateOrCreate(['id' => $this->sop_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'sopable_type' => $this->sopable_type,
            'sopable_id' => $this->sopable_id,

        ]);

        $this->alertSuccessMessage($this->sop_id ? 'Sop updated.' : 'Sop created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $sop = Sop::findOrFail($id);
        $this->sop_id = $id;
        $this->title = $sop->title;
        $this->summary = $sop->summary;
        $this->body_format = $sop->body_format;
        $this->body = $sop->body;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Sop::find($id)->delete();
        $this->alertSuccessMessage('Sop deleted.');
    }


}
