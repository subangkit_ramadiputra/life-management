<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ContextMonitoring;

class ContextMonitorings extends Component
{
    use AlertMessageTrait;

    public $context_monitorings, $context_monitoring_id;
    public $title;
    public $summary;
    public $progress;
    public $strategy;
    public $parent_id;

    public $isModalOpen = 0;
    public $total_rows = 0;
    public $context_monitor_list = [];

    public function render()
    {
        $this->context_monitor_list = ContextMonitoring::all();
        $this->context_monitorings = ContextMonitoring::all();
        $this->total_rows = ContextMonitoring::count();
        return view('livewire.context_monitorings');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->context_monitoring_id = '';
        $this->title = '';
        $this->summary = '';
        $this->progress = '0.0';
        $this->strategy = '';
        $this->parent_id = '0';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'progress' => 'required',
            'parent_id' => 'required',

        ]);

        ContextMonitoring::updateOrCreate(['id' => $this->context_monitoring_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'progress' => $this->progress,
            'strategy' => $this->strategy,
            'parent_id' => $this->parent_id,

        ]);

        $this->alertSuccessMessage($this->context_monitoring_id ? 'ContextMonitoring updated.' : 'ContextMonitoring created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $context_monitoring = ContextMonitoring::findOrFail($id);
        $this->context_monitoring_id = $id;
        $this->title = $context_monitoring->title;
        $this->summary = $context_monitoring->summary;
        $this->progress = $context_monitoring->progress;
        $this->strategy = $context_monitoring->strategy;
        $this->parent_id = $context_monitoring->parent_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ContextMonitoring::find($id)->delete();
        $this->alertSuccessMessage('ContextMonitoring deleted.');
    }
}
