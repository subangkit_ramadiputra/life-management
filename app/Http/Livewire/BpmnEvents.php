<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnEvent;

class BpmnEvents extends Component
{
    use AlertMessageTrait;

    public $bpmn_events, $bpmn_event_id;
    public $type_id;
    public $code;
    public $name;
    public $summary;
    public $eventable;
    public $role_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_events = BpmnEvent::all();
        $this->total_rows = BpmnEvent::count();
        return view('livewire.bpmn_events');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_event_id = '';
        $this->type_id = '';
        $this->code = '';
        $this->name = '';
        $this->summary = '';
        $this->eventable = '';
        $this->role_id = '';

    }

    public function store()
    {
        $this->validate([
            'type_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'eventable' => 'required',
            'role_id' => 'required',

        ]);

        BpmnEvent::updateOrCreate(['id' => $this->bpmn_event_id], [
            'type_id' => $this->type_id,
            'code' => $this->code,
            'name' => $this->name,
            'summary' => $this->summary,
            'eventable' => $this->eventable,
            'role_id' => $this->role_id,

        ]);

        $this->alertSuccessMessage($this->bpmn_event_id ? 'BpmnEvent updated.' : 'BpmnEvent created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_event = BpmnEvent::findOrFail($id);
        $this->bpmn_event_id = $id;
        $this->type_id = $bpmn_event->type_id;
        $this->code = $bpmn_event->code;
        $this->name = $bpmn_event->name;
        $this->summary = $bpmn_event->summary;
        $this->eventable = $bpmn_event->eventable;
        $this->role_id = $bpmn_event->role_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnEvent::find($id)->delete();
        $this->alertSuccessMessage('BpmnEvent deleted.');
    }
}
