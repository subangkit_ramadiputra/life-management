<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationComponent;

class ApplicationComponents extends Component
{
    use AlertMessageTrait;

    public $application_components, $application_component_id;
    public $column_num;
    public $row_num;
    public $cell_composition;
    public $componentable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_components = ApplicationComponent::all();
        $this->total_rows = ApplicationComponent::count();
        return view('livewire.application_components');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_component_id = '';
        $this->column_num = '1';
        $this->row_num = '1';
        $this->cell_composition = '';
        $this->componentable = '';

    }

    public function store()
    {
        $this->validate([
            'column_num' => 'required',
            'row_num' => 'required',
            'cell_composition' => 'required',
            'componentable' => 'required',

        ]);

        ApplicationComponent::updateOrCreate(['id' => $this->application_component_id], [
            'column_num' => $this->column_num,
            'row_num' => $this->row_num,
            'cell_composition' => $this->cell_composition,
            'componentable' => $this->componentable,

        ]);

        $this->alertSuccessMessage($this->application_component_id ? 'ApplicationComponent updated.' : 'ApplicationComponent created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_component = ApplicationComponent::findOrFail($id);
        $this->application_component_id = $id;
        $this->column_num = $application_component->column_num;
        $this->row_num = $application_component->row_num;
        $this->cell_composition = $application_component->cell_composition;
        $this->componentable = $application_component->componentable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationComponent::find($id)->delete();
        $this->alertSuccessMessage('ApplicationComponent deleted.');
    }
}
