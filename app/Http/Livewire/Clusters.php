<?php
namespace App\Http\Livewire;
use App\Models\DatabaseType;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Cluster;
use App\Models\DatabaseServer;

class Clusters extends Component
{
    use AlertMessageTrait;


    public $clusters, $cluster_id;
    public $name;
    public $summary;
    public $body_format;
    public $body;
    public $clusterable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    protected $queryString = ['filter_polymorph_id','filter_polymorph_object'];
    public $filter_polymorph_id = 0;
    public $filter_polymorph_object = '';
    public function filterResult() {
        $query = Cluster::select('*');
        if ($this->filter_polymorph_object != '') {
            $this->sopable_type = 'App\\Models\\'.$this->filter_polymorph_object;
            $this->sopable_id = $this->filter_polymorph_id;
            $query->where('clusterable_type','App\\Models\\'.$this->filter_polymorph_object);
            $query->where('clusterable_id', $this->filter_polymorph_id);
        }

        $this->clusters = $query->get();
        $this->total_rows = $query->count();
    }

    public function render()
    {
        $this->filterResult();
        return view('livewire.clusters');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->cluster_id = '';
        $this->name = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';
        $this->clusterable = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'body_format' => 'required',
            'body' => 'required',
        ]);

        $clusterable_type = '';
        $clusterable_id = '';

        if ($this->filter_polymorph_id != 0) {
            $clusterable_type = "App\\Models\\".$this->filter_polymorph_object;
            $clusterable_id = $this->filter_polymorph_id;
        }

        Cluster::updateOrCreate(['id' => $this->cluster_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'clusterable_type' => $clusterable_type,
            'clusterable_id' => $clusterable_id,

        ]);

        $this->alertSuccessMessage($this->cluster_id ? 'Cluster updated.' : 'Cluster created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $cluster = Cluster::findOrFail($id);
        $this->cluster_id = $id;
        $this->name = $cluster->name;
        $this->summary = $cluster->summary;
        $this->body_format = $cluster->body_format;
        $this->body = $cluster->body;
        $this->clusterable = $cluster->clusterable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Cluster::find($id)->delete();
        $this->alertSuccessMessage('Cluster deleted.');
    }

    // DatabaseServer Section
    public $isDatabaseServerModalOpen = 0;
    public $recent_database_servers = [];
    public $database_servers = [];
    public $database_types = [];
    public function addDatabaseServer($id) {
        $this->cluster_id = $id;
        $cluster = Cluster::findOrFail($this->cluster_id);
        $this->database_types = DatabaseType::all();
        $this->recent_database_servers = $cluster->database_servers()->get();
        $this->resetDatabaseServerCreateForm();
        $this->openDatabaseServerModalPopover();
    }

    public function openDatabaseServerModalPopover()
    {
        $this->isDatabaseServerModalOpen = true;
    }
    public function closeDatabaseServerModalPopover()
    {
        $this->isDatabaseServerModalOpen = false;
    }

    public function editDatabaseServer($database_server_id) {
        $database_server = DatabaseServer::findOrFail($database_server_id);
        $this->database_server_id = $database_server_id;
        $this->database_server_title = $database_server->title;
        $this->database_server_summary = $database_server->summary;
        $this->database_server_type_id = $database_server->type_id;
        $this->database_server_databaseable = $database_server->databaseable;

    }

    public function resetDatabaseServerCreateForm(){
        $this->database_server_id = '';
        $this->database_server_title = '';
        $this->database_server_summary = '';
        $this->database_server_type_id = '';
        $this->database_server_databaseable = '';

    }

    public function assignDatabaseServer() {
        $cluster = Cluster::findOrFail($this->cluster_id);
        if ($this->database_server_id == '') {
            $database_server = new DatabaseServer();
            $database_server->title = $this->database_server_title;
            $database_server->summary = $this->database_server_summary;
            $database_server->type_id = $this->database_server_type_id;

            $cluster->database_servers()->save($database_server);

            $this->alertSuccessMessage('DatabaseServer created.');
            //$this->closeDatabaseServerModalPopover();
            $this->resetDatabaseServerCreateForm();
        } else {
            DatabaseServer::updateOrCreate(['id' => $this->database_server_id], [
                'title' => $this->database_server_title,
                'summary' => $this->database_server_summary,
                'type_id' => $this->database_server_type_id,

            ]);
            $this->alertSuccessMessage('DatabaseServer updated.');
        }

        $this->recent_database_servers = $cluster->database_servers()->get();
    }

    public function deleteDatabaseServer($cluster_id, $database_server_id) {
        $cluster = Cluster::findOrFail($cluster_id);

        $database_server = DatabaseServer::find($database_server_id);
        if ($database_server) {
            $cluster->database_servers()->where('id', '=', $database_server->id)->delete();
        }

        $this->recent_database_servers = $cluster->database_servers()->get();

        $this->alertSuccessMessage('DatabaseServer deleted.');
    }

    public function deleteExistingDatabaseServer($database_server_id) {
        $cluster = Cluster::findOrFail($this->cluster_id);

        $database_server = DatabaseServer::find($database_server_id);
        if ($database_server) {
            $cluster->database_servers()->where('id', '=', $database_server->id)->delete();
        }

        $this->recent_database_servers = $cluster->database_servers()->get();

        $this->alertSuccessMessage('DatabaseServer deleted.');
    }
    // End of Section
}
