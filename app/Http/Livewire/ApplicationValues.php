<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationValue;

class ApplicationValues extends Component
{
    use AlertMessageTrait;

    public $application_values, $application_value_id;
    public $title;
    public $summary;
    public $detail;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_values = ApplicationValue::all();
        $this->total_rows = ApplicationValue::count();
        return view('livewire.application_values');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_value_id = '';
        $this->title = '';
        $this->summary = '';
        $this->detail = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationValue::updateOrCreate(['id' => $this->application_value_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'detail' => $this->detail,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_value_id ? 'ApplicationValue updated.' : 'ApplicationValue created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_value = ApplicationValue::findOrFail($id);
        $this->application_value_id = $id;
        $this->title = $application_value->title;
        $this->summary = $application_value->summary;
        $this->detail = $application_value->detail;
        $this->application_id = $application_value->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationValue::find($id)->delete();
        $this->alertSuccessMessage('ApplicationValue deleted.');
    }
}
