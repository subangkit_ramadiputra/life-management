<?php
namespace App\Http\Livewire;
use App\Models\ApplicationConcept;
use App\Models\AppTech;
use App\Models\Quadrant;
use App\Models\Task;
use App\Models\Technology;
use App\Traits\AlertMessageTrait;
use App\Models\Concept;
use App\Models\Feature;
use App\Models\Server;
use App\Models\Upload;
use App\Models\Stage;
use App\Models\Role;
use App\Models\Link;
use App\Models\Note;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\Application;
use Livewire\WithFileUploads;

class Applications extends Component
{
    use AlertMessageTrait;

    protected $queryString = ['filter_concept_id'];

    public $applications, $application_id;
    public $name;
    public $summary;
    public $concept_id;
    public $programming_language;
    public $total_rows = 0;

    public $isModalOpen = 0;

    public $filter_concept_id = 0;

    // Relation
    public $concepts;

    public function render()
    {
        $this->changeFilter();

        // Relation
        $this->concepts = Concept::all();

        return view('livewire.applications');
    }

    // Filter Section
    public function changeFilter() {
        if ($this->filter_concept_id != 0) {
            $query = Application::where('concept_id', '=', $this->filter_concept_id);
            $this->applications = $query->get();
            $this->total_rows = $query->count();
        } else {
            $this->applications = Application::all();
            $this->total_rows = Application::count();
        }
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function fetchConcepts() {
            $this->concepts = Concept::all();
    }
    public function openModalPopover()
    {
        $this->fetchConcepts();
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_id = '';
        $this->name = '';
        $this->summary = '';
        $this->concept_id = 0;
        if ($this->filter_concept_id != 0) {
            $this->concept_id = $this->filter_concept_id;
        }
        $this->programming_language = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',

        ]);

        Application::updateOrCreate(['id' => $this->application_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'concept_id' => $this->concept_id,
            'programming_language' => $this->programming_language,

        ]);
        $this->alertSuccessMessage($this->application_id ? 'Application updated.' : 'Application created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $this->fetchConcepts();
        $application = Application::findOrFail($id);
        $this->application_id = $id;
        $this->name = $application->name;
        $this->summary = $application->summary;
        $this->concept_id = $application->concept_id;
        $this->programming_language = $application->programming_language;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Application::find($id)->delete();
        $this->alertSuccessMessage('Application deleted.');
    }


    // Feature Section
    public $isFeatureModalOpen = 0;
    public $recent_features = [];
    public $features = [];
    public function addFeature($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_features = $application->features()->get();
        $this->resetFeatureCreateForm();
        $this->openFeatureModalPopover();
    }

    public function openFeatureModalPopover()
    {
        $this->isFeatureModalOpen = true;
    }
    public function closeFeatureModalPopover()
    {
        $this->isFeatureModalOpen = false;
    }

    public function editFeature($feature_id) {
        $feature = Feature::findOrFail($feature_id);
        $this->feature_id = $feature_id;
        $this->feature_title = $feature->title;
        $this->feature_summary = $feature->summary;
        $this->feature_body = $feature->body;
        $this->feature_body_format = $feature->body_format;
    }

    public function resetFeatureCreateForm(){
        $this->feature_id = '';
        $this->feature_title = '';
        $this->feature_summary = '';
        $this->feature_body = '';
        $this->feature_body_format = 'markdown';
    }

    public function assignFeature() {
        $application = Application::findOrFail($this->application_id);
        if ($this->feature_id == '') {
            $feature = new Feature();
            $feature->title = $this->feature_title;
            $feature->summary = $this->feature_summary;
            $feature->body = $this->feature_body;
            $feature->body_format = $this->feature_body_format;
            $application->features()->save($feature);

            $this->alertSuccessMessage('Feature created.');
            //$this->closeFeatureModalPopover();
            $this->resetFeatureCreateForm();
        } else {
            Feature::updateOrCreate(['id' => $this->feature_id], [
                'title' => $this->feature_title,
                'summary' => $this->feature_summary,
                'body' => $this->feature_body,
                'body_format' => $this->feature_body_format,
            ]);
            $this->alertSuccessMessage('Feature updated.');
        }

        $this->recent_features = $application->features()->get();
    }

    public function deleteFeature($application_id, $feature_id) {
        $application = Application::findOrFail($application_id);

        $feature = Feature::find($feature_id);
        if ($feature) {
            $application->features()->where('id', '=', $feature->id)->delete();
        }

        $this->recent_features = $application->features()->get();

        $this->alertSuccessMessage('Feature deleted.');
    }

    public function deleteExistingFeature($feature_id) {
        $application = Application::findOrFail($this->application_id);

        $feature = Feature::find($feature_id);
        if ($feature) {
            $application->features()->where('id', '=', $feature->id)->delete();
        }

        $this->recent_features = $application->features()->get();

        $this->alertSuccessMessage('Feature deleted.');
    }
    // End of Section

    // Feature Upload Section
    use WithFileUploads;
    public $isFeatureUploadModalOpen = 0;
    public $feature_uploads = [];
    public function addFeatureUpload($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_feature_uploads = $feature->uploads()->get();
        $this->resetFeatureUploadCreateForm();
        $this->openFeatureUploadModalPopover();
    }

    public function openFeatureUploadModalPopover()
    {
        $this->isFeatureUploadModalOpen = true;
    }
    public function closeFeatureUploadModalPopover()
    {
        $this->isFeatureUploadModalOpen = false;
    }
    private function resetFeatureUploadCreateForm(){
        $this->feature_uploads = [];
    }

    public function assignFeatureUpload() {
        $this->validate([
            'feature_uploads.*' => 'max:309600', // 1MB Max
        ]);

        $feature = Feature::findOrFail($this->feature_id);

        foreach ($this->feature_uploads as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload();
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $feature->uploads()->save($upload);
        }

        $this->recent_feature_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        $this->closeFeatureUploadModalPopover();
        $this->resetFeatureUploadCreateForm();
    }

    public function deleteFeatureUpload($feature_id, $upload_id) {
        $feature = Feature::findOrFail($feature_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $feature->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_feature_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingFeatureUpload($upload_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $feature->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_feature_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // Stage Section
    public $isStageModalOpen = 0;
    public $recent_stages = [];
    public $stages = [];
    public $servers = [];

    public function addStage($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_stages = $application->stages()->get();
        $this->servers = Server::all();
        $this->resetStageCreateForm();
        $this->openStageModalPopover();
    }

    public function openStageModalPopover()
    {
        $this->isStageModalOpen = true;
    }
    public function closeStageModalPopover()
    {
        $this->isStageModalOpen = false;
    }

    public function editStage($stage_id) {
        $stage = Stage::findOrFail($stage_id);
        $this->stage_id = $stage_id;
        $this->stage_title = $stage->title;
        $this->stage_summary = $stage->summary;
        $this->stage_body_format = $stage->body_format;
        $this->stage_body = $stage->body;
        $this->stage_server_id = $stage->server_id;
        $this->stage_path = $stage->path;
        $this->stage_git_branch = $stage->git_branch;
        $this->stage_cicd_user_access = $stage->cicd_user_access;
        $this->stage_cicd_private_key = $stage->cicd_private_key;
        $this->stage_stage = $stage->stage;
        $this->stage_cicd_format = $stage->cicd_format;
        $this->stage_stageable = $stage->stageable;

    }

    public function resetStageCreateForm(){
        $this->stage_id = '';
        $this->stage_title = '';
        $this->stage_summary = '';
        $this->stage_body_format = 'markdown';
        $this->stage_body = '';
        $this->stage_server_id = '';
        $this->stage_path = '';
        $this->stage_git_branch = '';
        $this->stage_cicd_user_access = '';
        $this->stage_cicd_private_key = '';
        $this->stage_stage = 'development';
        $this->stage_cicd_format = 'lumen';
        $this->stage_stageable = '';

    }

    public function assignStage() {
        $application = Application::findOrFail($this->application_id);
        if ($this->stage_id == '') {
            $stage = new Stage();
            $stage->title = $this->stage_title;
            $stage->summary = $this->stage_summary;
            $stage->body_format = $this->stage_body_format;
            $stage->body = $this->stage_body;
            $stage->server_id = $this->stage_server_id;
            $stage->path = $this->stage_path;
            $stage->git_branch = $this->stage_git_branch;
            $stage->cicd_user_access = $this->stage_cicd_user_access;
            $stage->cicd_private_key = $this->stage_cicd_private_key;
            $stage->stage = $this->stage_stage;
            $stage->cicd_format = $this->stage_cicd_format;

            $application->stages()->save($stage);

            $this->alertSuccessMessage('Stage created.');
            //$this->closeStageModalPopover();
            $this->resetStageCreateForm();
        } else {
            Stage::updateOrCreate(['id' => $this->stage_id], [
                'title' => $this->stage_title,
                'summary' => $this->stage_summary,
                'body_format' => $this->stage_body_format,
                'body' => $this->stage_body,
                'server_id' => $this->stage_server_id,
                'path' => $this->stage_path,
                'git_branch' => $this->stage_git_branch,
                'cicd_user_access' => $this->stage_cicd_user_access,
                'cicd_private_key' => $this->stage_cicd_private_key,
                'stage' => $this->stage_stage,
                'cicd_format' => $this->stage_cicd_format,

            ]);
            $this->alertSuccessMessage('Stage updated.');
        }

        $this->recent_stages = $application->stages()->get();
    }

    public function deleteStage($application_id, $stage_id) {
        $application = Application::findOrFail($application_id);

        $stage = Stage::find($stage_id);
        if ($stage) {
            $application->stages()->where('id', '=', $stage->id)->delete();
        }

        $this->recent_stages = $application->stages()->get();

        $this->alertSuccessMessage('Stage deleted.');
    }

    public function deleteExistingStage($stage_id) {
        $application = Application::findOrFail($this->application_id);

        $stage = Stage::find($stage_id);
        if ($stage) {
            $application->stages()->where('id', '=', $stage->id)->delete();
        }

        $this->recent_stages = $application->stages()->get();

        $this->alertSuccessMessage('Stage deleted.');
    }
    // End of Section

    // GenerateCICD Section
    public function generateCICD($application_id) {
        $application = Application::findOrFail($application_id);
        if ($application) {
            $stages = $application->stages()->select('title')->get()->pluck('title')->toArray();
            $stages = implode(',',$stages);

            $stages = $application->stages()->get();
            $stage_properties = [];
            foreach ($stages as $stage) {
                $stage_properties[$stage->title] = $stage->toArray();
            }

            Artisan::call('generate:cicd',[
                '--application' => strtolower($application->name),
                '--stages' => $stages,
                '--stage_properties' => json_encode($stage_properties)
            ]);

            var_dump(Artisan::output()); exit;
            $this->alertSuccessMessage(Artisan::output());
        }
    }
    // End of Section

    // Role Section
    public $isRoleModalOpen = 0;
    public $recent_roles = [];
    public $roles = [];
    public function addRole($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_roles = $application->roles()->get();
        $this->resetRoleCreateForm();
        $this->openRoleModalPopover();
    }

    public function openRoleModalPopover()
    {
        $this->isRoleModalOpen = true;
    }
    public function closeRoleModalPopover()
    {
        $this->isRoleModalOpen = false;
    }

    public function editRole($role_id) {
        $role = Role::findOrFail($role_id);
        $this->role_id = $role_id;
        $this->role_name = $role->name;
        $this->role_slug = $role->slug;
        $this->role_description = $role->description;
        $this->role_roleable = $role->roleable;

    }

    public function resetRoleCreateForm(){
        $this->role_id = '';
        $this->role_name = '';
        $this->role_slug = '';
        $this->role_description = '';
        $this->role_roleable = '';

    }

    public function assignRole() {
        $application = Application::findOrFail($this->application_id);
        if ($this->role_id == '') {
            $role = new Role();
            $role->name = $this->role_name;
            $role->slug = $this->role_slug;
            $role->description = $this->role_description;

            $application->roles()->save($role);

            $this->alertSuccessMessage('Role created.');
            //$this->closeRoleModalPopover();
            $this->resetRoleCreateForm();
        } else {
            Role::updateOrCreate(['id' => $this->role_id], [
                'name' => $this->role_name,
                'slug' => $this->role_slug,
                'description' => $this->role_description,

            ]);

            $this->alertSuccessMessage('Role updated.');
        }

        $this->recent_roles = $application->roles()->get();
    }

    public function deleteRole($application_id, $role_id) {
        $application = Application::findOrFail($application_id);

        $role = Role::find($role_id);
        if ($role) {
            $application->roles()->where('id', '=', $role->id)->delete();
        }

        $this->recent_roles = $application->roles()->get();

        $this->alertSuccessMessage('Role deleted.');
    }

    public function deleteExistingRole($role_id) {
        $application = Application::findOrFail($this->application_id);

        $role = Role::find($role_id);
        if ($role) {
            $application->roles()->where('id', '=', $role->id)->delete();
        }

        $this->recent_roles = $application->roles()->get();

        $this->alertSuccessMessage('Role deleted.');
    }
    // End of Section

    // Task Section
    public $isTaskModalOpen = 0;
    public $recent_tasks = [];
    public $tasks = [];
    public function addTask($id) {
        $this->application_id = $id;

        $this->application_tasks = Task::all();

        $this->quadrans = Quadrant::all();
        $this->employees = [];
        $this->statuses = [
            'todo' => 'To Do',
            'in_progress' => 'In Progress',
            'done' => 'Done',
        ];
        $application = Application::findOrFail($this->application_id);
        $this->recent_tasks = $application->tasks()->get();
        $this->resetTaskCreateForm();
        $this->openTaskModalPopover();
    }

    public function openTaskModalPopover()
    {
        $this->isTaskModalOpen = true;
    }
    public function closeTaskModalPopover()
    {
        $this->isTaskModalOpen = false;
    }

    public function editTask($task_id) {
        $task = Task::findOrFail($task_id);
        $this->task_id = $task_id;
        $this->task_quadrant_id = $task->quadrant_id;
        $this->task_title = $task->title;
        $this->task_summary = $task->summary;
        $this->task_body = $task->body;
        $this->task_body_format = $task->body_format;
        $this->task_assignee_id = $task->assignee_id;
        $this->task_status = $task->status;
        $this->task_due_date = $task->due_date;
        $this->task_parent_id = $task->parent_id;

    }

    public function resetTaskCreateForm(){
        $this->task_id = '';
        $this->task_quadrant_id = '';
        $this->task_title = '';
        $this->task_summary = '';
        $this->task_body = '';
        $this->task_body_format = 'markdown';
        $this->task_assignee_id = '0';
        $this->task_status = 'todo';
        $this->task_due_date = date('Y-m-d H:i:s');
        $this->task_parent_id = '0';

    }

    public function assignTask() {
        $application = Application::findOrFail($this->application_id);
        if ($this->task_id == '') {
            $task = new Task();
            $task->quadrant_id = $this->task_quadrant_id;
            $task->title = $this->task_title;
            $task->summary = $this->task_summary;
            $task->body = $this->task_body;
            $task->body_format = $this->task_body_format;
            $task->assignee_id = $this->task_assignee_id;
            $task->status = $this->task_status;
            $task->due_date = $this->task_due_date;
            $task->parent_id = $this->task_parent_id;

            $application->tasks()->save($task);

            $this->alertSuccessMessage('Task created.');
            //$this->closeTaskModalPopover();
            $this->resetTaskCreateForm();
        } else {
            Task::updateOrCreate(['id' => $this->task_id], [
                'quadrant_id' => $this->task_quadrant_id,
                'title' => $this->task_title,
                'summary' => $this->task_summary,
                'body' => $this->task_body,
                'body_format' => $this->task_body_format,
                'assignee_id' => $this->task_assignee_id,
                'status' => $this->task_status,
                'due_date' => $this->task_due_date,
                'parent_id' => $this->task_parent_id,

            ]);

            $this->alertSuccessMessage('Task updated.');
        }

        $this->recent_tasks = $application->tasks()->get();
    }

    public function deleteTask($application_id, $task_id) {
        $application = Application::findOrFail($application_id);

        $task = Task::find($task_id);
        if ($task) {
            $application->tasks()->where('id', '=', $task->id)->delete();
        }

        $this->recent_tasks = $application->tasks()->get();

        $this->alertSuccessMessage('Task deleted.');
    }

    public function deleteExistingTask($task_id) {
        $application = Application::findOrFail($this->application_id);

        $task = Task::find($task_id);
        if ($task) {
            $application->tasks()->where('id', '=', $task->id)->delete();
        }

        $this->recent_tasks = $application->tasks()->get();

        $this->alertSuccessMessage('Task deleted.');
    }
    // End of Section

    // AppTech Section
    public $isAppTechModalOpen = 0;
    public $recent_app_techs = [];
    public $app_techs = [];
    public $technologies = [];
    public function addAppTech($id) {
        $this->application_id = $id;
        $this->technologies = Technology::all();
        $application = Application::findOrFail($this->application_id);
        $this->recent_app_techs = $application->app_techs()->get();
        $this->resetAppTechCreateForm();
        $this->openAppTechModalPopover();
    }

    public function openAppTechModalPopover()
    {
        $this->isAppTechModalOpen = true;
    }
    public function closeAppTechModalPopover()
    {
        $this->isAppTechModalOpen = false;
    }

    public function editAppTech($app_tech_id) {
        $app_tech = AppTech::findOrFail($app_tech_id);
        $this->app_tech_id = $app_tech_id;
        $this->app_tech_title = $app_tech->title;
        $this->app_tech_summary = $app_tech->summary;
        $this->app_tech_body_format = $app_tech->body_format;
        $this->app_tech_body = $app_tech->body;
        $this->app_tech_techable = $app_tech->techable;
        $this->app_tech_technology_id = $app_tech->technology_id;
        $this->app_tech_local_path = $app_tech->local_path;
        $this->app_tech_virtual_host = $app_tech->virtual_host;

    }

    public function resetAppTechCreateForm(){
        $this->app_tech_id = '';
        $this->app_tech_title = '';
        $this->app_tech_summary = '';
        $this->app_tech_body_format = 'markdown';
        $this->app_tech_body = '';
        $this->app_tech_techable = '';
        $this->app_tech_technology_id = '';
        $this->app_tech_local_path = '';
        $this->app_tech_virtual_host = '';

    }

    public function assignAppTech() {
        $application = Application::findOrFail($this->application_id);
        if ($this->app_tech_id == '') {
            $app_tech = new AppTech();
            $app_tech->title = $this->app_tech_title;
            $app_tech->summary = $this->app_tech_summary;
            $app_tech->body_format = $this->app_tech_body_format;
            $app_tech->body = $this->app_tech_body;
            $app_tech->technology_id = $this->app_tech_technology_id;
            $app_tech->local_path = $this->app_tech_local_path;
            $app_tech->virtual_host = $this->app_tech_virtual_host;

            $application->app_techs()->save($app_tech);

            $this->alertSuccessMessage('AppTech created.');
            //$this->closeAppTechModalPopover();
            $this->resetAppTechCreateForm();
        } else {
            AppTech::updateOrCreate(['id' => $this->app_tech_id], [
                'title' => $this->app_tech_title,
                'summary' => $this->app_tech_summary,
                'body_format' => $this->app_tech_body_format,
                'body' => $this->app_tech_body,
                'technology_id' => $this->app_tech_technology_id,
                'local_path' => $this->app_tech_local_path,
                'virtual_host' => $this->app_tech_virtual_host,

            ]);

            $this->alertSuccessMessage('AppTech updated.');
        }

        $this->recent_app_techs = $application->app_techs()->get();
    }

    public function deleteAppTech($application_id, $app_tech_id) {
        $application = Application::findOrFail($application_id);

        $app_tech = AppTech::find($app_tech_id);
        if ($app_tech) {
            $application->app_techs()->where('id', '=', $app_tech->id)->delete();
        }

        $this->recent_app_techs = $application->app_techs()->get();

        $this->alertSuccessMessage('AppTech deleted.');
    }

    public function deleteExistingAppTech($app_tech_id) {
        $application = Application::findOrFail($this->application_id);

        $app_tech = AppTech::find($app_tech_id);
        if ($app_tech) {
            $application->app_techs()->where('id', '=', $app_tech->id)->delete();
        }

        $this->recent_app_techs = $application->app_techs()->get();

        $this->alertSuccessMessage('AppTech deleted.');
    }
    // End of Section


    // Link Section
    public $isLinkModalOpen = 0;
    public $recent_links = [];
    public $links = [];
    public function addLink($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_links = $application->links()->get();
        $this->resetLinkCreateForm();
        $this->openLinkModalPopover();
    }

    public function openLinkModalPopover()
    {
        $this->isLinkModalOpen = true;
    }
    public function closeLinkModalPopover()
    {
        $this->isLinkModalOpen = false;
    }

    public function editLink($link_id) {
        $link = Link::findOrFail($link_id);
        $this->link_id = $link_id;
        $this->link_title = $link->title;
        $this->link_url = $link->url;
        $this->link_summary = $link->summary;
        $this->link_linkable = $link->linkable;

    }

    public function resetLinkCreateForm(){
        $this->link_id = '';
        $this->link_title = '';
        $this->link_url = '';
        $this->link_summary = '';
        $this->link_linkable = '';

    }

    public function assignLink() {
        $application = Application::findOrFail($this->application_id);
        if ($this->link_id == '') {
            $link = new Link();
            $link->title = $this->link_title;
            $link->url = $this->link_url;
            $link->summary = $this->link_summary;

            $application->links()->save($link);

            $this->alertSuccessMessage('Link created.');
            //$this->closeLinkModalPopover();
            $this->resetLinkCreateForm();
        } else {
            Link::updateOrCreate(['id' => $this->link_id], [
                'title' => $this->link_title,
                'url' => $this->link_url,
                'summary' => $this->link_summary,

            ]);
            $this->alertSuccessMessage('Link updated.');
        }

        $this->recent_links = $application->links()->get();
    }

    public function deleteLink($application_id, $link_id) {
        $application = Application::findOrFail($application_id);

        $link = Link::find($link_id);
        if ($link) {
            $application->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $application->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }

    public function deleteExistingLink($link_id) {
        $application = Application::findOrFail($this->application_id);

        $link = Link::find($link_id);
        if ($link) {
            $application->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $application->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }
    // End of Section

    // Note Section
    public $isNoteModalOpen = 0;
    public $recent_notes = [];
    public $notes = [];
    public function addNote($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_notes = $application->notes()->get();
        $this->resetNoteCreateForm();
        $this->openNoteModalPopover();
    }

    public function openNoteModalPopover()
    {
        $this->isNoteModalOpen = true;
    }
    public function closeNoteModalPopover()
    {
        $this->isNoteModalOpen = false;
    }

    public function editNote($note_id) {
        $note = Note::findOrFail($note_id);
        $this->note_id = $note_id;
        $this->note_title = $note->title;
        $this->note_body = $note->body;
        $this->note_noteable = $note->noteable;

    }

    public function resetNoteCreateForm(){
        $this->note_id = '';
        $this->note_title = '';
        $this->note_body = '';
        $this->note_noteable = '';

    }

    public function assignNote() {
        $application = Application::findOrFail($this->application_id);
        if ($this->note_id == '') {
            $note = new Note();
            $note->title = $this->note_title;
            $note->body = $this->note_body;

            $application->notes()->save($note);

            $this->alertSuccessMessage('Note created.');
            //$this->closeNoteModalPopover();
            $this->resetNoteCreateForm();
        } else {
            Note::updateOrCreate(['id' => $this->note_id], [
                'title' => $this->note_title,
                'body' => $this->note_body,

            ]);
            $this->alertSuccessMessage('Note updated.');
        }

        $this->recent_notes = $application->notes()->get();
    }

    public function deleteNote($application_id, $note_id) {
        $application = Application::findOrFail($application_id);

        $note = Note::find($note_id);
        if ($note) {
            $application->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $application->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }

    public function deleteExistingNote($note_id) {
        $application = Application::findOrFail($this->application_id);

        $note = Note::find($note_id);
        if ($note) {
            $application->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $application->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }
    // End of Section

    // ApplicationConcept Section
    public $isApplicationConceptModalOpen = 0;
    public $recent_application_concepts = [];
    public $application_concepts = [];
    public function addApplicationConcept($id) {
        $this->application_id = $id;
        $application = Application::findOrFail($this->application_id);
        $this->recent_application_concepts = $application->application_concepts()->get();
        $this->resetApplicationConceptCreateForm();
        $this->openApplicationConceptModalPopover();
    }

    public function openApplicationConceptModalPopover()
    {
        $this->isApplicationConceptModalOpen = true;
    }
    public function closeApplicationConceptModalPopover()
    {
        $this->isApplicationConceptModalOpen = false;
    }

    public function editApplicationConcept($application_concept_id) {
        $application_concept = ApplicationConcept::findOrFail($application_concept_id);
        $this->application_concept_id = $application_concept_id;
        $this->application_concept_name = $application_concept->name;
        $this->application_concept_summary = $application_concept->summary;
        $this->application_concept_conceptable = $application_concept->conceptable;

    }

    public function resetApplicationConceptCreateForm(){
        $this->application_concept_id = '';
        $this->application_concept_name = '';
        $this->application_concept_summary = '';
        $this->application_concept_conceptable = '';

    }

    public function assignApplicationConcept() {
        $application = Application::findOrFail($this->application_id);
        if ($this->application_concept_id == '') {
            $application_concept = new ApplicationConcept();
            $application_concept->name = $this->application_concept_name;
            $application_concept->summary = $this->application_concept_summary;

            $application->application_concepts()->save($application_concept);

            $this->alertSuccessMessage('ApplicationConcept created.');
            //$this->closeApplicationConceptModalPopover();
            $this->resetApplicationConceptCreateForm();
        } else {
            ApplicationConcept::updateOrCreate(['id' => $this->application_concept_id], [
                'name' => $this->application_concept_name,
                'summary' => $this->application_concept_summary,

            ]);
            $this->alertSuccessMessage('ApplicationConcept updated.');
        }

        $this->recent_application_concepts = $application->application_concepts()->get();
    }

    public function deleteApplicationConcept($application_id, $application_concept_id) {
        $application = Application::findOrFail($application_id);

        $application_concept = ApplicationConcept::find($application_concept_id);
        if ($application_concept) {
            $application->application_concepts()->where('id', '=', $application_concept->id)->delete();
        }

        $this->recent_application_concepts = $application->application_concepts()->get();

        $this->alertSuccessMessage('ApplicationConcept deleted.');
    }

    public function deleteExistingApplicationConcept($application_concept_id) {
        $application = Application::findOrFail($this->application_id);

        $application_concept = ApplicationConcept::find($application_concept_id);
        if ($application_concept) {
            $application->application_concepts()->where('id', '=', $application_concept->id)->delete();
        }

        $this->recent_application_concepts = $application->application_concepts()->get();

        $this->alertSuccessMessage('ApplicationConcept deleted.');
    }
    // End of Section
}
