<?php
namespace App\Http\Livewire;
use App\Models\Quadrant;
use App\Models\Vendor;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Task;

class Tasks extends Component
{
    use AlertMessageTrait;

    public $tasks, $task_id;
    public $quadrant_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $assignee_id;
    public $status;
    public $due_date;
    public $taskable;
    public $parent_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public $options = [];

    public function render()
    {
        $this->tasks = Task::all();
        $this->parent_tasks = Task::all();

        $this->quadrans = Quadrant::all();
        $this->employees = [];
        $this->statuses = [
            'todo' => 'To Do',
            'in_progress' => 'In Progress',
            'done' => 'Done',
        ];

        $this->total_rows = Task::count();
        return view('livewire.tasks');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->task_id = '';
        $this->quadrant_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';
        $this->assignee_id = '';
        $this->status = 'todo';
        $this->due_date = '';
        $this->taskable = '';
        $this->parent_id = '0';

    }

    public function store()
    {
        $this->validate([
            'quadrant_id' => 'required',
            'title' => 'required',
            'summary' => 'required',
            'body' => 'required',
            'body_format' => 'required',
            'status' => 'required',
            'due_date' => 'required',

        ]);

        Task::updateOrCreate(['id' => $this->task_id], [
            'quadrant_id' => $this->quadrant_id,
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'assignee_id' => $this->assignee_id,
            'status' => $this->status,
            'due_date' => $this->due_date,
            'taskable_type' => $this->taskable,
            'taskable_id' => $this->taskable,
            'parent_id' => $this->parent_id,

        ]);

        $this->alertSuccessMessage($this->task_id ? 'Task updated.' : 'Task created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $this->task_id = $id;
        $this->quadrant_id = $task->quadrant_id;
        $this->title = $task->title;
        $this->summary = $task->summary;
        $this->body = $task->body;
        $this->body_format = $task->body_format;
        $this->assignee_id = $task->assignee_id;
        $this->status = $task->status;
        $this->due_date = $task->due_date;
        $this->taskable = $task->taskable;
        $this->parent_id = $task->parent_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Task::find($id)->delete();
        $this->alertSuccessMessage('Task deleted.');
    }
}
