<?php
namespace App\Http\Livewire;
use App\Http\Traits\Generator;
use App\Models\Application;
use App\Models\Entity;
use App\Models\Navigation;
use App\Models\NavigationGroup;
use App\Traits\AlertMessageTrait;
use Illuminate\Support\Facades\Artisan;
use Livewire\Component;
use App\Models\AppTech;

class AppTechs extends Component
{
    use AlertMessageTrait;

    public $app_techs, $app_tech_id;
    public $title;
    public $summary;
    public $body_format;
    public $body;
    public $techable;
    public $technology_id;
    public $local_path;
    public $virtual_host;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->app_techs = AppTech::all();
        $this->total_rows = AppTech::count();
        return view('livewire.app_techs');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->app_tech_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body_format = 'markdown';
        $this->body = '';
        $this->techable = '';
        $this->technology_id = '';
        $this->local_path = '';
        $this->virtual_host = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'body' => 'required',
            'techable' => 'required',
            'technology_id' => 'required',

        ]);

        AppTech::updateOrCreate(['id' => $this->app_tech_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body_format' => $this->body_format,
            'body' => $this->body,
            'techable' => $this->techable,
            'technology_id' => $this->technology_id,
            'local_path' => $this->local_path,
            'virtual_host' => $this->virtual_host,

        ]);

        $this->alertSuccessMessage($this->app_tech_id ? 'AppTech updated.' : 'AppTech created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $app_tech = AppTech::findOrFail($id);
        $this->app_tech_id = $id;
        $this->title = $app_tech->title;
        $this->summary = $app_tech->summary;
        $this->body_format = $app_tech->body_format;
        $this->body = $app_tech->body;
        $this->techable = $app_tech->techable;
        $this->technology_id = $app_tech->technology_id;
        $this->local_path = $app_tech->local_path;
        $this->virtual_host = $app_tech->virtual_host;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        AppTech::find($id)->delete();
        $this->alertSuccessMessage('AppTech deleted.');
    }

    // Generate Application Section
    use Generator;
    public function generateApplication($app_tech_id) {
        $app_tech = AppTech::findOrFail($app_tech_id);
        if ($app_tech) {
            // Clone to app folder
            // execute command
            exec("mkdir -p ".$app_tech->local_path, $output);
            $this->alertSuccessMessage('Application Directory Successfully Created');
            // Running Script
        }
    }
    // End of Section
}
