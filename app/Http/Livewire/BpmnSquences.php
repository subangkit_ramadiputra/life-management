<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnSquence;

class BpmnSquences extends Component
{
    use AlertMessageTrait;

    public $bpmn_squences, $bpmn_squence_id;
    public $code;
    public $source;
    public $destination;
    public $summary;
    public $sequenceable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_squences = BpmnSquence::all();
        $this->total_rows = BpmnSquence::count();
        return view('livewire.bpmn_squences');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_squence_id = '';
        $this->code = '';
        $this->source = '';
        $this->destination = '';
        $this->summary = '';
        $this->sequenceable = '';

    }

    public function store()
    {
        $this->validate([
            'code' => 'required',
            'source' => 'required',
            'destination' => 'required',
            'sequenceable' => 'required',

        ]);

        BpmnSquence::updateOrCreate(['id' => $this->bpmn_squence_id], [
            'code' => $this->code,
            'source' => $this->source,
            'destination' => $this->destination,
            'summary' => $this->summary,
            'sequenceable' => $this->sequenceable,

        ]);

        $this->alertSuccessMessage($this->bpmn_squence_id ? 'BpmnSquence updated.' : 'BpmnSquence created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_squence = BpmnSquence::findOrFail($id);
        $this->bpmn_squence_id = $id;
        $this->code = $bpmn_squence->code;
        $this->source = $bpmn_squence->source;
        $this->destination = $bpmn_squence->destination;
        $this->summary = $bpmn_squence->summary;
        $this->sequenceable = $bpmn_squence->sequenceable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnSquence::find($id)->delete();
        $this->alertSuccessMessage('BpmnSquence deleted.');
    }
}
