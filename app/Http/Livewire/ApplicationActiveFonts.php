<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationActiveFont;

class ApplicationActiveFonts extends Component
{
    use AlertMessageTrait;

    public $application_active_fonts, $application_active_font_id;
    public $name;
    public $purpose;
    public $type;
    public $font_url;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_active_fonts = ApplicationActiveFont::all();
        $this->total_rows = ApplicationActiveFont::count();
        return view('livewire.application_active_fonts');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_active_font_id = '';
        $this->name = '';
        $this->purpose = '';
        $this->type = 'main';
        $this->font_url = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'type' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationActiveFont::updateOrCreate(['id' => $this->application_active_font_id], [
            'name' => $this->name,
            'purpose' => $this->purpose,
            'type' => $this->type,
            'font_url' => $this->font_url,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_active_font_id ? 'ApplicationActiveFont updated.' : 'ApplicationActiveFont created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_active_font = ApplicationActiveFont::findOrFail($id);
        $this->application_active_font_id = $id;
        $this->name = $application_active_font->name;
        $this->purpose = $application_active_font->purpose;
        $this->type = $application_active_font->type;
        $this->font_url = $application_active_font->font_url;
        $this->application_id = $application_active_font->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationActiveFont::find($id)->delete();
        $this->alertSuccessMessage('ApplicationActiveFont deleted.');
    }
}
