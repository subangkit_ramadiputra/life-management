<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\MinuteOfMeeting;

class MinuteOfMeetings extends Component
{
    use AlertMessageTrait;

    public $minute_of_meetings, $minute_of_meeting_id;
    public $title;
    public $summary;
    public $meeting_date;
    public $momable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->minute_of_meetings = MinuteOfMeeting::all();
        $this->total_rows = MinuteOfMeeting::count();
        return view('livewire.minute_of_meetings');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->minute_of_meeting_id = '';
        $this->title = '';
        $this->summary = '';
        $this->meeting_date = '';
        $this->momable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'meeting_date' => 'required',
            'momable' => 'required',

        ]);

        MinuteOfMeeting::updateOrCreate(['id' => $this->minute_of_meeting_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'meeting_date' => $this->meeting_date,
            'momable' => $this->momable,

        ]);

        $this->alertSuccessMessage($this->minute_of_meeting_id ? 'MinuteOfMeeting updated.' : 'MinuteOfMeeting created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $minute_of_meeting = MinuteOfMeeting::findOrFail($id);
        $this->minute_of_meeting_id = $id;
        $this->title = $minute_of_meeting->title;
        $this->summary = $minute_of_meeting->summary;
        $this->meeting_date = $minute_of_meeting->meeting_date;
        $this->momable = $minute_of_meeting->momable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        MinuteOfMeeting::find($id)->delete();
        $this->alertSuccessMessage('MinuteOfMeeting deleted.');
    }
}
