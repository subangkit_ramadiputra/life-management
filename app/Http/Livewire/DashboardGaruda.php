<?php

namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;

use App\Models\Concept;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class DashboardGaruda extends Component
{
    use AlertMessageTrait;

    use AlertMessageTrait;
    public $player_stats;

    public function render()
    {
        $this->alertInfoMessage('Data berdasarkan database production garudaku');
        $this->fetchPlayerStatistic();
        $this->getLinks();
        return view('livewire.dashboard-garuda');
    }

    public function fetchPlayerStatistic() {
        $this->player_stats = (array) DB::connection('garudaku')->select(DB::raw("SELECT
               COUNT(1) TOTAL_ROW_PLAYER,
               SUM(CASE WHEN verified = 1 THEN 1 ELSE 0 END) VERIFIED,
               SUM(CASE WHEN username IS NOT NULL THEN 1 ELSE 0 END) ADDPROFILE,
               SUM(CASE WHEN gender = 1 THEN 1 ELSE 0 END) MALE,
               SUM(CASE WHEN gender = 0 THEN 1 ELSE 0 END) FEMALE,
               SUM(CASE WHEN gender IS NULL THEN 1 ELSE 0 END) JKNULL,
               SUM(otp_attemp) TOTAL_SMS_SENT
        FROM players"))[0];
    }

    public $links = [];
    public function getLinks() {
        $concept_id = 11;
        $concept = Concept::findOrFail($concept_id);
        $this->links = $concept->links()->get();
    }
}
