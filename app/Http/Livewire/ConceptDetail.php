<?php

namespace App\Http\Livewire;

use App\Models\Application;
use App\Models\Concept;
use App\Models\Entity;
use App\Models\Feature;
use App\Models\Task;
use Livewire\Component;

class ConceptDetail extends Component
{
    public $concept_id;
    public $concept;

    public function mount($concept_id)
    {
        $this->concept = Concept::findOrFail($concept_id);
        $this->concept_id = $concept_id;
    }

    public function render()
    {
        $this->getStatistic($this->concept_id);
        $this->getLinks($this->concept_id);
        $this->getTasks($this->concept_id);
        $this->getNotes($this->concept_id);
        $this->getFiles($this->concept_id);
        return view('livewire.concept-detail');
    }

    public $links = [];
    public function getLinks($concept_id) {
        $concept = Concept::findOrFail($concept_id);
        $this->links = $concept->links()->get();
    }

    public $notes = [];
    public function getNotes($concept_id) {
        $concept = Concept::findOrFail($concept_id);
        $this->notes = $concept->notes()->get();
    }

    public $q1 = [];
    public $q2 = [];
    public $q3 = [];
    public $q4 = [];

    public function getTasks($concept_id)
    {
        $concept = Concept::findOrFail($concept_id);
        $this->q1 = $concept->tasks()->where('quadrant_id',1)->where('status','!=','done')->get();
        $this->q2 = $concept->tasks()->where('quadrant_id',2)->where('status','!=','done')->get();
        $this->q3 = $concept->tasks()->where('quadrant_id',3)->where('status','!=','done')->get();
        $this->q4 = $concept->tasks()->where('quadrant_id',4)->where('status','!=','done')->get();
    }

    public $files = [];
    public function getFiles($concept_id)
    {
        $concept = Concept::findOrFail($concept_id);
        $this->files = $concept->uploads()->get();
    }

    public $analytics = [
        'applications' => 0,
        'features' => 0,
        'entities' => 0,
    ];

    public function getStatistic($concept_id)
    {
        $this->analytics = [
            'applications' => Application::where('concept_id', $concept_id)->count(),
            'features' => Feature::whereHasMorph(
                'featureable',
                [Application::class],
                function ($query) use ($concept_id) {
                    $query->where('concept_id', '=', $concept_id);
                }
            )->count(),
            'entities' => Entity::whereHasMorph(
                'entitiable',
                [Feature::class],
                function ($query) use ($concept_id) {
                    $query->whereHasMorph(
                        'featureable',
                        [Application::class],
                        function ($query) use ($concept_id) {
                            $query->where('concept_id', '=', $concept_id);
                        }
                    );
                }
            )->count(),
        ];
    }
}
