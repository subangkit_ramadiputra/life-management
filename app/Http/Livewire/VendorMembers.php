<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Models\User;
use App\Models\Vendor;
use Livewire\Component;
use App\Models\VendorMember;

class VendorMembers extends Component
{
    use AlertMessageTrait;

    public $vendor_members, $vendor_member_id;
    public $vendor_id;
    public $user_id;
    public $status;
    public $is_banned;
    public $role;

    public $isModalOpen = 0;
    public $total_rows = 0;

    // Relation
    public $vendors = [];
    public $users = [];

    public function render()
    {
        $this->vendor_members = VendorMember::all();
        $this->total_rows = VendorMember::count();

        // Relation
        $this->vendors = Vendor::all();
        $this->users = User::all();

        return view('livewire.vendor_members');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->vendor_member_id = '';
        $this->vendor_id = '';
        $this->user_id = '';
        $this->status = 'approved';
        $this->is_banned = '0';
        $this->role = '';

    }

    public function store()
    {
        $this->validate([
            'vendor_id' => 'required',
            'user_id' => 'required',
            'status' => 'required',
            'is_banned' => 'required',
            'role' => 'required',

        ]);

        VendorMember::updateOrCreate(['id' => $this->vendor_member_id], [
            'vendor_id' => $this->vendor_id,
            'user_id' => $this->user_id,
            'status' => $this->status,
            'is_banned' => $this->is_banned,
            'role' => $this->role,

        ]);
        $this->alertSuccessMessage($this->vendor_member_id ? 'VendorMember updated.' : 'VendorMember created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $vendor_member = VendorMember::findOrFail($id);
        $this->vendor_member_id = $id;
        $this->vendor_id = $vendor_member->vendor_id;
        $this->user_id = $vendor_member->user_id;
        $this->status = $vendor_member->status;
        $this->is_banned = $vendor_member->is_banned;
        $this->role = $vendor_member->role;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        VendorMember::find($id)->delete();
        $this->alertSuccessMessage('VendorMember deleted.');
    }
}
