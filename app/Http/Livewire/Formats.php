<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\Format;
use App\Models\Attribute;

class Formats extends Component
{
    use AlertMessageTrait;

    public $formats, $format_id;
    public $title;
    public $summary;
    public $format_text;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->formats = Format::all();
        $this->total_rows = Format::count();
        return view('livewire.formats');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->format_id = '';
        $this->title = '';
        $this->summary = '';
        $this->format_text = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'format_text' => 'required',

        ]);

        Format::updateOrCreate(['id' => $this->format_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'format_text' => $this->format_text,

        ]);

        if ($this->format_id == '') {
            $format_id = DB::getPdo()->lastInsertId();
        } else {
            $format_id = $this->format_id;
        }

        Storage::disk('twig')->put('format_'.$format_id.'.twig',$this->format_text);


        $this->alertSuccessMessage($this->format_id ? 'Format updated.' : 'Format created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }

    public $edit_attributes = [];
    public function edit($id)
    {
        $format = Format::findOrFail($id);
        $this->edit_attributes = $format->attributes()->get();
        $this->format_id = $id;
        $this->title = $format->title;
        $this->summary = $format->summary;
        $this->format_text = $format->format_text;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Format::find($id)->delete();
        $this->alertSuccessMessage('Format deleted.');
    }


    // Attribute Section
    public $isAttributeModalOpen = 0;
    public $recent_attributes = [];
    public $attributes = [];
    public function addAttribute($id) {
        $this->format_id = $id;
        $format = Format::findOrFail($this->format_id);
        $this->recent_attributes = $format->attributes()->get();
        $this->resetAttributeCreateForm();
        $this->openAttributeModalPopover();
    }

    public function openAttributeModalPopover()
    {
        $this->isAttributeModalOpen = true;
    }
    public function closeAttributeModalPopover()
    {
        $this->isAttributeModalOpen = false;
    }

    public function editAttribute($attribute_id) {
        $attribute = Attribute::findOrFail($attribute_id);
        $this->attribute_id = $attribute_id;
        $this->attribute_field_name = $attribute->field_name;
        $this->attribute_nullable = $attribute->nullable;
        $this->attribute_title = $attribute->title;
        $this->attribute_type = $attribute->type;
        $this->attribute_length = $attribute->length;
        $this->attribute_precision = $attribute->precision;
        $this->attribute_scale = $attribute->scale;
        $this->attribute_digits = $attribute->digits;
        $this->attribute_decimal = $attribute->decimal;
        $this->attribute_default = $attribute->default;
        $this->attribute_view_table = $attribute->view_table;
        $this->attribute_view_create = $attribute->view_create;
        $this->attribute_attributable = $attribute->attributable;

    }

    public function resetAttributeCreateForm(){
        $this->attribute_id = '';
        $this->attribute_field_name = '';
        $this->attribute_nullable = '0';
        $this->attribute_title = '';
        $this->attribute_type = 'string';
        $this->attribute_length = '';
        $this->attribute_precision = '0';
        $this->attribute_scale = '0';
        $this->attribute_digits = '0';
        $this->attribute_decimal = '0';
        $this->attribute_default = '';
        $this->attribute_view_table = '1';
        $this->attribute_view_create = '1';
        $this->attribute_attributable = '';

    }

    public function assignAttribute() {
        $format = Format::findOrFail($this->format_id);
        if ($this->attribute_id == '') {
            $attribute = new Attribute();
            $attribute->field_name = $this->attribute_field_name;
            $attribute->nullable = $this->attribute_nullable;
            $attribute->title = $this->attribute_title;
            $attribute->type = $this->attribute_type;
            $attribute->length = $this->attribute_length;
            $attribute->precision = $this->attribute_precision;
            $attribute->scale = $this->attribute_scale;
            $attribute->digits = $this->attribute_digits;
            $attribute->decimal = $this->attribute_decimal;
            $attribute->default = $this->attribute_default;
            $attribute->view_table = $this->attribute_view_table;
            $attribute->view_create = $this->attribute_view_create;

            $format->attributes()->save($attribute);

            $this->alertSuccessMessage('Attribute created.');
            //$this->closeAttributeModalPopover();
            $this->resetAttributeCreateForm();
        } else {
            Attribute::updateOrCreate(['id' => $this->attribute_id], [
                'field_name' => $this->attribute_field_name,
                'nullable' => $this->attribute_nullable,
                'title' => $this->attribute_title,
                'type' => $this->attribute_type,
                'length' => $this->attribute_length,
                'precision' => $this->attribute_precision,
                'scale' => $this->attribute_scale,
                'digits' => $this->attribute_digits,
                'decimal' => $this->attribute_decimal,
                'default' => $this->attribute_default,
                'view_table' => $this->attribute_view_table,
                'view_create' => $this->attribute_view_create,

            ]);
            $this->alertSuccessMessage('Attribute updated.');
        }

        $this->recent_attributes = $format->attributes()->get();
    }

    public function deleteAttribute($format_id, $attribute_id) {
        $format = Format::findOrFail($format_id);

        $attribute = Attribute::find($attribute_id);
        if ($attribute) {
            $format->attributes()->where('id', '=', $attribute->id)->delete();
        }

        $this->recent_attributes = $format->attributes()->get();

        $this->alertSuccessMessage('Attribute deleted.');
    }

    public function deleteExistingAttribute($attribute_id) {
        $format = Format::findOrFail($this->format_id);

        $attribute = Attribute::find($attribute_id);
        if ($attribute) {
            $format->attributes()->where('id', '=', $attribute->id)->delete();
        }

        $this->recent_attributes = $format->attributes()->get();

        $this->alertSuccessMessage('Attribute deleted.');
    }

    public function resetTypeOptions() {
        $this->attribute_length = '';
        $this->attribute_precision = '0';
        $this->attribute_scale = '0';
        $this->attribute_digits = '0';
        $this->attribute_decimal = '0';
        $this->attribute_default = '';
    }

    public function changeType() {
        $this->resetTypeOptions();
        switch($this->attribute_type) {
            case 'string' :
                $this->attribute_length = 100;
                break;
        }
    }
    // End of Section
}
