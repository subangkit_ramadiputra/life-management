<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\NavigationGroup;

class NavigationGroups extends Component
{
    use AlertMessageTrait;

    public $navigation_groups, $navigation_group_id;
    public $name;
    public $summary;
    public $weight;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->navigation_groups = NavigationGroup::all();
        $this->total_rows = NavigationGroup::count();
        return view('livewire.navigation_groups');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->navigation_group_id = '';
        $this->name = '';
        $this->summary = '';
        $this->weight = '100';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'weight' => 'required',

        ]);

        NavigationGroup::updateOrCreate(['id' => $this->navigation_group_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'weight' => $this->weight,

        ]);
        $this->alertSuccessMessage($this->navigation_group_id ? 'NavigationGroup updated.' : 'NavigationGroup created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $navigation_group = NavigationGroup::findOrFail($id);
        $this->navigation_group_id = $id;
        $this->name = $navigation_group->name;
        $this->summary = $navigation_group->summary;
        $this->weight = $navigation_group->weight;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        NavigationGroup::find($id)->delete();
        $this->alertSuccessMessage('NavigationGroup deleted.');
    }
}
