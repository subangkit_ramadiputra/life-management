<?php
namespace App\Http\Livewire;
use App\Http\Traits\Generator;
use App\Models\ContextGeneration;
use App\Models\Entity;
use App\Models\Framework;
use App\Models\Navigation;
use App\Models\NavigationGroup;
use App\Traits\AlertMessageTrait;
use App\Traits\ContextGeneratorTraits;
use App\Traits\GenerateTraits;
use Illuminate\Support\Facades\Artisan;
use Livewire\Component;
use App\Models\ContextDefinition;

class ContextDefinitions extends Component
{
    use AlertMessageTrait;

    public $context_definitions, $context_definition_id;
    public $frameworks, $framework_id;
    public $context_name;
    public $requirement;
    public $main_template;
    public $mode;
    public $generator_parameters = [];
    public $testing_template = '';

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->context_definitions = ContextDefinition::all();
        $this->frameworks = Framework::all();
        $this->total_rows = ContextDefinition::count();
        return view('livewire.context_definitions');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->context_definition_id = '';
        $this->context_name = 'Component';
        $this->requirement = '';
        $this->main_template = '';
        $this->framework_id = '';
        $this->mode = '';

    }

    public function store()
    {
        $this->validate([
            'context_name' => 'required',
            'main_template' => 'required',
            'framework_id' => 'required',
            'mode' => 'required',

        ]);

        ContextDefinition::updateOrCreate(['id' => $this->context_definition_id], [
            'context_name' => $this->context_name,
            'requirement' => $this->requirement,
            'main_template' => $this->main_template,
            'framework_id' => $this->framework_id,
            'mode' => $this->mode,

        ]);

        $this->alertSuccessMessage($this->context_definition_id ? 'ContextDefinition updated.' : 'ContextDefinition created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $context_definition = ContextDefinition::findOrFail($id);
        $this->context_definition_id = $id;
        $this->context_name = $context_definition->context_name;
        $this->requirement = $context_definition->requirement;
        $this->main_template = $context_definition->main_template;
        $this->framework_id = $context_definition->framework_id;
        $this->mode = $context_definition->mode;

        $this->generator_parameters = $this->getGeneratorParameters();

        $this->openModalPopover();
    }

    public function getGeneratorParameters() {
        $parameters = [];
        $context_definition_requirement = json_decode($this->requirement);
        if (isset($context_definition_requirement->requirements))
            foreach ($context_definition_requirement->requirements as $parameter_name => $type) {
                $parameters[$parameter_name] = '';
            }

        $generators = ContextGeneration::where('context_definition_id', $this->context_definition_id)->get();
        foreach ($generators as $generator) {
            $customData = json_decode($generator->custom_data);
            if (isset($customData->integrations)) {
                if (is_array($customData->integrations)) {
                    foreach($customData->integrations as $integration) {
                        // $driver = $this->getDriver($integration->type);
                        // $variables[$integration->name] = $driver->handle((array) $integration);
                        if (isset($integration->parameters)) {
                            foreach ($integration->parameters as $parameter_name => $default_value) {
                                $parameters[$parameter_name] = $default_value;
                            }
                        }
                    }
                }
            }
        }

        return $parameters;
    }

    public function duplicate($id)
    {
        $context_definition = ContextDefinition::findOrFail($id);
        ContextDefinition::updateOrCreate(['id' => null], [
            'context_name' => $context_definition->context_name,
            'requirement' => $context_definition->requirement,
            'main_template' => $context_definition->main_template,
            'framework_id' => $context_definition->framework_id,
            'mode' => $context_definition->mode,

        ]);

        $this->render();
    }

    public function delete($id)
    {
        ContextDefinition::find($id)->delete();
        $this->alertSuccessMessage('ContextDefinition deleted.');
    }

    use ContextGeneratorTraits;
    public function testTemplate() {
        //Collecting Variable
        $variables = [];
        $context_definition_data = (object) [
            'id' => $this->context_definition_id,
            'context_name' => $this->context_name,
            'requirement' => $this->requirement,
            'main_template' => $this->main_template,
            'framework_id' => $this->framework_id,
            'mode' => $this->mode,
        ];

        // Check all Scope
        $options = json_decode($this->requirement);
        if (isset($options->scopes))
            foreach ($options->scopes as $scope) {
                if (!isset($variables[$scope.'_scope']))
                    $variables[$scope.'_scope'] = '';
            }

        $this->parseDefinitionObject($variables, $context_definition_data);
        $variables = array_merge($variables, $this->generator_parameters);
        $generators = ContextGeneration::where('context_definition_id', $this->context_definition_id)->get();
        foreach ($generators as $generator) {
            $variables[$generator->scope_generation.'_scope'] = $this->getGeneratorOutput($context_definition_data, $generator, $variables);
        }

        $template = $this->main_template;
        $this->testing_template = $this->getStubContents($template,$variables,'$$','$$');
    }

    // GenerateCRUD Section
    use Generator;
    public function getRequirement($context_definition_object) {
        $json_requirement = json_decode($context_definition_object->requirement,true);
        if (!isset($json_requirement['requirements']))
            return [];

        return $json_requirement['requirements'];
    }

    public function getFieldNameArray($context_definition_object) {
        $requirements = $this->getRequirement($context_definition_object);
        $field_names = [];
        foreach ($requirements as $field_name => $type) {
            if ($type == 'array') {
                continue;
            }

            $field_names[] = $field_name;
        }

        return $field_names;
    }

    public function getStringDefinition($field_name) {
        return [
            'field_name' => $field_name,
            'nullable' => '',
            'title' => '',
            'type' => '',
            'length' => '',
            'precision' => '',
            'scale' => '',
            'digits' => '',
            'decimal' => '',
            'default' => '',
            'view_table' => '',
            'view_create' => '',
            'attributable_type' => '',
            'attributable_id' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }

    public function getTextDefinition($field_name) {
        return [
            'field_name' => $field_name,
            'nullable' => '',
            'title' => '',
            'type' => '',
            'length' => '',
            'precision' => '',
            'scale' => '',
            'digits' => '',
            'decimal' => '',
            'default' => '',
            'view_table' => '',
            'view_create' => '',
            'attributable_type' => '',
            'attributable_id' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }

    public function getIntegerDefinition($field_name) {
        return [
            'field_name' => $field_name,
            'nullable' => '',
            'title' => '',
            'type' => '',
            'length' => '',
            'precision' => '',
            'scale' => '',
            'digits' => '',
            'decimal' => '',
            'default' => '',
            'view_table' => '',
            'view_create' => '',
            'attributable_type' => '',
            'attributable_id' => '',
            'created_at' => '',
            'updated_at' => '',
        ];
    }
    public function getDefinition($type) {
        if ($type == 'string')
            return $this->getStringDefinition();

        if ($type == 'text')
            return $this->getTextDefinition();

        if ($type == 'integer')
            return $this->getIntegerDefinition();
    }
    public function getAttributeDefinitions($context_definition_object) {
        $requirements = $this->getRequirement($context_definition_object);
        $field_definitions = [];
        foreach ($requirements as $field_name => $type) {
            if ($type == 'array') {
                continue;
            }

            $field_definitions[$field_name] = $this->getDefinition($typ);
        }

        return $field_definitions;
    }
    public function generateCRUD($context_definition_id) {
        $context = ContextDefinition::findOrFail($context_definition_id);
        if ($context) {
            // Generate CRUD

            $attributes = $this->getFieldNameArray($context);
            $fields = implode(',',$attributes);

            $attributes = $entity->attributes()->get();
            foreach ($attributes as $attribute) {
                $field_attributes[$attribute->field_name] = $attribute->toArray();
            }

            Artisan::call('generate:crud',[
                '--no-ask' => true,
                '--entity' => strtolower($context->context_name),
                '--entity_id' => $entity->id,
                '--name' => $context->context_name,
                '--fields' => $fields,
                '--field_properties' => json_encode($field_attributes)
            ]);
            $this->alertSuccessMessage(Artisan::output());
        }
    }
    // End of Section
}
