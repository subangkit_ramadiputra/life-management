<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BPMNEditor extends Component
{
    public function render()
    {
        return view('livewire.b-p-m-n-editor');
    }
}
