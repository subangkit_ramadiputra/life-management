<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationContent;

class ApplicationContents extends Component
{
    use AlertMessageTrait;

    public $application_contents, $application_content_id;
    public $name;
    public $title_format;
    public $application_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_contents = ApplicationContent::all();
        $this->total_rows = ApplicationContent::count();
        return view('livewire.application_contents');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_content_id = '';
        $this->name = '';
        $this->title_format = '';
        $this->application_id = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'title_format' => 'required',
            'application_id' => 'required',

        ]);

        ApplicationContent::updateOrCreate(['id' => $this->application_content_id], [
            'name' => $this->name,
            'title_format' => $this->title_format,
            'application_id' => $this->application_id,

        ]);

        $this->alertSuccessMessage($this->application_content_id ? 'ApplicationContent updated.' : 'ApplicationContent created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_content = ApplicationContent::findOrFail($id);
        $this->application_content_id = $id;
        $this->name = $application_content->name;
        $this->title_format = $application_content->title_format;
        $this->application_id = $application_content->application_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationContent::find($id)->delete();
        $this->alertSuccessMessage('ApplicationContent deleted.');
    }
}
