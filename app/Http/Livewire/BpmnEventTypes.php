<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnEventType;

class BpmnEventTypes extends Component
{
    use AlertMessageTrait;

    public $bpmn_event_types, $bpmn_event_type_id;
    public $code;
    public $name;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_event_types = BpmnEventType::all();
        $this->total_rows = BpmnEventType::count();
        return view('livewire.bpmn_event_types');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_event_type_id = '';
        $this->code = '';
        $this->name = '';

    }

    public function store()
    {
        $this->validate([
            'code' => 'required',
            'name' => 'required',

        ]);

        BpmnEventType::updateOrCreate(['id' => $this->bpmn_event_type_id], [
            'code' => $this->code,
            'name' => $this->name,

        ]);

        $this->alertSuccessMessage($this->bpmn_event_type_id ? 'BpmnEventType updated.' : 'BpmnEventType created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_event_type = BpmnEventType::findOrFail($id);
        $this->bpmn_event_type_id = $id;
        $this->code = $bpmn_event_type->code;
        $this->name = $bpmn_event_type->name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnEventType::find($id)->delete();
        $this->alertSuccessMessage('BpmnEventType deleted.');
    }
}
