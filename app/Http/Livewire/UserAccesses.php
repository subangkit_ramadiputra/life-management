<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\UserAccess;

class UserAccesses extends Component
{
    use AlertMessageTrait;

    public $user_accesses, $user_access_id;
    public $title;
    public $summary;
    public $username;
    public $password;
    public $login_url;
    public $accessable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->user_accesses = UserAccess::all();
        $this->total_rows = UserAccess::count();
        return view('livewire.user_accesses');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->user_access_id = '';
        $this->title = '';
        $this->summary = '';
        $this->username = '';
        $this->password = '';
        $this->login_url = '';
        $this->accessable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'username' => 'required',
            'password' => 'required',
            'login_url' => 'required',
            'accessable' => 'required',

        ]);

        UserAccess::updateOrCreate(['id' => $this->user_access_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'username' => $this->username,
            'password' => $this->password,
            'login_url' => $this->login_url,
            'accessable' => $this->accessable,

        ]);

        $this->alertSuccessMessage($this->user_access_id ? 'UserAccess updated.' : 'UserAccess created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $user_access = UserAccess::findOrFail($id);
        $this->user_access_id = $id;
        $this->title = $user_access->title;
        $this->summary = $user_access->summary;
        $this->username = $user_access->username;
        $this->password = $user_access->password;
        $this->login_url = $user_access->login_url;
        $this->accessable = $user_access->accessable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        UserAccess::find($id)->delete();
        $this->alertSuccessMessage('UserAccess deleted.');
    }
}
