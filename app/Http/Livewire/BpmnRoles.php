<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnRole;

class BpmnRoles extends Component
{
    use AlertMessageTrait;

    public $bpmn_roles, $bpmn_role_id;
    public $code;
    public $name;
    public $roleable;
    public $summary;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_roles = BpmnRole::all();
        $this->total_rows = BpmnRole::count();
        return view('livewire.bpmn_roles');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_role_id = '';
        $this->code = '';
        $this->name = '';
        $this->roleable = '';
        $this->summary = '';

    }

    public function store()
    {
        $this->validate([
            'code' => 'required',
            'name' => 'required',
            'roleable' => 'required',

        ]);

        BpmnRole::updateOrCreate(['id' => $this->bpmn_role_id], [
            'code' => $this->code,
            'name' => $this->name,
            'roleable' => $this->roleable,
            'summary' => $this->summary,

        ]);

        $this->alertSuccessMessage($this->bpmn_role_id ? 'BpmnRole updated.' : 'BpmnRole created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_role = BpmnRole::findOrFail($id);
        $this->bpmn_role_id = $id;
        $this->code = $bpmn_role->code;
        $this->name = $bpmn_role->name;
        $this->roleable = $bpmn_role->roleable;
        $this->summary = $bpmn_role->summary;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnRole::find($id)->delete();
        $this->alertSuccessMessage('BpmnRole deleted.');
    }
}
