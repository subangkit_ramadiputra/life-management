<?php
namespace App\Http\Livewire;
use App\Models\Sop;
use App\Traits\AlertMessageTrait;
use App\Models\Link;
use App\Models\Tag;
use App\Models\Upload;
use App\Models\Task;
use App\Models\Quadrant;
use App\Models\UserAccess;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\Concept;
use App\Models\Note;
use Livewire\WithFileUploads;
use App\Models\Cluster;

class Concepts extends Component
{
    use AlertMessageTrait;

    public $concepts, $concept_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $source_url;
    public $owner_id;
    public $total_rows = 0;

    public $isModalOpen = 0;
    public function render()
    {
        $this->concepts = Concept::orderBy('weight','desc')->get();
        $this->total_rows = Concept::count();
        return view('livewire.concepts');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->concept_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';
        $this->source_url = '';
        $this->owner_id = '';
        $this->weight = '0';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
        ]);

        Concept::updateOrCreate(['id' => $this->concept_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'source_url' => $this->source_url,
            'owner_id' => Auth::user()->id,
            'weight' => $this->weight,

        ]);

        $this->alertSuccessMessage($this->concept_id ? 'Concept updated.' : 'Concept created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $concept = Concept::findOrFail($id);
        $this->concept_id = $id;
        $this->title = $concept->title;
        $this->summary = $concept->summary;
        $this->body = $concept->body;
        $this->body_format = $concept->body_format;
        $this->source_url = $concept->source_url;
        $this->owner_id = $concept->owner_id;
        $this->weight = $concept->weight;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Concept::find($id)->delete();
        $this->alertSuccessMessage('Concept deleted.');
    }

    // Tag Section
    public $isTagModalOpen = 0;
    public function addTag($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_tags = $concept->tags()->get();
        $this->tags = Tag::all();
        $this->resetTagCreateForm();
        $this->openTagModalPopover();
    }

    public function openTagModalPopover()
    {
        $this->isTagModalOpen = true;
    }
    public function closeTagModalPopover()
    {
        $this->isTagModalOpen = false;
    }
    private function resetTagCreateForm(){
        $this->tag_id = '';
        $this->name = '';
    }

    public function assignTag() {
        $this->validate([
            'name' => 'required',
        ]);

        $concept = Concept::findOrFail($this->concept_id);

        $tag = Tag::updateOrCreate(['name' => $this->name]);
        if ($concept->tags()->where('tag_id', $tag->id)->count() == 0) {
            $concept->tags()->attach($tag);
        }

        $this->alertSuccessMessage('Tag updated.');
        $this->closeTagModalPopover();
        $this->resetTagCreateForm();
    }

    public function deleteTag($concept_id, $tag_id) {
        $concept = Concept::findOrFail($concept_id);

        $tag = Tag::find($tag_id);
        if ($tag)
            $concept->tags()->detach($tag);

        $this->alertSuccessMessage('Tag deleted.');
    }

    public function deleteExistingTag($tag_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $tag = Tag::find($tag_id);
        if ($tag)
            $concept->tags()->detach($tag);

        $this->recent_tags = $concept->tags()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }

    public function assignExistingTag($tag_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $tag = Tag::find($tag_id);
        if ($tag)
            if ($concept->tags()->where('tag_id', $tag->id)->count() == 0) {
                $concept->tags()->attach($tag);
            }

        $this->recent_tags = $concept->tags()->get();
        $this->alertSuccessMessage('Tag assigned.');
    }
    // End of Section

    // Upload Section
    use WithFileUploads;
    public $isUploadModalOpen = 0;
    public $uploads = [];
    public function addUpload($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_uploads = $concept->uploads()->get();
        $this->resetUploadCreateForm();
        $this->openUploadModalPopover();
    }

    public function openUploadModalPopover()
    {
        $this->isUploadModalOpen = true;
    }
    public function closeUploadModalPopover()
    {
        $this->isUploadModalOpen = false;
    }
    private function resetUploadCreateForm(){
        $this->files = [];
    }

    public function assignUpload() {
        $this->validate([
            'files.*' => 'max:309600', // 1MB Max
        ]);

        $concept = Concept::findOrFail($this->concept_id);

        foreach ($this->files as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload();
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $concept->uploads()->save($upload);
        }

        $this->recent_uploads = $concept->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        $this->closeUploadModalPopover();
        $this->resetUploadCreateForm();
    }

    public function deleteUpload($concept_id, $upload_id) {
        $concept = Concept::findOrFail($concept_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $concept->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $concept->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingUpload($upload_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $concept->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $concept->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // Link Section
    public $isLinkModalOpen = 0;
    public $recent_links = [];
    public $links = [];
    public function addLink($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_links = $concept->links()->get();
        $this->resetLinkCreateForm();
        $this->openLinkModalPopover();
    }

    public function openLinkModalPopover()
    {
        $this->isLinkModalOpen = true;
    }
    public function closeLinkModalPopover()
    {
        $this->isLinkModalOpen = false;
    }

    public function editLink($link_id) {
        $link = Link::findOrFail($link_id);
        $this->link_id = $link_id;
        $this->link_title = $link->title;
        $this->link_url = $link->url;
        $this->link_summary = $link->summary;
        $this->link_linkable = $link->linkable;

    }

    public function resetLinkCreateForm(){
        $this->link_id = '';
        $this->link_title = '';
        $this->link_url = '';
        $this->link_summary = '';
        $this->link_linkable = '';

    }

    public function assignLink() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->link_id == '') {
            $link = new Link();
            $link->title = $this->link_title;
            $link->url = $this->link_url;
            $link->summary = $this->link_summary;

            $concept->links()->save($link);

            $this->alertSuccessMessage('Link created.');
            //$this->closeLinkModalPopover();
            $this->resetLinkCreateForm();
        } else {
            Link::updateOrCreate(['id' => $this->link_id], [
                'title' => $this->link_title,
                'url' => $this->link_url,
                'summary' => $this->link_summary,

            ]);

            $this->alertSuccessMessage('Link updated.');
        }

        $this->recent_links = $concept->links()->get();
    }

    public function deleteLink($concept_id, $link_id) {
        $concept = Concept::findOrFail($concept_id);

        $link = Link::find($link_id);
        if ($link) {
            $concept->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $concept->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }

    public function deleteExistingLink($link_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $link = Link::find($link_id);
        if ($link) {
            $concept->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $concept->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }
    // End of Section

    // Task Section
    public $isTaskModalOpen = 0;
    public $recent_tasks = [];
    public $tasks = [];
    public function addTask($id) {
        $this->concept_id = $id;

        $this->concept_tasks = Task::all();

        $this->quadrans = Quadrant::all();
        $this->employees = [];
        $this->statuses = [
            'todo' => 'To Do',
            'in_progress' => 'In Progress',
            'done' => 'Done',
        ];
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_tasks = $concept->tasks()->get();
        $this->resetTaskCreateForm();
        $this->openTaskModalPopover();
    }

    public function openTaskModalPopover()
    {
        $this->isTaskModalOpen = true;
    }
    public function closeTaskModalPopover()
    {
        $this->isTaskModalOpen = false;
    }

    public function editTask($task_id) {
        $task = Task::findOrFail($task_id);
        $this->task_id = $task_id;
        $this->task_quadrant_id = $task->quadrant_id;
        $this->task_title = $task->title;
        $this->task_summary = $task->summary;
        $this->task_body = $task->body;
        $this->task_body_format = $task->body_format;
        $this->task_assignee_id = $task->assignee_id;
        $this->task_status = $task->status;
        $this->task_due_date = $task->due_date;
        $this->task_parent_id = $task->parent_id;

    }

    public function resetTaskCreateForm(){
        $this->task_id = '';
        $this->task_quadrant_id = '';
        $this->task_title = '';
        $this->task_summary = '';
        $this->task_body = '';
        $this->task_body_format = 'markdown';
        $this->task_assignee_id = '0';
        $this->task_status = 'todo';
        $this->task_due_date = date('Y-m-d H:i:s');
        $this->task_parent_id = '0';

    }

    public function assignTask() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->task_id == '') {
            $task = new Task();
            $task->quadrant_id = $this->task_quadrant_id;
            $task->title = $this->task_title;
            $task->summary = $this->task_summary;
            $task->body = $this->task_body;
            $task->body_format = $this->task_body_format;
            $task->assignee_id = $this->task_assignee_id;
            $task->status = $this->task_status;
            $task->due_date = $this->task_due_date;
            $task->parent_id = $this->task_parent_id;

            $concept->tasks()->save($task);

            $this->alertSuccessMessage('Task updated.');
            //$this->closeTaskModalPopover();
            $this->resetTaskCreateForm();
        } else {
            Task::updateOrCreate(['id' => $this->task_id], [
                'quadrant_id' => $this->task_quadrant_id,
                'title' => $this->task_title,
                'summary' => $this->task_summary,
                'body' => $this->task_body,
                'body_format' => $this->task_body_format,
                'assignee_id' => $this->task_assignee_id,
                'status' => $this->task_status,
                'due_date' => $this->task_due_date,
                'parent_id' => $this->task_parent_id,

            ]);
            $this->alertSuccessMessage('Task created.');
        }

        $this->recent_tasks = $concept->tasks()->get();
    }

    public function deleteTask($concept_id, $task_id) {
        $concept = Concept::findOrFail($concept_id);

        $task = Task::find($task_id);
        if ($task) {
            $concept->tasks()->where('id', '=', $task->id)->delete();
        }

        $this->recent_tasks = $concept->tasks()->get();

        $this->alertSuccessMessage('Task deleted.');
    }

    public function deleteExistingTask($task_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $task = Task::find($task_id);
        if ($task) {
            $concept->tasks()->where('id', '=', $task->id)->delete();
        }

        $this->recent_tasks = $concept->tasks()->get();

        $this->alertSuccessMessage('Task deleted.');
    }
    // End of Section

    // Note Section
    public $isNoteModalOpen = 0;
    public $recent_notes = [];
    public $notes = [];
    public function addNote($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_notes = $concept->notes()->get();
        $this->resetNoteCreateForm();
        $this->openNoteModalPopover();
    }

    public function openNoteModalPopover()
    {
        $this->isNoteModalOpen = true;
    }
    public function closeNoteModalPopover()
    {
        $this->isNoteModalOpen = false;
    }

    public function editNote($note_id) {
        $note = Note::findOrFail($note_id);
        $this->note_id = $note_id;
        $this->note_title = $note->title;
        $this->note_body = $note->body;
        $this->note_noteable = $note->noteable;

    }

    public function resetNoteCreateForm(){
        $this->note_id = '';
        $this->note_title = '';
        $this->note_body = '';
        $this->note_noteable = '';

    }

    public function assignNote() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->note_id == '') {
            $note = new Note();
            $note->title = $this->note_title;
            $note->body = $this->note_body;

            $concept->notes()->save($note);

            $this->alertSuccessMessage('Note Crated.');
            //$this->closeNoteModalPopover();
            $this->resetNoteCreateForm();
        } else {
            Note::updateOrCreate(['id' => $this->note_id], [
                'title' => $this->note_title,
                'body' => $this->note_body,

            ]);
            $this->alertSuccessMessage('Note Updated.');
        }

        $this->recent_notes = $concept->notes()->get();
    }

    public function deleteNote($concept_id, $note_id) {
        $concept = Concept::findOrFail($concept_id);

        $note = Note::find($note_id);
        if ($note) {
            $concept->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $concept->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }

    public function deleteExistingNote($note_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $note = Note::find($note_id);
        if ($note) {
            $concept->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $concept->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }
    // End of Section

    // Sop Section
    public $isSopModalOpen = 0;
    public $recent_sops = [];
    public $sops = [];
    public function addSop($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_sops = $concept->sops()->get();
        $this->resetSopCreateForm();
        $this->openSopModalPopover();
    }

    public function openSopModalPopover()
    {
        $this->isSopModalOpen = true;
    }
    public function closeSopModalPopover()
    {
        $this->isSopModalOpen = false;
    }

    public function editSop($sop_id) {
        $sop = Sop::findOrFail($sop_id);
        $this->sop_id = $sop_id;
        $this->sop_title = $sop->title;
        $this->sop_summary = $sop->summary;
        $this->sop_body_format = $sop->body_format;
        $this->sop_body = $sop->body;
        $this->sop_sopable = $sop->sopable;

    }

    public function resetSopCreateForm(){
        $this->sop_id = '';
        $this->sop_title = '';
        $this->sop_summary = '';
        $this->sop_body_format = 'markdown';
        $this->sop_body = '';
        $this->sop_sopable = '';

    }

    public function assignSop() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->sop_id == '') {
            $sop = new Sop();
            $sop->title = $this->sop_title;
            $sop->summary = $this->sop_summary;
            $sop->body_format = $this->sop_body_format;
            $sop->body = $this->sop_body;

            $concept->sops()->save($sop);

            $this->alertSuccessMessage('Sop updated.');
            //$this->closeSopModalPopover();
            $this->resetSopCreateForm();
        } else {
            Sop::updateOrCreate(['id' => $this->sop_id], [
                'title' => $this->sop_title,
                'summary' => $this->sop_summary,
                'body_format' => $this->sop_body_format,
                'body' => $this->sop_body,

            ]);
        }

        $this->recent_sops = $concept->sops()->get();
    }

    public function deleteSop($concept_id, $sop_id) {
        $concept = Concept::findOrFail($concept_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $concept->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $concept->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }

    public function deleteExistingSop($sop_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $concept->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $concept->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }
    // End of Section

    // UserAccess Section
    public $isUserAccessModalOpen = 0;
    public $recent_user_accesses = [];
    public $user_accesses = [];
    public function addUserAccess($id) {
        $this->concepts_id = $id;
        $concepts = Concept::findOrFail($this->concepts_id);
        $this->recent_user_accesses = $concepts->user_accesses()->get();
        $this->resetUserAccessCreateForm();
        $this->openUserAccessModalPopover();
    }

    public function openUserAccessModalPopover()
    {
        $this->isUserAccessModalOpen = true;
    }
    public function closeUserAccessModalPopover()
    {
        $this->isUserAccessModalOpen = false;
    }

    public function editUserAccess($user_access_id) {
        $user_access = UserAccess::findOrFail($user_access_id);
        $this->user_access_id = $user_access_id;
        $this->user_access_title = $user_access->title;
        $this->user_access_summary = $user_access->summary;
        $this->user_access_username = $user_access->username;
        $this->user_access_password = $user_access->password;
        $this->user_access_login_url = $user_access->login_url;
        $this->user_access_accessable = $user_access->accessable;

    }

    public function resetUserAccessCreateForm(){
        $this->user_access_id = '';
        $this->user_access_title = '';
        $this->user_access_summary = '';
        $this->user_access_username = '';
        $this->user_access_password = '';
        $this->user_access_login_url = '';
        $this->user_access_accessable = '';

    }

    public function assignUserAccess() {
        $concepts = Concept::findOrFail($this->concepts_id);
        if ($this->user_access_id == '') {
            $user_access = new UserAccess();
            $user_access->title = $this->user_access_title;
            $user_access->summary = $this->user_access_summary;
            $user_access->username = $this->user_access_username;
            $user_access->password = $this->user_access_password;
            $user_access->login_url = $this->user_access_login_url;

            $concepts->user_accesses()->save($user_access);

            $this->alertSuccessMessage('UserAccess created.');
            //$this->closeUserAccessModalPopover();
            $this->resetUserAccessCreateForm();
        } else {
            UserAccess::updateOrCreate(['id' => $this->user_access_id], [
                'title' => $this->user_access_title,
                'summary' => $this->user_access_summary,
                'username' => $this->user_access_username,
                'password' => $this->user_access_password,
                'login_url' => $this->user_access_login_url,

            ]);
            $this->alertSuccessMessage('UserAccess updated.');
        }

        $this->recent_user_accesses = $concepts->user_accesses()->get();
    }

    public function deleteUserAccess($concepts_id, $user_access_id) {
        $concepts = Concept::findOrFail($concepts_id);

        $user_access = UserAccess::find($user_access_id);
        if ($user_access) {
            $concepts->user_accesses()->where('id', '=', $user_access->id)->delete();
        }

        $this->recent_user_accesses = $concepts->user_accesses()->get();

        $this->alertSuccessMessage('UserAccess deleted.');
    }

    public function deleteExistingUserAccess($user_access_id) {
        $concepts = Concept::findOrFail($this->concepts_id);

        $user_access = UserAccess::find($user_access_id);
        if ($user_access) {
            $concepts->user_accesses()->where('id', '=', $user_access->id)->delete();
        }

        $this->recent_user_accesses = $concepts->user_accesses()->get();

        $this->alertSuccessMessage('UserAccess deleted.');
    }
    // End of Section

    // Cluster Section
    public $isClusterModalOpen = 0;
    public $recent_clusters = [];
    public $clusters = [];
    public function addCluster($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_clusters = $concept->clusters()->get();
        $this->resetClusterCreateForm();
        $this->openClusterModalPopover();
    }

    public function openClusterModalPopover()
    {
        $this->isClusterModalOpen = true;
    }
    public function closeClusterModalPopover()
    {
        $this->isClusterModalOpen = false;
    }

    public function editCluster($cluster_id) {
        $cluster = Cluster::findOrFail($cluster_id);
        $this->cluster_id = $cluster_id;
        $this->cluster_name = $cluster->name;
        $this->cluster_summary = $cluster->summary;
        $this->cluster_body_format = $cluster->body_format;
        $this->cluster_body = $cluster->body;
        $this->cluster_clusterable = $cluster->clusterable;

    }

    public function resetClusterCreateForm(){
        $this->cluster_id = '';
        $this->cluster_name = '';
        $this->cluster_summary = '';
        $this->cluster_body_format = 'markdown';
        $this->cluster_body = '';
        $this->cluster_clusterable = '';

    }

    public function assignCluster() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->cluster_id == '') {
            $cluster = new Cluster();
            $cluster->name = $this->cluster_name;
            $cluster->summary = $this->cluster_summary;
            $cluster->body_format = $this->cluster_body_format;
            $cluster->body = $this->cluster_body;

            $concept->clusters()->save($cluster);

            $this->alertSuccessMessage('Cluster created.');
            //$this->closeClusterModalPopover();
            $this->resetClusterCreateForm();
        } else {
            Cluster::updateOrCreate(['id' => $this->cluster_id], [
                'name' => $this->cluster_name,
                'summary' => $this->cluster_summary,
                'body_format' => $this->cluster_body_format,
                'body' => $this->cluster_body,

            ]);
            $this->alertSuccessMessage('Cluster updated.');
        }

        $this->recent_clusters = $concept->clusters()->get();
    }

    public function deleteCluster($concept_id, $cluster_id) {
        $concept = Concept::findOrFail($concept_id);

        $cluster = Cluster::find($cluster_id);
        if ($cluster) {
            $concept->clusters()->where('id', '=', $cluster->id)->delete();
        }

        $this->recent_clusters = $concept->clusters()->get();

        $this->alertSuccessMessage('Cluster deleted.');
    }

    public function deleteExistingCluster($cluster_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $cluster = Cluster::find($cluster_id);
        if ($cluster) {
            $concept->clusters()->where('id', '=', $cluster->id)->delete();
        }

        $this->recent_clusters = $concept->clusters()->get();

        $this->alertSuccessMessage('Cluster deleted.');
    }
    // End of Section

    // File Section
    public $isFileModalOpen = 0;
    public $recent_files = [];
    public $files = [];
    public $file_content_txt = '';
    public $fileEditorShow = false;
    public $fileFormShow = true;
    public function addFile($id) {
        $this->concept_id = $id;
        $concept = Concept::findOrFail($this->concept_id);
        $this->recent_files = $concept->files()->get();
        $this->resetFileCreateForm();
        $this->openFileModalPopover();
    }

    public function openFileModalPopover()
    {
        $this->isFileModalOpen = true;
    }
    public function closeFileModalPopover()
    {
        $this->isFileModalOpen = false;
        $this->resetFileCreateForm();
    }

    public function editFile($file_id) {

        $file = File::findOrFail($file_id);
        $this->file_content_txt = file_get_contents($file->file_path);
        $this->file_id = $file_id;
        $this->file_name = $file->name;
        $this->file_summary = $file->summary;
        $this->file_file_path = $file->file_path;
        $this->file_Mode = $file->Mode;
        $this->file_fileable = $file->fileable;

        $this->file_content_txt = '';
        $this->fileEditorShow = false;
        $this->fileFormShow = true;


    }

    public function resetFileCreateForm(){
        $this->fileEditorShow = false;
        $this->fileFormShow = true;
        $this->file_id = '';
        $this->file_name = '';
        $this->file_summary = '';
        $this->file_file_path = '';
        $this->file_Mode = 'html';
        $this->file_fileable = '';
        $this->file_content_txt = '';

    }

    public function assignFile() {
        $concept = Concept::findOrFail($this->concept_id);
        if ($this->file_id == '') {
            $file = new File();
            $file->name = $this->file_name;
            $file->summary = $this->file_summary;
            $file->file_path = $this->file_file_path;
            $file->Mode = $this->file_Mode;

            $concept->files()->save($file);

            $this->alertSuccessMessage('File created.');
            //$this->closeFileModalPopover();
            $this->resetFileCreateForm();
        } else {
            File::updateOrCreate(['id' => $this->file_id], [
                'name' => $this->file_name,
                'summary' => $this->file_summary,
                'file_path' => $this->file_file_path,
                'Mode' => $this->file_Mode,

            ]);
            $this->alertSuccessMessage('File updated.');
        }

        $this->fileEditorShow = false;
        $this->fileFormShow = true;
        $this->recent_files = $concept->files()->get();
    }

    public function deleteFile($concept_id, $file_id) {
        $concept = Concept::findOrFail($concept_id);

        $file = File::find($file_id);
        if ($file) {
            $concept->files()->where('id', '=', $file->id)->delete();
        }

        $this->recent_files = $concept->files()->get();
        $this->fileEditorShow = false;
        $this->fileFormShow = false;

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingFile($file_id) {
        $concept = Concept::findOrFail($this->concept_id);

        $file = File::find($file_id);
        if ($file) {
            $concept->files()->where('id', '=', $file->id)->delete();
        }

        $this->recent_files = $concept->files()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function editFileContent($file_id) {
        $this->file_content_txt = '';
        $this->fileEditorShow = true;
        $this->fileFormShow = false;

        $file = File::findOrFail($file_id);
        $this->file_id = $file_id;
        $this->file_name = $file->name;
        $this->file_summary = $file->summary;
        $this->file_file_path = $file->file_path;
        $this->file_Mode = $file->Mode;
        $this->file_fileable = $file->fileable;

        $this->file_content_txt = file_get_contents($file->file_path);
        $this->alertInfoMessage($file->name.' File loaded.');

    }

    public function saveFileContent($file_id, $newContent) {
        $file = File::findOrFail($file_id);
        $this->file_id = $file_id;
        $this->file_name = $file->name;
        $this->file_summary = $file->summary;
        $this->file_file_path = $file->file_path;
        $this->file_Mode = $file->Mode;
        $this->file_fileable = $file->fileable;
        $this->fileEditorShow = true;
        $this->fileFormShow = false;

        file_put_contents($this->file_file_path,$newContent);
        $this->file_content_txt = file_get_contents($file->file_path);
        $this->alertSuccessMessage('File '.$this->file_name.' content updated.');
    }
    // End of Section
}
