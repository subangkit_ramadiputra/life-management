<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Team;

class Teams extends Component
{
    use AlertMessageTrait;

    public $teams, $team_id;
    public $user_id;
    public $name;
    public $personal_team;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->teams = Team::all();
        $this->total_rows = Team::count();
        return view('livewire.teams');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->team_id = '';
        $this->user_id = '';
        $this->name = '';
        $this->personal_team = '';

    }

    public function store()
    {
        $this->validate([
            'user_id' => 'required',
            'name' => 'required',
            'personal_team' => 'required',

        ]);

        Team::updateOrCreate(['id' => $this->team_id], [
            'user_id' => $this->user_id,
            'name' => $this->name,
            'personal_team' => $this->personal_team,

        ]);
        $this->alertSuccessMessage($this->team_id ? 'Team updated.' : 'Team created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $team = Team::findOrFail($id);
        $this->team_id = $id;
        $this->user_id = $team->user_id;
        $this->name = $team->name;
        $this->personal_team = $team->personal_team;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Team::find($id)->delete();
        $this->alertSuccessMessage('Team deleted.');
    }
}
