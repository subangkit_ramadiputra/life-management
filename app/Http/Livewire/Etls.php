<?php
namespace App\Http\Livewire;
use App\Models\Note;
use App\Models\Upload;
use App\Traits\AlertMessageTrait;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\Etl;
use Livewire\WithFileUploads;

class Etls extends Component
{
    use AlertMessageTrait;

    public $etls, $etl_id;
    public $title;
    public $summary;
    public $schedule;
    public $request_by;
    public $source_data;
    public $transform_summary;
    public $target_data;
    public $consumed_by;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->etls = Etl::all();
        $this->total_rows = Etl::count();
        return view('livewire.etls');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->etl_id = '';
        $this->title = '';
        $this->summary = '';
        $this->schedule = '';
        $this->request_by = '';
        $this->source_data = '';
        $this->transform_summary = '';
        $this->target_data = '';
        $this->consumed_by = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'schedule' => 'required',
            'request_by' => 'required',

        ]);

        Etl::updateOrCreate(['id' => $this->etl_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'schedule' => $this->schedule,
            'request_by' => $this->request_by,
            'source_data' => $this->source_data,
            'transform_summary' => $this->transform_summary,
            'target_data' => $this->target_data,
            'consumed_by' => $this->consumed_by,

        ]);
        $this->alertSuccessMessage($this->etl_id ? 'Etl updated.' : 'Etl created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $etl = Etl::findOrFail($id);
        $this->etl_id = $id;
        $this->title = $etl->title;
        $this->summary = $etl->summary;
        $this->schedule = $etl->schedule;
        $this->request_by = $etl->request_by;
        $this->source_data = $etl->source_data;
        $this->transform_summary = $etl->transform_summary;
        $this->target_data = $etl->target_data;
        $this->consumed_by = $etl->consumed_by;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Etl::find($id)->delete();
        $this->alertSuccessMessage('Etl deleted.');
    }

    // Upload Section
    use WithFileUploads;
    public $isUploadModalOpen = 0;
    public $files = [];
    public function addUpload($id) {
        $this->etl_id = $id;
        $etl = Etl::findOrFail($this->etl_id);
        $this->recent_uploads = $etl->uploads()->get();
        $this->resetUploadCreateForm();
        $this->openUploadModalPopover();
    }

    public function openUploadModalPopover()
    {
        $this->isUploadModalOpen = true;
    }
    public function closeUploadModalPopover()
    {
        $this->isUploadModalOpen = false;
    }
    private function resetUploadCreateForm(){
        $this->files = [];
    }

    public function assignUpload() {
        $this->validate([
            'files.*' => 'max:309600', // 1MB Max
        ]);

        $etl = Etl::findOrFail($this->etl_id);

        foreach ($this->files as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload;
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $etl->uploads()->save($upload);
        }

        $this->recent_uploads = $etl->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        $this->closeUploadModalPopover();
        $this->resetUploadCreateForm();
    }

    public function deleteUpload($etl_id, $upload_id) {
        $etl = Etl::findOrFail($etl_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $etl->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $etl->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingUpload($upload_id) {
        $etl = Etl::findOrFail($this->etl_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $etl->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $etl->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // Note Section
    public $isNoteModalOpen = 0;
    public $recent_notes = [];
    public $notes = [];
    public function addNote($id) {
        $this->etl_id = $id;
        $etl = Etl::findOrFail($this->etl_id);
        $this->recent_notes = $etl->notes()->get();
        $this->resetNoteCreateForm();
        $this->openNoteModalPopover();
    }

    public function openNoteModalPopover()
    {
        $this->isNoteModalOpen = true;
    }
    public function closeNoteModalPopover()
    {
        $this->isNoteModalOpen = false;
    }

    public function editNote($note_id) {
        $note = Note::findOrFail($note_id);
        $this->note_id = $note_id;
        $this->note_title = $note->title;
        $this->note_body = $note->body;
        $this->note_noteable = $note->noteable;

    }

    public function resetNoteCreateForm(){
        $this->note_id = '';
        $this->note_title = '';
        $this->note_body = '';
        $this->note_noteable = '';

    }

    public function assignNote() {
        $etl = Etl::findOrFail($this->etl_id);
        if ($this->note_id == '') {
            $note = new Note();
            $note->title = $this->note_title;
            $note->body = $this->note_body;

            $etl->notes()->save($note);

            $this->alertSuccessMessage('Note created.');
            //$this->closeNoteModalPopover();
            $this->resetNoteCreateForm();
        } else {
            Note::updateOrCreate(['id' => $this->note_id], [
                'title' => $this->note_title,
                'body' => $this->note_body,

            ]);
            $this->alertSuccessMessage('Note updated.');
        }

        $this->recent_notes = $etl->notes()->get();
    }

    public function deleteNote($etl_id, $note_id) {
        $etl = Etl::findOrFail($etl_id);

        $note = Note::find($note_id);
        if ($note) {
            $etl->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $etl->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }

    public function deleteExistingNote($note_id) {
        $etl = Etl::findOrFail($this->etl_id);

        $note = Note::find($note_id);
        if ($note) {
            $etl->notes()->where('id', '=', $note->id)->delete();
        }

        $this->recent_notes = $etl->notes()->get();

        $this->alertSuccessMessage('Note deleted.');
    }
    // End of Section
}
