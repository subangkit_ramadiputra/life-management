<?php
namespace App\Http\Livewire;
use App\Models\Framework;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationConcept;
use App\Models\ApplicationNavigationGroup;

class ApplicationConcepts extends Component
{
    use AlertMessageTrait;

    public $application_concepts, $application_concept_id;
    public $frameworks, $framework_id;
    public $name;
    public $summary;
    public $conceptable;
    public $project_path;
    public $framework;
    public $config;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_concepts = ApplicationConcept::all();
        $this->frameworks = Framework::all();
        $this->total_rows = ApplicationConcept::count();
        return view('livewire.application_concepts');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_concept_id = '';
        $this->name = '';
        $this->summary = '';
        $this->conceptable = '';
        $this->project_path = '';
        $this->framework = '';
        $this->config = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'summary' => 'required',

        ]);

        ApplicationConcept::updateOrCreate(['id' => $this->application_concept_id], [
            'name' => $this->name,
            'summary' => $this->summary,
            'project_path' => $this->project_path,
            'framework' => $this->framework,
            'config' => $this->config,

        ]);

        $this->alertSuccessMessage($this->application_concept_id ? 'ApplicationConcept updated.' : 'ApplicationConcept created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_concept = ApplicationConcept::findOrFail($id);
        $this->application_concept_id = $id;
        $this->name = $application_concept->name;
        $this->summary = $application_concept->summary;
        $this->project_path = $application_concept->project_path;
        $this->framework = $application_concept->framework;
        $this->config = $application_concept->config;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationConcept::find($id)->delete();
        $this->alertSuccessMessage('ApplicationConcept deleted.');
    }


    // ApplicationNavigationGroup Section
    public $isApplicationNavigationGroupModalOpen = 0;
    public $recent_application_navigation_groups = [];
    public $application_navigation_groups = [];
    public function addApplicationNavigationGroup($id) {
        $this->application_concept_id = $id;
        $application_concept = ApplicationConcept::findOrFail($this->application_concept_id);
        $this->recent_application_navigation_groups = $application_concept->application_navigation_groups()->get();
        $this->resetApplicationNavigationGroupCreateForm();
        $this->openApplicationNavigationGroupModalPopover();
    }

    public function openApplicationNavigationGroupModalPopover()
    {
        $this->isApplicationNavigationGroupModalOpen = true;
    }
    public function closeApplicationNavigationGroupModalPopover()
    {
        $this->isApplicationNavigationGroupModalOpen = false;
    }

    public function editApplicationNavigationGroup($application_navigation_group_id) {
        $application_navigation_group = ApplicationNavigationGroup::findOrFail($application_navigation_group_id);
        $this->application_navigation_group_id = $application_navigation_group_id;
        $this->application_navigation_group_name = $application_navigation_group->name;
        $this->application_navigation_group_Summary = $application_navigation_group->Summary;
        $this->application_navigation_group_direction = $application_navigation_group->direction;
        $this->application_navigation_group_style = $application_navigation_group->style;
        $this->application_navigation_group_application_id = $application_navigation_group->application_id;

    }

    public function resetApplicationNavigationGroupCreateForm(){
        $this->application_navigation_group_id = '';
        $this->application_navigation_group_name = '';
        $this->application_navigation_group_Summary = '';
        $this->application_navigation_group_direction = 'Horizontal';
        $this->application_navigation_group_style = '';
        $this->application_navigation_group_application_id = '';

    }

    public function assignApplicationNavigationGroup() {
        $application_concept = ApplicationConcept::findOrFail($this->application_concept_id);
        if ($this->application_navigation_group_id == '') {
            $application_navigation_group = new ApplicationNavigationGroup();
            $application_navigation_group->name = $this->application_navigation_group_name;
            $application_navigation_group->Summary = $this->application_navigation_group_Summary;
            $application_navigation_group->direction = $this->application_navigation_group_direction;
            $application_navigation_group->style = $this->application_navigation_group_style;
            $application_navigation_group->application_id = $this->application_navigation_group_application_id;

            $application_concept->application_navigation_groups()->save($application_navigation_group);

            $this->alertSuccessMessage('ApplicationNavigationGroup created.');
            //$this->closeApplicationNavigationGroupModalPopover();
            $this->resetApplicationNavigationGroupCreateForm();
        } else {
            ApplicationNavigationGroup::updateOrCreate(['id' => $this->application_navigation_group_id], [
                'name' => $this->application_navigation_group_name,
                'Summary' => $this->application_navigation_group_Summary,
                'direction' => $this->application_navigation_group_direction,
                'style' => $this->application_navigation_group_style,
                'application_id' => $this->application_navigation_group_application_id,

            ]);
            $this->alertSuccessMessage('ApplicationNavigationGroup updated.');
        }

        $this->recent_application_navigation_groups = $application_concept->application_navigation_groups()->get();
    }

    public function deleteApplicationNavigationGroup($application_concept_id, $application_navigation_group_id) {
        $application_concept = ApplicationConcept::findOrFail($application_concept_id);

        $application_navigation_group = ApplicationNavigationGroup::find($application_navigation_group_id);
        if ($application_navigation_group) {
            $application_concept->application_navigation_groups()->where('id', '=', $application_navigation_group->id)->delete();
        }

        $this->recent_application_navigation_groups = $application_concept->application_navigation_groups()->get();

        $this->alertSuccessMessage('ApplicationNavigationGroup deleted.');
    }

    public function deleteExistingApplicationNavigationGroup($application_navigation_group_id) {
        $application_concept = ApplicationConcept::findOrFail($this->application_concept_id);

        $application_navigation_group = ApplicationNavigationGroup::find($application_navigation_group_id);
        if ($application_navigation_group) {
            $application_concept->application_navigation_groups()->where('id', '=', $application_navigation_group->id)->delete();
        }

        $this->recent_application_navigation_groups = $application_concept->application_navigation_groups()->get();

        $this->alertSuccessMessage('ApplicationNavigationGroup deleted.');
    }
    // End of Section
}
