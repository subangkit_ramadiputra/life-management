<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\FieldFormat;

class FieldFormats extends Component
{
    use AlertMessageTrait;

    public $field_formats, $field_format_id;
    public $format_name;
    public $summary;
    public $input_requirement;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->field_formats = FieldFormat::all();
        $this->total_rows = FieldFormat::count();
        return view('livewire.field_formats');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->field_format_id = '';
        $this->format_name = '';
        $this->summary = '';
        $this->input_requirement = '';

    }

    public function store()
    {
        $this->validate([
            'format_name' => 'required',
            'input_requirement' => 'required',

        ]);

        FieldFormat::updateOrCreate(['id' => $this->field_format_id], [
            'format_name' => $this->format_name,
            'summary' => $this->summary,
            'input_requirement' => $this->input_requirement,

        ]);

        $this->alertSuccessMessage($this->field_format_id ? 'FieldFormat updated.' : 'FieldFormat created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $field_format = FieldFormat::findOrFail($id);
        $this->field_format_id = $id;
        $this->format_name = $field_format->format_name;
        $this->summary = $field_format->summary;
        $this->input_requirement = $field_format->input_requirement;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        FieldFormat::find($id)->delete();
        $this->alertSuccessMessage('FieldFormat deleted.');
    }
}
