<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\VendorCategory;

class VendorCategories extends Component
{
    use AlertMessageTrait;

    public $vendor_categories, $vendor_category_id;
    public $name;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->vendor_categories = VendorCategory::all();
        $this->total_rows = VendorCategory::count();
        return view('livewire.vendor_categories');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->vendor_category_id = '';
        $this->name = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',

        ]);

        VendorCategory::updateOrCreate(['id' => $this->vendor_category_id], [
            'name' => $this->name,

        ]);
        $this->alertSuccessMessage($this->vendor_category_id ? 'VendorCategory updated.' : 'VendorCategory created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $vendor_category = VendorCategory::findOrFail($id);
        $this->vendor_category_id = $id;
        $this->name = $vendor_category->name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        VendorCategory::find($id)->delete();
        $this->alertSuccessMessage('VendorCategory deleted.');
    }
}
