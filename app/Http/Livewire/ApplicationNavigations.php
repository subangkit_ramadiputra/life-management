<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\ApplicationNavigation;

class ApplicationNavigations extends Component
{
    use AlertMessageTrait;

    public $application_navigations, $application_navigation_id;
    public $name;
    public $title_format;
    public $seo_url_format;
    public $page_type;
    public $navigation_group_id;
    public $navigation_back_schema;
    public $reset_navigation_back;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->application_navigations = ApplicationNavigation::all();
        $this->total_rows = ApplicationNavigation::count();
        return view('livewire.application_navigations');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->application_navigation_id = '';
        $this->name = '';
        $this->title_format = '';
        $this->seo_url_format = '';
        $this->page_type = 'page';
        $this->navigation_group_id = '';
        $this->navigation_back_schema = 'stack';
        $this->reset_navigation_back = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'title_format' => 'required',
            'seo_url_format' => 'required',
            'page_type' => 'required',
            'navigation_group_id' => 'required',
            'navigation_back_schema' => 'required',
            'reset_navigation_back' => 'required',

        ]);

        ApplicationNavigation::updateOrCreate(['id' => $this->application_navigation_id], [
            'name' => $this->name,
            'title_format' => $this->title_format,
            'seo_url_format' => $this->seo_url_format,
            'page_type' => $this->page_type,
            'navigation_group_id' => $this->navigation_group_id,
            'navigation_back_schema' => $this->navigation_back_schema,
            'reset_navigation_back' => $this->reset_navigation_back,

        ]);

        $this->alertSuccessMessage($this->application_navigation_id ? 'ApplicationNavigation updated.' : 'ApplicationNavigation created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $application_navigation = ApplicationNavigation::findOrFail($id);
        $this->application_navigation_id = $id;
        $this->name = $application_navigation->name;
        $this->title_format = $application_navigation->title_format;
        $this->seo_url_format = $application_navigation->seo_url_format;
        $this->page_type = $application_navigation->page_type;
        $this->navigation_group_id = $application_navigation->navigation_group_id;
        $this->navigation_back_schema = $application_navigation->navigation_back_schema;
        $this->reset_navigation_back = $application_navigation->reset_navigation_back;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        ApplicationNavigation::find($id)->delete();
        $this->alertSuccessMessage('ApplicationNavigation deleted.');
    }
}
