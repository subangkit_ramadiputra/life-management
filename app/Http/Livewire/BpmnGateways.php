<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\BpmnGateway;

class BpmnGateways extends Component
{
    use AlertMessageTrait;

    public $bpmn_gateways, $bpmn_gateway_id;
    public $type_id;
    public $code;
    public $name;
    public $summary;
    public $gatewayable;
    public $role_id;
    public $default_squence_id;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->bpmn_gateways = BpmnGateway::all();
        $this->total_rows = BpmnGateway::count();
        return view('livewire.bpmn_gateways');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->bpmn_gateway_id = '';
        $this->type_id = '';
        $this->code = '';
        $this->name = '';
        $this->summary = '';
        $this->gatewayable = '';
        $this->role_id = '';
        $this->default_squence_id = '';

    }

    public function store()
    {
        $this->validate([
            'type_id' => 'required',
            'code' => 'required',
            'name' => 'required',
            'gatewayable' => 'required',
            'role_id' => 'required',
            'default_squence_id' => 'required',

        ]);

        BpmnGateway::updateOrCreate(['id' => $this->bpmn_gateway_id], [
            'type_id' => $this->type_id,
            'code' => $this->code,
            'name' => $this->name,
            'summary' => $this->summary,
            'gatewayable' => $this->gatewayable,
            'role_id' => $this->role_id,
            'default_squence_id' => $this->default_squence_id,

        ]);

        $this->alertSuccessMessage($this->bpmn_gateway_id ? 'BpmnGateway updated.' : 'BpmnGateway created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $bpmn_gateway = BpmnGateway::findOrFail($id);
        $this->bpmn_gateway_id = $id;
        $this->type_id = $bpmn_gateway->type_id;
        $this->code = $bpmn_gateway->code;
        $this->name = $bpmn_gateway->name;
        $this->summary = $bpmn_gateway->summary;
        $this->gatewayable = $bpmn_gateway->gatewayable;
        $this->role_id = $bpmn_gateway->role_id;
        $this->default_squence_id = $bpmn_gateway->default_squence_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        BpmnGateway::find($id)->delete();
        $this->alertSuccessMessage('BpmnGateway deleted.');
    }
}
