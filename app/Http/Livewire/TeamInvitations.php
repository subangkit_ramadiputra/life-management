<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\TeamInvitation;

class TeamInvitations extends Component
{
    use AlertMessageTrait;

    public $team_invitations, $team_invitation_id;
    public $team_id;
    public $email;
    public $role;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->team_invitations = TeamInvitation::all();
        $this->total_rows = TeamInvitation::count();
        return view('livewire.team_invitations');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->team_invitation_id = '';
        $this->team_id = '';
        $this->email = '';
        $this->role = '';

    }

    public function store()
    {
        $this->validate([
            'team_id' => 'required',
            'email' => 'required',

        ]);

        TeamInvitation::updateOrCreate(['id' => $this->team_invitation_id], [
            'team_id' => $this->team_id,
            'email' => $this->email,
            'role' => $this->role,

        ]);
        $this->alertSuccessMessage($this->team_invitation_id ? 'TeamInvitation updated.' : 'TeamInvitation created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $team_invitation = TeamInvitation::findOrFail($id);
        $this->team_invitation_id = $id;
        $this->team_id = $team_invitation->team_id;
        $this->email = $team_invitation->email;
        $this->role = $team_invitation->role;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        TeamInvitation::find($id)->delete();
        $this->alertSuccessMessage('TeamInvitation deleted.');
    }
}
