<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AlertMessageRender extends Component
{
    public function render()
    {
        return view('livewire.alert-message-render');
    }
}
