<?php

namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;

use App\Models\Application;
use App\Models\Concept;
use App\Models\Entity;
use App\Models\Feature;
use App\Models\Place;
use App\Models\Tag;
use App\Models\Taggable;
use App\Models\Transition;
use Livewire\Component;

class DashboardLife extends Component
{
    use AlertMessageTrait;

    public $analytics = [
        'concepts' => 0,
        'applications' => 0,
        'features' => 0,
        'entites' => 0,
    ];
    public $workflows = [
        'places' => 0,
        'transitions' => 0,
    ];

    public $priority_concepts = [];
    public function render()
    {
        $this->analytics = [
            'concepts' => Concept::count(),
            'applications' => Application::count(),
            'features' => Feature::count(),
            'entities' => Entity::count(),
        ];
        $this->workflows = [
            'places' => Place::count(),
            'transitions' => Transition::count(),
        ];

        $tag = Tag::where('name','Priority')->first();
        $concept_ids = Taggable::where('tag_id',$tag->id)->where('taggable_type',Concept::class)->get()->pluck('taggable_id');
        $this->priority_concepts = Concept::whereIn('id',$concept_ids)->get();

        return view('livewire.dashboard-life');
    }
}
