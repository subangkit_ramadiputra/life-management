<?php
namespace App\Http\Livewire;
use App\Models\ContextDefinition;
use App\Models\Framework;
use App\Traits\AlertMessageTrait;
use App\Traits\GenerateTraits;
use Livewire\Component;
use App\Models\ContextGeneration;

class ContextGenerations extends Component
{
    use AlertMessageTrait, GenerateTraits;

    public $context_generations, $context_generation_id;
    public $context_definitions, $context_definition_id;
    public $scopes = [];
    public $scope_generation;
    public $frameworks, $framework_id;
    public $template;
    public $custom_data;
    public $mode;
    public $testing_template = '';

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->context_generations = ContextGeneration::all();
        $this->context_definitions = ContextDefinition::all();
        $this->frameworks = Framework::all();
        $this->total_rows = ContextGeneration::count();
        return view('livewire.context_generations');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->context_generation_id = '';
        $this->context_definition_id = '';
        $this->scope_generation = 'body';
        $this->framework_id = '';
        $this->template = '';
        $this->custom_data = '';
        $this->mode = 'javascript';

    }

    public function store()
    {
        $this->validate([
            'context_definition_id' => 'required',
            'scope_generation' => 'required',
            'framework_id' => 'required',
            'template' => 'required',
            'custom_data' => 'required',
            'mode' => 'required',

        ]);

        ContextGeneration::updateOrCreate(['id' => $this->context_generation_id], [
            'context_definition_id' => $this->context_definition_id,
            'scope_generation' => $this->scope_generation,
            'framework_id' => $this->framework_id,
            'template' => $this->template,
            'custom_data' => $this->custom_data,
            'mode' => $this->mode,

        ]);

        $this->alertSuccessMessage($this->context_generation_id ? 'ContextGeneration updated.' : 'ContextGeneration created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $context_generation = ContextGeneration::findOrFail($id);
        $this->context_generation_id = $id;
        $this->context_definition_id = $context_generation->context_definition_id;
        $this->scope_generation = $context_generation->scope_generation;
        $this->framework_id = $context_generation->framework_id;
        $this->template = $context_generation->template;
        $this->custom_data = $context_generation->custom_data;
        $this->mode = $context_generation->mode;

        $this->changeDefinition();

        $this->openModalPopover();
    }

    public function duplicate($id)
    {
        $context_generation = ContextGeneration::findOrFail($id);
        ContextGeneration::updateOrCreate(['id' => null], [
            'context_definition_id' => $context_generation->context_definition_id,
            'scope_generation' => $context_generation->scope_generation,
            'framework_id' => $context_generation->framework_id,
            'template' => $context_generation->template,
            'custom_data' => $context_generation->custom_data,
            'mode' => $context_generation->mode,

        ]);

        $this->render();
    }

    public function delete($id)
    {
        ContextGeneration::find($id)->delete();
        $this->alertSuccessMessage('ContextGeneration deleted.');
    }
    public function changeDefinition() {
        $context = ContextDefinition::find($this->context_definition_id);
        $this->scopes = [];
        $options = json_decode($context->requirement);
        if (isset($options->scopes))
            $this->scopes = $options->scopes;
        $this->framework_id = $context->framework_id;
    }

    public $format_output = '';
    public function testTemplate() {
        //Collecting Variable
        $variables = [];
        $context_generation_data = (object) [
            'id' => $this->context_generation_id,
            'context_definition_id' => $this->context_definition_id,
            'scope_generation' => $this->scope_generation,
            'framework_id' => $this->framework_id,
            'template' => $this->template,
            'custom_data' => $this->custom_data,
            'mode' => $this->mode,
        ];
        $context_definition_data = ContextDefinition::find($this->context_definition_id);

        $this->parseDefinitionObject($variables, $context_definition_data);
        $this->parseGenerationObject($variables, $context_generation_data);

        $template = $this->template;
        $this->testing_template = $this->getStubContents($template,$variables,'$$','$$');
    }

    public function parseDefinitionObject(&$variables, $object) {
        $variables['context_definition_id'] = $object->id;
        $variables['context_name'] = $object->context_name;
    }

    public function parseGenerationObject(&$variables, $object) {
        $variables['context_generation_id'] = $object->id;
        $variables['scope_generation'] = $object->scope_generation;

        //Integrate Custom Data
        $custom_data = json_decode($object->custom_data);
        $this->parseGenerationCustomData($variables, $custom_data);
    }

    public function getDriver($driver) {
        $driver = "\\App\\Http\\ContextDrivers\\".$driver;
        $driverInstance = new $driver;
        return $driverInstance;
    }
    public function parseGenerationCustomData(&$variables, $customData) {
        if (isset($customData->integrations)) {
            if (is_array($customData->integrations)) {
                foreach($customData->integrations as $integration) {
                    $driver = $this->getDriver($integration->type);
                    $variables[$integration->name] = $driver->handle(array_merge($variables, (array) $integration, (array) $integration->parameters));
                }
            }
        }
    }
}
