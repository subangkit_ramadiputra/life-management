<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Transition;

class Transitions extends Component
{
    use AlertMessageTrait;

    public $transitions, $transition_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $from_place_id;
    public $to_place_id;
    public $total_rows = 0;

    public $isModalOpen = 0;
    public function render()
    {
        $this->transitions = Transition::all();
        $this->total_rows = Transition::count();
        return view('livewire.transitions');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->transition_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';
        $this->from_place_id = 0;
        $this->to_place_id = 0;

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'from_place_id' => 'required',
            'to_place_id' => 'required',

        ]);

        Transition::updateOrCreate(['id' => $this->transition_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'from_place_id' => $this->from_place_id,
            'to_place_id' => $this->to_place_id,

        ]);
        $this->alertSuccessMessage($this->transition_id ? 'Transition updated.' : 'Transition created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $transition = Transition::findOrFail($id);
        $this->transition_id = $id;
        $this->title = $transition->title;
        $this->summary = $transition->summary;
        $this->body = $transition->body;
        $this->body_format = $transition->body_format;
        $this->from_place_id = $transition->from_place_id;
        $this->to_place_id = $transition->to_place_id;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Transition::find($id)->delete();
        $this->alertSuccessMessage('Transition deleted.');
    }
}
