<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use App\Models\Application;
use App\Models\Concept;
use App\Models\Entity;
use App\Models\Transition;
use App\Models\Upload;
use App\Models\Sop;
use App\Models\Link;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use App\Models\Feature;
use Livewire\WithFileUploads;
use Symfony\Component\Process\Process;
use Symfony\Component\Workflow\DefinitionBuilder;
use Symfony\Component\Workflow\Dumper\GraphvizDumper;
use Symfony\Component\Workflow\Dumper\MermaidDumper;

class Features extends Component
{
    use AlertMessageTrait;

    protected $queryString = ['filter_concept_id','filter_application_id'];
    public $features = [];
    public $feature_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $total_rows = 0;

    public $isModalOpen = 0;
    /**
     * @var mixed
     */
    private $recent_entity_uploads;

    public $filter_concept_id = 0;
    public $filter_application_id = 0;

    public function render()
    {
        $this->changeFilter();
        $this->concepts = Concept::all();
        return view('livewire.features');
    }
    // Filter Section
    public function changeFilter() {
        if ($this->filter_concept_id != 0) {
            $this->applications = Application::where('concept_id', $this->filter_concept_id)->get();
            if ($this->filter_application_id != 0) {
                $query = Feature::whereHasMorph(
                    'featureable',
                    [Application::class],
                    function ($query) {
                        $query->where('id', '=', $this->filter_application_id);
                    }
                );
                $this->features = $query->get();
                $this->total_rows = $query->count();
            } else {
                $query = Feature::whereHasMorph(
                    'featureable',
                    [Application::class],
                    function ($query) {
                        $query->where('concept_id', '=', $this->filter_concept_id);
                    }
                );
                $this->features = $query->get();
                $this->total_rows = $query->count();
            }
        } else {
            $this->features = Feature::all();
            $this->total_rows = Feature::count();
            $this->applications = [];
        }
    }
    // End of Section
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->feature_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',

        ]);

        $featureable_type = '';
        $featureable_id = '';

        if ($this->filter_application_id != 0) {
            $featureable_type = "App\\Models\\Application";
            $featureable_id = $this->filter_application_id;
        }

        Feature::updateOrCreate(['id' => $this->feature_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'featureable_type' => $featureable_type,
            'featureable_id' => $featureable_id,

        ]);
        $this->alertSuccessMessage($this->feature_id ? 'Feature updated.' : 'Feature created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $feature = Feature::findOrFail($id);
        $this->feature_id = $id;
        $this->title = $feature->title;
        $this->summary = $feature->summary;
        $this->body = $feature->body;
        $this->body_format = $feature->body_format;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Feature::find($id)->delete();
        $this->alertSuccessMessage('Feature deleted.');
    }

    // Entity Section
    public $isEntityModalOpen = 0;
    public $recent_entities = [];
    public $entities = [];
    public function addEntity($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_entities = $feature->entities()->get();
        $this->resetEntityCreateForm();
        $this->openEntityModalPopover();
    }

    public function openEntityModalPopover()
    {
        $this->isEntityModalOpen = true;
    }
    public function closeEntityModalPopover()
    {
        $this->isEntityModalOpen = false;
    }

    public function editEntity($entity_id) {
        $entity = Entity::findOrFail($entity_id);
        $this->entity_id = $entity_id;
        $this->entity_title = $entity->title;
        $this->entity_summary = $entity->summary;
        $this->entity_body = $entity->body;
        $this->entity_body_format = $entity->body_format;
    }

    public function resetEntityCreateForm(){
        $this->entity_id = '';
        $this->entity_title = '';
        $this->entity_summary = '';
        $this->entity_body = '';
        $this->entity_body_format = 'markdown';
    }

    public function assignEntity() {
        $feature = Feature::findOrFail($this->feature_id);
        if ($this->entity_id == '') {
            $entity = new Entity();
            $entity->title = $this->entity_title;
            $entity->summary = $this->entity_summary;
            $entity->body = $this->entity_body;
            $entity->body_format = $this->entity_body_format;
            $feature->entities()->save($entity);

            $this->alertSuccessMessage('Entity created.');
            //$this->closeEntityModalPopover();
            $this->resetEntityCreateForm();
        } else {
            Entity::updateOrCreate(['id' => $this->entity_id], [
                'title' => $this->entity_title,
                'summary' => $this->entity_summary,
                'body' => $this->entity_body,
                'body_format' => $this->entity_body_format,
            ]);

            $this->alertSuccessMessage('Entity updated.');
        }

        $this->recent_entities = $feature->entities()->get();
    }

    public function deleteEntity($feature_id, $entity_id) {
        $feature = Feature::findOrFail($feature_id);

        $entity = Entity::find($entity_id);
        if ($entity) {
            $feature->entities()->where('id', '=', $entity->id)->delete();
        }

        $this->recent_entities = $feature->entities()->get();

        $this->alertSuccessMessage('Entity deleted.');
    }

    public function deleteExistingEntity($entity_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $entity = Entity::find($entity_id);
        if ($entity) {
            $feature->entities()->where('id', '=', $entity->id)->delete();
        }

        $this->recent_entities = $feature->entities()->get();

        $this->alertSuccessMessage('Entity deleted.');
    }
    // End of Section


    // Entity Upload Section
    use WithFileUploads;
    public $isEntityUploadModalOpen = 0;
    public $entity_uploads = [];
    public function addEntityUpload($id) {
        $this->entity_id = $id;
        $entity = Entity::findOrFail($this->entity_id);
        $this->recent_entity_uploads = $entity->uploads()->get();
        $this->resetEntityUploadCreateForm();
        $this->openEntityUploadModalPopover();
    }

    public function openEntityUploadModalPopover()
    {
        $this->isEntityUploadModalOpen = true;
    }
    public function closeEntityUploadModalPopover()
    {
        $this->isEntityUploadModalOpen = false;
    }
    private function resetEntityUploadCreateForm(){
        $this->entity_uploads = [];
    }

    public function assignEntityUpload() {
        $this->validate([
            'entity_uploads.*' => 'max:309600', // 1MB Max
        ]);

        $entity = Entity::findOrFail($this->entity_id);

        foreach ($this->entity_uploads as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload();
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $entity->uploads()->save($upload);
        }

        $this->recent_entity_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        $this->closeEntityUploadModalPopover();
        $this->resetEntityUploadCreateForm();
    }

    public function deleteEntityUpload($entity_id, $upload_id) {
        $entity = Entity::findOrFail($entity_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $entity->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_entity_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingEntityUpload($upload_id) {
        $entity = Entity::findOrFail($this->entity_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $entity->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_entity_uploads = $entity->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // Transition Section
    public $isTransitionModalOpen = 0;
    public $recent_transitions = [];
    public $transitions = [];
    public $from_places;
    public $to_places;
    public $transition_id;
    public $transition_title;
    public $transition_summary;
    public $transition_body;
    public $transition_body_format;
    public $transition_from_place_id;
    public $transition_to_place_id;

    public function changeFromPlace() {
        $feature = Feature::findOrFail($this->feature_id);
        // $this->to_places = $feature->entities()->where('id','!=',$this->transition_from_place_id)->get();
    }

    public function changeToPlace() {
        $feature = Feature::findOrFail($this->feature_id);
        // $this->from_places = $feature->entities()->where('id','!=',$this->transition_to_place_id)->get();
    }

    public function addTransition($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_transitions = $feature->transitions()->get();
        $this->resetTransitionCreateForm();
        $this->openTransitionModalPopover();
        $this->from_places = $feature->entities()->get();
        $this->to_places = $feature->entities()->get();
    }

    public function openTransitionModalPopover()
    {
        $this->isTransitionModalOpen = true;
    }
    public function closeTransitionModalPopover()
    {
        $this->isTransitionModalOpen = false;
    }

    public function editTransition($transition_id) {
        $transition = Transition::findOrFail($transition_id);
        $this->transition_id = $transition_id;
        $this->transition_title = $transition->title;
        $this->transition_summary = $transition->summary;
        $this->transition_body = $transition->body;
        $this->transition_body_format = $transition->body_format;
        $this->transition_from_place_id = $transition->from_place_id;
        $this->transition_to_place_id = $transition->to_place_id;
    }

    public function resetTransitionCreateForm(){
        $this->transition_id = '';
        $this->transition_title = '';
        $this->transition_summary = '';
        $this->transition_body = '';
        $this->transition_body_format = 'markdown';
        $this->from_place_id = 0;
        $this->to_place_id = 0;
    }

    public function assignTransition() {
        $feature = Feature::findOrFail($this->feature_id);
        if ($this->transition_id == '') {
            $transition = new Transition();
            $transition->title = $this->transition_title;
            $transition->summary = $this->transition_summary;
            $transition->body = $this->transition_body;
            $transition->body_format = $this->transition_body_format;
            $transition->from_place_id = $this->transition_from_place_id;
            $transition->to_place_id = $this->transition_to_place_id;

            $feature->transitions()->save($transition);

            $this->alertSuccessMessage('Transition created.');
            //$this->closeTransitionModalPopover();
            $this->resetTransitionCreateForm();
        } else {
            Transition::updateOrCreate(['id' => $this->transition_id], [
                'title' => $this->transition_title,
                'summary' => $this->transition_summary,
                'body' => $this->transition_body,
                'body_format' => $this->transition_body_format,
                'from_place_id' => $this->transition_from_place_id,
                'to_place_id' => $this->transition_to_place_id,
            ]);

            $this->alertSuccessMessage('Transition updated.');
        }

        $this->recent_transitions = $feature->transitions()->get();
        $this->from_places = $feature->entities()->get();
        $this->to_places = $feature->entities()->get();
    }

    public function deleteTransition($feature_id, $transition_id) {
        $feature = Feature::findOrFail($feature_id);

        $transition = Transition::find($transition_id);
        if ($transition) {
            $feature->transitions()->where('id', '=', $transition->id)->delete();
        }

        $this->recent_transitions = $feature->transitions()->get();
        $this->from_places = $feature->entities()->get();
        $this->to_places = $feature->entities()->get();

        $this->alertSuccessMessage('Transition deleted.');
    }

    public function deleteExistingTransition($transition_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $transition = Transition::find($transition_id);
        if ($transition) {
            $feature->transitions()->where('id', '=', $transition->id)->delete();
        }

        $this->recent_transitions = $feature->transitions()->get();
        $this->from_places = $feature->entities()->get();
        $this->to_places = $feature->entities()->get();

        $this->alertSuccessMessage('Transition deleted.');
    }
    // End of Section

    // Workflow Section
    public function generateWorkflow($feature_id) {
        $this->feature_id = $feature_id;
        $feature = Feature::findOrFail($this->feature_id);

        // Workflow Section
        $definitionBuilder = new DefinitionBuilder();
        $places = $feature->entities()->get()->pluck('title')->toArray();
        $definition = $definitionBuilder->addPlaces($places);
        foreach($feature->transitions()->get() as $transition) {
            // Transitions are defined with a unique name, an origin place and a destination place
            $definition->addTransition(new \Symfony\Component\Workflow\Transition($transition->title, $transition->from_entity->title, $transition->to_entity->title));
        }
        $definition = $definition->build();

        Storage::disk('public')->makeDirectory('/workflows');
        $path = Storage::disk('public')->path('/workflows');

        $dumper = 'dot';
        switch($dumper) {
            case 'dot' :
                // Dot
                $dumper = new GraphvizDumper();
                $format = 'png';
                $workflowName = $feature->title.'_'.date('YmdHis');
                $dotCommand = ['dot', "-T${format}", '-o', "${workflowName}.${format}"];
                // End of Section
                break;
            case 'mermaid' :
                // Mermaid
                $dumper = new MermaidDumper(MermaidDumper::TRANSITION_TYPE_WORKFLOW);
                $format = 'png';
                $workflowName = $feature->title.'_'.date('YmdHis');
                $dotCommand = ['mmdc', '-o', "${workflowName}.${format}"];
                // End of Section
                break;
        }

        $process = new Process($dotCommand);
        $process->setWorkingDirectory($path);
        $process->setInput($dumper->dump($definition));
        $process->mustRun();

        $upload = new Upload();
        $upload->file_name = $workflowName.'.'.$format;
        $upload->file_size = Storage::disk('public')->size('workflows/'.$workflowName.'.'.$format);
        $upload->file_extension = $format;
        $upload->path = $path;
        $upload->url = Storage::disk('public')->url('workflows/'.$workflowName.'.'.$format);
        $feature->uploads()->save($upload);
    }
    // End of Section

    // Upload Section
    use WithFileUploads;
    public $isUploadModalOpen = 0;
    public $files = [];
    public function addUpload($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_uploads = $feature->uploads()->get();
        $this->resetUploadCreateForm();
        $this->openUploadModalPopover();
    }

    public function openUploadModalPopover()
    {
        $this->isUploadModalOpen = true;
    }
    public function closeUploadModalPopover()
    {
        $this->isUploadModalOpen = false;
    }
    private function resetUploadCreateForm(){
        $this->files = [];
    }

    public function assignUpload() {
        $this->validate([
            'files.*' => 'max:309600', // 1MB Max
        ]);

        $feature = Feature::findOrFail($this->feature_id);

        foreach ($this->files as $file) {
            $file->store('file');
            $file_name = $file->getClientOriginalName();
            $extension = $file->extension();
            $size = $file->getSize();

            $md5Name = md5_file($file->getRealPath());
            $guessExtension = $file->guessExtension();
            $file_path = $file->storeAs('uploads', pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension  ,'public');

            $upload = new Upload();
            $upload->file_name = $file_name;
            $upload->file_size = $size;
            $upload->file_extension = $extension;
            $upload->path = $file_path;
            $upload->url = Storage::disk('public')->url('uploads/'.pathinfo($file_name, PATHINFO_FILENAME).'_'.$md5Name.'.'.$guessExtension);

            $feature->uploads()->save($upload);
        }

        $this->recent_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('File updated.');
        // $this->closeUploadModalPopover();
        $this->resetUploadCreateForm();
    }

    public function deleteUpload($feature_id, $upload_id) {
        $feature = Feature::findOrFail($feature_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $feature->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('File deleted.');
    }

    public function deleteExistingUpload($upload_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $upload = Upload::find($upload_id);
        if ($upload) {
            Storage::disk('public')->delete($upload->path);
            $feature->uploads()->where('id', '=', $upload->id)->delete();
        }

        $this->recent_uploads = $feature->uploads()->get();

        $this->alertSuccessMessage('Tag deleted.');
    }
    // End of Section

    // Sop Section
    public $isSopModalOpen = 0;
    public $recent_sops = [];
    public $sops = [];
    public function addSop($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_sops = $feature->sops()->get();
        $this->resetSopCreateForm();
        $this->openSopModalPopover();
    }

    public function openSopModalPopover()
    {
        $this->isSopModalOpen = true;
    }
    public function closeSopModalPopover()
    {
        $this->isSopModalOpen = false;
    }

    public function editSop($sop_id) {
        $sop = Sop::findOrFail($sop_id);
        $this->sop_id = $sop_id;
        $this->sop_title = $sop->title;
        $this->sop_summary = $sop->summary;
        $this->sop_body_format = $sop->body_format;
        $this->sop_body = $sop->body;
        $this->sop_sopable = $sop->sopable;

    }

    public function resetSopCreateForm(){
        $this->sop_id = '';
        $this->sop_title = '';
        $this->sop_summary = '';
        $this->sop_body_format = 'markdown';
        $this->sop_body = '';
        $this->sop_sopable = '';

    }

    public function assignSop() {
        $feature = Feature::findOrFail($this->feature_id);
        if ($this->sop_id == '') {
            $sop = new Sop();
            $sop->title = $this->sop_title;
            $sop->summary = $this->sop_summary;
            $sop->body_format = $this->sop_body_format;
            $sop->body = $this->sop_body;

            $feature->sops()->save($sop);

            $this->alertSuccessMessage('Sop created.');
            //$this->closeSopModalPopover();
            $this->resetSopCreateForm();
        } else {
            Sop::updateOrCreate(['id' => $this->sop_id], [
                'title' => $this->sop_title,
                'summary' => $this->sop_summary,
                'body_format' => $this->sop_body_format,
                'body' => $this->sop_body,

            ]);

            $this->alertSuccessMessage('Sop updated.');
        }

        $this->recent_sops = $feature->sops()->get();
    }

    public function deleteSop($feature_id, $sop_id) {
        $feature = Feature::findOrFail($feature_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $feature->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $feature->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }

    public function deleteExistingSop($sop_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $sop = Sop::find($sop_id);
        if ($sop) {
            $feature->sops()->where('id', '=', $sop->id)->delete();
        }

        $this->recent_sops = $feature->sops()->get();

        $this->alertSuccessMessage('Sop deleted.');
    }
    // End of Section

    // Link Section
    public $isLinkModalOpen = 0;
    public $recent_links = [];
    public $links = [];
    public function addLink($id) {
        $this->feature_id = $id;
        $feature = Feature::findOrFail($this->feature_id);
        $this->recent_links = $feature->links()->get();
        $this->resetLinkCreateForm();
        $this->openLinkModalPopover();
    }

    public function openLinkModalPopover()
    {
        $this->isLinkModalOpen = true;
    }
    public function closeLinkModalPopover()
    {
        $this->isLinkModalOpen = false;
    }

    public function editLink($link_id) {
        $link = Link::findOrFail($link_id);
        $this->link_id = $link_id;
        $this->link_title = $link->title;
        $this->link_url = $link->url;
        $this->link_summary = $link->summary;
        $this->link_linkable = $link->linkable;

    }

    public function resetLinkCreateForm(){
        $this->link_id = '';
        $this->link_title = '';
        $this->link_url = '';
        $this->link_summary = '';
        $this->link_linkable = '';

    }

    public function assignLink() {
        $feature = Feature::findOrFail($this->feature_id);
        if ($this->link_id == '') {
            $link = new Link();
            $link->title = $this->link_title;
            $link->url = $this->link_url;
            $link->summary = $this->link_summary;

            $feature->links()->save($link);

            $this->alertSuccessMessage('Link created.');
            //$this->closeLinkModalPopover();
            $this->resetLinkCreateForm();
        } else {
            Link::updateOrCreate(['id' => $this->link_id], [
                'title' => $this->link_title,
                'url' => $this->link_url,
                'summary' => $this->link_summary,

            ]);

        }

        $this->recent_links = $feature->links()->get();
    }

    public function deleteLink($feature_id, $link_id) {
        $feature = Feature::findOrFail($feature_id);

        $link = Link::find($link_id);
        if ($link) {
            $feature->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $feature->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }

    public function deleteExistingLink($link_id) {
        $feature = Feature::findOrFail($this->feature_id);

        $link = Link::find($link_id);
        if ($link) {
            $feature->links()->where('id', '=', $link->id)->delete();
        }

        $this->recent_links = $feature->links()->get();

        $this->alertSuccessMessage('Link deleted.');
    }
    // End of Section
}
