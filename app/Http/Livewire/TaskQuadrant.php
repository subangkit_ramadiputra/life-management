<?php

namespace App\Http\Livewire;

use App\Models\Task;
use Livewire\Component;

class TaskQuadrant extends Component
{
    public $q1 = [];
    public $q2 = [];
    public $q3 = [];
    public $q4 = [];

    public function render()
    {
        $this->q1 = Task::where('quadrant_id',1)->where('status','!=','done')->get();
        $this->q2 = Task::where('quadrant_id',2)->where('status','!=','done')->get();
        $this->q3 = Task::where('quadrant_id',3)->where('status','!=','done')->get();
        $this->q4 = Task::where('quadrant_id',4)->where('status','!=','done')->get();
        return view('livewire.task-quadrant');
    }
}
