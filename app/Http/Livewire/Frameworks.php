<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Framework;

class Frameworks extends Component
{
    use AlertMessageTrait;

    public $frameworks, $framework_id;
    public $Framework;
    public $short_name;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->frameworks = Framework::all();
        $this->total_rows = Framework::count();
        return view('livewire.frameworks');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->framework_id = '';
        $this->Framework = '';
        $this->short_name = '';

    }

    public function store()
    {
        $this->validate([
            'Framework' => 'required',
            'short_name' => 'required',

        ]);

        Framework::updateOrCreate(['id' => $this->framework_id], [
            'Framework' => $this->Framework,
            'short_name' => $this->short_name,

        ]);

        $this->alertSuccessMessage($this->framework_id ? 'Framework updated.' : 'Framework created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $framework = Framework::findOrFail($id);
        $this->framework_id = $id;
        $this->Framework = $framework->Framework;
        $this->short_name = $framework->short_name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Framework::find($id)->delete();
        $this->alertSuccessMessage('Framework deleted.');
    }
}
