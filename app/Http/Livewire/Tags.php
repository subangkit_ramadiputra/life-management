<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Tag;

class Tags extends Component
{
    use AlertMessageTrait;

    public $tags, $tag_id;
    public $name;
    public $total_rows = 0;

    public $isModalOpen = 0;
    public function render()
    {
        $this->tags = Tag::all();
        $this->total_rows = Tag::count();
        return view('livewire.tags');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->tag_id = '';
        $this->name = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',

        ]);

        Tag::updateOrCreate(['id' => $this->tag_id], [
            'name' => $this->name,

        ]);
        $this->alertSuccessMessage($this->tag_id ? 'Tag updated.' : 'Tag created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        $this->tag_id = $id;
        $this->name = $tag->name;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Tag::find($id)->delete();
        $this->alertSuccessMessage('Tag deleted.');
    }
}
