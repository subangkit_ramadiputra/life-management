<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Place;

class Places extends Component
{
    use AlertMessageTrait;

    public $places, $place_id;
    public $title;
    public $summary;
    public $body;
    public $body_format;
    public $weight;
    public $total_rows = 0;

    public $isModalOpen = 0;
    public function render()
    {
        $this->places = Place::all();
        $this->total_rows = Place::count();
        return view('livewire.places');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->place_id = '';
        $this->title = '';
        $this->summary = '';
        $this->body = '';
        $this->body_format = 'markdown';
        $this->weight = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'body_format' => 'required',
            'weight' => 'required',

        ]);

        Place::updateOrCreate(['id' => $this->place_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'body' => $this->body,
            'body_format' => $this->body_format,
            'weight' => $this->weight,

        ]);
        $this->alertSuccessMessage($this->place_id ? 'Place updated.' : 'Place created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $place = Place::findOrFail($id);
        $this->place_id = $id;
        $this->title = $place->title;
        $this->summary = $place->summary;
        $this->body = $place->body;
        $this->body_format = $place->body_format;
        $this->weight = $place->weight;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Place::find($id)->delete();
        $this->alertSuccessMessage('Place deleted.');
    }
}
