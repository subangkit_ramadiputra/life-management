<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\GenericContext;

class GenericContexts extends Component
{
    use AlertMessageTrait;

    public $generic_contexts, $generic_context_id;
    public $group;
    public $context_name;
    public $definition;
    public $goals;
    public $inputs;
    public $suppliers;
    public $activities;
    public $participants;
    public $deliverables;
    public $consumers;
    public $techniques;
    public $tools;
    public $metrics;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->generic_contexts = GenericContext::all();
        $this->total_rows = GenericContext::count();
        return view('livewire.generic_contexts');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->generic_context_id = '';
        $this->group = '';
        $this->context_name = '';
        $this->definition = '';
        $this->goals = '';
        $this->inputs = '';
        $this->suppliers = '';
        $this->activities = '';
        $this->participants = '';
        $this->deliverables = '';
        $this->consumers = '';
        $this->techniques = '';
        $this->tools = '';
        $this->metrics = '';

    }

    public function store()
    {
        $this->validate([
            'group' => 'required',
            'context_name' => 'required',
            'definition' => 'required',
            'goals' => 'required',

        ]);

        GenericContext::updateOrCreate(['id' => $this->generic_context_id], [
            'group' => $this->group,
            'context_name' => $this->context_name,
            'definition' => $this->definition,
            'goals' => $this->goals,
            'inputs' => $this->inputs,
            'suppliers' => $this->suppliers,
            'activities' => $this->activities,
            'participants' => $this->participants,
            'deliverables' => $this->deliverables,
            'consumers' => $this->consumers,
            'techniques' => $this->techniques,
            'tools' => $this->tools,
            'metrics' => $this->metrics,

        ]);

        $this->alertSuccessMessage($this->generic_context_id ? 'GenericContext updated.' : 'GenericContext created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $generic_context = GenericContext::findOrFail($id);
        $this->generic_context_id = $id;
        $this->group = $generic_context->group;
        $this->context_name = $generic_context->context_name;
        $this->definition = $generic_context->definition;
        $this->goals = $generic_context->goals;
        $this->inputs = $generic_context->inputs;
        $this->suppliers = $generic_context->suppliers;
        $this->activities = $generic_context->activities;
        $this->participants = $generic_context->participants;
        $this->deliverables = $generic_context->deliverables;
        $this->consumers = $generic_context->consumers;
        $this->techniques = $generic_context->techniques;
        $this->tools = $generic_context->tools;
        $this->metrics = $generic_context->metrics;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        GenericContext::find($id)->delete();
        $this->alertSuccessMessage('GenericContext deleted.');
    }
}
