<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\DatabaseServer;
use App\Models\Hostable;

class DatabaseServers extends Component
{
    use AlertMessageTrait;

    public $database_servers, $database_server_id;
    public $title;
    public $summary;
    public $type_id;
    public $databaseable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    protected $queryString = ['filter_polymorph_id','filter_polymorph_object'];
    public $filter_polymorph_id = 0;
    public $filter_polymorph_object = '';
    public function filterResult() {
        $query = DatabaseServer::select('*');
        if ($this->filter_polymorph_object != '') {
            $this->sopable_type = 'App\\Models\\'.$this->filter_polymorph_object;
            $this->sopable_id = $this->filter_polymorph_id;
            $query->where('databaseable_type','App\\Models\\'.$this->filter_polymorph_object);
            $query->where('databaseable_id', $this->filter_polymorph_id);
        }

        $this->database_servers = $query->get();
        $this->total_rows = $query->count();
    }

    public function render()
    {
        $this->filterResult();
        return view('livewire.database_servers');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->database_server_id = '';
        $this->title = '';
        $this->summary = '';
        $this->type_id = '';
        $this->databaseable = '';

    }

    public function store()
    {
        $this->validate([
            'title' => 'required',
            'summary' => 'required',
            'type_id' => 'required',
            'databaseable' => 'required',

        ]);

        DatabaseServer::updateOrCreate(['id' => $this->database_server_id], [
            'title' => $this->title,
            'summary' => $this->summary,
            'type_id' => $this->type_id,
            'databaseable' => $this->databaseable,

        ]);

        $this->alertSuccessMessage($this->database_server_id ? 'DatabaseServer updated.' : 'DatabaseServer created.');

        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $database_server = DatabaseServer::findOrFail($id);
        $this->database_server_id = $id;
        $this->title = $database_server->title;
        $this->summary = $database_server->summary;
        $this->type_id = $database_server->type_id;
        $this->databaseable = $database_server->databaseable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        DatabaseServer::find($id)->delete();
        $this->alertSuccessMessage('DatabaseServer deleted.');
    }

    // Hostable Section
    public $isHostableModalOpen = 0;
    public $recent_hostables = [];
    public $hostables = [];
    public function addHostable($id) {
        $this->database_server_id = $id;
        $database_server = DatabaseServer::findOrFail($this->database_server_id);
        $this->recent_hostables = $database_server->hostables()->get();
        $this->resetHostableCreateForm();
        $this->openHostableModalPopover();
    }

    public function openHostableModalPopover()
    {
        $this->isHostableModalOpen = true;
    }
    public function closeHostableModalPopover()
    {
        $this->isHostableModalOpen = false;
    }

    public function editHostable($hostable_id) {
        $hostable = Hostable::findOrFail($hostable_id);
        $this->hostable_id = $hostable_id;
        $this->hostable_hostable = $hostable->hostable;
        $this->hostable_host = $hostable->host;
        $this->hostable_port = $hostable->port;
        $this->hostable_instance = $hostable->instance;
        $this->hostable_environment = $hostable->environment;

    }

    public function resetHostableCreateForm(){
        $this->hostable_id = '';
        $this->hostable_hostable = '';
        $this->hostable_host = '';
        $this->hostable_port = '';
        $this->hostable_instance = '';
        $this->hostable_environment = 'Production';

    }

    public function assignHostable() {
        $database_server = DatabaseServer::findOrFail($this->database_server_id);
        if ($this->hostable_id == '') {
            $hostable = new Hostable();
            $hostable->host = $this->hostable_host;
            $hostable->port = $this->hostable_port;
            $hostable->instance = $this->hostable_instance;
            $hostable->environment = $this->hostable_environment;

            $database_server->hostables()->save($hostable);

            $this->alertSuccessMessage('Hostable created.');
            //$this->closeHostableModalPopover();
            $this->resetHostableCreateForm();
        } else {
            Hostable::updateOrCreate(['id' => $this->hostable_id], [
                'host' => $this->hostable_host,
                'port' => $this->hostable_port,
                'instance' => $this->hostable_instance,
                'environment' => $this->hostable_environment,

            ]);
            $this->alertSuccessMessage('Hostable updated.');
        }

        $this->recent_hostables = $database_server->hostables()->get();
    }

    public function deleteHostable($database_server_id, $hostable_id) {
        $database_server = DatabaseServer::findOrFail($database_server_id);

        $hostable = Hostable::find($hostable_id);
        if ($hostable) {
            $database_server->hostables()->where('id', '=', $hostable->id)->delete();
        }

        $this->recent_hostables = $database_server->hostables()->get();

        $this->alertSuccessMessage('Hostable deleted.');
    }

    public function deleteExistingHostable($hostable_id) {
        $database_server = DatabaseServer::findOrFail($this->database_server_id);

        $hostable = Hostable::find($hostable_id);
        if ($hostable) {
            $database_server->hostables()->where('id', '=', $hostable->id)->delete();
        }

        $this->recent_hostables = $database_server->hostables()->get();

        $this->alertSuccessMessage('Hostable deleted.');
    }
    // End of Section
}
