<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\User;

class Users extends Component
{
    use AlertMessageTrait;

    public $users, $user_id;
    public $name;
    public $email;
    public $email_verified_at;
    public $password;
    public $two_factor_secret;
    public $two_factor_recovery_codes;
    public $two_factor_confirmed_at;
    public $remember_token;
    public $current_team_id;
    public $profile_photo_path;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->users = User::all();
        $this->total_rows = User::count();
        return view('livewire.users');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->user_id = '';
        $this->name = '';
        $this->email = '';
        $this->email_verified_at = '';
        $this->password = '';
        $this->two_factor_secret = '';
        $this->two_factor_recovery_codes = '';
        $this->two_factor_confirmed_at = '';
        $this->remember_token = '';
        $this->current_team_id = '';
        $this->profile_photo_path = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'email' => 'required',
            'email_verified_at' => 'required',
            'password' => 'required',
            'current_team_id' => 'required',

        ]);

        User::updateOrCreate(['id' => $this->user_id], [
            'name' => $this->name,
            'email' => $this->email,
            'email_verified_at' => $this->email_verified_at,
            'password' => $this->password,
            'two_factor_secret' => $this->two_factor_secret,
            'two_factor_recovery_codes' => $this->two_factor_recovery_codes,
            'two_factor_confirmed_at' => $this->two_factor_confirmed_at,
            'remember_token' => $this->remember_token,
            'current_team_id' => $this->current_team_id,
            'profile_photo_path' => $this->profile_photo_path,

        ]);
        $this->alertSuccessMessage($this->user_id ? 'User updated.' : 'User created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $this->user_id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->email_verified_at = $user->email_verified_at;
        $this->password = $user->password;
        $this->two_factor_secret = $user->two_factor_secret;
        $this->two_factor_recovery_codes = $user->two_factor_recovery_codes;
        $this->two_factor_confirmed_at = $user->two_factor_confirmed_at;
        $this->remember_token = $user->remember_token;
        $this->current_team_id = $user->current_team_id;
        $this->profile_photo_path = $user->profile_photo_path;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        User::find($id)->delete();
        $this->alertSuccessMessage('User deleted.');
    }
}
