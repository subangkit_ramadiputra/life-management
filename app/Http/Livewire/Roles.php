<?php
namespace App\Http\Livewire;
use App\Traits\AlertMessageTrait;
use Livewire\Component;
use App\Models\Role;

class Roles extends Component
{
    use AlertMessageTrait;

    public $roles, $role_id;
    public $name;
    public $slug;
    public $description;
    public $roleable;

    public $isModalOpen = 0;
    public $total_rows = 0;

    public function render()
    {
        $this->roles = Role::all();
        $this->total_rows = Role::count();
        return view('livewire.roles');
    }
    public function create()
    {
        $this->resetCreateForm();
        $this->openModalPopover();
    }
    public function openModalPopover()
    {
        $this->isModalOpen = true;
    }
    public function closeModalPopover()
    {
        $this->isModalOpen = false;
    }
    private function resetCreateForm(){
        $this->role_id = '';
        $this->name = '';
        $this->slug = '';
        $this->description = '';
        $this->roleable = '';

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'slug' => 'required',
            'description' => 'required',
            'roleable' => 'required',

        ]);

        Role::updateOrCreate(['id' => $this->role_id], [
            'name' => $this->name,
            'slug' => $this->slug,
            'description' => $this->description,
            'roleable' => $this->roleable,

        ]);
        $this->alertSuccessMessage($this->role_id ? 'Role updated.' : 'Role created.');
        $this->closeModalPopover();
        $this->resetCreateForm();
    }
    public function edit($id)
    {
        $role = Role::findOrFail($id);
        $this->role_id = $id;
        $this->name = $role->name;
        $this->slug = $role->slug;
        $this->description = $role->description;
        $this->roleable = $role->roleable;


        $this->openModalPopover();
    }

    public function delete($id)
    {
        Role::find($id)->delete();
        $this->alertSuccessMessage('Role deleted.');
    }
}
