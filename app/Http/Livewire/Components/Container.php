<?php

namespace App\Http\Livewire\Components;

use App\Traits\AlertMessageTrait;
use Livewire\Component;

class Container extends Component
{
    use AlertMessageTrait;

    public function render()
    {
        return view('livewire.components.container');
    }
}
