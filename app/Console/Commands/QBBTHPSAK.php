<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class QBBTHPSAK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:query:bbth:psak';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $list_jenis_pelayanan = [
            'rjtp' => [
                'beban' => '71001011',
                'pembayaran' => '31001011'
            ],
            'ritp' => [
                'beban' => '71001021',
                'pembayaran' => '31001021'
            ],
            'rjtl' => [
                'beban' => '71002011',
                'pembayaran' => '31002011'
            ],
            'ritl' => [
                'beban' => '71002021',
                'pembayaran' => '31002021'
            ],
            'promprev' => [
                'beban' => '72001011',
                'pembayaran' => '32001011'
            ],
        ];

        $list_jenis_report = [
            'beban',
            'pembayaran',
        ];

        $list_tahun = [2020, 2021, 2022];

        $counter = 1;
        foreach ($list_jenis_report as $report) {
            foreach ($list_jenis_pelayanan as $jenis_pelayanan => $detail) {
                $table = 'bbth_psak_' . $jenis_pelayanan . '_' . $report;
                $table_tahunan = 'bbth_psak_' . $jenis_pelayanan . '_' . $report.'_tahunan';
                $file_name = sprintf("%s_%s %s %s.sql", $counter, strtoupper($jenis_pelayanan), $report, 'Bulanan');
                $akun = $detail[$report];
                $report_text = sprintf("%s :: %s %s %s", $counter, strtoupper($jenis_pelayanan), 'Bulanan',ucfirst($report));
                $this->line($report_text);
                $query_bulanan = view('query.psak.bulanan', compact('list_tahun', 'report', 'jenis_pelayanan', 'akun', 'table'))->render();
                Storage::disk('local')->put($file_name, $query_bulanan);
                $counter++;
                $report_text = sprintf("%s :: %s %s %s", $counter, strtoupper($jenis_pelayanan), 'Tahunan',ucfirst($report));
                $this->line($report_text);
                $file_name = sprintf("%s_%s %s %s.sql", $counter, strtoupper($jenis_pelayanan), $report, 'Tahunan');
                $query_tahunan = view('query.psak.tahunan', compact('list_tahun', 'report', 'jenis_pelayanan', 'akun', 'table', 'table_tahunan'))->render();
                Storage::disk('local')->put($file_name, $query_tahunan);
                $counter++;
            }
        }

    }
}
