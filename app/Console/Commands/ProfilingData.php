<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ProfilingData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:query:profile
                            {server : sqlserver, hive, mysql}
                            {db : database name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $base_stubs = 'data_profile/';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $server = $this->argument('server');
        $db = $this->argument('db');
        $this->fields = $this->ask('Fields (separated with (,) ex: name,description) ?');
        $this->fields = ($this->fields == '') ? 'name' : $this->fields;
        $array_fields = explode(',',$this->fields);

        $this->showInformation($server);

        // Get Column Information
        $this->getColumnInformation($server, $db, $array_fields);
        // SELECT DISTINCT colA, colB FROM table1;
        // SELECT * FROM iteblog1 TABLESAMPLE(1000 ROWS)
    }

    public function getColumnInformation($server, $db, $array_fields) {
        $stub_path = sprintf('servers/%s/column_information', $server);
        switch($server) {
            case 'hive' :
                $fields_string = implode(", ", $array_fields);
                $this->line($this->getSourceFile($stub_path,[
                    'DATABASE' => $db,
                    'LIST_FIELDS' => $fields_string,
                ]));

                foreach ($array_fields as $field) {
                    $this->line(sprintf("SELECT DISTINCT %s FROM %s;",$field, $db));
                }
                break;
        }
    }
    public function showInformation($server) {
        switch($server) {
            case 'hive' :
                $this->line('-- Tips');
                $this->line('Use Intellij Database');
                $this->line('');
                break;
        }
    }

    /**
     * Return the stub file path
     * @return string
     *
     */
    public function getStubPath($filename)
    {
        return __DIR__ . '/stubs/'.$this->base_stubs.$filename. '.stub';
    }

    /**
     * Get the stub path and the stub variables
     *
     * @return bool|mixed|string
     *
     */
    public function getSourceFile($filename,$variables)
    {
        return $this->getStubContents($this->getStubPath($filename), $variables);
    }

    /**
     * Replace the stub variables(key) with the desire value
     *
     * @param $stub
     * @param array $stubVariables
     * @return bool|mixed|string
     */
    public function getStubContents($stub , $stubVariables = [])
    {
        $contents = file_get_contents($stub);

        foreach ($stubVariables as $search => $replace)
        {
            $contents = str_replace('$'.$search.'$' , $replace, $contents);
        }

        return $contents;
    }


}
