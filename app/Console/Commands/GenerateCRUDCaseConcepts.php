<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Pluralizer;
use Storage;

class GenerateCRUDCaseConcepts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:crud:concepts
                            {--no-ask : Whether the job should be queued}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    public $entity;
    public $entity_properties = [];
    public $fields;
    public $field_properties = [];
    public $base_stubs = 'generator/crud/';
    public $default = [
        'entity' => 'concepts',
        'fields' => 'title,summary,body,body_format,source_url,owner_id',
        'fields_properties' => [
            'title' => [
                'field_name' => 'title',
                'nullable' => false,
                'title' => 'Title',
                'type' => 'string',
                'length' => 100,
                'default' => '',
                'view_table' => true,
                'view_create' => true,
            ],
            'summary' => [
                'field_name' => 'summary',
                'nullable' => true,
                'title' => 'Summary',
                'type' => 'text',
                'view_table' => true,
                'view_create' => true,
            ],
            'body' => [
                'field_name' => 'body',
                'nullable' => true,
                'title' => 'Body',
                'type' => 'longText',
                'view_table' => false,
                'view_create' => true,
            ],
            'body_format' => [
                'field_name' => 'body_format',
                'nullable' => false,
                'title' => 'Body Format',
                'type' => 'string',
                'length' => 100,
                'default' => 'markdown',
                'view_table' => false,
                'view_create' => true,
            ],
            'source_url' => [
                'field_name' => 'source_url',
                'nullable' => true,
                'title' => 'Source URL',
                'type' => 'string',
                'length' => 1000,
                'view_table' => true,
                'view_create' => true,
            ],
            'owner_id' => [
                'field_name' => 'owner_id',
                'nullable' => true,
                'title' => 'Owner',
                'type' => 'bigInteger',
                'view_table' => false,
                'view_create' => false,
            ]
        ]
    ];

    public $options = [];

    public function askQuestion() {
        if (!$this->options['no-ask']) {
            $this->entity = $this->ask('Entity Name (ex: Student) ?');
            $this->entity = ($this->entity == '') ? 'students' : $this->entity;
            $this->entity_properties = [
                'singular' => $this->getSingularClassName($this->entity),
                'singular_lowercase' => strtolower($this->getSingularClassName($this->entity)),
                'plural' => $this->getPluralClassName($this->entity),
                'plural_lowercase' => strtolower($this->getPluralClassName($this->entity)),
            ];

            $this->fields = $this->ask('Fields (separated with (,) ex: name,description) ?');
            $this->fields = ($this->fields == '') ? 'name' : $this->fields;
            $array_fields = explode(',', $this->fields);
            foreach ($array_fields as $field_name) {
                $field_definition = [
                    'field_name' => $field_name
                ];
                if ($field_name == 'id') {
                    $field_definition = [
                        'title' => 'ID',
                        'type' => 'unsignedBigInteger',
                    ];
                }
                if ($this->confirm($field_name . ' is nullable ? ')) {
                    $field_definition['nullable'] = true;
                } else {
                    $field_definition['nullable'] = false;
                    $field_definition['default'] = $this->ask($field_name . ' | Default value (left blank if not defined) ? ');
                    $field_definition['default'] = ($field_definition['default'] == '') ? '' : $field_definition['default'];
                }

                $field_definition['title'] = $this->ask($field_name . ' | Title ? ');
                $field_definition['title'] = ($field_definition['title'] == '') ? 'Title' : $field_definition['title'];
                $field_definition['type'] = $this->ask($field_name . ' | Type (ex:string) ? ');
                $field_definition['type'] = ($field_definition['type'] == '') ? 'string' : $field_definition['type'];
                $field_definition['view_table'] = $this->ask($field_name . ' | On Table (default:true) ? ');
                $field_definition['view_table'] = ($field_definition['view_table'] == '') ? true : $field_definition['view_table'];
                $field_definition['view_create'] = $this->ask($field_name . ' | On Create/Update (default:true) ? ');
                $field_definition['view_create'] = ($field_definition['view_create'] == '') ? true : $field_definition['view_create'];
                switch ($field_definition['type']) {
                    case 'char' :
                    case 'string' :
                        $field_definition['length'] = $this->ask($field_name . ' | Length [default : 100] ? ');
                        $field_definition['length'] = ($field_definition['length'] == '') ? 100 : $field_definition['length'];
                        break;
                    case 'dateTimeTz' :
                    case 'dateTime' :
                        $field_definition['precision'] = $this->ask($field_name . ' | Precision (default: 0) ? ');
                        $field_definition['precision'] = ($field_definition['precision'] == '') ? 0 : $field_definition['precision'];
                        break;
                    case 'decimal' :
                        $field_definition['precision'] = $this->ask($field_name . ' | Precision (default: 8) ? ');
                        $field_definition['precision'] = ($field_definition['precision'] == '') ? 8 : $field_definition['precision'];

                        $field_definition['scale'] = $this->ask($field_name . ' | Scale (default: 2) ? ');
                        $field_definition['scale'] = ($field_definition['scale'] == '') ? 2 : $field_definition['scale'];
                        break;
                    case 'double' :
                    case 'float' :
                        $field_definition['digits'] = $this->ask($field_name . ' | Precision (default: 8) ? ');
                        $field_definition['digits'] = ($field_definition['digits'] == '') ? 8 : $field_definition['digits'];

                        $field_definition['decimal'] = $this->ask($field_name . ' | Scale (default: 2) ? ');
                        $field_definition['decimal'] = ($field_definition['decimal'] == '') ? 2 : $field_definition['decimal'];
                        break;
                }
                $this->field_properties[$field_name] = $field_definition;
            }
        } else {
            $this->entity = $this->default['entity'];
            $this->entity_properties = [
                'singular' => $this->getSingularClassName($this->entity),
                'singular_lowercase' => strtolower($this->getSingularClassName($this->entity)),
                'plural' => $this->getPluralClassName($this->entity),
                'plural_lowercase' => strtolower($this->getPluralClassName($this->entity)),
            ];
            $this->fields = $this->default['fields'];
            $this->field_properties = $this->default['fields_properties'];
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->options = $this->options();

        $this->askQuestion();
        // Source : https://www.positronx.io/build-laravel-livewire-crud-application-using-jetstream/
        $this->generateModel();

        $this->generateMigration();
        // Create Livewire CRUD Controller -> app/Http/Livewire/Students.php
        $this->generateController();
        // Add routes -> routes/web.php
        // Create CRUD view -> resources/views/livewire/students.blade.php
        $this->generateTableView();
        // Create CRUD CREATE view -> resources/views/livewire/create.blade.php
        $this->generateCreateView();
    }

    public function generateModel() {
        // Create Model -> app/Models/Student.php
        $class_name = $this->entity_properties['singular'];
        $path = 'app/Models/'.$class_name.'.php';
        $field_definitions = $this->getSourceFileByFields('model/field');
        Storage::disk('laravel')->put($path, $this->getSourceFile('model',[
            'CLASS_NAME' => $class_name,
            'FIELD_DEFINITIONS' => $field_definitions
        ]));
    }

    public function generateMigration() {
        // Create Migration ->
        $class_name = 'Create'.$this->entity_properties['plural'].'Table';
        $path = 'database/migrations/'.$this->getDatePrefix().'_create_'.strtolower($this->entity_properties['plural']).'_table.php';
        $field_definitions = $this->getSourceFileByFields('migration/fields', true,
            null,
            function(&$variables) {
                $variables['nullable_string'] = ($variables['nullable']) ? '->nullable()' : '';
                $variables['default_string'] = (isset($variables['default'])) ? ($variables['default'] != '') ? "->default('".$variables['default']."')" : '' : '';
            }
        );
        Storage::disk('laravel')->put($path,
            $this->getSourceFile('migration',array_merge($this->entity_properties,[
                'CLASS_NAME' => $class_name,
                'FIELD_DEFINITIONS' => $field_definitions,
                'plural_lowercase' => strtolower($this->entity_properties['plural'])
        ])));
    }

    public function generateController() {
        // Create Migration ->
        $class_name = $this->entity_properties['plural'];
        $path = 'app/Http/Livewire/'.$this->entity_properties['plural'].'.php';
        Storage::disk('laravel')->put($path,
            $this->getSourceFile('livewire.controller',array_merge($this->entity_properties,[
                'CLASS_NAME' => $class_name,
                'MODEL_NAME' => $this->entity_properties['singular'],
                'field_variable' => $this->getSourceFileByFields('livewire/controller/field_variable'),
                'field_init' => $this->getSourceFileByFields('livewire/controller/field_init', false,
                    null,
                    function(&$variables) {
                        $variables['default_value'] = '';
                        if (isset($variables['default'])) {
                            $variables['default_value'] = $variables['default'];
                        }
                    }
                ),
                'field_validation' => $this->getSourceFileByFields('livewire/controller/field_validation', false, function ($variables) {
                    return !$variables['nullable'];
                }),
                'field_assign' => $this->getSourceFileByFields('livewire/controller/field_assign'),
                'field_edit' => $this->getSourceFileByFields('livewire/controller/field_edit'),
                'field_create' => $this->getSourceFileByFields('livewire/controller/field_create'),
            ])));
    }

    public function generateTableView() {
        // Create Migration ->
        $class_name = $this->entity_properties['plural'];
        $path = 'resources/views/livewire/'.$this->entity_properties['plural_lowercase'].'.blade.php';
        Storage::disk('laravel')->put($path,
            $this->getSourceFile('livewire.table',array_merge($this->entity_properties,[
                'CLASS_NAME' => $class_name,
                'MODEL_NAME' => $this->entity_properties['singular'],
                'field_table' => $this->getSourceFileByFields('livewire/view/table/field_table', false, function ($variables) {
                    return $variables['view_table'];
                }),
                'field_row' => $this->getSourceFileByFields('livewire/view/table/field_row',false, function ($variables) {
                    return $variables['view_table'];
                }),
            ])));
    }

    public function generateCreateView() {
        // Create Migration ->
        $class_name = $this->entity_properties['plural'];
        $path = 'resources/views/livewire/'.$this->entity_properties['plural_lowercase'].'/create.blade.php';
        Storage::disk('laravel')->put($path,
            $this->getSourceFile('livewire.create',array_merge($this->entity_properties,[
                'CLASS_NAME' => $class_name,
                'MODEL_NAME' => $this->entity_properties['singular'],
                'field_form' => $this->getSourceFileByFields('livewire/view/create/fields',true, function ($variables) {
                    return $variables['view_create'];
                }),
            ])));
    }

    /**
     * Get the date prefix for the migration.
     *
     * @return string
     */
    protected function getDatePrefix()
    {
        return '0000_00_00_000000';
        // return date('Y_m_d_His');
    }

    public function getSourceFileByFields($stub, $by_type = false, $showFunction = null, $variableModifier = null) {
        $output = '';
        $array_fields = explode(',',$this->fields);
        foreach($array_fields as $field_name) {
            $variables = array_merge($this->entity_properties, $this->field_properties[$field_name]);

            if (is_callable($variableModifier)) {
                $variableModifier($variables);
            }

            if (is_callable($showFunction)){
                if (!$showFunction($variables)) continue;
            }

            if (!$by_type) {
                $output .= $this->getSourceFile($stub, $variables);
            } else {
                $stub_file = $stub.'/';
                switch($variables['type']) {
                    case 'text' :
                    case 'longText' :
                        $stub_file .= 'text';
                        break;
                    case 'string' :
                        $stub_file .= 'string';
                        break;
                    default:
                        $stub_file .= 'default';
                }

                var_dump($variables);

                $output .= $this->getSourceFile($stub_file, $variables);
            }
        }

        return $output;
    }

    /**
     * Return the Singular Capitalize Name
     * @param $name
     * @return string
     */
    public function getSingularClassName($name)
    {
        return ucwords(Pluralizer::singular($name));
    }

    /**
     * Return the Singular Capitalize Name
     * @param $name
     * @return string
     */
    public function getPluralClassName($name)
    {
        return ucwords(Pluralizer::plural($name));
    }

    /**
     * Return the stub file path
     * @return string
     *
     */
    public function getStubPath($filename)
    {
        return __DIR__ . '/stubs/'.$this->base_stubs.$filename. '.stub';
    }

    /**
     **
     * Map the stub variables present in stub to its value
     *
     * @return array
     *
     */
    public function getStubVariables()
    {
        return [
            'NAMESPACE'         => 'App\\Interfaces',
            'CLASS_NAME'        => $this->getSingularClassName($this->argument('name')),
        ];
    }

    /**
     * Get the stub path and the stub variables
     *
     * @return bool|mixed|string
     *
     */
    public function getSourceFile($filename,$variables)
    {
        return $this->getStubContents($this->getStubPath($filename), $variables);
    }

    /**
     * Replace the stub variables(key) with the desire value
     *
     * @param $stub
     * @param array $stubVariables
     * @return bool|mixed|string
     */
    public function getStubContents($stub , $stubVariables = [])
    {
        $contents = file_get_contents($stub);

        foreach ($stubVariables as $search => $replace)
        {
            $contents = str_replace('$'.$search.'$' , $replace, $contents);
        }

        return $contents;
    }
}
