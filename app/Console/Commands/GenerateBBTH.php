<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class GenerateBBTH extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:bbth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $year = '2022';
        $months = [
            // 1, 2, 3, 4, 5, 6,
            7, 8, 9, 10, 11, 12
        ];

        $storagePath  = app_path().'/public';

        foreach ($months as $month) {
            $sql_count = <<<HERESQL
WITH PROGRAM_UNIT_REFINE AS
(
SELECT KDPROG,KDUNIT, COUNT(1) AS JML FROM PROGRAM_UNIT
GROUP BY KDPROG, KDUNIT
)
  SELECT
  COUNT(1) JML
  FROM
  TRANSAKSIKEU AS AA
  INNER JOIN TRANSAKSIKEUDTL AS AB ON AA.KDJNSTRSKEU = AB.KDJNSTRSKEU AND AA.NOBUKTI = AB.NOBUKTI AND AA.KDKANTOR = AB.KDKANTOR AND AA.KDJNSKC = AB.KDJNSKC
  INNER JOIN GRUPSUMBERDANA AS RA ON AA.KDGRUPSUMBERDANA = RA.KDGRUPSUMBERDANA AND AB.KDGRUPSUMBERDANA = RA.KDGRUPSUMBERDANA
  LEFT JOIN SUMBERDANA_TBL AS RB ON AA.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA AND AA.KDSUMBERDANA = RB.KDSUMBERDANA AND AB.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA AND AB.KDSUMBERDANA = RB.KDSUMBERDANA AND RA.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA
  INNER JOIN KANTOR_TBL AS RC ON AA.KDKANTOR = RC.KDKANTOR AND AB.KDKANTOR = RC.KDKANTOR
  INNER JOIN JNSKC_TBL AS RD ON AA.KDJNSKC = RD.KDJNSKC AND AB.KDJNSKC = RD.KDJNSKC AND RC.KDJNSKC = RD.KDJNSKC
  left outer JOIN PROGRAM_TBL AS RE ON AB.KDPROG = RE.KDPROG AND YEAR(RE.lupdate) = YEAR(TGLENTRI)
  INNER JOIN AKUN_TBL AS RF ON AB.KDAKUN = RF.KDAKUN
  LEFT JOIN REFKDTBH_TBL TBH ON AB.KDKANTOR = TBH.KDKANTOR AND
                                AB.KDJNSTBH = TBH.KDJNSTBH AND
                                AB.KDTBH = TBH.KDTBH AND
                                AB.KDJNSKC = TBH.KDJNSKC

  WHERE
  (AA.STATUS >= 2)  and AB.KDPROG <> ''
  AND YEAR(AA.TGLENTRI) = $year AND MONTH(AA.TGLENTRI) = $month
HERESQL;

            //echo $sql_count;
            $count_data = DB::connection('keu')->select($sql_count);
            $totalRows = (int) $count_data[0]->JML;
            echo "\nExtracting Data $year-$month ( $totalRows rows )";
            $offset = 0;
            $limit = 1000000;

            $fields = [
                'A' => 'TAHUN',
                'B' => 'BULAN',
                'C' => 'HARI',
                'D' => 'NOBUKTI',
                'E' => 'KDKANTOR',
                'F' => 'NMKANTOR',
                'G' => 'UNIT',
                'H' => 'NMJNSKC',
                'I' => 'NMGRUPSUMBERDANA',
                'J' => 'NAMASUMBERDANA',
                'K' => 'KDAKUN',
                'L' => 'NMAKUN',
                'M' => 'KDPROG',
                'N' => 'NMPROG',
                'O' => 'KDTBH',
                'P' => 'NAMAKDTBH',
                'Q' => 'DK',
                'R' => 'URAIAN',
                'S' => 'NILAI',
                'T' => 'KDUNIT',
                'U' => 'KdGrup',
                'V' => 'GRUP',
                'W' => 'kdDirektorat',
                'X' => 'DIREKTORAT'
            ];

            $part = 1;
            while ($offset < $totalRows) {
                $spreadsheet = new Spreadsheet();
                $sheet = $spreadsheet->getActiveSheet();
                echo "\nExtracting Data $year-$month ( $totalRows rows ) :: Part $part to -> BBTH_".$year."_".$month."_".$part.".xlsx";
                $sql = <<<HERESQL
WITH PROGRAM_UNIT_REFINE AS
(
SELECT KDPROG,KDUNIT, COUNT(1) AS JML FROM PROGRAM_UNIT
GROUP BY KDPROG, KDUNIT
), TABELDASAR  AS
(
  SELECT
  YEAR(AA.TGLENTRI) AS TAHUN,
  MONTH(AA.TGLENTRI) AS BULAN,
  DAY(AA.TGLENTRI) AS HARI,
  AA.NOBUKTI,
  REPLACE(AA.KDKANTOR,' ','') KDKANTOR,
  RC.NMKANTOR,
  ( SELECT TOP 1 UP.kdunit FROM PROGRAM_UNIT_REFINE PU
      LEFT OUTER JOIN PROGRAM_UNIT_PENANGGUNG UP ON CAST(PU.KDUNIT AS VARCHAR(10)) = CAST(UP.kdunit AS VARCHAR(10))
      WHERE RE.KDPROG = PU.KDPROG
  ) as KDUNIT,
  RD.NMJNSKC,
  RA.NMGRUPSUMBERDANA,
  RB.NAMASUMBERDANA,
  AB.KDAKUN,
  RF.NMAKUN,
  AB.KDPROG,
  RE.NMPROG,
  CASE WHEN AB.DK = 'D' THEN 'DEBIT' ELSE 'KREDIT' END DK ,
  AB.NILAI,
  AB.URAIAN,
  AB.KDTBH,
  TBH.NAMA AS NAMAKDTBH,
  ROW_NUMBER() OVER (ORDER BY REPLACE(AA.KDKANTOR,' ',''), AA.TGLENTRI) AS RowNum
  FROM
  TRANSAKSIKEU AS AA
  INNER JOIN TRANSAKSIKEUDTL AS AB ON AA.KDJNSTRSKEU = AB.KDJNSTRSKEU AND AA.NOBUKTI = AB.NOBUKTI AND AA.KDKANTOR = AB.KDKANTOR AND AA.KDJNSKC = AB.KDJNSKC
  INNER JOIN GRUPSUMBERDANA AS RA ON AA.KDGRUPSUMBERDANA = RA.KDGRUPSUMBERDANA AND AB.KDGRUPSUMBERDANA = RA.KDGRUPSUMBERDANA
  LEFT JOIN SUMBERDANA_TBL AS RB ON AA.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA AND AA.KDSUMBERDANA = RB.KDSUMBERDANA AND AB.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA AND AB.KDSUMBERDANA = RB.KDSUMBERDANA AND RA.KDGRUPSUMBERDANA = RB.KDGRUPSUMBERDANA
  INNER JOIN KANTOR_TBL AS RC ON AA.KDKANTOR = RC.KDKANTOR AND AB.KDKANTOR = RC.KDKANTOR
  INNER JOIN JNSKC_TBL AS RD ON AA.KDJNSKC = RD.KDJNSKC AND AB.KDJNSKC = RD.KDJNSKC AND RC.KDJNSKC = RD.KDJNSKC
  left outer JOIN PROGRAM_TBL AS RE ON AB.KDPROG = RE.KDPROG AND YEAR(RE.lupdate) = YEAR(TGLENTRI)
  INNER JOIN AKUN_TBL AS RF ON AB.KDAKUN = RF.KDAKUN
  LEFT JOIN REFKDTBH_TBL TBH ON AB.KDKANTOR = TBH.KDKANTOR AND
                                AB.KDJNSTBH = TBH.KDJNSTBH AND
                                AB.KDTBH = TBH.KDTBH AND
                                AB.KDJNSKC = TBH.KDJNSKC

  WHERE
  (AA.STATUS >= 2)  and AB.KDPROG <> ''
  AND YEAR(AA.TGLENTRI) = $year AND MONTH(AA.TGLENTRI) = $month
)
SELECT
  TAHUN,
  BULAN,
  HARI,
  NOBUKTI,
  KDKANTOR,
  NMKANTOR,
  COALESCE(UK.NmUnit, UT.NMUNIT) UNIT,
  NMJNSKC,
  NMGRUPSUMBERDANA,
  NAMASUMBERDANA,
  KDAKUN,
  NMAKUN,
  KDPROG,
  NMPROG,
  KDTBH,
  NAMAKDTBH,
  DK,
  URAIAN,
  NILAI,
  TABELDASAR.KDUNIT,
  UK.KdGrup,
  RG.nmGrup GRUP,
  RG.kdDirektorat,
  RD.nmDirektorat DIREKTORAT
FROM
  TABELDASAR
LEFT JOIN RefUnitKerja UK ON UK.KdUnit = TABELDASAR.KDUNIT
    LEFT JOIN RefGrup RG ON RG.kdGrup = UK.KdGrup
    LEFT JOIN RefDirektorat RD on RG.kdDirektorat = RD.kdDirektorat
    LEFT JOIN UNITKERJA_TBL UT on TABELDASAR.KDUNIT = UT.KDUNIT
WHERE RowNum >= $offset
AND RowNum < $offset + $limit
HERESQL;
                // echo $sql;

                $data = DB::connection('keu')->select($sql);
                $row = 1;
                // Set Header
                foreach ($fields as $index => $field) {
                    $sheet->setCellValue($index.$row, $field);
                }
                $row++;

                foreach ($data as $item) {
                    foreach ($fields as $index => $field) {
                        $sheet->setCellValue($index.$row, $item->{$field});
                    }
                    $row++;
                }

                $offset += $limit;

                $writer = new Xlsx($spreadsheet);
                $writer->save("BBTH_".$year."_".$month."_".$part.".xlsx");
                $part++;
            }


        }
    }
}
