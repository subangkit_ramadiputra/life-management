<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class VHostYamisokPay extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'apache:host:yamisokpay';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $base_path = '/Users/subangkit/Projects/GarudaPay';
        $storage = 'yamisok_pay';
        $domain_root = 'yamisok_pay.suba';
        $server_host = '127.0.0.1';
        $ignore_directories = [
            'docker'
        ];
        $format = [
            'ID',
            'Folder',
            'Framework',
            'Workfile',
            'Domain',
            'Hosts',
        ];
        $framework_files = 'FRAMEWORK.csv';
        $separator = ",";
        Storage::disk('yamisok_pay')->put($framework_files, implode($separator,$format));
        $directories = Storage::disk('yamisok_pay')->directories();

        Storage::disk('vhost')->put($domain_root . '.conf', '');

        $counter = 1;
        foreach ($directories as $directory) {
            if (in_array($directory, $ignore_directories))
                continue;

            $framework = '';
            $web = true;
            $web_root_folder = '';
            $domain = $directory.'.'.$domain_root;
            $host = sprintf('%s %s', $server_host, $domain);

            if (substr($directory,0,1) !== '.') {
                if (Storage::disk($storage)->exists($directory . '/public')) {
                    $framework = 'Laravel';
                    $web_root_folder = $base_path.'/'.$directory.'/public';
                    $this->line($framework.' :: '.$directory);
                } else if (Storage::disk($storage)->exists($directory . '/etc')) {
                    $framework = 'MIM';
                    $web_root_folder = $base_path.'/'.$directory.'';
                    $this->line($framework.' :: '.$directory);
                } else if (Storage::disk($storage)->exists($directory . '/tools')) {
                    $framework = 'Codeigniter';
                    $web_root_folder = $base_path.'/'.$directory.'';
                    $this->line($framework.' :: '.$directory);
                } else if (Storage::disk($storage)->exists($directory . '/android')) {
                    $framework = 'Flutter';
                    $web = false;
                    $this->line($framework.' :: '.$directory);
                } else {
                    $identification = 'Unknown :: '.$directory;
                    $this->line(Storage::disk($storage)->path($directory));
                    $this->line($identification);
                    $check_directories = Storage::disk($storage)->directories($directory);
                    foreach ($check_directories as $d) {
                        $this->line($identification.' / '.$d);
                    }
                }

                $data = [
                    $counter, // 'ID',
                    $directory, // 'Folder',
                    $framework, // 'Framework',
                    $web_root_folder, //'Workfile',
                    ($web) ? $domain : '', //'Domain',
                    ($web) ? $host : '', //'Hosts',
                ];

                if ($web) {
                    $vhost = view('apache.vhost', compact('counter', 'directory', 'framework', 'web_root_folder', 'domain'))->render();
                    Storage::disk('vhost')->append($domain_root . '.conf', $vhost, PHP_EOL);
                }

                Storage::disk($storage)->append($framework_files, implode($separator,$data), PHP_EOL);
                $counter++;
            }
        }
    }
}
