<?php


namespace App\Traits;


use App\Models\BpmnActivity;
use App\Models\BpmnActivityType;
use App\Models\BpmnEvent;
use App\Models\BpmnEventType;
use App\Models\BpmnGateway;
use App\Models\BpmnGatewayType;
use App\Models\BpmnRole;
use App\Models\BpmnSquence;
use App\Models\Sop;
use Illuminate\Support\Facades\DB;

trait SopBpmnTrait
{
    public function codefication($text) {
        $words = explode(" ",$text);
        $code = '';
        foreach ($words as $word) {
            $word = strtolower($word);
            $code .= ucfirst($word);
        }

        return $code;
    }
    // BpmnRole Section
    public $isBpmnRoleModalOpen = 0;
    public $recent_bpmn_roles = [];
    public $bpmn_roles = [];
    public function addBpmnRole($id) {
        $this->sop_id = $id;
        $sop = Sop::findOrFail($this->sop_id);
        $this->recent_bpmn_roles = $sop->bpmn_roles()->get();
        $this->resetBpmnRoleCreateForm();
        $this->openBpmnRoleModalPopover();
    }

    public function openBpmnRoleModalPopover()
    {
        $this->isBpmnRoleModalOpen = true;
    }
    public function closeBpmnRoleModalPopover()
    {
        $this->isBpmnRoleModalOpen = false;
    }

    public function editBpmnRole($bpmn_role_id) {
        $bpmn_role = BpmnRole::findOrFail($bpmn_role_id);
        $this->bpmn_role_id = $bpmn_role_id;
        $this->bpmn_role_code = $bpmn_role->code;
        $this->bpmn_role_name = $bpmn_role->name;
        $this->bpmn_role_roleable = $bpmn_role->roleable;
        $this->bpmn_role_summary = $bpmn_role->summary;

    }

    public function resetBpmnRoleCreateForm(){
        $sop = Sop::findOrFail($this->sop_id);
        $this->bpmn_role_id = '';
        $this->bpmn_role_code = $this->codefication($sop->title);
        $this->bpmn_role_name = '';
        $this->bpmn_role_roleable = '';
        $this->bpmn_role_summary = '';
    }

    public function changeRoleName() {
        $sop = Sop::findOrFail($this->sop_id);
        $this->bpmn_role_code = $this->codefication($sop->title.' '.$this->bpmn_role_name);
    }

    public function assignBpmnRole() {
        $sop = Sop::findOrFail($this->sop_id);
        if ($this->bpmn_role_id == '') {
            $bpmn_role = new BpmnRole();
            $bpmn_role->code = $this->bpmn_role_code;
            $bpmn_role->name = $this->bpmn_role_name;
            $bpmn_role->summary = $this->bpmn_role_summary;

            $sop->bpmn_roles()->save($bpmn_role);

            $this->alertSuccessMessage('BpmnRole created.');
            //$this->closeBpmnRoleModalPopover();
            $this->resetBpmnRoleCreateForm();
        } else {
            BpmnRole::updateOrCreate(['id' => $this->bpmn_role_id], [
                'code' => $this->bpmn_role_code,
                'name' => $this->bpmn_role_name,
                'summary' => $this->bpmn_role_summary,

            ]);
            $this->alertSuccessMessage('BpmnRole updated.');
        }

        $this->recent_bpmn_roles = $sop->bpmn_roles()->get();
    }

    public function deleteBpmnRole($sop_id, $bpmn_role_id) {
        $sop = Sop::findOrFail($sop_id);

        $bpmn_role = BpmnRole::find($bpmn_role_id);
        if ($bpmn_role) {
            $sop->bpmn_roles()->where('id', '=', $bpmn_role->id)->delete();
        }

        $this->recent_bpmn_roles = $sop->bpmn_roles()->get();

        $this->alertSuccessMessage('BpmnRole deleted.');
    }

    public function deleteExistingBpmnRole($bpmn_role_id) {
        $sop = Sop::findOrFail($this->sop_id);

        $bpmn_role = BpmnRole::find($bpmn_role_id);
        if ($bpmn_role) {
            $sop->bpmn_roles()->where('id', '=', $bpmn_role->id)->delete();
        }

        $this->recent_bpmn_roles = $sop->bpmn_roles()->get();

        $this->alertSuccessMessage('BpmnRole deleted.');
    }
    // End of Section

    // BpmnEvent Section
    public $isBpmnEventModalOpen = 0;
    public $recent_bpmn_events = [];
    public $bpmn_events = [];
    public function addBpmnEvent($id) {
        $this->sop_id = $id;
        $sop = Sop::findOrFail($this->sop_id);
        $this->recent_bpmn_events = $sop->bpmn_events()->get();
        $this->resetBpmnEventCreateForm();
        $this->openBpmnEventModalPopover();
    }

    public function openBpmnEventModalPopover()
    {
        $this->isBpmnEventModalOpen = true;
    }
    public function closeBpmnEventModalPopover()
    {
        $this->isBpmnEventModalOpen = false;
    }

    public function editBpmnEvent($bpmn_event_id) {
        $bpmn_event = BpmnEvent::findOrFail($bpmn_event_id);
        $this->bpmn_event_id = $bpmn_event_id;
        $this->bpmn_event_type_id = $bpmn_event->type_id;
        $this->bpmn_event_code = $bpmn_event->code;
        $this->bpmn_event_name = $bpmn_event->name;
        $this->bpmn_event_summary = $bpmn_event->summary;
        $this->bpmn_event_eventable = $bpmn_event->eventable;
        $this->bpmn_event_role_id = $bpmn_event->role_id;

    }

    public $event_types = [];
    public $roles = [];
    public function resetBpmnEventCreateForm(){
        $sop = Sop::findOrFail($this->sop_id);
        $this->roles = $sop->bpmn_roles()->get();
        $this->bpmn_event_id = '';
        $this->event_types = BpmnEventType::all();
        $this->bpmn_event_type_id = '0';
        $this->bpmn_event_code = $this->codefication($sop->title);
        $this->bpmn_event_name = '';
        $this->bpmn_event_summary = '';
        $this->bpmn_event_eventable = '';
        $this->bpmn_event_role_id = '';

    }

    public function changeEventType() {
        $sop = Sop::findOrFail($this->sop_id);
        $type = BpmnEventType::findOrFail($this->bpmn_event_type_id);
        $this->bpmn_event_code = $this->codefication($sop->title.' '.$type->code.' '.$this->bpmn_event_name);
    }

    public function assignBpmnEvent() {
        $sop = Sop::findOrFail($this->sop_id);
        if ($this->bpmn_event_id == '') {
            $bpmn_event = new BpmnEvent();
            $bpmn_event->type_id = $this->bpmn_event_type_id;
            $bpmn_event->code = $this->bpmn_event_code;
            $bpmn_event->name = $this->bpmn_event_name;
            $bpmn_event->summary = $this->bpmn_event_summary;
            $bpmn_event->role_id = $this->bpmn_event_role_id;

            $sop->bpmn_events()->save($bpmn_event);

            $this->alertSuccessMessage('BpmnEvent created.');
            //$this->closeBpmnEventModalPopover();
            $this->resetBpmnEventCreateForm();
        } else {
            BpmnEvent::updateOrCreate(['id' => $this->bpmn_event_id], [
                'type_id' => $this->bpmn_event_type_id,
                'code' => $this->bpmn_event_code,
                'name' => $this->bpmn_event_name,
                'summary' => $this->bpmn_event_summary,
                'role_id' => $this->bpmn_event_role_id,

            ]);
            $this->alertSuccessMessage('BpmnEvent updated.');
        }

        $this->recent_bpmn_events = $sop->bpmn_events()->get();
    }

    public function deleteBpmnEvent($sop_id, $bpmn_event_id) {
        $sop = Sop::findOrFail($sop_id);

        $bpmn_event = BpmnEvent::find($bpmn_event_id);
        if ($bpmn_event) {
            $sop->bpmn_events()->where('id', '=', $bpmn_event->id)->delete();
        }

        $this->recent_bpmn_events = $sop->bpmn_events()->get();

        $this->alertSuccessMessage('BpmnEvent deleted.');
    }

    public function deleteExistingBpmnEvent($bpmn_event_id) {
        $sop = Sop::findOrFail($this->sop_id);

        $bpmn_event = BpmnEvent::find($bpmn_event_id);
        if ($bpmn_event) {
            $sop->bpmn_events()->where('id', '=', $bpmn_event->id)->delete();
        }

        $this->recent_bpmn_events = $sop->bpmn_events()->get();

        $this->alertSuccessMessage('BpmnEvent deleted.');
    }
    // End of Section

    // BpmnActivity Section
    public $isBpmnActivityModalOpen = 0;
    public $recent_bpmn_activities = [];
    public $bpmn_activities = [];
    public function addBpmnActivity($id) {
        $this->sop_id = $id;
        $sop = Sop::findOrFail($this->sop_id);
        $this->recent_bpmn_activities = $sop->bpmn_activities()->get();
        $this->resetBpmnActivityCreateForm();
        $this->openBpmnActivityModalPopover();
    }

    public function openBpmnActivityModalPopover()
    {
        $this->isBpmnActivityModalOpen = true;
    }
    public function closeBpmnActivityModalPopover()
    {
        $this->isBpmnActivityModalOpen = false;
    }

    public function editBpmnActivity($bpmn_activity_id) {
        $bpmn_activity = BpmnActivity::findOrFail($bpmn_activity_id);
        $this->bpmn_activity_id = $bpmn_activity_id;
        $this->bpmn_activity_type_id = $bpmn_activity->type_id;
        $this->bpmn_activity_code = $bpmn_activity->code;
        $this->bpmn_activity_name = $bpmn_activity->name;
        $this->bpmn_activity_summary = $bpmn_activity->summary;
        $this->bpmn_activity_activitiable = $bpmn_activity->activitiable;
        $this->bpmn_activity_role_id = $bpmn_activity->role_id;

    }

    public $activity_types = [];
    public function resetBpmnActivityCreateForm(){
        $sop = Sop::findOrFail($this->sop_id);
        $this->roles = $sop->bpmn_roles()->get();
        $this->bpmn_activity_id = '';
        $this->activity_types = BpmnActivityType::all();
        $this->bpmn_activity_type_id = '';
        $this->bpmn_activity_code = $this->codefication($sop->title);
        $this->bpmn_activity_name = '';
        $this->bpmn_activity_summary = '';
        $this->bpmn_activity_activitiable = '';
        $this->bpmn_activity_role_id = '';

    }

    public function changeActivityType() {
        $sop = Sop::findOrFail($this->sop_id);
        $type = BpmnActivityType::findOrFail($this->bpmn_activity_type_id);
        $this->bpmn_activity_code = $this->codefication($sop->title.' '.$type->code.' '.$this->bpmn_activity_name);
    }

    public function assignBpmnActivity() {
        $sop = Sop::findOrFail($this->sop_id);
        if ($this->bpmn_activity_id == '') {
            $bpmn_activity = new BpmnActivity();
            $bpmn_activity->type_id = $this->bpmn_activity_type_id;
            $bpmn_activity->code = $this->bpmn_activity_code;
            $bpmn_activity->name = $this->bpmn_activity_name;
            $bpmn_activity->summary = $this->bpmn_activity_summary;
            $bpmn_activity->role_id = $this->bpmn_activity_role_id;

            $sop->bpmn_activities()->save($bpmn_activity);

            $this->alertSuccessMessage('BpmnActivity created.');
            //$this->closeBpmnActivityModalPopover();
            $this->resetBpmnActivityCreateForm();
        } else {
            BpmnActivity::updateOrCreate(['id' => $this->bpmn_activity_id], [
                'type_id' => $this->bpmn_activity_type_id,
                'code' => $this->bpmn_activity_code,
                'name' => $this->bpmn_activity_name,
                'summary' => $this->bpmn_activity_summary,
                'role_id' => $this->bpmn_activity_role_id,

            ]);
            $this->alertSuccessMessage('BpmnActivity updated.');
        }

        $this->recent_bpmn_activities = $sop->bpmn_activities()->get();
    }

    public function deleteBpmnActivity($sop_id, $bpmn_activity_id) {
        $sop = Sop::findOrFail($sop_id);

        $bpmn_activity = BpmnActivity::find($bpmn_activity_id);
        if ($bpmn_activity) {
            $sop->bpmn_activities()->where('id', '=', $bpmn_activity->id)->delete();
        }

        $this->recent_bpmn_activities = $sop->bpmn_activities()->get();

        $this->alertSuccessMessage('BpmnActivity deleted.');
    }

    public function deleteExistingBpmnActivity($bpmn_activity_id) {
        $sop = Sop::findOrFail($this->sop_id);

        $bpmn_activity = BpmnActivity::find($bpmn_activity_id);
        if ($bpmn_activity) {
            $sop->bpmn_activities()->where('id', '=', $bpmn_activity->id)->delete();
        }

        $this->recent_bpmn_activities = $sop->bpmn_activities()->get();

        $this->alertSuccessMessage('BpmnActivity deleted.');
    }
    // End of Section

    // BpmnGateway Section
    public $isBpmnGatewayModalOpen = 0;
    public $recent_bpmn_gateways = [];
    public $bpmn_gateways = [];
    public function addBpmnGateway($id) {
        $this->sop_id = $id;
        $sop = Sop::findOrFail($this->sop_id);
        $this->recent_bpmn_gateways = $sop->bpmn_gateways()->get();
        $this->resetBpmnGatewayCreateForm();
        $this->openBpmnGatewayModalPopover();
    }

    public function openBpmnGatewayModalPopover()
    {
        $this->isBpmnGatewayModalOpen = true;
    }
    public function closeBpmnGatewayModalPopover()
    {
        $this->isBpmnGatewayModalOpen = false;
    }

    public function editBpmnGateway($bpmn_gateway_id) {
        $bpmn_gateway = BpmnGateway::findOrFail($bpmn_gateway_id);
        $this->bpmn_gateway_id = $bpmn_gateway_id;
        $this->bpmn_gateway_type_id = $bpmn_gateway->type_id;
        $this->bpmn_gateway_code = $bpmn_gateway->code;
        $this->bpmn_gateway_name = $bpmn_gateway->name;
        $this->bpmn_gateway_summary = $bpmn_gateway->summary;
        $this->bpmn_gateway_gatewayable = $bpmn_gateway->gatewayable;
        $this->bpmn_gateway_role_id = $bpmn_gateway->role_id;
        $this->bpmn_gateway_default_squence_id = $bpmn_gateway->default_squence_id;

    }

    public $gateway_types = [];
    public $default_squences = [];
    public function resetBpmnGatewayCreateForm(){
        $sop = Sop::findOrFail($this->sop_id);
        $this->default_squences = $sop->bpmn_squences()->get();
        $this->roles = $sop->bpmn_roles()->get();
        $this->bpmn_gateway_id = '';
        $this->gateway_types = BpmnGatewayType::all();
        $this->bpmn_gateway_type_id = '';
        $this->bpmn_gateway_code = $this->codefication($sop->title);
        $this->bpmn_gateway_name = '';
        $this->bpmn_gateway_summary = '';
        $this->bpmn_gateway_gatewayable = '';
        $this->bpmn_gateway_role_id = '';
        $this->bpmn_gateway_default_squence_id = '';

    }

    public function changeGatewayType() {
        $sop = Sop::findOrFail($this->sop_id);
        $type = BpmnGatewayType::findOrFail($this->bpmn_gateway_type_id);
        $this->bpmn_gateway_code = $this->codefication($sop->title.' '.$type->code.' '.$this->bpmn_gateway_name);
    }

    public function assignBpmnGateway() {
        $sop = Sop::findOrFail($this->sop_id);
        if ($this->bpmn_gateway_id == '') {
            $bpmn_gateway = new BpmnGateway();
            $bpmn_gateway->type_id = $this->bpmn_gateway_type_id;
            $bpmn_gateway->code = $this->bpmn_gateway_code;
            $bpmn_gateway->name = $this->bpmn_gateway_name;
            $bpmn_gateway->summary = $this->bpmn_gateway_summary;
            $bpmn_gateway->role_id = $this->bpmn_gateway_role_id;
            $bpmn_gateway->default_squence_id = $this->bpmn_gateway_default_squence_id;

            $sop->bpmn_gateways()->save($bpmn_gateway);

            $this->alertSuccessMessage('BpmnGateway created.');
            //$this->closeBpmnGatewayModalPopover();
            $this->resetBpmnGatewayCreateForm();
        } else {
            BpmnGateway::updateOrCreate(['id' => $this->bpmn_gateway_id], [
                'type_id' => $this->bpmn_gateway_type_id,
                'code' => $this->bpmn_gateway_code,
                'name' => $this->bpmn_gateway_name,
                'summary' => $this->bpmn_gateway_summary,
                'role_id' => $this->bpmn_gateway_role_id,
                'default_squence_id' => $this->bpmn_gateway_default_squence_id,

            ]);
            $this->alertSuccessMessage('BpmnGateway updated.');
        }

        $this->recent_bpmn_gateways = $sop->bpmn_gateways()->get();
    }

    public function deleteBpmnGateway($sop_id, $bpmn_gateway_id) {
        $sop = Sop::findOrFail($sop_id);

        $bpmn_gateway = BpmnGateway::find($bpmn_gateway_id);
        if ($bpmn_gateway) {
            $sop->bpmn_gateways()->where('id', '=', $bpmn_gateway->id)->delete();
        }

        $this->recent_bpmn_gateways = $sop->bpmn_gateways()->get();

        $this->alertSuccessMessage('BpmnGateway deleted.');
    }

    public function deleteExistingBpmnGateway($bpmn_gateway_id) {
        $sop = Sop::findOrFail($this->sop_id);

        $bpmn_gateway = BpmnGateway::find($bpmn_gateway_id);
        if ($bpmn_gateway) {
            $sop->bpmn_gateways()->where('id', '=', $bpmn_gateway->id)->delete();
        }

        $this->recent_bpmn_gateways = $sop->bpmn_gateways()->get();

        $this->alertSuccessMessage('BpmnGateway deleted.');
    }
    // End of Section
    // BpmnSquence Section
    public $isBpmnSquenceModalOpen = 0;
    public $recent_bpmn_squences = [];
    public $bpmn_squences = [];
    public function addBpmnSquence($id) {
        $this->sop_id = $id;
        $sop = Sop::findOrFail($this->sop_id);
        $this->recent_bpmn_squences = $sop->bpmn_squences()->get();
        $this->resetBpmnSquenceCreateForm();
        $this->openBpmnSquenceModalPopover();
    }

    public function openBpmnSquenceModalPopover()
    {
        $this->isBpmnSquenceModalOpen = true;
    }
    public function closeBpmnSquenceModalPopover()
    {
        $this->isBpmnSquenceModalOpen = false;
    }

    public function editBpmnSquence($bpmn_squence_id) {
        $bpmn_squence = BpmnSquence::findOrFail($bpmn_squence_id);
        $this->bpmn_squence_id = $bpmn_squence_id;
        $this->bpmn_squence_code = $bpmn_squence->code;
        $this->bpmn_squence_source = $bpmn_squence->source;
        $this->bpmn_squence_destination = $bpmn_squence->destination;
        $this->bpmn_squence_summary = $bpmn_squence->summary;
        $this->bpmn_squence_sequenceable = $bpmn_squence->sequenceable;

    }

    public $bpmn_squence_source_poly_type = '';
    public $bpmn_squence_source_poly_id = '';
    public $bpmn_squence_sources = [];
    public $bpmn_squence_destination_poly_type = '';
    public $bpmn_squence_destination_poly_id = '';
    public $bpmn_squence_destinations = [];
    public $bpmn_squence_default_type = '';
    public $bpmn_squence_default_id = '';
    public $bpmn_squence_defaults = [];
    public function resetBpmnSquenceCreateForm(){
        $this->bpmn_squence_id = '';
        $this->bpmn_squence_code = '';
        $this->bpmn_squence_sources = $this->getBPMNObjects();
        $this->bpmn_squence_source_id = '';
        $this->bpmn_squence_destinations = $this->getBPMNObjects();
        $this->bpmn_squence_destination_id = '';
        $this->bpmn_squence_default_id = '';
        $this->bpmn_squence_summary = '';
        $this->bpmn_squence_sequenceable = '';
    }

    protected function getBPMNObjects() {
        $sop = Sop::findOrFail($this->sop_id);
        // Event
        $events = $sop->bpmn_events()->select([
            DB::raw('CONCAT("BpmnEvent__",id) as select_code'),
            DB::raw('CONCAT("[Event] ",name) as name')
        ]);
        // Activity
        $activity = $sop->bpmn_activities()->select([
            DB::raw('CONCAT("BpmnActivity__",id) as select_code'),
            DB::raw('CONCAT("[Activity] ",name) as name')
        ]);
        // Gateway
        $gateway = $sop->bpmn_gateways()->select([
            DB::raw('CONCAT("BpmnGateway__",id) as select_code'),
            DB::raw('CONCAT("[Gateway] ",name) as name')
        ]);

        return $events->union($activity)->union($gateway)->get()->toArray();
    }

    public function changeBPMNSource() {
        $select_code = explode("__",$this->bpmn_squence_source_id);
        $model = $select_code[0];
        $id = $select_code[1];
        $this->bpmn_squence_source_poly_type = 'App\\Models\\'.$model;
        $this->bpmn_squence_source_poly_id = $id;
    }

    public function changeBPMNDestination() {
        $select_code = explode("__",$this->bpmn_squence_destination_id);
        $model = $select_code[0];
        $id = $select_code[1];
        $this->bpmn_squence_destination_poly_type = 'App\\Models\\'.$model;
        $this->bpmn_squence_destination_poly_id = $id;
    }

    public function assignBpmnSquence() {
        $sop = Sop::findOrFail($this->sop_id);
        if ($this->bpmn_squence_id == '') {
            $bpmn_squence = new BpmnSquence();
            $bpmn_squence->code = $this->bpmn_squence_code;

            $bpmn_squence->source_type = $this->bpmn_squence_source_poly_type;
            $bpmn_squence->source_id = $this->bpmn_squence_source_poly_id;

            $bpmn_squence->destination_type = $this->bpmn_squence_destination_poly_type;
            $bpmn_squence->destination_id = $this->bpmn_squence_destination_poly_id;

            $bpmn_squence->summary = $this->bpmn_squence_summary;

            $sop->bpmn_squences()->save($bpmn_squence);

            $this->alertSuccessMessage('BpmnSquence created.');
            //$this->closeBpmnSquenceModalPopover();
            $this->resetBpmnSquenceCreateForm();
        } else {
            BpmnSquence::updateOrCreate(['id' => $this->bpmn_squence_id], [
                'code' => $this->bpmn_squence_code,
                'source_type' => $this->bpmn_squence_source_poly_type,
                'source_id' => $this->bpmn_squence_source_poly_id,

                'destination_type' => $this->bpmn_squence_destination_poly_type,
                'destination_id' => $this->bpmn_squence_destination_poly_id,

                'summary' => $this->bpmn_squence_summary,

            ]);
            $this->alertSuccessMessage('BpmnSquence updated.');
        }

        $this->recent_bpmn_squences = $sop->bpmn_squences()->get();
    }

    public function deleteBpmnSquence($sop_id, $bpmn_squence_id) {
        $sop = Sop::findOrFail($sop_id);

        $bpmn_squence = BpmnSquence::find($bpmn_squence_id);
        if ($bpmn_squence) {
            $sop->bpmn_squences()->where('id', '=', $bpmn_squence->id)->delete();
        }

        $this->recent_bpmn_squences = $sop->bpmn_squences()->get();

        $this->alertSuccessMessage('BpmnSquence deleted.');
    }

    public function deleteExistingBpmnSquence($bpmn_squence_id) {
        $sop = Sop::findOrFail($this->sop_id);

        $bpmn_squence = BpmnSquence::find($bpmn_squence_id);
        if ($bpmn_squence) {
            $sop->bpmn_squences()->where('id', '=', $bpmn_squence->id)->delete();
        }

        $this->recent_bpmn_squences = $sop->bpmn_squences()->get();

        $this->alertSuccessMessage('BpmnSquence deleted.');
    }
    // End of Section
}
