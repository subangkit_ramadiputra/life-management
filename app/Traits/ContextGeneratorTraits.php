<?php


namespace App\Traits;


trait ContextGeneratorTraits
{
    use GenerateTraits;
    public function getGeneratorOutput($context_definition, $context_generation, $variables=[]) {
        $this->parseDefinitionObject($variables, $context_definition);
        $this->parseGenerationObject($variables, $context_generation);

        $template = $context_generation->template;
        return $this->getStubContents($template,$variables,'$$','$$');
    }

    public function parseDefinitionObject(&$variables, $object) {
        $variables['context_definition_id'] = $object->id;
        $variables['context_name'] = $object->context_name;
    }

    public function parseGenerationObject(&$variables, $object) {
        $variables['context_generation_id'] = $object->id;
        $variables['scope_generation'] = $object->scope_generation;

        //Integrate Custom Data
        $custom_data = json_decode($object->custom_data);
        $this->parseGenerationCustomData($variables, $custom_data);
    }

    public function getDriver($driver) {
        $driver = "\\App\\Http\\ContextDrivers\\".$driver;
        $driverInstance = new $driver;
        return $driverInstance;
    }
    public function parseGenerationCustomData(&$variables, $customData) {
        if (isset($customData->integrations)) {
            if (is_array($customData->integrations)) {
                foreach($customData->integrations as $integration) {
                    $driver = $this->getDriver($integration->type);
                    $variables[$integration->name] = $driver->handle(array_merge($variables, (array) $integration));
                }
            }
        }
    }
}
