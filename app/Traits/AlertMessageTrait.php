<?php


namespace App\Traits;

use Illuminate\Support\Facades\Session;

trait AlertMessageTrait
{
    /**
     * @param $type string
     * @param $messages string|array
     */
    public function alertMessage($type, $messages) {
        $messages_array = [];
        if (is_string($messages))
            array_push($messages_array, $messages);

        if (is_array($messages))
            $messages_array = $messages;

        session()->flash('alert-'.$type.'-messages', $messages_array);
    }

    /**
     * @param $messages string|array
     */
    public function alertSuccessMessage($messages) {
        $this->alertMessage('success', $messages);
    }

    /**
     * @param $messages string|array
     */
    public function alertInfoMessage($messages) {
        $this->alertMessage('info', $messages);
    }

    /**
     * @param $messages string|array
     */
    public function alertErrorMessage($messages) {
        $this->alertMessage('error', $messages);
    }

    /**
     * @param $messages string|array
     */
    public function alertWarningMessage($messages) {
        $this->alertMessage('warning', $messages);
    }
}
