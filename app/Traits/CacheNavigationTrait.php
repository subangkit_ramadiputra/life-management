<?php


namespace App\Traits;


use App\Models\Player;
use App\Models\TeamMember;
use Illuminate\Support\Facades\Cache;

trait CacheNavigationTrait
{
    use CacheTraits;

    /**
     * User Navigation Cache
     */
    public function userNavigation($user) {
        return $this->cachePrefix().'_navigation_'.$user->id;
    }
    public function userNavigationCache($user) {
        return Cache::get($this->userNavigation($user));
    }
    public function rememberUserNavigationCache($user, $function) {
        return Cache::remember($this->userNavigation($user), $this->cacheLong(), $function);
    }
    public function forgetUserNavigationCache($user) {
        return Cache::forget($this->userNavigation($user));
    }

    /**
     * Global Navigation
     */
    public function navigationName() {
        return $this->cachePrefix().'_navigation';
    }
    public function navigationCache() {
        return Cache::get($this->navigationName());
    }
    public function rememberNavigationCache($function) {
        return Cache::remember($this->navigationName(), $this->cacheLong(), $function);
    }
    public function forgetNavigationCache() {
        return Cache::forget($this->navigationName());
    }

}
