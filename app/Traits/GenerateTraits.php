<?php


namespace App\Traits;


use Illuminate\Support\Facades\DB;

trait GenerateTraits
{
    // General Functionality
    /**
     * Replace the stub variables(key) with the desire value
     *
     * @param $stub
     * @param array $stubVariables
     * @return bool|mixed|string
     */
    public function getStubContents($stub , $stubVariables = [], $prefix = '{{ ', $suffix = ' }}')
    {
        $contents = $stub; //file_get_contents($stub);

        foreach ($stubVariables as $search => $replace)
        {
            $contents = str_replace($prefix.$search.$suffix , $replace, $contents);
        }

        return $contents;
    }
    public function isValidIntegrationObject($integration_object) {
        if (!isset($integration_object['type']))
            return false;

        if (!isset($integration_object['name']))
            return false;

        return true;
    }

    public function getIntegrationType($integration_object) {
        return $integration_object['type'];
    }

    public function getIntegrationData($data) {
        $output_data = $data;
        $integrations = $this->getIntegration($data);
        foreach ($integrations as $integration_object) {
            $integration_object = (array) $integration_object;
            if ($this->isValidIntegrationObject($integration_object)) {
                $integration_type = $this->getIntegrationType($integration_object);
                $class = 'App\\Integrations\\'.$integration_type;
                $integration_class = new $class($integration_object);
                $output_data[$integration_object['name']] = $integration_class->getOutput();
            }
        }

        return $output_data;
    }

    public function hasIntegration($data) {
        return isset($data['integrations']);
    }
    public function getIntegration($data) {
        if ($this->hasIntegration($data)) {
            return $data['integrations'];
        }

        return [];
    }

    // Row Formatter
    public function runRowFormatterTemplate($connection, $query, $template) {
        $rows = DB::connection($connection)->select(DB::raw($query));
        $results = '';
        foreach ($rows as $row) {
            $results .= $this->getStubContents($template, (array) $row, '$$','$$')."\n";
        }
        return $results;
    }

    // Context Generation


    public function runContextGenerationTemplate($template, $custom_data) {
        $data = $custom_data;
        $integrate_data = $this->getIntegrationData($data);

        return $this->getStubContents($template, array_merge($data, $integrate_data));
    }
}
