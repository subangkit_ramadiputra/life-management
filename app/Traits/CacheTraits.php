<?php


namespace App\Traits;


use Illuminate\Support\Facades\Cache;

trait CacheTraits
{
    public function cacheShort() {
        // 1 Minute
        return env('CACHE_SHORT',60);
    }
    public function cacheMedium() {
        // 1 Hour
        return env('CACHE_MEDIUM',3600);
    }
    public function cacheLong() {
        // 1 Day
        return env('CACHE_Long',86400);
    }
    public function cacheVeryShort() {
        // 1 Second
        return env('CACHE_VERY_SHORT',1);
    }
    public function cachePrefix() {
        return env('CACHE_PREFIX','dev');
    }

    /**
     * User Cache
     */
    public function userInfoCacheName($user) {
        return $this->cachePrefix().'_user_info_'.$user->id;
    }
    public function userInfoCache($user) {
        return Cache::get($this->userInfoCacheName($user));
    }
    public function rememberUserInfo($user, $function) {
        return Cache::remember($this->userInfoCacheName($user), $this->cacheLong(), $function);
    }
    public function forgetUserInfo($user) {
        return Cache::forget($this->userInfoCacheName($user));
    }

}
