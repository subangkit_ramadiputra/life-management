<?php


namespace App\Providers;

use App\Http\Traits\NavigationTrait;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends \Illuminate\Support\ServiceProvider
{
    use NavigationTrait;
    public function boot(): void
    {
        if (! app()->runningInConsole()) {
            $this->getNavigation(0,2);
            View::share('view_navigations', $this->view_navigations);
        }
    }
}
