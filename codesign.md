# Documentation to make Codesign

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@8.2/lib/httpd/modules/libphp.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@8.2/lib/httpd/modules/libphp.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@8.2/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@7.1/lib/httpd/modules/libphp7.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@7.1/lib/httpd/modules/libphp7.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@7.1/lib/httpd/modules/libphp7.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@7.2/lib/httpd/modules/libphp7.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@7.2/lib/httpd/modules/libphp7.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@7.2/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@7.3/lib/httpd/modules/libphp7.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@7.3/lib/httpd/modules/libphp7.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@7.3/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@7.4/lib/httpd/modules/libphp7.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@7.4/lib/httpd/modules/libphp7.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@7.4/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@8.0/lib/httpd/modules/libphp.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@8.0/lib/httpd/modules/libphp.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@8.0/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"

codesign --sign "Subangkit Ramadiputra"  --force --keychain ~/Library/Keychains/login.keychain-db /opt/homebrew/opt/php@8.1/lib/httpd/modules/libphp.so
codesign -dv --verbose=4 "/opt/homebrew/opt/php@8.1/lib/httpd/modules/libphp.so"

# Add module in httpd.conf
# LoadModule php_module /opt/homebrew/opt/php@8.1/lib/httpd/modules/libphp.so "Subangkit Ramadiputra’s CA"
