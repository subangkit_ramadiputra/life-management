<?php

return [
    'temporary_file_upload' => [
        'rules' => 'max:102400', // (100MB max, and only pngs, jpegs, and pdfs.)
    ],
];
