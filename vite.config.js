import { defineConfig } from 'vite';
import laravel, { refreshPaths } from 'laravel-vite-plugin';
import path from 'path';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'resources/css/style.scss',
                'resources/js/app.js',
                'resources/js/jquery-3.6.1.min.js',
                'resources/js/tinymce.min.js',
                'node_modules/bpmn-js/dist/assets/bpmn-js.css',
                'node_modules/bpmn-js/dist/assets/diagram-js.css'
            ],
            refresh: [
                ...refreshPaths,
                'app/Http/Livewire/**',
            ],
        }),
    ],
    resolve: {
        alias: {
            '~flatpickr': path.resolve(__dirname, './node_modules/flatpickr'),
        },
    },
});
